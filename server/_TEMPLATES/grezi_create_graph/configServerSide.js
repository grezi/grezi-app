/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

const graphVersion = "V 4.0.0";

const templateVersion = "V 4.0.0";

const title_GRAPH = "GREZI : créer un graphe";

const description_GRAPH = "Gabarit de base pour créer un graphe avec le mode éditeur.";

const card_GRAPH = "img/card.png";

const datasetSelection_AUTH = false;

const datasetIdDefault_AUTH = {"id": "data_cache", "name": ""};

const config_featuresGraph = {
	"showLabels": true,
	"editorOnLoad": true
};

const config_featuresButtons = {
	"toggle_modeEditor": true,
	"toggle_fcose": true
};

const config_featuresLoading_FORCING = {
	"showInformation": "true"
};

const config_options_Modifier = JSON.stringify([

	{
		"selector": "edge",
		"style": {
			"width": "2rem",
			"curve-style": "bezier",
			"control-point-weight": "0.4",
			"target-arrow-shape": "triangle",
			"source-distance-from-node": "5px",
			"target-distance-from-node": "5px"
		}
	}

]);

module.exports = {
	graphVersion,
	templateVersion,
    title_GRAPH,
    description_GRAPH,
	card_GRAPH,
	datasetSelection_AUTH,
	datasetIdDefault_AUTH,
	config_featuresGraph,
	config_featuresButtons,
	config_featuresLoading_FORCING,
	config_options_Modifier
};

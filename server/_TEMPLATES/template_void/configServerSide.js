/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

require('dotenv').config();

/*
-------------------------------------------
META INFORMATION
-------------------------------------------
*/

const graphVersion = ""; /* /!\ MANDATORY /!\ */

const templateVersion = ""; /* /!\ MANDATORY /!\ */

const title_GRAPH = "";

const description_GRAPH = "";

const card_GRAPH = "";

const author_GRAPH = "";
const icon_GRAPH = "";
const domain_GRAPH = "";

const ogSiteName_GRAPH = "";
const ogImage_GRAPH = "";

const twitterSiteName_GRAPH = "";
const twitterImage_GRAPH = "";

const showNavigation_GRAPH = true;
const logoShow_GRAPH = true;
const logoImg_GRAPH = "";
const logoUrl_GRAPH = "";
const updateShow_GRAPH = true;

const fullDescription_GRAPH = "";

const datasetSelection_AUTH = false;

const datasetIdDefault_AUTH = {"id": "", "name": ""}; /* /!\ MANDATORY /!\ */

const datasetIdList_AUTH = [];
const specifications_AUTH = "";

const blank_mode = false;

/*
-------------------------------------------
MAIN FEATURES
-------------------------------------------
*/

const config_featuresGraph = {
	"lightView": false,
	"webgl": false,
	"showLabels": false,
	"animation": false,
	"physics": false,
	"static": false,
	"compaction": 1,
	"padding": 50,
	"minZoom" : 0.05,
	"maxZoom": 3,
	"modeHighlighter": JSON.stringify(""),
	"typeHighlighter": JSON.stringify(""),
	"edgeOpacity": 1,
	"edgeHovering": true,
	"fcoseOnLoad": false,
	"lateralOnLoad": false,
	"editorOnLoad": false,
	"mapOnLoad": false,
	"featureReload": false,
	"featureMillesime": false,
	"featureClustering": false,
	"featureSearchBar": false,
	"featureLegend": false,
	"featureFarView": false,
	"featureFarView_automatic": false,
	"featureContextMenu": false,
	"featureScreenShot": false,
	"featureLlm": false,
	"featureBubblesets": false,
	"tooltips": false,
	"modalSelection": false,
	"infoBubbles": false,
	"infoTags": false
};

const config_featuresButtons = {
	"toggle_fullscreen": true,
	"toggle_graphs": false,
	"toggle_lateralScreen": false,
	"toggle_stabilization": false,
	"toggle_physics": false,
	"toggle_fcose": false,
	"toggle_ori_pos": false,
	"toggle_ind_link": false,
	"toggle_highlighter": false,
	"toggle_modeEditor": false,
	"toggle_modeProximity": false,
	"toggle_modeMap": false,
	"toggle_summary": false,
	"toggle_save": false,
	"toggle_information": true,
	"toggle_bug": true
};

const config_featuresLoading_FORCING = {
	"eulerComputation": "",
	"showInformation": "false",
	"clickToBegin": "",
	"debug": "false"
};

/*
-------------------------------------------
SPECIFIC FEATURES
-------------------------------------------
*/

const config_featuresGraph_MillesimeOptions = {
	"millesimeSeparator": "",
	"millesimeField_Nodes": "",
	"millesimeField_Edges": ""
};

const config_featuresGraph_SearchBarOptions = {
	"categoriesToHide": JSON.stringify([]),
	"categoriesToHide_EvenInDebugMode": JSON.stringify([]),
	"categoriesToRename": JSON.stringify([])
};

const config_featuresGraph_LegendOptions = {
	"showTypes": false,
	"showTypes_WithCategories": false,
	"customLabel_Types": "",
	"showNodes": false,
	"showNodes_WithCategories": false,
	"showNodes_Dictionnary": JSON.stringify([]),
	"customLabel_Nodes": "",
	"showEdges": false,
	"showEdges_WithCategories": false,
	"showEdges_Dictionnary": JSON.stringify([]),
	"customLabel_Edges": "",
	"showClusterCategories_Entity": false,
	"showClusterCategories_ClusterGroup": false,
	"showClusterCategories_Type": false,
	"clusterLabel_Entity": "",
	"clusterLabel_ClusterGroup": "", 
	"clusterLabel_Type": "",
	"showClusterCategories_WithClusteringButtons": false,
	"showFarViewControl_automatic": false,
	"showFarViewControl_manual": false,
	"showBubblesetsControl": false,
	"showLabelsControl": false,
	"showLabelsOnLoad": false,
	"hideTypesInCloseView": false,
	"showTags": false,
	"openLegendOnLoad": false
};

const config_featuresGraph_ProximityModeOptions = {
	"classExternalCircle": ""
};

const config_featuresGraph_BubblesetsOptions = {
	"showBubblesetsOnLoad": false,
	"listBubblesets": JSON.stringify([
		{"nodeSelector": "", "edgeSelector": "", "avoidNodeSelector": "", "fill_color": "", "stroke_color": "", "stroke_width": 2}
	])
};

/*
-------------------------------------------
APPEARANCE
-------------------------------------------
*/

const config_showingCredentials = {
	"activate": false,
	"title": "",
	"img": "",
};

const config_styleMapbox = "";

const config_rootColors_FORCING = {
	"main_color_01": "",
	"main_color_02": "",
	"main_color_03": "",
	"main_color_04": "",
	"main_color_05": "",
	"main_color_01_faded": "",
	"main_color_03_faded": "",
	"main_color_node": "",
	"main_color_editor_01" :"",
	"main_color_editor_02" :""
};

const config_rootColors_NEW = [
	{name: "", color: ""}
];

const config_featuresColors_FILTER = "";

const config_dictionnaryIntro_graph = JSON.stringify({
	"introFirstText": "",
	"mainText": "",
	"editorText": "",
	"llmText": "",
	"animationText": "",
	"screenshotText": "",
	"viewAllText": "",
	"millesimeText": "",
	"legendText": "",
	"searchBarText": "",
	"searchBarText_step1": "",
	"searchBarText_step2": "",
	"searchBarText_step3": "",
	"credentialsText": ""
});

const config_dictColorsTags_custom = JSON.stringify({});

const config_dataGraph_LEGEND = "";

const config_options_Modifier = JSON.stringify([]);

/*
-------------------------------------------
DATA TYPING
-------------------------------------------
*/

const config_animationOnLaunch = false;

const config_possibilityToFullScreen = true;

const config_possibilityToPass = true;

const config_dictIntro = JSON.stringify([

	{
		"img": "",
		"title": "",
		"isFadeOut": false,
		"pauseOncomplete": true,
		"onBeginInstructions": "",
		"onCompleteInstructions": "",
		"onCompleteForm": {activate: false, instructions: ""},
		"onCompleteButtons": [{text: "", instructions: ""}],
		"strings_to_type": [
			"What you want to say.",
			"Set some ^1000pause.",
			"And 'bulk typing'"
		]
	}

]);

/*
-------------------------------------------
CUSTOM SCRIPTS
-------------------------------------------
*/

const config_scriptGraph_GET = JSON.stringify([]);

/*
-------------------------------------------
EXPORT
-------------------------------------------
*/

module.exports = {
	graphVersion,
	templateVersion,
    title_GRAPH,
    description_GRAPH,
	card_GRAPH,
	author_GRAPH,
	icon_GRAPH,
	domain_GRAPH,
	ogSiteName_GRAPH,
	ogImage_GRAPH,
	twitterSiteName_GRAPH,
	twitterImage_GRAPH,
	showNavigation_GRAPH,
	logoShow_GRAPH,
	logoImg_GRAPH,
	logoUrl_GRAPH,
	updateShow_GRAPH,
	fullDescription_GRAPH,
	datasetSelection_AUTH,
	datasetIdDefault_AUTH,
	datasetIdList_AUTH,
	specifications_AUTH,
	blank_mode,
	config_featuresGraph,
	config_featuresButtons,
	config_featuresLoading_FORCING,
	config_featuresGraph_MillesimeOptions,
	config_featuresGraph_SearchBarOptions,
	config_featuresGraph_LegendOptions,
	config_featuresGraph_ProximityModeOptions,
	config_featuresGraph_BubblesetsOptions,
	config_showingCredentials,
	config_styleMapbox,
	config_rootColors_FORCING,
	config_rootColors_NEW,
	config_featuresColors_FILTER,
	config_dictionnaryIntro_graph,
	config_dictColorsTags_custom,
	config_dataGraph_LEGEND,
	config_options_Modifier,
	config_animationOnLaunch,
	config_possibilityToFullScreen,
	config_possibilityToPass,
	config_dictIntro,
	config_scriptGraph_GET
};
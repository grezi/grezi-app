/*
    *
	* Here is all the native data connectors (a graphic description is available in the README file) :
	* 
	* - MODE ONLY NODES : a hardcoded list of nodes
		* cache_object = [{...}, {...}, ...]
	* 
	* - MODE MULTI FORMAT : a hardcoded object with a list of nodes and a list of edges (as in template_ex)
		* cache_object = {
		*  "nodes": [{...}, {...}, ...],
		*  "edges": [{...}, {...}, ...],
		* }
	* 
	* - MODE CSV : an object with the key "csv" containing a list of csv nodes urls paired with json config files (as in template_csv)
		* cache_object = {"csv": [
 		* {
 		*  "data": "data/____.csv",
 		*  "config": "data/____.json"
		*  },
		*  {...}
		* ]}
	* 
	* - MODE CSV BIS : an object with the key "csv" containing a list of csv nodes and edges urls paired with json config files (as in template_csv)
		* cache_object = {"csv": [
		*   {
 		*   "nodes": "data/____.csv",
		*     "config": "data/____.json"
		*   },
		*  {
		*    "edges": "data/____.csv",
		*    "config": "data/____.json"
		*   },
		*   {...}
		* ]}
	* 
	* - MODE SEMAPPS : an object to structure semapps data (as in template_semapps)
		* cache_object = {"___SEMAPPS___": {
		*   "urls": [
		*    {
		*     "dict": "https://___",
		*     "config": ["___","___"]
		*    },
		*    {
		*     "traductor": "https://___",
		*     "config": ["___","___"]
		*    },
		*    {
		*     "nodes": "https://___",
 		*     "config": "data/____.json"
 		*    },
 		*    {
 		*    "nodes": "https://___",
		*     "config": "data/____.json"
		*    },
		*    {...}
		* 	],
		*   "mainConfig": "data/___.json"
		*  }
		* };
	* 
	* - MODE GOGOCARTO : an object to structure gagocarto api data (as in template_gogocarto)
		* cache_object = {"___GOGOCARTO___": [
		*   {
		*    "data": "https://___/api/elements.json",
		*    "config": "data/____.json"
		*   },
		*   {...}
		* ]}
	* 
	* - MODE CUSTOM : a list with a custom data connecting script (as in template_custom)
		* cache_object = {"___CUSTOM___": "js/_____.js"}
	*
*/

const data_cache = [];

module.exports = {
	data_cache
};
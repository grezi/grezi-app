/*

The position file must be a json in the same strucvture as data_cache_Example1_POSITIONS.json file.

To help you to generate such a file, you can use the editor mode on your template :
* Go to "Mode avancé" section,
* Then copy paste the text of "Position de tous les éléments" (click anywhere in the canvas to generate or update the positions).

*/

const data_cache_Example1 = "data/data_cache_Example1_POSITIONS.json"; /* position file path with the same name as the dataset (mandatory) */

module.exports = {
	data_cache_Example1
};
const data_cache_Example1 = [ /* list of the nodes with their configuration options */

	{
		"id": "compound_00",
		"group": "elementCOMPOUND",
		"label": "COMPOUND",
		"style": {"text-margin-y": "-10px", "text-max-width": "600px"},
		"sourcesNodes": []
	},

	{
		"id": "title_00",
		"lat": 43.59594438557529,
		"lng": 1.4525441351628603,
		"group": "elementCENTRAL",
		"label": "TITLE",
		"title": "TITLE",
		"titleComplete": "TITLE_COMPLETE",
		"other": "Any other custom object",
		"image": "IMG_PATH",
		"selectable": false,
		"grabbable": false,
		"pannable": false,
		"insensitiveNode": false,
		"parent": "compound_00",
		"sourcesNodes": [],
		"sourcesNodesIndirect": []
	},
	
	{
		"id": "elem_00",
		"lat": 43.605360942883834,
		"lng": 1.4240912610897487,
		"group": "element",
		"label": "LABEL",
		"searchTag": {"tag_1": ["value_1"], "tag_2": ["value2_1","value2_2"]},
		"additionalData": {"mapColorNode": "#5FBEB8", "mapImageNode": "IMG_PATH"},
		"sourcesNodes": [
			"title_00",
			{"id": "title_00", "data": {"precision": "EDGE_LABEL", "group": "flux_red_SEGMENTS"}, "style": {"segment-distances": [100], "segment-weights": [0.55]}}
		]
	},

	{
		"id": "elem_01",
		"label": "INITIALLY_UNCHECKED",
		"classes": "unchecked unchecked_type unchecked_searched unchecked_proximity unchecked_millesime unchecked_map",
		"sourcesNodes": [
			{"id": "title_00", "classes": "unchecked_edge"}
		]
	},

	{
		"id": "e_00",
		"group": "entity",
		"searchTag": {"tag_1": ["value_1"]},
		"parent": "sg_00",
		"label": "ENTITY"
	},

	{
		"id": "sg_00",
		"group": "subGroup",
		"parent": "g_00",
		"label": "SUBGROUP"
	},

	{
		"id": "g_00",
		"group": "clusterGroup",
		"parent": "t_00",
		"additionalData": {"darkColor": "#EA668A", "lightColor": "#F8C9D5"},
		"label": "CLUSTER"
	},

	{
		"id": "t_00",
		"group": "type",
		"label": "TYPE"
	}

];

const data_cache_Example2 = { /* An alternative to set data : an objet with nodes and edges declared separately => Rename in "dataGraph_GET" and remove the previous one if you want to test it */

"nodes": [

    {
        "id": "compound_00",
        "group": "elementCOMPOUND",
        "label": "COMPOUND",
        "style": {"text-margin-y": "-10px", "text-max-width": "600px"}
    },

    {
        "id": "title_00",
        "lat": 43.59594438557529,
        "lng": 1.4525441351628603,
        "group": "elementCENTRAL",
        "label": "TITLE",
        "title": "TITLE",
        "titleComplete": "TITLE_COMPLETE",
        "other": "Any other custom object",
        "image": "IMG_PATH",
        "selectable": false,
        "grabbable": false,
        "pannable": false,
        "insensitiveNode": false,
        "parent": "compound_00"
    },
    
    {
        "id": "elem_00",
        "lat": 43.605360942883834,
        "lng": 1.4240912610897487,
        "group": "element",
        "label": "LABEL",
        "searchTag": {"tag_1": ["value_1"], "tag_2": ["value2_1","value2_2"]},
        "additionalData": {"mapColorNode": "#5FBEB8", "mapImageNode": "IMG_PATH"},
    },

    {
        "id": "elem_00",
        "lat": 43.605360942883834,
        "lng": 1.4240912610897487,
        "group": "element",
        "label": "LABEL",
        "searchTag": {"tag_1": ["value_1"], "tag_2": ["value2_1","value2_2"]},
        "additionalData": {"mapColorNode": "#5FBEB8", "mapImageNode": "IMG_PATH"},
    },

    {
        "id": "elem_01",
        "label": "INITIALLY_UNCHECKED",
        "classes": "unchecked unchecked_type unchecked_searched unchecked_proximity unchecked_millesime unchecked_map",
        "sourcesNodes": [
            {"id": "title_00", "classes": "unchecked_edge"}
        ]
    },

    {
        "id": "e_00",
        "group": "entity",
        "searchTag": {"tag_1": ["value_1"]},
        "parent": "sg_00",
        "label": "ENTITY"
    },

    {
        "id": "sg_00",
        "group": "subGroup",
        "parent": "g_00",
        "label": "SUBGROUP"
    },

    {
        "id": "g_00",
        "group": "clusterGroup",
        "parent": "t_00",
        "additionalData": {"darkColor": "#EA668A", "lightColor": "#F8C9D5"},
        "label": "CLUSTER"
    },

    {
        "id": "t_00",
        "group": "type",
        "label": "TYPE"
    }

],

"edges": [

    {
        "edge_id": "edge_0000", /* Optional edge id */
        "from": "elem_00",
        "to": "title_00"
    },

    {
        "from": "elem_00",
        "to": "title_00",
        "data": {"precision": "EDGE_LABEL","group": "flux_red_SEGMENTS"},
        "style": {"segment-distances": [100], "segment-weights": [0.55]}
    },

    {
        "from": "elem_01",
        "to": "title_00",
        "classes": "unchecked_edge"
    }		

]

};

module.exports = {
	data_cache_Example1,
    data_cache_Example2
};
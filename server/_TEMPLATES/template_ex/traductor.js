/*

Here is an example of how use traductor data implementation with a csv file stored in data folder.

It is possible to develop custom data implementation if :
* you keep mandatory lines,
* the data is a two columns object with "code" and "value" as a header

*/

function custom_traductor_function (templateName,datasetName,serverPath) { /* MANDATORY LINE */
    return new Promise((resolve_CustomTraductor) => { /* MANDATORY LINE */

		const csv = require('jquery-csv');

		const fs = require('fs');

		console.log(serverPath + 'data/' + datasetName + '_TRADUCTOR.csv')

		fs.readFile(serverPath + 'data/' + '_TRADUCTOR.csv', 'utf8', function (err, data) {
			if (err) {
				console.error('No traductor loaded for : ', datasetName);
				return;
			};
			resolve_CustomTraductor(csv.toObjects(data.replace(/^\s+|\s+$/g, ''),{"separator": ",", "headers": true, "skipEmptyLines": true})); /* MANDATORY LINE */
		});

	}); /* MANDATORY LINE */
}; /* MANDATORY LINE */

module.exports = { /* MANDATORY LINE */
	custom_traductor_function /* MANDATORY LINE */
};	 /* MANDATORY LINE */
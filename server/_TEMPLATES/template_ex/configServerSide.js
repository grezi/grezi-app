/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

require('dotenv').config(); /* Especilly to retrieve root color */

/*
-------------------------------------------
META INFORMATION
-------------------------------------------
*/

const graphVersion = "V 4.0.0"; /* /!\ MANDATORY /!\ */

const templateVersion = "V 4.0.0";  /* /!\ MANDATORY /!\ */

const title_GRAPH = "TITLE"; /* title that appears in :
	- the graph list of the app,
	- the meta title of the page,
	- the meta og title of the page,
	- the meta twitter title of the page,
	- the nav bar of the page.
*/

const description_GRAPH = "DESCRIPTION"; /* description that appears in :
	- the graph list of the app,
	- the meta description of the page,
	- the meta og description of the page,
	- the meta twitter description of the page.
*/

const card_GRAPH = "img/card.png"; /* image public path of the card that appears in the homepage template list (set the path in from the template folder) */

const author_GRAPH = "AUTHOR"; /* meta author of the page */
const icon_GRAPH = "ICON"; /* meta icon of the page */
const domain_GRAPH = "DOMAIN"; /* meta domain of the page */

const ogSiteName_GRAPH = "SITENAME"; /* meta os sitename of the page */
const ogImage_GRAPH = "CARD"; /* meta og image public path of the page (set the path in from the template folder) */

const twitterSiteName_GRAPH = "SITENAME"; /* meta twitter sitename of the page */
const twitterImage_GRAPH = "CARD"; /* meta twitter image public path of the page (set the path in from the template folder) */

const showNavigation_GRAPH = true; /* show or not the navigation arrow (if the page is from home or update page) instead of the logo */
const logoShow_GRAPH = true; /* show or not logo of the navbar */
const logoImg_GRAPH = "IMG"; /* custom image of the logo of the navbar */
const logoUrl_GRAPH = "URL"; /* custom url of the logo of the navbar */
const updateShow_GRAPH = true; /* show or not button of the navbar to load the page /update */

const fullDescription_GRAPH = "FULL_DESCRIPTION"; /* full description that appears in the modal information screen */

const datasetSelection_AUTH = true; /* use the dataset list (true) or the default (false) */

/* /!\ MANDATORY /!\ */
const datasetIdDefault_AUTH = {"id": "example1", "name": "Example 1"}; /* the dataset to use by default (dictionnary with id and name) */
/* /!\ MANDATORY /!\ */

const datasetIdList_AUTH = [{"id": "example1", "name": "Example 1"},{"id": "example2", "name": "Example 2"}]; /* list of the datasets with dictionnaries as above */
const specifications_AUTH = "Use the example password « demo1 »."; /* give a hint to the user (where to find their passwords) */

const blank_mode = false; /* hide some partials : creativeCommonsDiv - homeDonateBloc */

/*
-------------------------------------------
MAIN FEATURES
-------------------------------------------
*/

const config_featuresGraph = {
	"lightView": false, /* if true, it will set some less computational intensive cystoscape's options */
	"webgl": false, /* activate the webgl renderer (it might create side effects and troubleshots as it's a new cytoscape implementation) */
	"showLabels": true,  /* always show the labels of the nodes (do not need to hover them) */
	"animation": true, /* activate the animation based on the _data_typing.js script */
	"physics": false, /* activate the physics engine of the graph (except for hierarchical layout) (if no positions are set, perform a cola layout) */
	"static": false, /* lock all the nodes (even if the physics option is activated) */
	"compaction": 1, /* set the compaction of the graph when physics is activated or when a cola layout occurs */
	"padding": 50, /* set the padding when a zoom is activated by double-tap of the stabilization button */
	"minZoom" : 0.05, /* Minimum zoom for the view (0.05 is an empiric threshold) */
	"maxZoom": 3, /* Maximum zoom for the view (3 is an empiric threshold) */
	"modeHighlighter": JSON.stringify(""), /* set a highlighting mode between '' (no highligh), 'self', 'ascendency', 'downdency' and 'both' */
	"typeHighlighter": JSON.stringify(""), /* set a highlighting type between '' (both), 'both', 'node', and 'edge' */
	"edgeOpacity": 0.5, /* main edge opacity */
	"edgeHovering": true, /* add a special class on hovering an edge (with 100% opacity) */
	"fcoseOnLoad": false, /* launch a fcose layout onload of the graph */
	"lateralOnLoad": false, /* open the lateral screen onload of the graph (priority 1 on load) */
	"editorOnLoad": false, /* open the editor mode onload of the graph (priority 2 on load) */
	"mapOnLoad": false, /* toggle the cartography onload of the graph (priority 3 on load) */
	"featureReload": true, /* show or not the button to reload the graph at his opening state */
	"featureMillesime": true, /* show or not the year millesime collection */
	"featureClustering": true, /* allow to expand and collapse compound nodes and edges (collapse by default all the subGroups) */
	"featureSearchBar": true, /* show or not the search bar */
	"featureLegend": true, /* show or not the legend accordion on the right */
	"featureFarView": true, /* activate or not the far view mode (for the cluster levels nodes) */
	"featureFarView_automatic": true, /* active or not the automatic far view mode */
	"featureContextMenu": true, /* activate or not the context menu (right click on canva and objects) */
	"featureScreenShot": true, /* Screenshot button on the top of the screen */
	"featureLlm": true, /* grezIA button on the top of the screen */
	"featureBubblesets": true, /* Activate bubblesets (set options in config_featuresGraph_BubblesetsOptions) */
	"tooltips": true, /* show the title attribute of the nodes in a tooltip (if titleComplete is set and title undefined, the first will be show */
	"modalSelection": true, /* show the titleComplete attribute of the nodes in a modal screen */
	"infoBubbles": true, /* create bubbles nodes around the nodes that have tooltips or info modal screen */
	"infoTags": true /* create tags nodes based on the searchTag attribute of the nodes (and an activation button and complete the legend of the graph */
};

const config_featuresButtons = { /* if "false" is set to the attributes of this dict, it will force to desactivate the association button (if "true" it will keep it, if "" set the default value). To see the functions linked to these buttons, look at the information modal screen of the graph */
	"toggle_fullscreen": true,
	"toggle_graphs": true,
	"toggle_lateralScreen": true,
	"toggle_stabilization": true,
	"toggle_physics": true,
	"toggle_fcose": true,
	"toggle_ori_pos": true,
	"toggle_ind_link": true,
	"toggle_highlighter": true,
	"toggle_modeEditor": true,
	"toggle_modeProximity": true,
	"toggle_modeMap": true,
	"toggle_summary": true,
	"toggle_save": true,
	"toggle_information": true,
	"toggle_bug": true
};

const config_featuresLoading_FORCING = { /* force the url parameters to be "true" or "false" regardless of the url paramets that are set ("" for default value) */
	"eulerComputation": "",
	"showInformation": "true",
	"clickToBegin": "",
	"debug": ""
};

/*
-------------------------------------------
SPECIFIC FEATURES
-------------------------------------------
*/

const config_featuresGraph_MillesimeOptions = {
	"millesimeSeparator": ",", /* Separator of millesime data field */
	"millesimeField_Nodes": "year", /* data field of nodes that store millesime information */
	"millesimeField_Edges": "year" /* data field of edges that store millesime information */
};

const config_featuresGraph_SearchBarOptions = {
	"categoriesToHide": JSON.stringify(["tag_1"]), /* List the categories of tags to not show in the search bar (visible in the debug mode) */
	"categoriesToHide_EvenInDebugMode": JSON.stringify(["tag_2"]), /* List the categories of tags to not show in the search bar (even in the debug mode) */
	"categoriesToRename": JSON.stringify([["Labels","CustomCategoryName"]]) /* Set couple of strings where the first is a category of tags to rename and the other the name to apply */
};

const config_featuresGraph_LegendOptions = {
	"showTypes": true, /* show or not the option to hide/view all the cluster levels nodes */
	"showTypes_WithCategories": true, /* show or not the option to hide/view all the cluster levels nodes by types categories */
	"customLabel_Types": "CUSTOM_TYPES_LABEL", /* modify the default label 'Types' */
	"showNodes": true, /* show or not the option to hide/view all the nodes */
	"showNodes_WithCategories": true, /* show or not the option to hide/view all the nodes by custom categories */
	"showNodes_Dictionnary":  JSON.stringify([ /* set the nodes categories to hide/view with a name, a color, an image, a selector, an optional description and an optional hr before or after the item */
		{"name": "custom_name", "color": "var(--quit_color_02)", "image": "IMG_PATH", "selector": 'node:grabbable', "description": "custom_description", "hr": "before"}
	]),
	"customLabel_Nodes": "CUSTOM_NODES_LABEL", /* modify the default label 'Entités' */
	"showEdges": true, /* show or not the option to hide/view all the edges */
	"showEdges_WithCategories": true, /* show or not the option to hide/view all the edges by custom categories */
	"showEdges_Dictionnary": JSON.stringify([ /* set the edges categories to hide/view with a name, a color, a widht, a selector, an optional description and an optional hr before or after the item */
		{"name": "custom_name", "color": "var(--quit_color_01)", "dasharray": "1,1", "width": 1, "selector": "edge[group = &quot;flux_red_SEGMENTS&quot;]", "description": "custom_description", "hr": "after"}
	]),
	"customLabel_Edges": "CUSTOM_EDGES_LABEL", /* modify the default label 'Liens' */
	"showClusterCategories_Entity": true, /* show or not the option to hide/view the entities and sub-groups */
	"showClusterCategories_ClusterGroup": true, /* show or not the option to hide/view the cluster-groups */
	"showClusterCategories_Type": true, /* show or not the option to hide/view the types */
	"clusterLabel_Entity": "custom_entity_label", /* modify the default label 'Entités' */
	"clusterLabel_ClusterGroup": "custom_cluster_label", /* modify the default label 'Groupes' */
	"clusterLabel_Type": "custom_type_label", /* modify the default label 'Types' */
	"showClusterCategories_WithClusteringButtons": true, /* add buttons to collapse/expand all the cluster-groupes and types */
	"showFarViewControl_automatic": true, /* add a checkbox to control the automatic farview mode */
	"showFarViewControl_manual": true, /* add a checkbox to manually control the farview if the automatic mode is inactive */
	"showBubblesetsControl": true, /* add a checkbox to manually control the computation of bubblesets (if the feature is activated) */
	"showLabelsControl": true, /* add a checkbox to manually continuously show the labels (if the feature is activated) */
	"showLabelsOnLoad": true, /* show or not to show the labels on load */
	"hideTypesInCloseView": false, /* types are hidden in close view and the option in the legend is desactivate */
	"showTags": true, /* show the legend of the displayed tags */
	"openLegendOnLoad": true /* open the legend accordion on load of the app */
};

const config_featuresGraph_ProximityModeOptions = {
	"classExternalCircle": "" /* If set, the proximity graph will set an external ring of all the elements of the class indicate in this variable */
};

const config_featuresGraph_BubblesetsOptions = {
	"showBubblesetsOnLoad": true, /* Show the bubblesets on load or on reinitialize fo the graph */
	"listBubblesets": JSON.stringify([
		{"nodeSelector": ".defaultNode", "edgeSelector": "", "avoidNodeSelector": "", "fill_color": "--new_color_01", "stroke_color": "--new_color_02", "stroke_width": 6} /* Use cytoscape selectors */
	])
};

/*
-------------------------------------------
APPEARANCE
-------------------------------------------
*/

const config_showingCredentials = {
	"activate": true, /* if true, show credentials on launch of the script with a default "Powered by GREZI" title (false by default) */
	"title": "", /* Custom title of the credentials */
	"img": "" /* Custom image of the credentials */
};

const config_styleMapbox = "mapbox://styles/mapbox/outdoors-v12"; /* Mapbox background style */

const config_rootColors_FORCING = { /* modify the colors of the graph (ONLY HEXADECIMAL VALUES) */
	"main_color_01": "#3f64bc",
	"main_color_02": "",
	"main_color_03": "",
	"main_color_04": "",
	"main_color_05": "",
	"main_color_01_faded": "",
	"main_color_03_faded": "",
	"main_color_node": "",
	"main_color_editor_01" :"",
	"main_color_editor_02" :""
};

const config_rootColors_NEW = [ /* add new colors to the root (ONLY HEXADECIMAL VALUES) */
	{name: "--new_color_01", color: "#98bc3f"},
	{name: "--new_color_02", color: "#bc3f87"}
];

const config_featuresColors_FILTER = ""; /* custom colorization of the images of the buttons (use the filter value generated in the the informationModal, the div's id is #appli_graph_filter_color) */

const config_dictionnaryIntro_graph = JSON.stringify({ /* allow to modify the texts of the intro tutorial */
	"introFirstText": "Voici quelques indications utiles pour faciliter votre prise en main. Vous pouvez cliquer en dehors de la fenêtre pour quitter le tutoriel. Bonne navigation !",
	"mainText": "Pour naviguer dans l’interface, déplacez-vous par \"glisser/déposer\" comme dans une cartographie en ligne.</p><p>Un double-clic vous permet de recenter la vue.</p><p>Vous pouvez également zoomer et dézoomer (molette de votre souris ou zoom du pavé tactile)</p><p>Certains éléments sont interactifs. Vous pouvez les déplacer par \"glisser/déposer\", les survoler pour faire apparaître des informations complémentaires et les sélectionner en cliquant dessus.",
	"editorText": "Le mode éditeur est actif au chargement de ce gabarit, double-cliquez pour créer des éléments.",
	"llmText": "Interagir avec l'ia générative basée sur Mixtral",
	"animationText": "Introduction guidée du graphe",
	"screenshotText": "Capture la vue actuelle pour un export en haute qualité",
	"viewAllText": "Remettre à zéro la visualisation et relancer le tutoriel.",
	"millesimeText": "Filtrer en fonction du millésime des données",
	"legendText": "Cliquer sur le bandeau \"Légende\" pour ouvrir ou fermer le cadre. Choisir les éléments à faire apparaître à l’écran en cochant ou décochant les cases grises.",
	"searchBarText": "Barre de recherche pour choisir les éléments affichés.",
	"searchBarText_step1": "Étape 1 : sélectionner une catégorie de recherche dans \"Nouvelle recherche\".",
	"searchBarText_step2": "Étape 2 : saisir ou sélectionner le ou les éléments à rechercher.",
	"searchBarText_step3": "Étape 3 : choisir un mode (zoom ou filtre) pour appliquer la recherche. Pour revenir à la visualisation complète, appliquer \"Filtrer\" à vide.",
	"credentialsText": "Consulter le site internet de GREZI et les licences d\'utilisation."
});

const config_dictColorsTags_custom = JSON.stringify({"tag_1": "#c9c9c9", "tag_2": "#079fd5"}); /* custom colors for tags */

let config_dataGraph_LEGEND = ""; /* html written legend show before all other legend items */
config_dataGraph_LEGEND += "<p>"+"<svg width=\"45\" height=\"30\"><polygon points=\"0,30 30,30 45,15 30,0 0,0\"  style=\"fill:"+"#fcc300"+"\"/></svg>"+" : "+"Legend_A"+"</p>";
config_dataGraph_LEGEND += "<p>"+"<svg width=\"45\" height=\"30\"><polygon points=\"0,30 30,30 45,15 30,0 0,0\"  style=\"fill:"+"#079fd5"+"\"/></svg>"+" : "+"Legend_B"+"</p>";
config_dataGraph_LEGEND += "<p>"+"<svg width=\"45\" height=\"30\"><polygon points=\"0,30 30,30 45,15 30,0 0,0\"  style=\"fill:"+"#ffff00"+"\"/></svg>"+" : "+"Legend_C"+"</p>";

const config_options_Modifier = JSON.stringify([ /* option of the graph to be modified (see the cytoscape documentation to see those options) */

	{
		"selector": "node[group = &quot;elementCOMPOUND&quot;]",
		"style": {
			"shape": "round-rectangle",
			"background-opacity": 0.5,
			"border-width": "0px",
			"text-max-width": "400px",
			"text-halign": "center",
			"text-valign": "top"
		}
	},

	{
		"selector": "node[group = &quot;elementCENTRAL&quot;]",
		"style": {
			"width": "30px",
			"height": "30px",
			"color": "red",
			"text-valign": "bottom",
			"text-margin-y": "3rem",
			"background-image": "data(image)",
			"background-fit": "cover",
			"background-clip": "node"
		}
	},
	
	{
		"selector": "node[group = &quot;element&quot;]",
		"style": {}
	},
	
	{
		"selector": "edge[group = &quot;flux_red_SEGMENTS&quot;]",
		"style": {
			"curve-style": "segments",
			"line-color": process.env.CSS_QUIT_COLOR_01, /* do not forget to retrieve dot env */
			"target-arrow-color": "red",
			"target-arrow-shape": "triangle",
			"width": "6rem"
		}
	}

	
]);

/*
-------------------------------------------
DATA TYPING
-------------------------------------------
*/

const config_animationOnLaunch = true; /* if true, launch the animation when the app is launched (false by default) */

const config_possibilityToFullScreen = true; /* show the fullscreen button of the animation (true by default) */

const config_possibilityToPass = true; /* show the pass button of the animation (true by default) */

const config_dictIntro = JSON.stringify([ /* list of block strings to deploy in the talking frame. Each block can be associated with some options */

	{
		"img": "", /* OPTIONAL : image to show in the talking frame */
		"title": "", /* OPTIONAL : title to show in the talking frame */
		"isFadeOut": false, /* OPTIONAL : if false, it will type back before go to the next sentence (true by default) */
		"pauseOncomplete": true, /* OPTIONAL : if true it will wait for a user interaction before go to the next sentence (false by default) */
		"onBeginInstructions": "", /* OPTIONAL : some code to apply at the beginning of this block strings */
		"onCompleteInstructions": "console.log('Complete instructions done');resolve_F();", /* OPTIONAL : some code to apply at the end of this block strings. It cans includes the function resolve_F() to control when the promise end */
		"onCompleteForm": {activate: true, instructions: "console.log(talkingFormMessage);resolve_F();"},
		"onCompleteButtons": [{text: "Exit", instructions: "console.log('Button clicked');resolve_F();"}],
		"strings_to_type": [ /* MANDATORY : list of the sentences to deploy in the talking frame */
			"What you want to say.",
			"Set some ^1000pause.",
			"And 'bulk typing'"
		]
	}

]);

/*
-------------------------------------------
CUSTOM SCRIPTS
-------------------------------------------
*/

const config_scriptGraph_GET = JSON.stringify([]); /* list of the customs scripts to load */

/*
-------------------------------------------
EXPORT
-------------------------------------------
*/

module.exports = {
	graphVersion,
	templateVersion,
    title_GRAPH,
    description_GRAPH,
	card_GRAPH,
	author_GRAPH,
	icon_GRAPH,
	domain_GRAPH,
	ogSiteName_GRAPH,
	ogImage_GRAPH,
	twitterSiteName_GRAPH,
	twitterImage_GRAPH,
	showNavigation_GRAPH,
	logoShow_GRAPH,
	logoImg_GRAPH,
	logoUrl_GRAPH,
	updateShow_GRAPH,
	fullDescription_GRAPH,
	datasetSelection_AUTH,
	datasetIdDefault_AUTH,
	datasetIdList_AUTH,
	specifications_AUTH,
	blank_mode,
	config_featuresGraph,
	config_featuresButtons,
	config_featuresLoading_FORCING,
	config_featuresGraph_MillesimeOptions,
	config_featuresGraph_SearchBarOptions,
	config_featuresGraph_LegendOptions,
	config_featuresGraph_ProximityModeOptions,
	config_featuresGraph_BubblesetsOptions,
	config_showingCredentials,
	config_styleMapbox,
	config_rootColors_FORCING,
	config_rootColors_NEW,
	config_featuresColors_FILTER,
	config_dictionnaryIntro_graph,
	config_dictColorsTags_custom,
	config_dataGraph_LEGEND,
	config_options_Modifier,
	config_animationOnLaunch,
	config_possibilityToFullScreen,
	config_possibilityToPass,
	config_dictIntro,
	config_scriptGraph_GET
};

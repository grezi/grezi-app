/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

const graphVersion = "V 4.0.0";

const templateVersion = "V 4.0.0";

const datasetIdDefault_AUTH = {"id": "example", "name": ""};

const config_featuresGraph = {
	"featureSearchBar": true,
	"showLabels": true,
	"fcoseOnLoad": true
};

const config_featuresButtons = {
	"toggle_fcose": true,
	"toggle_modeEditor": true,
	"toggle_summary": true
};

module.exports = {
	graphVersion,
	templateVersion,
	datasetIdDefault_AUTH,
	config_featuresGraph,
	config_featuresButtons
};

const data_cache = {"csv": [{"data": "data/_template.csv", "config": "data/_template.json"}]};

const data_cache_ALTERNATE = { /* An alternative to set data : an objet with nodes and edges declared separately => Rename in "dataGraph_GET" and remove the previous one if you want to test it */
	"csv": [ 
		{"nodes": "data/_templateAlternate_Nodes.csv", "config": "data/_templateAlternate_Nodes.json"},
		{"edges": "data/_templateAlternate_Edges.csv", "config": "data/_templateAlternate_Edges.json"} /* If no config files are set for the edges, "from" and "to" are mandotories */
	]
};

module.exports = {
	data_cache,
    data_cache_ALTERNATE
};
const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
    host: process.env.MAIL_FROM_SMTP,
    port: 587,
    secure: false,
    auth: {
        user: process.env.MAIL_FROM_USER,
        pass: process.env.MAIL_FROM_PASS,
    },
    tls: {
        ciphers: 'SSLv3'
    }
});

const sendMail = (subject,message) => {
    transporter.sendMail({
        from: "<"+process.env.MAIL_FROM_USER+">", // sender address
        to: process.env.MAIL_TO, // list of receivers
        subject: subject, // Subject line
        text: message, // plain text body
    }).then(info => {
        console.log("Message sent: %s", info.messageId);
    }).catch(error => {
        console.error("Message not sent: %s", error);
    });
}

module.exports = {
    sendMail
}

var Handlebars = require('handlebars');

const fs = require('fs');
const externalLibrairiesRaw = fs.readFileSync('external_librairies.json', 'utf-8');
const externalLibrairiesConfig = JSON.parse(externalLibrairiesRaw);

// AND OPERATOR IN HBS PAGES
Handlebars.registerHelper('and', function () {
    return Array.prototype.slice.call(arguments).every(Boolean);
});

// OR OPERATOR IN HBS PAGES
Handlebars.registerHelper('or', function () {
    return Array.prototype.slice.call(arguments, 0, -1).some(Boolean);
});

// EQ OPERATOR IN HBS PAGES
Handlebars.registerHelper('eq', function (a, b) {
    return a === b;
});

// HELPER TO COMPARE STRINGS
Handlebars.registerHelper('compare', function (left, operator, right, options) {
    if (arguments.length < 3) {
        throw new Error('Handlerbars Helper "compare" needs 2 parameters');
    }

    const operators = {
        '==': (left, right) => left == right,
        '===': (left, right) => left === right,
        '!=': (left, right) => left != right,
        '!==': (left, right) => left !== right,
    };

    if (!operators[operator]) {
        throw new Error(`Handlerbars Helper "compare" doesn't know the operator ${operator}`);
    }

    const result = operators[operator](left, right);

    if (result) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});

// HELPER TO CHECK SPECIFICALLY FALSE VARIABLE
Handlebars.registerHelper('isFalse', function(value) {
    return value !== false;
});

// SET META DATA WITH HELPER
Handlebars.registerHelper('mainDomain', function (context) {
    return context.data.root.graph_mainDomain || process.env.MAIN_DOMAIN;
});
Handlebars.registerHelper('metaTitle', function (context) {
    return context.data.root.graph_metaTitle || process.env.META_TITLE;
});
Handlebars.registerHelper('metaPageIcon', function (context) {
    try {
        return context.data.root.graph_metaPageIcon() || process.env.META_PAGE_ICON;
    } catch (error) {
        return process.env.META_PAGE_ICON;
    };
});
Handlebars.registerHelper('metaDescription', function (context) {
    return context.data.root.graph_metaDescription || process.env.META_DESCRIPTION;
});
Handlebars.registerHelper('metaAuthor', function (context) {
    return context.data.root.graph_metaAuthor || process.env.META_AUTHOR;
});
Handlebars.registerHelper('metaGitRepository', function () {
    return process.env.META_GIT_REPOSITORY;
});
Handlebars.registerHelper('ogTitle', function (context) {
    return context.data.root.graph_ogTitle || process.env.OG_TITLE;
});
Handlebars.registerHelper('ogDescription', function (context) {
    return context.data.root.graph_ogDescription || process.env.OG_DESCRIPTION;
});
Handlebars.registerHelper('ogSiteName', function (context) {
    return context.data.root.graph_ogSiteName || process.env.OG_SITE_NAME;
});
Handlebars.registerHelper('ogImage', function (context) {
    return context.data.root.graph_ogImage || process.env.OG_IMAGE;
});
Handlebars.registerHelper('twitterSite', function (context) {
    return context.data.root.graph_twitterSite || process.env.TWITTER_SITE;
});
Handlebars.registerHelper('twitterTitle', function (context) {
    return context.data.root.graph_twitterTitle || process.env.TWITTER_TITLE;
});
Handlebars.registerHelper('twitterDescription', function (context) {
    return context.data.root.graph_twitterDescription || process.env.TWITTER_DESCRIPTION;
});
Handlebars.registerHelper('twitterImage', function (context) {
    return context.data.root.graph_twitterImage || process.env.TWITTER_IMAGE;
});

// SET NAVBAR DATA WITH HELPER
Handlebars.registerHelper('navbarTitle', function (context) {
    return context.data.root.navbar_Title || process.env.NAVBAR_TITLE;
});
Handlebars.registerHelper('navbarLogoImg', function (context) {
    try {
        return context.data.root.navbar_LogoImg() || process.env.NAVBAR_LOGO_IMG;
    } catch (error) {
        return process.env.NAVBAR_LOGO_IMG;
    };
});
Handlebars.registerHelper('navbarLogoUrl', function (context) {
    if (context.data.root.isHomePage) {
        return "#appli_graph";
    } else {
        return context.data.root.navbar_LogoUrl || process.env.NAVBAR_LOGO_URL;
    };
});

// SET MATOMO DATA WITH HELPER
Handlebars.registerHelper('matomoUrl', function () {
    return process.env.MATOMO_URL;
});

// SET EXTERNAL LIBRAIRIES WITH HELPER FOR main-layout.hbs
Handlebars.registerHelper('externalLib_BootstrapCoreCSS', function (context) {
    return externalLibrairiesConfig.BOOTSTRAP_CORE_CSS;
});
Handlebars.registerHelper('externalLib_BootstrapPluginCSS', function (context) {
    return externalLibrairiesConfig.BOOTSTRAP_PLUGIN_CSS;
});
Handlebars.registerHelper('externalLib_JqueryCoreJS', function (context) {
    return externalLibrairiesConfig.JQUERY_CORE_JS;
});
Handlebars.registerHelper('externalLib_JqueryEasingJS', function (context) {
    return externalLibrairiesConfig.JQUERY_EASING_JS;
});
Handlebars.registerHelper('externalLib_BootstrapCoreJS', function (context) {
    return externalLibrairiesConfig.BOOTSTRAP_CORE_JS;
});
Handlebars.registerHelper('externalLib_BootstrapPluginJS', function (context) {
    return externalLibrairiesConfig.BOOTSTRAP_PLUGIN_JS;
});
Handlebars.registerHelper('externalLib_LazysizesCoreJS', function (context) {
    return externalLibrairiesConfig.LAZYSIZES_CORE_JS;
});

// SET EXTERNAL LIBRAIRIES WITH HELPER FOR graphs.hbs
Handlebars.registerHelper('externalLib_CytoscapeCoreJS', function (context) {
    return externalLibrairiesConfig.CYTOSCAPE_CORE_JS;
});
Handlebars.registerHelper('externalLib_ProgressbarCoreJS', function (context) {
    return externalLibrairiesConfig.PROGRESSBAR_CORE_JS;
});

// SET CSS COLORS FOR main-layout.hbs
Handlebars.registerHelper('main_color_01', function (context) {
    return (context.data.root.config_rootColors_FORCING && context.data.root.config_rootColors_FORCING.main_color_01) || process.env.CSS_MAIN_COLOR_01;
});
Handlebars.registerHelper('main_color_02', function (context) {
    return (context.data.root.config_rootColors_FORCING && context.data.root.config_rootColors_FORCING.main_color_02) || process.env.CSS_MAIN_COLOR_02;
});
Handlebars.registerHelper('main_color_03', function (context) {
    return (context.data.root.config_rootColors_FORCING && context.data.root.config_rootColors_FORCING.main_color_03) || process.env.CSS_MAIN_COLOR_03;
});
Handlebars.registerHelper('main_color_04', function (context) {
    return (context.data.root.config_rootColors_FORCING && context.data.root.config_rootColors_FORCING.main_color_04) || process.env.CSS_MAIN_COLOR_04;
});
Handlebars.registerHelper('main_color_05', function (context) {
    return (context.data.root.config_rootColors_FORCING && context.data.root.config_rootColors_FORCING.main_color_05) || process.env.CSS_MAIN_COLOR_05;
});
Handlebars.registerHelper('main_color_01_faded', function (context) {
    return (context.data.root.config_rootColors_FORCING && context.data.root.config_rootColors_FORCING.main_color_01_faded) || process.env.CSS_MAIN_COLOR_01_FADED;
});
Handlebars.registerHelper('main_color_03_faded', function (context) {
    return (context.data.root.config_rootColors_FORCING && context.data.root.config_rootColors_FORCING.main_color_03_faded) || process.env.CSS_MAIN_COLOR_03_FADED;
});
Handlebars.registerHelper('main_color_02_dark', function (context) {
    return (context.data.root.config_rootColors_FORCING && context.data.root.config_rootColors_FORCING.main_color_02_dark) || process.env.CSS_MAIN_COLOR_02_DARK;
});
Handlebars.registerHelper('main_color_node', function (context) {
    return (context.data.root.config_rootColors_FORCING && context.data.root.config_rootColors_FORCING.main_color_node) || process.env.CSS_MAIN_COLOR_NODE;
});
Handlebars.registerHelper('main_color_editor_01', function (context) {
    return (context.data.root.config_rootColors_FORCING && context.data.root.config_rootColors_FORCING.main_color_editor_01) || process.env.CSS_MAIN_COLOR_EDITOR_01;
});
Handlebars.registerHelper('main_color_editor_02', function (context) {
    return (context.data.root.config_rootColors_FORCING && context.data.root.config_rootColors_FORCING.main_color_editor_02) || process.env.CSS_MAIN_COLOR_EDITOR_02;
});
Handlebars.registerHelper('quit_color_01', function (context) {
    return (context.data.root.config_rootColors_FORCING && context.data.root.config_rootColors_FORCING.quit_color_01) || process.env.CSS_QUIT_COLOR_01;
});
Handlebars.registerHelper('quit_color_02', function (context) {
    return (context.data.root.config_rootColors_FORCING && context.data.root.config_rootColors_FORCING.quit_color_02) || process.env.CSS_QUIT_COLOR_02;
});
Handlebars.registerHelper('quit_color_03', function (context) {
    return (context.data.root.config_rootColors_FORCING && context.data.root.config_rootColors_FORCING.quit_color_03) || process.env.CSS_QUIT_COLOR_03;
});
/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : MILLESIME
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
-------------------------------------------------------------------------------
POPULATE HTML
-------------------------------------------------------------------------------
*/

let modalContent_Millesime = document.getElementById("appli_graph_buttons_MILLESIME");
let text_Millesime = "";
text_Millesime += "<div id=\"millesime-container\" class=\"container-fluid pl-0 pr-0 mb-1\">";
text_Millesime += "<div style=\"display: flex; justify-content: space-between; align-items: center;\">";
text_Millesime += "<button id=\"prevYear\" class=\"btn btn-primary\">&lt;&lt;</button>";
text_Millesime += "<div id=\"yearPagination\" style=\"display: flex;\"></div>";
text_Millesime += "<button id=\"nextYear\" class=\"btn btn-primary\">&gt;&gt;</button>";
text_Millesime += "</div>";
text_Millesime += "</div>";
text_Millesime = text_Millesime.concat("");
modalContent_Millesime.innerHTML = text_Millesime;

/*
-------------------------------------------------------------------------------
FUNCTIONS AND BEHAVIOUR
-------------------------------------------------------------------------------
*/

var triggerMillesime_CustomOperations;

let selectedYears = [];

function getUniqueDates(data) {
    let dates = new Set();
    data.forEach(item => {
        if (item) {
            let itemDates = item.split(featuresGraph_MillesimeOptions.millesimeSeparator);
            itemDates.forEach(date => {
                date = date.trim();
                if (date) {
                    dates.add(date);
                }
            });
        };
    });
    return dates;
};

const datesNodes = cy.nodes().map(node => node.data(featuresGraph_MillesimeOptions.millesimeField_Nodes));
const datesEdges = cy.edges().map(edge => edge.data(featuresGraph_MillesimeOptions.millesimeField_Edges));

const uniqueDatesNodes = getUniqueDates(datesNodes);
const uniqueDatesEdges = getUniqueDates(datesEdges);

const yearsToShow = Array.from(new Set([...uniqueDatesNodes, ...uniqueDatesEdges])).sort((a, b) => b - a);

function generateYearPagination(currentYear) {

	const paginationContainer = document.getElementById('yearPagination');

	if (yearsToShow.length === 0) {

		paginationContainer.innerHTML = 'Aucun millésime';

	} else {

		if (!currentYear) {currentYear = yearsToShow[0]};

		paginationContainer.innerHTML = '';

		const currentIndex = yearsToShow.indexOf(currentYear);
	
		let showYearsSelected = [];
	
		if (currentIndex === 0) {
			showYearsSelected = yearsToShow.slice(currentIndex, currentIndex + 2);
			showYearsSelected.unshift('-');
		} else {
			showYearsSelected = yearsToShow.slice(currentIndex - 1, currentIndex + 2);
		};
		if (!yearsToShow[currentIndex + 1]) {showYearsSelected = showYearsSelected.concat(['-'])};
	
		showYearsSelected.forEach((year) => {
			if (year === '-') {
				const spanDot = document.createElement('span');
				spanDot.textContent = '----';
				paginationContainer.appendChild(spanDot);
			} else {
				const button = document.createElement('button');
				button.classList.add('btn', 'btn-link', 'year-btn');
				if (selectedYears.includes(year)) {button.classList.add('year-btn-selected');};
				button.textContent = year;
				button.addEventListener('click', () => {
					handleYearClick(year);
				});
				paginationContainer.appendChild(button);
			};
		});

	};

	windowCustomResize();

};

function handleYearClick(clickedYear) {
	if (featuresGraph.featureBubblesets) {removeBubblesetsPaths();};
	if (selectedYears.includes(clickedYear)) {
		selectedYears = selectedYears.filter(year => year !== clickedYear);
		if (selectedYears.length > 0) {
			let elementsToHidden = cy.elements('['+featuresGraph_MillesimeOptions.millesimeField_Nodes+' *= '+"'"+clickedYear+"'"+']')
			selectedYears.forEach(function(item) {
				elementsToHidden = elementsToHidden.difference('['+featuresGraph_MillesimeOptions.millesimeField_Nodes+' *= '+"'"+item+"'"+']');
			});
			elementsToHidden.addClass("unchecked_millesime");
			elementsToHidden.removeClass("selected_millesime");
		} else {
			cy.elements().removeClass("unchecked_millesime");
			cy.elements().removeClass("selected_millesime");
		};
	} else {
		selectedYears.push(clickedYear);
		cy.elements().difference('.selected_millesime,['+featuresGraph_MillesimeOptions.millesimeField_Nodes+' *= '+"'"+clickedYear+"'"+']').addClass("unchecked_millesime");
		cy.elements('['+featuresGraph_MillesimeOptions.millesimeField_Nodes+' *= '+"'"+clickedYear+"'"+']').addClass("selected_millesime");
		cy.elements('.selected_millesime').removeClass("unchecked_millesime");
	};
	generateYearPagination(currentYear);
    if (featuresGraph.featureBubblesets) {if (authComputeBubblePath) {chain_ModeManager = chain_ModeManager.then(generateBubblesetsPaths.bind(null));};};
	if (triggerMillesime_CustomOperations !== undefined) {
		triggerMillesime_CustomOperations();
	};
};

let currentYear = yearsToShow[0];
generateYearPagination(currentYear);

document.getElementById('prevYear').addEventListener('click', () => {
	const currentIndex = yearsToShow.indexOf(currentYear);
	if (currentIndex > 0) {
		currentYear = yearsToShow[currentIndex - 1];
		generateYearPagination(currentYear);
	};
});

document.getElementById('nextYear').addEventListener('click', () => {
	const currentIndex = yearsToShow.indexOf(currentYear);
	if (currentIndex < yearsToShow.length - 1) {
		currentYear = yearsToShow[currentIndex + 1];
		generateYearPagination(currentYear);
	};
});

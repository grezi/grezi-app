/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : CLUSTERING (expand and collapse nodes and edges)
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
--------------------------------
AURBA.B.04 : Expand/Collapse feature
--------------------------------
*/

/* Initiate the expand/collapse API */

var api;

var optionsExpandCollapse = {
	"fisheye": false,
	"cueEnabled": true,
	"zIndex": 3,
	"edgeTypeInfo": "type",
	"groupEdgesOfSameTypeOnCollapse": true
};

var redoCollapseOnNodes;

var avoidLayoutAfterExpand = false;

cy.expandCollapse(optionsExpandCollapse);

/* Expand/collapse events */

cy.on("expandcollapse.beforecollapse", function(event) {
	if (graphIsLoad === true) {
		cy.elements().unselect();
		api.expandAllEdges();
		if (event.target.hasClass("subGroup")) {
			event.target.data("mapColorNode",event.target.children()[0].data("mapColorNode"));
			let originalTag = event.target.data("searchTag");
			event.target.children().forEach(function (child_n) {
				let searchTag_n = child_n.data("searchTag");
				for (label_k in searchTag_n) {
					if (label_k !== "Labels") {
						for (tag_t in searchTag_n[label_k]) {
							if (searchTag_n[label_k][tag_t] !== undefined) {
								if (originalTag[label_k].indexOf(searchTag_n[label_k][tag_t]) === -1) {originalTag[label_k].push(searchTag_n[label_k][tag_t]);};
							};
						};
					};
				};
			});
		};
	};
});

cy.on("expandcollapse.beforeexpand", function(event) {
	if (graphIsLoad === true) {
		cy.elements().unselect();
		api.expandAllEdges();
	};
});

cy.on("expandcollapse.afterexpand", function(event) {
	if (graphIsLoad === true) {
		cy.elements().unselect();
		api.collapseAllEdges();
	};
});

cy.on("expandcollapse.aftercollapse", function(event) {
	if (graphIsLoad === true) {
		cy.elements().unselect();
		api.collapseAllEdges();
		if (featuresGraph.featureSearchBar && !ongoingReinitialize) {creatingSearchBar(true,false);};
	};
});

cy.on("expandcollapse.afterexpand", function(event) {
	if (graphIsLoad === true) {
		if (avoidLayoutAfterExpand === false) {
			let thisDescendants = event.target.descendants();
			let layoutDescendants = thisDescendants.layout({
				"name":"fcose",
				"quality": "default",
				"animate": true,
				"fit": false,
				"tile": true,
				"nodeDimensionsIncludeLabels": true
			});
			layoutDescendants.run();
		};
		if (featuresGraph.featureSearchBar && !ongoingReinitialize) {creatingSearchBar(true,false);};
	};
});

/* Run the expand/collapse api */

api = cy.expandCollapse("get");

/* Expand/collapse search bar consideration */

function toggleSearchBar_CustomOpening () {
	return new Promise((resolve_done) => {
		redoCollapseOnNodes = api.expandableNodes(cy.nodes());
		redoCollapseOnNodes = redoCollapseOnNodes.difference(cy.$(".unchecked"));;
		resolve_done(api.expandRecursively());
	});
};

function toggleSearchBar_CustomClosing () {
	return new Promise((resolve_done) => {
		resolve_done(api.collapse(api.collapsibleNodes(redoCollapseOnNodes)));
	});
};
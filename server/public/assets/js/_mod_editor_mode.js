/*
 * Copyright : GREZI
 * Author : Adrien Solacroup
 * Encoding : UTF-8
 * Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
 * Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
 */

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : EDITOR MODE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/* Global variables */

cy.dblclick();

let chain_EditorModeProcessing = Promise.resolve();

var tapped_dom = false;

var tapped_node = false;

var popperEDITOR = '';

var nodeEdited;

var x = null;
var y = null;

var physicsMemory = false;

function onMouseUpdate(e) {
    x = e.pageX;
    y = e.pageY;
}

document.addEventListener('mousemove', onMouseUpdate, false);
document.addEventListener('mouseenter', onMouseUpdate, false);

function getMouseX() {
    return x;
}

function getMouseY() {
    return y;
}

function createNode(label, x_pos, y_pos) {
    var newNode = { group: 'nodes', data: {} };
    newNode['data']['id'] = 'en_' + cy.$("[id ^= 'en_']").length;
    newNode['data']['label'] = label;
    newNode_renderedPosition = { x: x, y: y };
    cy.add(newNode);
    cy.getElementById(newNode['data']['id']).addClass('defaultNode');
    cy.getElementById(newNode['data']['id']).renderedPosition(newNode_renderedPosition);
}

/* Creating the edge handles */

var eh = cy.edgehandles({
    handleNodes: 'node:childless.defaultNode',
    complete: function (sourceNode, targetNode, addedEles) {
        if (targetNode.hasClass('infoBubble') || targetNode.hasClass('infoTag') || targetNode.hasClass('compoundCorner') || targetNode.isParent()) {
            cy.remove(addedEles);
        } else {
            var formContent = document.getElementById('editor_form_edge');
            addedEles.style({ label: formContent.value });
        }
    },
});

cy.style()
    .selector('.eh-handle')
    .style({
        label: 'Lier',
        color: getComputedStyle(document.documentElement).getPropertyValue('--main_color_editor_01'),
        'font-weight': 'bold',
        'font-size': '10px',
        'text-valign': 'center',
        'background-color': getComputedStyle(document.documentElement).getPropertyValue('--main_color_editor_02'),
    });
cy.style()
    .selector('.eh-source')
    .style({
        'border-color': getComputedStyle(document.documentElement).getPropertyValue('--main_color_editor_02'),
        'border-width': '4px',
    });
cy.style()
    .selector('.eh-target')
    .style({
        'background-color': getComputedStyle(document.documentElement).getPropertyValue('--main_color_editor_02'),
    });
cy.style()
    .selector('.eh-ghost-edge')
    .style({
        'line-color': getComputedStyle(document.documentElement).getPropertyValue('--main_color_editor_02'),
        'line-style': 'dashed',
        'target-arrow-color': getComputedStyle(document.documentElement).getPropertyValue('--main_color_editor_02'),
        'target-arrow-shape': 'triangle',
    });
cy.style()
    .selector('.eh-preview')
    .style({
        'line-color': getComputedStyle(document.documentElement).getPropertyValue('--main_color_editor_02'),
        'target-arrow-color': getComputedStyle(document.documentElement).getPropertyValue('--main_color_editor_02'),
        'target-arrow-shape': 'triangle',
    });
cy.style().selector('.eh-ghost-edge.eh-preview-active').style({
    opacity: 0,
});

eh.disable();

/* Bind the function to the button */

function editorModeActivation(closingMode = 'default') {
    return new Promise((resolve_editorMode) => {
        if (closingMode === 'Done') {
            var closingModeAdjust = 'default';
        } else {
            closingModeAdjust = closingMode;
        }

        ongoingProcessingLateralScreen = true;

        if (editorModeActivated === false) {

            chain_EditorModeProcessing = chain_EditorModeProcessing.then(function () {
                return new Promise((resolve_EditorModeProcessing) => {
                    /* Mode button */
                    $('#modeIndicator').html("Mode \"éditeur\" activé <button id=\"modeIndicator_QuitButton\" class=\"btn btn-primary\" onclick=\"modeManager_toggler();\">Quitter</button>");
                    resolve_EditorModeProcessing('Done');
                });
            });

            chain_EditorModeProcessing = chain_EditorModeProcessing.then(function () {
                return new Promise((resolve_EditorModeProcessing) => {
                    /* Deal with physics engine */
                    if (featuresGraph['physics'] === true) {
                        physicsMemory = true;
                        featuresGraph['physics'] = false;
                        resolve_EditorModeProcessing(InitialLayout('euler_cola', false));
                    } else {
                        physicsMemory = false;
                        resolve_EditorModeProcessing('Done');
                    }
                });
            });

            chain_EditorModeProcessing = chain_EditorModeProcessing.then(function () {
                return new Promise((resolve_EditorModeProcessing) => {
                    /* Deal with physics button */
                    if ($('#toggle_physics').css('display') === 'inline') {
                        featuresGraph['physicsPresence'] = true;
                        $('#toggle_physics').css('display', 'none');
                        $('#toggle_physics_BR').css('display', 'none');
                    } else {
                        featuresGraph['physicsPresence'] = false;
                    }
                    resolve_EditorModeProcessing('Done');
                });
            });

            chain_EditorModeProcessing = chain_EditorModeProcessing.then(function () {
                return new Promise((resolve_EditorModeProcessing) => {
                    /* Generating the editor body */
                    var textTEMP = '';
                    textTEMP = textTEMP.concat('<h1>Mode éditeur</h1>');
                    textTEMP = textTEMP.concat('<div class="lateral-hr"><hr class="my-6"></div>');
                    textTEMP = textTEMP.concat('<div id="editor_accordion">');

                    textTEMP = textTEMP.concat('<div class="card">');
                    textTEMP = textTEMP.concat('<div class="card-header" id="headingOne">');
                    textTEMP = textTEMP.concat(
                        '<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Fonctionnalités d\'édition</button>'
                    );
                    textTEMP = textTEMP.concat('</div>');
                    textTEMP = textTEMP.concat('<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#editor_accordion">');
                    textTEMP = textTEMP.concat(
                        '<p><button class="btn btn-primary mb-1" id="editor_button_saveGraph"><img class="lazyload"  data-src="../../assets/icon/download.svg"></img></button> : sauvegarder le graphe.</p>'
                    );
                    textTEMP = textTEMP.concat('<form>');
                    textTEMP = textTEMP.concat('<div class="custom-control custom-checkbox">');
                    textTEMP = textTEMP.concat('<input type="checkbox" class="custom-control-input" form="form_dowload" name="checkModeDownload" id="checkModeDownload">');
                    textTEMP = textTEMP.concat('<label class="custom-control-label" for="checkModeDownload">Enregistrer les données au format GREZI (sans mise en forme)</label>');
                    textTEMP = textTEMP.concat('</div>');
                    textTEMP = textTEMP.concat('</form>');
                    textTEMP = textTEMP.concat('<hr class="my-6">');
                    textTEMP = textTEMP.concat(
                        '<p><button class="btn btn-primary mb-1" id="editor_button_loadGraph"><img class="lazyload"  data-src="../../assets/icon/upload.svg"></img></button> : charger un graphe.</p>'
                    );
                    textTEMP = textTEMP.concat('<p><input id="editor_file" type="file" accept=".json"></p>');
                    textTEMP = textTEMP.concat('<hr class="my-6">');
                    textTEMP = textTEMP.concat(
                        '<p><button class="btn btn-primary mb-1" id="editor_button_removeSelection"><img class="lazyload"  data-src="../../assets/icon/trash.svg"></img></button> : supprimer la sélection.</p>'
                    );
                    textTEMP = textTEMP.concat(
                        '<p><button class="btn btn-primary mb-1" id="editor_button_restoreSelection"><img class="lazyload"  data-src="../../assets/icon/trash-restore.svg"></img></button> : restaurer la dernière supression.</p>'
                    );
                    textTEMP = textTEMP.concat(
                        '<p><button class="btn btn-primary mb-1" id="editor_button_setAsideSelection"><img class="lazyload"  data-src="../../assets/icon/boxes.svg"></img></button> : mettre de côté la sélection.</p>'
                    );
                    textTEMP = textTEMP.concat('<p><i>Activez la mise de côté avec la touche "Supprimer".</i></p>');
                    textTEMP = textTEMP.concat('<div id="editor_set_aside"></div>');
                    textTEMP = textTEMP.concat('</div>');
                    textTEMP = textTEMP.concat('</div>');

                    textTEMP = textTEMP.concat('<div class="card">');
                    textTEMP = textTEMP.concat('<div class="card-header" id="headingTwo">');
                    textTEMP = textTEMP.concat(
                        '<button class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">Créer / modifier un élément</button>'
                    );
                    textTEMP = textTEMP.concat('</div>');
                    textTEMP = textTEMP.concat('<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#editor_accordion">');
                    textTEMP = textTEMP.concat('<p><input id="editor_form_label" placeholder="Label"></input></p>');
                    textTEMP = textTEMP.concat('<p><textarea id="editor_form_tooltip" placeholder="Descriptif du tooltip"></textarea></p>');
                    textTEMP = textTEMP.concat('<p><textarea id="editor_form_modal" placeholder="Descriptif de la fenêtre modale"></textarea></p>');
                    textTEMP = textTEMP.concat(
                        '<p><button class="btn btn-primary mb-1" id="editor_button_createElement"><img class="lazyload"  data-src="../../assets/icon/plus-circle.svg"></img></button> : créer un élément. <i>(double cliquer dans le canevas fonctionne également)</i></p>'
                    );
                    textTEMP = textTEMP.concat(
                        '<p><button class="btn btn-primary mb-1" id="editor_button_updateElement"><img class="lazyload"  data-src="../../assets/icon/edit.svg"></img></button> : mettre à jour le ou les éléments sélectionnés. <i>(double cliquer dans le canevas fonctionne également)</i></p>'
                    );
                    textTEMP = textTEMP.concat('<p><i>Activez la création (ou la mise à jour) avec la touche "Entrée" (si vous êtes dans la boîte de saisie "Label".</i></p>');
                    textTEMP = textTEMP.concat('</div>');
                    textTEMP = textTEMP.concat('</div>');

                    textTEMP = textTEMP.concat('<div class="card">');
                    textTEMP = textTEMP.concat('<div class="card-header" id="headingThree">');
                    textTEMP = textTEMP.concat(
                        '<button class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">Créer un nouveau lien</button>'
                    );
                    textTEMP = textTEMP.concat('</div>');
                    textTEMP = textTEMP.concat('<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#editor_accordion">');
                    textTEMP = textTEMP.concat('<ul>');
                    textTEMP = textTEMP.concat('<li style="margin:.5em 10% .5em 5%">Survolez un élément pour faire apparaître une mire (où il apparaît marqué «&nbsp;Lier&nbsp;»,</li>');
                    textTEMP = textTEMP.concat('<li style="margin:.5em 10% .5em 5%">Cliquez-glissez sur la mire pour étendre un nouveau lien vers un autre élément,</li>');
                    textTEMP = textTEMP.concat('<li style="margin:.5em 10% .5em 5%">Relâchez pour créer le nouveau lien.</li>');
                    textTEMP = textTEMP.concat('</ul>');
                    textTEMP = textTEMP.concat('<p>Si vous souhaitez associée une sémantique à vos liens, complétez le champ suivant :</p>');
                    textTEMP = textTEMP.concat('<p><input id="editor_form_edge" placeholder="Lien sémantique"></input></p>');
                    textTEMP = textTEMP.concat('</div>');
                    textTEMP = textTEMP.concat('</div>');

                    textTEMP = textTEMP.concat('<div class="card">');
                    textTEMP = textTEMP.concat('<div class="card-header" id="headingFour">');
                    textTEMP = textTEMP.concat(
                        '<button class="btn btn-link" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">Construction rapide</button>'
                    );
                    textTEMP = textTEMP.concat('</div>');
                    textTEMP = textTEMP.concat('<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#editor_accordion">');
                    textTEMP = textTEMP.concat('<p><input id="editor_form_bulkSource" placeholder="Source"></input></p>');
                    textTEMP = textTEMP.concat('<p><input id="editor_form_bulkEdge" placeholder="Lien sémantique"></input></p>');
                    textTEMP = textTEMP.concat('<p><input id="editor_form_bulkTarget" placeholder="Cible"></input></p>');
                    textTEMP = textTEMP.concat(
                        '<p><button class="btn btn-primary mb-1" id="editor_button_bulk"><img class="lazyload"  data-src="../../assets/icon/plus-circle.svg"></img></button> : créer des éléments.</p>'
                    );
                    textTEMP = textTEMP.concat('<p><i>Activez la création avec la touche "Entrée" (si vous êtes dans une des boîtes de saisie.</i></p>');
                    textTEMP = textTEMP.concat('</div>');
                    textTEMP = textTEMP.concat('</div>');

                    textTEMP = textTEMP.concat('<div class="card">');
                    textTEMP = textTEMP.concat('<div class="card-header" id="headingFive">');
                    textTEMP = textTEMP.concat(
                        '<button class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">Mode avancé</button>'
                    );
                    textTEMP = textTEMP.concat('</div>');
                    textTEMP = textTEMP.concat('<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#editor_accordion">');
                    textTEMP = textTEMP.concat('<h3>Détails des éléments :</h3>');
                    textTEMP = textTEMP.concat('<p><textarea id="editor_form_json" placeholder="Configuration au format json"></textarea></p>');
                    textTEMP = textTEMP.concat(
                        '<p><button class="btn btn-primary mb-1" id="editor_button_updateJson"><img class="lazyload"  data-src="../../assets/icon/edit.svg"></img></button> : mettre à jour la configuration du ou des éléments sélectionnés.</p>'
                    );
                    textTEMP = textTEMP.concat('<h3>Position de tous les éléments :</h3>');
                    textTEMP = textTEMP.concat('<p><textarea id="editor_get_positions" placeholder="Cliquez dans le canevas pour obtenir les positions"></textarea></p>');
                    textTEMP = textTEMP.concat('<h3>Position au survol :</h3>');
                    textTEMP = textTEMP.concat('<p>Axe X : <span id="hover_position_X"><i>survoler un élément</i></span></p>');
                    textTEMP = textTEMP.concat('<p>Axe Y : <span id="hover_position_Y"><i>survoler un élément</i></span></p>');
                    textTEMP = textTEMP.concat('<h3>Déplacer les éléments sélectionnés :</h3>');
                    textTEMP = textTEMP.concat('<p><input id="editor_move_elements" placeholder="Nombre de pixels (positifs ou négatifs)"></input></p>');
                    textTEMP = textTEMP.concat('<p><button class="btn btn-primary mb-1" id="editor_button_moveX" style="width:100%;">Déplacer (axe horizontal)</button></p>');
                    textTEMP = textTEMP.concat('<p><button class="btn btn-primary mb-1" id="editor_button_moveY" style="width:100%;">Déplacer (axe vertical)</button></p>');
                    textTEMP = textTEMP.concat('<p><i>Utilisez directement les flèches directionnelles pour déplacer les éléments sélectionnés.</i></p>');
                    textTEMP = textTEMP.concat('</div>');
                    textTEMP = textTEMP.concat('</div>');

                    textTEMP = textTEMP.concat('<div class="card">');
                    textTEMP = textTEMP.concat('<div class="card-header" id="headingSix">');
                    textTEMP = textTEMP.concat(
                        '<button class="btn btn-link" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">Propriétés du gabarit</button>'
                    );
                    textTEMP = textTEMP.concat('</div>');
                    textTEMP = textTEMP.concat('<div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#editor_accordion">');
                    textTEMP = textTEMP.concat("<h3>Styles des groupes d'éléments :</h3>");
                    textTEMP = textTEMP.concat('<p><a href="https://js.cytoscape.org/#style" target="_blank">Aide</a></p>');
                    textTEMP = textTEMP.concat('<p><a id="templateForm_options_Load_BTN">Charger le style existant</a></p>');
                    textTEMP = textTEMP.concat('<p><textarea id="templateForm_options_Modifier" rows="6"></textarea></p>');
                    textTEMP = textTEMP.concat('<p style="text-align:center;"><button class="btn btn-primary mb-1" id="templateForm_options_Modifier_BTN">Valider</button></p>');
                    textTEMP = textTEMP.concat('<h3>Testeur de layout :</h3>');
                    textTEMP = textTEMP.concat('<p><a id="templateForm_layoutButtons_Load_BTN">Charger le dernier layout</a></p>');
                    textTEMP = textTEMP.concat('<p><input id="templateForm_layoutSelector" placeholder="Sélecteur"></input></p>');
                    textTEMP = textTEMP.concat('<p><textarea id="templateForm_layoutButtons" rows="6"></textarea></p>');
                    textTEMP = textTEMP.concat(
                        '<p style="text-align:center;"><button class="btn btn-primary mb-1" id="templateForm_layoutButtons_BTN">Exécuter sur les éléments visés par le sélecteur</button></p>'
                    );
                    textTEMP = textTEMP.concat('</div>');
                    textTEMP = textTEMP.concat('</div>');

                    textTEMP = textTEMP.concat('</div>');
                    textTEMP = textTEMP.concat('<hr class="my-6">');
                    textTEMP = textTEMP.concat('<p><i>N. B. : Toutes les modifications non enregistrées seront perdus lors de la réinitiatilisation du graphe ou de la fermeture de la page.</i></p>');
                    var lateralContent = document.getElementById('tooltipLateral_body');
                    lateralContent.innerHTML = textTEMP;

                    /* Save the current graph */
                    document.getElementById('editor_button_saveGraph').addEventListener('click', function () {
                        cy.nodes().unselect();
                        cy.remove(cy.$('.eh-handle'));
                        cy.remove(cy.$('.infoBubble'));
                        cy.remove(cy.$('.infoTag'));
                        if (document.getElementById('checkModeDownload').checked === false) {
                            var dataStr = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(cy.json(), null, '\t'));
                        } else {
                            var dataStr_TEMP = [];
                            cy.$('node').forEach(function (elem) {
                                if (!elem.hasClass('compoundCorner')) {
                                    var elem_TEMP = elem.data();
                                    var incomers_TEMP = [];
                                    elem.incomers('node').forEach(function (elem_incomers) {
                                        incomers_TEMP.push(elem_incomers.id());
                                    });
                                    elem_TEMP['sourcesNodes'] = incomers_TEMP;
                                    dataStr_TEMP.push(elem_TEMP);
                                }
                            });
                            var dataStr = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(dataStr_TEMP, null, '\t'));
                        }
                        var downloadAnchorNode = document.createElement('a');
                        downloadAnchorNode.setAttribute('href', dataStr);
                        downloadAnchorNode.setAttribute('download', 'graphe.json');
                        document.body.appendChild(downloadAnchorNode);
                        downloadAnchorNode.click();
                        downloadAnchorNode.remove();
                    });

                    /* Load a graph from a json */
                    document.getElementById('editor_button_loadGraph').addEventListener('click', function () {
                        if ($('#editor_file').prop('files')[0] === undefined) {
                            var otherMessage = document.getElementById('otherModal_body');
                            otherMessage.innerHTML = "Sélectionnez un fichier à charger avant d'appuyer sur ce boutton.";
                            $('#otherModal').modal('show');
                        } else {
                            var fileReader = new FileReader();
                            fileReader.onload = function () {
                                var data = fileReader.result;
                                cy.json(JSON.parse(data));
                            };
                            fileReader.readAsText($('#editor_file').prop('files')[0]);
                        }
                    });

                    /* Remove selection */
                    var stampedNodes = undefined;
                    document.getElementById('editor_button_removeSelection').addEventListener('click', function () {
                        stampedNodes = cy.$('node:selected').union(cy.$('node:selected').connectedEdges());
                        cy.remove(stampedNodes);
                        stampedEdges = cy.$('edge:selected');
                        cy.remove(stampedEdges);
                    });

                    /* Restore selection */
                    document.getElementById('editor_button_restoreSelection').addEventListener('click', function () {
                        if (stampedNodes !== undefined) {
                            cy.add(stampedNodes);
                            stampedNodes = undefined;
                        }
                    });

                    /* Set aside selection */
                    document.getElementById('editor_button_setAsideSelection').addEventListener('click', function () {
                        cy.$('edge:selected').remove();
                        stampedNodes = cy.$('node:selected').union(cy.$('node:selected').connectedEdges());
                        if (stampedNodes.length > 0) {
                            cy.nodes().unselect();
                            cy.remove(stampedNodes);
                            var setAsideContent = document.getElementById('editor_set_aside');
                            var setAsideNodes = JSON.stringify(stampedNodes.jsons());
                            setAsideNodes = setAsideNodes.split("'").join("\\'");
                            setAsideNodes = setAsideNodes.split('"').join("'");
                            setAsideContent.innerHTML =
                                setAsideContent.innerHTML +
                                '<a onclick="cy.add(' +
                                setAsideNodes +
                                ');this.remove();">' +
                                stampedNodes.nodes().length +
                                ' élement(s) et ' +
                                stampedNodes.edges().length +
                                ' lien(s)<br></a>';
                        }
                        stampedNodes = undefined;
                    });
                    $('body').keydown(function (event) {
                        if (event.code === 'Delete') {
                            event.preventDefault();
                            document.getElementById('editor_button_setAsideSelection').click();
                        }
                    });

                    /* Auto-editing the values of the element section */
                    cy.on('mouseover', 'node:childless.defaultNode', function () {
                        if (cy.$('node:selected').length === 0) {
                            var formContent = document.getElementById('editor_form_label');
                            if (formContent) {formContent.value = this.data('label');};
                            var formContent = document.getElementById('editor_form_tooltip');
                            if (formContent) {formContent.value = this.data('title');};
                            var formContent = document.getElementById('editor_form_modal');
                            if (formContent) {formContent.value = this.data('titleComplete');};
                            var formContent = document.getElementById('editor_form_bulkSource');
                            if (formContent) {formContent.value = this.data('label');};
                            var formContent = document.getElementById('editor_form_bulkEdge');
                            if (formContent) {formContent.value = '';};
                            var formContent = document.getElementById('editor_form_bulkTarget');
                            if (formContent) {formContent.value = this.data('label');};
                            var formContent = document.getElementById('editor_form_json');
                            if (formContent) {formContent.value = JSON.stringify(this.json(), null, '\t');};
                        }
                    });
                    cy.on('mouseout', 'node:childless.defaultNode', function () {
                        if (cy.$('node:selected').length === 0) {
                            var formContent = document.getElementById('editor_form_label');
                            if (formContent) {formContent.value = '';};
                            var formContent = document.getElementById('editor_form_tooltip');
                            if (formContent) {formContent.value = '';};
                            var formContent = document.getElementById('editor_form_modal');
                            if (formContent) {formContent.value = '';};
                            var formContent = document.getElementById('editor_form_json');
                            if (formContent) {formContent.value = '';};
                        }
                    });
                    cy.on('mouseover', 'edge', function () {
                        if (cy.$('node:selected').length === 0) {
                            var formContent = document.getElementById('editor_form_json');
                            if (formContent) {formContent.value = JSON.stringify(this.json(), null, '\t');};
                        }
                    });
                    cy.on('mouseout', 'edge', function () {
                        if (cy.$(':selected').length === 0) {
                            var formContent = document.getElementById('editor_form_json');
                            if (formContent) {formContent.value = '';};
                        }
                    });

                    /* Auto-editing the template configuation */
                    var formContent = document.getElementById('templateForm_options_Modifier');
                    if (formContent) {formContent.value = JSON.stringify(options_Modifier, null, '\t');};
                    var formContent = document.getElementById('templateForm_layoutSelector');
                    if (formContent) {formContent.value = ':visible';};
                    var formContent = document.getElementById('templateForm_layoutButtons');
                    var cleanLayoutObject = {};
                    var keysToIgnore = ['_prototype', 'cy', 'eles', 'ready', 'stop'];
                    for (key in mainLayout.options) {
                        if (!keysToIgnore.includes(key)) {
                            cleanLayoutObject[key] = mainLayout.options[key];
                        }
                    }
                    if (formContent) {formContent.value = JSON.stringify(cleanLayoutObject, null, '\t');};

                    /* Creation of elements */
                    document.getElementById('editor_button_createElement').addEventListener('click', function () {
                        var formContent = document.getElementById('editor_form_label');
                        if (cy.$('[label = "' + formContent.value + '"]').length === 0) {
                            var newNode = { group: 'nodes', data: {} };
                            newNode['data']['id'] = 'nn_' + cy.$("[id ^= 'nn_']").length;
                            if (formContent.value !== '') {
                                newNode['data']['label'] = formContent.value;
                            } else {
                                newNode['data']['label'] = '';
                            }
                            var formContent = document.getElementById('editor_form_tooltip');
                            if (formContent.value !== '') {
                                newNode['data']['title'] = formContent.value;
                            } else {
                                newNode['data']['title'] = '';
                            }
                            var formContent = document.getElementById('editor_form_modal');
                            if (formContent.value !== '') {
                                newNode['data']['titleComplete'] = formContent.value;
                            } else {
                                newNode['data']['titleComplete'] = '';
                            }
                            var cyBoundingBox = cy.nodes().boundingBox();
                            newNode['position'] = { x: cyBoundingBox['x1'], y: cyBoundingBox['y1'] };
                            cy.add(newNode);
                            cy.getElementById(newNode['data']['id']).addClass('defaultNode');
                            cy.remove(cy.$('.infoBubble'));
                            cy.remove(cy.$('.infoTag'));
                            if (modulesList.includes('cytoscape/3.11.0/plugins/cytoscape-cola.min.js')) {
                                InitialLayout('euler_cola', true);
                            } else {
                                if (modulesList.includes('cytoscape/3.11.0/plugins/cytoscape-fcose.min.js')) {
                                    fcoseLayout(true);
                                } else {
                                    ForceStabilization();
                                }
                            }
                        }
                    });
                    $('#editor_form_label').keydown(function (event) {
                        if (event.which === 13) {
                            event.preventDefault();
                            if (cy.$('node:selected').length > 0) {
                                document.getElementById('editor_button_updateElement').click();
                            } else {
                                document.getElementById('editor_button_createElement').click();
                            }
                        }
                    });

                    /* Update of elements */
                    document.getElementById('editor_button_updateElement').addEventListener('click', function () {
                        cy.$('node:selected').forEach(function (elem) {
                            var formContent = document.getElementById('editor_form_label');
                            if (formContent.value !== '') {
                                elem.data('label', formContent.value);
                            }
                            var formContent = document.getElementById('editor_form_tooltip');
                            if (formContent.value !== '') {
                                elem.data('title', formContent.value);
                            }
                            var formContent = document.getElementById('editor_form_modal');
                            if (formContent.value !== '') {
                                elem.data('titleComplete', formContent.value);
                            }
                        });
                    });

                    /* Bulk creation */
                    document.getElementById('editor_button_bulk').addEventListener('click', function () {
                        /* Preprocessing */
                        var cyBoundingBox = cy.nodes().boundingBox();
                        /* Source node */
                        var formContent = document.getElementById('editor_form_bulkSource');
                        if (cy.$('[label = "' + formContent.value + '"]').length === 0) {
                            var newNode_source = { group: 'nodes', data: {} };
                            newNode_source['data']['id'] = 'nn_' + cy.$("[id ^= 'nn_']").length;
                            if (formContent.value !== '') {
                                newNode_source['data']['label'] = formContent.value;
                            } else {
                                newNode_source['data']['label'] = '';
                            }
                            newNode_source['position'] = { x: cyBoundingBox['x2'], y: cyBoundingBox['y2'] };
                            cy.add(newNode_source);
                            cy.getElementById(newNode_source['data']['id']).addClass('defaultNode');
                            var idSource_tmp = newNode_source['data']['id'];
                        } else {
                            var idSource_tmp = cy.$('[label = "' + formContent.value + '"]').id();
                        }
                        /* Target node */
                        var formContent = document.getElementById('editor_form_bulkTarget');
                        if (cy.$('[label = "' + formContent.value + '"]').length === 0) {
                            var newNode_target = { group: 'nodes', data: {} };
                            newNode_target['data']['id'] = 'nn_' + cy.$("[id ^= 'nn_']").length;
                            if (formContent.value !== '') {
                                newNode_target['data']['label'] = formContent.value;
                            } else {
                                newNode_target['data']['label'] = '';
                            }
                            newNode_target['position'] = { x: cyBoundingBox['x2'] + 10 + cyBoundingBox['w'] / 4, y: cyBoundingBox['y2'] + 10 + cyBoundingBox['h'] / 4 };
                            cy.add(newNode_target);
                            cy.getElementById(newNode_target['data']['id']).addClass('defaultNode');
                            var idTarget_tmp = newNode_target['data']['id'];
                        } else {
                            var idTarget_tmp = cy.$('[label = "' + formContent.value + '"]').id();
                        }
                        /* Edge */
                        var formContent = document.getElementById('editor_form_bulkEdge');
                        var linkNumber = cy.$("[id ^= 'link_']").length;
                        var existingLabel = false;
                        cy.$("[id ^= '" + 'link_' + idSource_tmp + '_' + idTarget_tmp + "']").forEach(function (elem) {
                            if (elem.data('label') === formContent.value) {
                                existingLabel = true;
                            }
                        });
                        if (cy.$("[id ^= '" + 'link_' + idSource_tmp + '_' + idTarget_tmp + "']").length === 0 || existingLabel === false) {
                            cy.add({
                                group: 'edges',
                                data: { id: 'link_' + idSource_tmp + '_' + idTarget_tmp + '_' + linkNumber, source: idSource_tmp, target: idTarget_tmp, label: formContent.value },
                            });
                            cy.getElementById('link_' + idSource_tmp + '_' + idTarget_tmp + '_' + linkNumber).style({ label: formContent.value });
                        }
                        /* Postprocessing */
                        cy.remove(cy.$('.infoBubble'));
                        cy.remove(cy.$('.infoTag'));
                        if (modulesList.includes('cytoscape/3.11.0/plugins/cytoscape-cola.min.js')) {
                            InitialLayout('euler_cola', true);
                        } else {
                            if (modulesList.includes('cytoscape/3.11.0/plugins/cytoscape-fcose.min.js')) {
                                fcoseLayout(true);
                            } else {
                                ForceStabilization();
                            }
                        }
                    });
                    $('#editor_form_bulkSource').keydown(function (event) {
                        if (event.which === 13) {
                            event.preventDefault();
                            document.getElementById('editor_button_bulk').click();
                        }
                    });
                    $('#editor_form_bulkEdge').keydown(function (event) {
                        if (event.which === 13) {
                            event.preventDefault();
                            document.getElementById('editor_button_bulk').click();
                        }
                    });
                    $('#editor_form_bulkTarget').keydown(function (event) {
                        if (event.which === 13) {
                            event.preventDefault();
                            document.getElementById('editor_button_bulk').click();
                        }
                    });

                    /* Update of json */
                    document.getElementById('editor_button_updateJson').addEventListener('click', function () {
                        cy.$('node:selected').forEach(function (elem) {
                            var formContent = document.getElementById('editor_form_json');
                            if (formContent.value !== '') {
                                elem.json(JSON.parse(formContent.value));
                            }
                        });
                    });

                    /* Position of hovered element */
                    cy.on('mouseover', 'node:childless', function () {
                        var positionContent = document.getElementById('hover_position_X');
                        positionContent.innerHTML = '<b>' + this.position('x') + '</b>';
                        var positionContent = document.getElementById('hover_position_Y');
                        positionContent.innerHTML = '<b>' + this.position('y') + '</b>';
                    });
                    cy.on('mouseout', 'node:childless', function () {
                        var positionContent = document.getElementById('hover_position_X');
                        positionContent.innerHTML = '<i>survoler un élément</i>';
                        var positionContent = document.getElementById('hover_position_Y');
                        positionContent.innerHTML = '<i>survoler un élément</i>';
                    });

                    /* Moving elements */
                    document.getElementById('editor_button_moveX').addEventListener('click', function () {
                        cy.$('node:selected').forEach(function (elem) {
                            var formContent = document.getElementById('editor_move_elements');
                            var x_temp = elem.position()['x'];
                            elem.position('x', x_temp + parseInt(formContent.value));
                        });
                    });
                    document.getElementById('editor_button_moveY').addEventListener('click', function () {
                        cy.$('node:selected').forEach(function (elem) {
                            var formContent = document.getElementById('editor_move_elements');
                            var y_temp = elem.position()['y'];
                            elem.position('y', y_temp + parseInt(formContent.value));
                        });
                    });
                    $('body').keydown(function (event) {
                        var formContent = document.getElementById('editor_move_elements');
                        if (event.code === 'ArrowLeft') {
                            event.preventDefault();
                            formContent.value = -1 * Math.abs(parseInt(formContent.value));
                            document.getElementById('editor_button_moveX').click();
                        }
                        if (event.code === 'ArrowRight') {
                            event.preventDefault();
                            formContent.value = 1 * Math.abs(parseInt(formContent.value));
                            document.getElementById('editor_button_moveX').click();
                        }
                        if (event.code === 'ArrowUp') {
                            event.preventDefault();
                            formContent.value = -1 * Math.abs(parseInt(formContent.value));
                            document.getElementById('editor_button_moveY').click();
                        }
                        if (event.code === 'ArrowDown') {
                            event.preventDefault();
                            formContent.value = 1 * Math.abs(parseInt(formContent.value));
                            document.getElementById('editor_button_moveY').click();
                        }
                    });

                    /* Template Configuration : options_Modifier */
                    document.getElementById('templateForm_options_Load_BTN').addEventListener('click', function () {
                        var formContent = document.getElementById('templateForm_options_Modifier');
                        formContent.value = JSON.stringify(cy.style().json(), null, '\t');
                    });
                    document.getElementById('templateForm_options_Modifier_BTN').addEventListener('click', function () {
                        var formContent = document.getElementById('templateForm_options_Modifier');
                        options_Modifier = JSON.parse(formContent.value.replace(/(\r\n|\n|\r|\t)/gm, ''));
                        for (i_group in options_Modifier) {
                            options['style'].push(options_Modifier[i_group]);
                            cy.style(options['style']);
                        }
                    });
                    $('#templateForm_options_Modifier').keydown(function (event) {
                        if (event.which === 13) {
                            event.preventDefault();
                            document.getElementById('templateForm_options_Modifier_BTN').click();
                        }
                    });

                    /* Template Configuration : layout */
                    document.getElementById('templateForm_layoutButtons_Load_BTN').addEventListener('click', function () {
                        var formContent = document.getElementById('templateForm_layoutButtons');
                        var cleanLayoutObject = {};
                        var keysToIgnore = ['_prototype', 'cy', 'eles', 'ready', 'stop'];
                        for (key in mainLayout.options) {
                            if (!keysToIgnore.includes(key)) {
                                cleanLayoutObject[key] = mainLayout.options[key];
                            }
                        }
                        formContent.value = JSON.stringify(cleanLayoutObject, null, '\t');
                    });
                    document.getElementById('templateForm_layoutButtons_BTN').addEventListener('click', function () {
                        var formContent_Selector = document.getElementById('templateForm_layoutSelector');
                        var formContent_Template = document.getElementById('templateForm_layoutButtons');
                        mainLayout.stop();
                        mainLayout = cy.$(formContent_Selector.value).layout(JSON.parse(formContent_Template.value.replace(/(\r\n|\n|\r|\t)/gm, '')));
                        mainLayout.run();
                    });
                    $('#templateForm_layoutButtons').keydown(function (event) {
                        if (event.which === 13) {
                            event.preventDefault();
                            document.getElementById('templateForm_layoutButtons_BTN').click();
                        }
                    });

                    /* Color button filter */
                    applyColorTransform('#editor_button_saveGraph > img', '', featuresColors_FILTER);
                    applyColorTransform('#editor_button_loadGraph > img', '', featuresColors_FILTER);
                    applyColorTransform('#editor_button_removeSelection > img', '', featuresColors_FILTER);
                    applyColorTransform('#editor_button_restoreSelection > img', '', featuresColors_FILTER);
                    applyColorTransform('#editor_button_setAsideSelection > img', '', featuresColors_FILTER);
                    applyColorTransform('#editor_button_createElement > img', '', featuresColors_FILTER);
                    applyColorTransform('#editor_button_updateElement > img', '', featuresColors_FILTER);

                    /* Simple mode (update) */
                    function simpleUpdate(node) {
                        if (popperEDITOR === '') {
                            $('#appli_graph_network_CONTAINER').css('pointer-events', 'none');

                            nodeEdited = node.id();

                            popperEDITOR = node.popper({
                                content: () => {
                                    let tooltipTEMP = document.createElement('div');
                                    tooltipTEMP.id = 'popperEditor';
                                    tooltipTEMP.innerHTML = '<span id="editorSimple_span">Mettre à jour</span><input id="editorSimple_form_label" placeholder="Label"></input>';
                                    document.body.appendChild(tooltipTEMP);
                                    let tooltipForm = document.getElementById('editorSimple_form_label');
                                    tooltipForm.value = node.data('label');
                                    tooltipForm.focus();
                                    $('#editorSimple_form_label').keydown(function (event) {
                                        if (event.which === 13) {
                                            if (popperEDITOR !== '') {
                                                cy.getElementById(nodeEdited).data('label', document.getElementById('editorSimple_form_label').value);
                                                popperEDITOR.destroy();
                                                document.getElementById('popperEditor').remove();
                                                popperEDITOR = '';
                                                $('#appli_graph_network_CONTAINER').css('pointer-events', 'auto');
                                            }
                                        }
                                    });
                                    $('#editorSimple_form_label').blur(function () {
                                        if (popperEDITOR !== '') {
                                            popperEDITOR.destroy();
                                            document.getElementById('popperEditor').remove();
                                            popperEDITOR = '';
                                            $('#appli_graph_network_CONTAINER').css('pointer-events', 'auto');
                                        }
                                    });
                                    return tooltipTEMP;
                                },
                                popper: {
                                    placement: 'left',
                                    modifiers: {
                                        flip: { behavior: ['left', 'bottom', 'top'] },
                                        preventOverflow: { boundariesElement: document.getElementById('appli_graph_main_screen') },
                                    },
                                },
                            });
                        }
                    }
                    cy.on('dblclick', 'node', function () {
                        simpleUpdate(this);
                    });
                    cy.on('touchstart', 'node', function () {
                        if (!tapped_node) {
                            tapped_node = setTimeout(function () {
                                tapped_node = null;
                            }, 300);
                        } else {
                            simpleUpdate(this);
                            clearTimeout(tapped_node);
                            tapped_node = null;
                        }
                    });

                    /* Simple mode (creation) */
                    function simpleCreation() {
                        if (popperEDITOR === '') {
                            $('#appli_graph_network_CONTAINER').css('pointer-events', 'none');

                            popperEDITOR = cy.popper({
                                content: () => {
                                    let tooltipTEMP = document.createElement('div');
                                    tooltipTEMP.id = 'popperEditor';
                                    tooltipTEMP.innerHTML = '<span id="editorSimple_span">Créer</span><input id="editorSimple_form_label" placeholder="Label"></input>';
                                    document.body.appendChild(tooltipTEMP);
                                    let tooltipForm = document.getElementById('editorSimple_form_label');
                                    tooltipForm.focus();
                                    $('#editorSimple_form_label').keydown(function (event) {
                                        if (event.which === 13) {
                                            if (popperEDITOR !== '') {
                                                createNode(document.getElementById('editorSimple_form_label').value, x, y);
                                                popperEDITOR.destroy();
                                                document.getElementById('popperEditor').remove();
                                                popperEDITOR = '';
                                                $('#appli_graph_network_CONTAINER').css('pointer-events', 'auto');
                                            }
                                        }
                                    });
                                    $('#editorSimple_form_label').blur(function () {
                                        if (popperEDITOR !== '') {
                                            popperEDITOR.destroy();
                                            document.getElementById('popperEditor').remove();
                                            popperEDITOR = '';
                                            $('#appli_graph_network_CONTAINER').css('pointer-events', 'auto');
                                        }
                                    });
                                    return tooltipTEMP;
                                },
                                popper: {
                                    modifiers: { preventOverflow: { boundariesElement: document.getElementById('appli_graph_main_screen') } },
                                },
                                renderedPosition: () => ({
                                    x: x,
                                    y: y,
                                }),
                            });
                        }
                    }
                    cy.on('dblclick', function () {
                        simpleCreation();
                    });
                    cy.on('touchstart', function () {
                        if (!tapped_dom) {
                            tapped_dom = setTimeout(function () {
                                tapped_dom = null;
                            }, 300);
                        } else {
                            simpleCreation();
                            clearTimeout(tapped_dom);
                            tapped_dom = null;
                        }
                    });

                    /* Deal cosmetics */

                    $('#appli_graph_lateral_body').css('background-color', getComputedStyle(document.documentElement).getPropertyValue('--main_color_editor_01'));

                    /* Timeout security */

                    setTimeout(function () {
                        resolve_EditorModeProcessing('Done');
                    }, 500);
                });
            });

            chain_EditorModeProcessing = chain_EditorModeProcessing.then(function () {
                return new Promise((resolve_EditorModeProcessing) => {
                    if (isLateral === 'closing') {
                        resolve_EditorModeProcessing(lateralScreen('default'));
                    } else {
                        resolve_EditorModeProcessing('Done');
                    }
                });
            });

            chain_EditorModeProcessing = chain_EditorModeProcessing.then(function () {
                return new Promise((resolve_EditorModeProcessing) => {
                    /* Initialisation */

                    if (featuresGraph['modalSelection'] === true) {
                        featuresGraph['modalSelection'] = false;
                        featuresGraph['modalSelectionPresence'] = true;
                    } else {
                        featuresGraph['modalSelectionPresence'] = false;
                    }

                    cy.remove(cy.$('.infoBubble'));

                    cy.remove(cy.$('.infoTag'));

                    cy.$('.compoundCorner').style('visibility', 'visible');

                    cy.$('.defaultNode').unlock();

                    /* Activate Edge Handles */
                    eh.enable();

                    /* Change mode editor state */
                    editorModeActivated = true;
                    ongoingProcessingLateralScreen = false;

                    /* End the current promise flow */
                    resolve_EditorModeProcessing('Done');

                    /* End the main promise */
                    resolve_editorMode(closingModeAdjust);
                });
            });
        } else {
            /* To close the editor mode */

            chain_EditorModeProcessing = chain_EditorModeProcessing.then(function () {
                return new Promise((resolve_EditorModeProcessing) => {
                    if (closingModeAdjust === 'default') {
                        resolve_EditorModeProcessing(lateralScreen('default'));
                    } else {
                        resolve_EditorModeProcessing('');
                    }
                });
            });

            chain_EditorModeProcessing = chain_EditorModeProcessing.then(function () {
                return new Promise((resolve_EditorModeProcessing) => {

                    $('#modeIndicator').html("");

                    $('#appli_graph_lateral_body').css('background-color', getComputedStyle(document.documentElement).getPropertyValue('--main_color_01'));

                    eh.disable();

                    cy.remove('.eh-handle');

                    cy.removeListener('mouseover', 'node:childless.defaultNode');

                    cy.removeListener('mouseover', 'node:childless');

                    cy.removeListener('mouseout', 'node:childless.defaultNode');

                    cy.removeListener('mouseout', 'node:childless');

                    cy.removeListener('dblclick', 'node');
                    cy.removeListener('touchstart', 'node');

                    cy.removeListener('dblclick');
                    cy.removeListener('touchstart');

                    if (featuresGraph['modalSelectionPresence'] === true) {
                        featuresGraph['modalSelection'] = true;
                        featuresGraph['modalSelectionPresence'] = false;
                    }

                    if (featuresGraph['infoBubbles'] === true) {
                        informationBubbles(cy.nodes());
                    }

                    cy.$('.compoundCorner').style('visibility', 'hidden');

                    if (featuresGraph['static']) {
                        cy.$('.defaultNode').lock();
                    }

                    resolve_EditorModeProcessing('Done');
                });
            });

            chain_EditorModeProcessing = chain_EditorModeProcessing.then(function () {
                return new Promise((resolve_EditorModeProcessing) => {
                    if (featuresGraph['mapPresence'] === true) {
                        $('#toggle_modeMap').css('display', 'inline');
                        $('#toggle_modeMap_BR').css('display', 'inline');
                        resolve_EditorModeProcessing('Done');
                    } else {
                        resolve_EditorModeProcessing('Done');
                    }
                });
            });

            chain_EditorModeProcessing = chain_EditorModeProcessing.then(function () {
                return new Promise((resolve_EditorModeProcessing) => {
                    if (featuresGraph['physicsPresence'] === true) {
                        $('#toggle_physics').css('display', 'inline');
                        $('#toggle_physics_BR').css('display', 'inline');
                        if (physicsMemory === true) {
                            physicsButton.setAttribute(
                                'style',
                                'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_01_faded') + ' !important'
                            );
                            $('#toggle_physics > a').attr('data-original-title', 'Physique activée');
                            featuresGraph['physics'] = true;
                            resolve_EditorModeProcessing(InitialLayout('euler_cola', false));
                        } else {
                            physicsButton.setAttribute(
                                'style',
                                'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_04') + ' !important'
                            );
                            $('#toggle_physics > a').attr('data-original-title', 'Physique désactivée');
                            resolve_EditorModeProcessing('Done');
                        }
                    } else {
                        resolve_EditorModeProcessing('Done');
                    }
                });
            });

            chain_EditorModeProcessing = chain_EditorModeProcessing.then(function () {
                return new Promise((resolve_EditorModeProcessing) => {

                    /* Regenerating the default lateral screen */

                    var textTEMP = '';
                    textTEMP = textTEMP.concat("<h1>Survolez les cercles pour plus d'informations</h1>");
                    textTEMP = textTEMP.concat('<div class="lateral-hr"><hr class="my-6"></div>');
                    var lateralContent = document.getElementById('tooltipLateral_body');
                    lateralContent.innerHTML = textTEMP;

                    /* Change mode editor state */

                    editorModeActivated = false;

                    ongoingProcessingLateralScreen = false;

                    /* End the current promise flow */

                    resolve_EditorModeProcessing('Done');

                    /* End the main promise */

                    resolve_editorMode(closingModeAdjust);
                });
            });
        }
    });
}

$('#toggle_modeEditor').on('click', function () {
    if (ongoingProcessingLateralScreen === false) {
        ongoingProcessingLateralScreen = true;
        chain_MainLateralScreen = chain_MainLateralScreen.then(modeManager_toggler.bind(null,'editorMode'));
    }
});

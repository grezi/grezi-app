/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : MAP MODE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

let chain_MapModeProcessing = Promise.resolve();

let cyMap = undefined;

var toggleMap_CustomAdditions;

var toggleMap_CustomOpening;

var toggleMap_CustomClosing;

var ongoingProcessingMapMode = false;

var keepAutoungrabify = false;

var average_lng = 0;

var average_lat = 0;

var cornersBackup = cy.nodes(".compoundCorner");

var physicsMemory = false;

cy.on("mouseover", ".mapNode", function() {
	this.addClass("hoveredMapNode");
});

cy.on("mouseout", ".mapNode", function() {
	this.removeClass("hoveredMapNode");
});

function toggleMap() {
	return new Promise((resolve_mapMode) => {

		chain_MapModeProcessing = chain_MapModeProcessing.then(function(){
			return new Promise((resolve_MapModeProcessing) => {
				$("#appli_graph_network_CONTAINER").css("pointer-events","none");
				$("#appli_graph_spinner").fadeIn("fast", function () {
					if (featuresGraph.static) {cy.$(".defaultNode").unlock()};
					cy.nodes().addClass("mapNode");
					cy.edges().addClass("mapEdge");
					resolve_MapModeProcessing("Done");
				});
			});
		});

		if (cyMap === undefined && mapModeActivated === false) {

            chain_MapModeProcessing = chain_MapModeProcessing.then(function () {
                return new Promise((resolve_MapModeProcessing) => {
					mapModeActivated = true;
					mapButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_01_faded")+" !important");
					$("#toggle_modeMap > a").attr("data-original-title","Basculer vers la carte heuristique (vue initiale)");
					resolve_MapModeProcessing('Done');
                });
            });

            chain_MapModeProcessing = chain_MapModeProcessing.then(function () {
                return new Promise((resolve_MapModeProcessing) => {
                    /* Mode button */
                    $('#modeIndicator').html("Mode \"géographique\" activé <button id=\"modeIndicator_QuitButton\" class=\"btn btn-primary\" onclick=\"modeManager_toggler();\">Quitter</button>");
                    resolve_MapModeProcessing('Done');
                });
            });

			chain_MapModeProcessing = chain_MapModeProcessing.then(function(){
				return new Promise((resolve_MapModeProcessing) => {
					if (toggleMap_CustomAdditions !== undefined) {
						resolve_MapModeProcessing(toggleMap_CustomAdditions());
					} else {
						resolve_MapModeProcessing("Done");
					};
				});
			});

			chain_MapModeProcessing = chain_MapModeProcessing.then(function(){
				return new Promise((resolve_MapModeProcessing) => {
			
					/* Deal with physics engine */
					if (featuresGraph["physics"] === true) {
						physicsMemory = true;
						featuresGraph["physics"] = false;
						resolve_MapModeProcessing(InitialLayout("euler_cola",false));
					} else {
						physicsMemory = false;
						resolve_MapModeProcessing("Done");
					};

				});
			});

			chain_MapModeProcessing = chain_MapModeProcessing.then(function(){
				return new Promise((resolve_MapModeProcessing) => {
			
					/* Deal with physics button */
					if ($("#toggle_physics").css("display") === "inline") {
						featuresGraph["physicsPresence"] = true;
						$("#toggle_physics").css("display","none");
						$("#toggle_physics_BR").css("display","none");
					} else {
						featuresGraph["physicsPresence"] = false;
					};

					/* Deal with fcose button */
					if ($("#toggle_fcose").css("display") === "inline") {
						featuresGraph["fcosePresence"] = true;
						$("#toggle_fcose").css("display","none");
						$("#toggle_fcose_BR").css("display","none");
					} else {
						featuresGraph["fcosePresence"] = false;
					};

					/* Deal with ori_pos button */
					if ($("#toggle_ori_pos").css("display") === "inline") {
						featuresGraph["ori_posPresence"] = true;
						$("#toggle_ori_pos").css("display","none");
						$("#toggle_ori_pos_BR").css("display","none");
					} else {
						featuresGraph["ori_posPresence"] = false;
					};

					resolve_MapModeProcessing("Done");

				});
			});

			chain_MapModeProcessing = chain_MapModeProcessing.then(function(){
				return new Promise((resolve_MapModeProcessing) => {
					/* cy.$("node:parent").addClass("mapCompound"); */
					cornersBackup = cy.nodes(".compoundCorner");
					cy.nodes(".compoundCorner").remove();
					if (cy.autoungrabify()) {
						keepAutoungrabify = true;
					} else {
						cy.autoungrabify(true);
					};
					average_lng = 0;
					average_lat = 0;
					cy.nodes().forEach(function(elem) {
						if (elem.data('lng') !== undefined) {average_lng += elem.data('lng');};
						if (elem.data('lat') !== undefined) {average_lat +=elem.data('lat');};
					});
					average_lng = average_lng/cy.nodes("[lng][lat]").length;
					average_lat = average_lat/cy.nodes("[lng][lat]").length;
					resolve_MapModeProcessing("Done");
				});
			});

			chain_MapModeProcessing = chain_MapModeProcessing.then(function(){
				return new Promise((resolve_MapModeProcessing) => {
					cyMap = cy.mapboxgl(
						{
							accessToken: tokenMapbox,
							style: styleMapbox
						},
						{
							getPosition: (node) => {
								if ((node.data('lng') !== undefined) && (node.data('lat') !== undefined)) {
									return [node.data('lng'), node.data('lat')];
								} else {
									node.addClass("unchecked_map");
									return [average_lng, average_lat];
								};
							},
							setPosition: (node, lngLat) => {
								node.data('lng', lngLat.lng);
								node.data('lat', lngLat.lat);
							},
							animate: true,
							animationDuration: 1000,
						}
					);
					resolve_MapModeProcessing("Done");
				});
			});

			chain_MapModeProcessing = chain_MapModeProcessing.then(function(){
				return new Promise((resolve_MapModeProcessing) => {
					cyMap.fit(undefined, {"padding":150});
					resolve_MapModeProcessing("Done");
				});
			});

			chain_MapModeProcessing = chain_MapModeProcessing.then(function(){
				return new Promise((resolve_MapModeProcessing) => {
					if (toggleMap_CustomOpening !== undefined) {
						resolve_MapModeProcessing(toggleMap_CustomOpening());
					} else {
						resolve_MapModeProcessing("Done");
					};
				});
			});

			chain_MapModeProcessing = chain_MapModeProcessing.then(function(){
				return new Promise((resolve_MapModeProcessing) => {
					cyMap.map.once('idle',function(){
						resolve_MapModeProcessing("Done");
					});
				});
			});

			chain_MapModeProcessing = chain_MapModeProcessing.then(function(){
				return new Promise((resolve_MapModeProcessing) => {
					$("#appli_graph_network_CONTAINER").css("pointer-events","initial");
					$("#appli_graph_spinner").fadeOut("fast");
					if (featuresGraph.featureSearchBar) {creatingSearchBar(true,false);};
					resolve_MapModeProcessing("Done");
					resolve_mapMode(ForceStabilization());
					ongoingProcessingMapMode = false;
				});
			});

		} else {

			chain_MapModeProcessing = chain_MapModeProcessing.then(function(){
				return new Promise((resolve_MapModeProcessing) => {
					mapModeActivated = false;
					mapButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_04")+" !important");
					$("#toggle_modeMap > a").attr("data-original-title","Basculer vers la carte géographique");
					$('#modeIndicator').html("");
					$("#appli_graph_network_CONTAINER").css("pointer-events","none");
					$("#appli_graph_spinner").fadeIn("fast");
					resolve_MapModeProcessing(cyMap.destroy());
				});
			});

			chain_MapModeProcessing = chain_MapModeProcessing.then(function(){
				return new Promise((resolve_MapModeProcessing) => {
					cyMap = undefined;
					resolve_MapModeProcessing("Done");
				});
			});

			chain_MapModeProcessing = chain_MapModeProcessing.then(function(){
				return new Promise((resolve_MapModeProcessing) => {
					cornersBackup.restore();
					if (keepAutoungrabify === false) {
						cy.autoungrabify(false);
					};
					keepAutoungrabify = false;
					$("#appli_graph_network_CONTAINER").css("pointer-events","initial");
					cy.nodes().removeClass("unchecked_map");
					/* cy.nodes().removeClass("mapCompound"); */
					resolve_MapModeProcessing("Done");
					ongoingProcessingMapMode = false;
				});
			});

			chain_MapModeProcessing = chain_MapModeProcessing.then(function(){
				return new Promise((resolve_MapModeProcessing) => {
					if (featuresGraph["fcosePresence"] === true) {
						$("#toggle_fcose").css("display","inline");
						$("#toggle_fcose_BR").css("display","inline");
					};
					if (featuresGraph["ori_posPresence"] === true) {
						$("#toggle_ori_pos").css("display","inline");
						$("#toggle_ori_pos_BR").css("display","inline");
					};
					if (featuresGraph["modeEditorPresence"] === true) {
						$("#toggle_modeEditor").css("display","inline");
						$("#toggle_modeEditor_BR").css("display","inline");
					};
					resolve_MapModeProcessing("Done");
				});
			});

			chain_MapModeProcessing = chain_MapModeProcessing.then(function(){
				return new Promise((resolve_MapModeProcessing) => {
					if (featuresGraph["physicsPresence"] === true) {
						$("#toggle_physics").css("display","inline");
						$("#toggle_physics_BR").css("display","inline");
						if (physicsMemory === true) {
							physicsButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_01_faded")+" !important");
							$("#toggle_physics > a").attr("data-original-title","Physique activée");
							featuresGraph["physics"] = true;
							resolve_MapModeProcessing(InitialLayout("euler_cola",false));
						} else {
							physicsButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_04")+" !important");
							$("#toggle_physics > a").attr("data-original-title","Physique désactivée");
							resolve_MapModeProcessing("Done");
						};
					} else {
						resolve_MapModeProcessing("Done");
					};
				});
			});

			chain_MapModeProcessing = chain_MapModeProcessing.then(function(){
				return new Promise((resolve_MapModeProcessing) => {
					if (toggleMap_CustomClosing !== undefined) {
						resolve_MapModeProcessing(toggleMap_CustomClosing());
					} else {
						resolve_MapModeProcessing("Done");
					};
				});
			});

			chain_MapModeProcessing = chain_MapModeProcessing.then(function(){
				return new Promise((resolve_MapModeProcessing) => {
					cy.nodes().removeClass("mapNode");
					cy.edges().removeClass("mapEdge");
					if (featuresGraph.featureSearchBar) {creatingSearchBar(true,false)};
					$("#appli_graph_network_CONTAINER").css("pointer-events","initial");
					$("#appli_graph_spinner").fadeOut("fast");
					resolve_MapModeProcessing("Done");
					resolve_mapMode("Done");
				});
			});

		};

	});
};

var mapButton = document.getElementById("toggle_modeMap");

mapButton.addEventListener("click",function() {
	if (tokenMapbox === "") {
		$("#bug_report_appli").html("<p><b>/!\\</b></p><p><b>Token Mapbox manquant pour utiliser la fonction d'affichage cartographique</b></p><p><b>/!\\</b></p>"); 
		$("#bugModal").modal("show"); 
	} else {
		if (ongoingProcessingMapMode === false) {
			ongoingProcessingMapMode = true;
			if (mapModeActivated === false) {
				modeManager_toggler("mapMode");
			} else {
				modeManager_toggler("mapMode");
			};
		};
	};
});

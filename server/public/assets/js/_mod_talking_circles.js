/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : TALKING CIRCLES
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
INITIALISATION
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
-------------------------------------------------------------------------------
GLOBAL VARIABLES
-------------------------------------------------------------------------------
*/

let chain_reading = Promise.resolve();

let chain_CompleteScreenTyping = Promise.resolve();

var div_talking_text = document.getElementById("_talking_title");

var div_talking_img = document.getElementById("_talking_image");

var pass_instruction = false;

var screenTyping = function(){};

var endingTyping = function(){};

var readingDict = function(){};

var typed;

var typed_Resolve = true;

var typed_ClickNext = false;

var typed_Form = false;

var typed_Button = false;

var currentStringIndex = 0;

var talkingFormMessage = "";

let onCompleteForm_TEXTAREA = document.getElementById("_talking_frameForm_MESSAGE");
onCompleteForm_TEXTAREA.addEventListener("keydown", function(event) {
    if (event.key === "Enter" && !event.shiftKey && !event.ctrlKey && !event.altKey) {
        event.preventDefault();
		let onCompleteForm_BTN = document.getElementById("_talking_frameForm_SEND")
        onCompleteForm_BTN.click();
    }
});

let chain_EventTalkingFrame = Promise.resolve();
function triggerPointerEvents_TalkingFrame (cssValue) {
	chain_EventTalkingFrame = chain_EventTalkingFrame.then(function(){
		return new Promise((resolve_EventTalkingFrame) => {
			$("#_talking_frame").css("pointer-events",cssValue);
			setTimeout(function() {resolve_EventTalkingFrame();},10);

		});
	});
};

/*
--------------------------------
Focus buttons with arrows
-------------------------------- 
*/

var buttonsToFocus = Array.from(document.querySelectorAll('#_talking_frameButtons button, #_talking_frameForm_SEND')).filter(button => window.getComputedStyle(button).display !== 'none');
var focusedButtonIndex = 0;

document.addEventListener('keydown', function(event) {
    if (!typed_Form) {

        if (event.code === 'ArrowRight') {

            focusedButtonIndex++;
            if (focusedButtonIndex < 0) {
                focusedButtonIndex = buttonsToFocus.length - 1;
            } else if (focusedButtonIndex >= buttonsToFocus.length) {
                focusedButtonIndex = 0;
            };
            if (buttonsToFocus[focusedButtonIndex]) {buttonsToFocus[focusedButtonIndex].focus();};

        } else if (event.code === 'ArrowLeft') {

            focusedButtonIndex--;
            if (focusedButtonIndex < 0) {
                focusedButtonIndex = buttonsToFocus.length - 1;
            } else if (focusedButtonIndex >= buttonsToFocus.length) {
                focusedButtonIndex = 0;
            };
            if (buttonsToFocus[focusedButtonIndex]) {buttonsToFocus[focusedButtonIndex].focus();};

        };

    };
});

var observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
        if (mutation.type === 'childList' || mutation.type === 'attributes') {
            buttonsToFocus = Array.from(document.querySelectorAll('#_talking_frameButtons button, #_talking_frameForm_SEND')).filter(button => window.getComputedStyle(button).display !== 'none');
        }
    });
});

var config = { childList: true, subtree: true, attributes: true, attributeFilter: ['style'] };

observer.observe(document.body, config);

/*
-------------------------------------------------------------------------------
PREPROCESSING
-------------------------------------------------------------------------------
*/

/* Options control */
var toggleBubbles;
if (featuresGraph["infoBubbles"] === true) {
	featuresGraph["infoBubbles"] = false;
	toggleBubbles = true;
};
var toggleTags;
if (featuresGraph["infoTags"] === true) {
	featuresGraph["infoTags"] = false;
	toggleTags = true;
};

/*
-------------------------------------------------------------------------------
LOADING JS LIBRARIES
-------------------------------------------------------------------------------
*/

chain_ScriptLoading = chain_ScriptLoading.then(function(){
	return new Promise((resolve_ScriptLoading) => {
		
		/*
		-------------------------------------------------------------------------------
		-------------------------------------------------------------------------------
		SCRIPT EXECUTION
		-------------------------------------------------------------------------------
		-------------------------------------------------------------------------------
		*/
		
		/*
		-------------------------------------------------------------------------------
		TYPING FUNCTIONS
		-------------------------------------------------------------------------------
		*/
		
		screenTyping = function(dictTyping) {
			return new Promise((resolve) => {
				
				if (typed_Resolve === true) {
				
					setTimeout(function() {
						
						if (typed !== undefined) {typed.destroy()};
				
						if ((dictTyping["title"] !== undefined) && (dictTyping["title"] !== "")) {
							div_talking_text.innerHTML = "<h1>" + dictTyping["title"] + "</h1>";
						} else {
							div_talking_text.innerHTML = "";
						}
						
						if ((dictTyping["img"] !== undefined) && (dictTyping["img"] !== "") && (dictTyping["img"] !== false)) {
							$("#_talking_image").css("display","flex");
							document.getElementById("_talking_image").className = "col-2";
							document.getElementById("_talking_body").className = "col-10";
							if (dictTyping.img.startsWith("../../assets")) {
								div_talking_img.innerHTML = "<img src=\""+dictTyping["img"]+"\"></img>";
							} else {
								div_talking_img.innerHTML = "<img src=\""+idGraph_path+dictTyping["img"]+"\"></img>";
							};
						}
						
						if ((dictTyping["img"] === false) || (dictTyping["img"] === "")) {
							$("#_talking_image").css("display","none");
							document.getElementById("_talking_image").className = "col-0";
							document.getElementById("_talking_body").className = "col-12";
							div_talking_img.innerHTML = "";
						};
						
						if (dictTyping["isFadeOut"] === false) {
							var isFadeOut = false;
						} else {
							var isFadeOut = true;
						};
						
						typed = new Typed("#_talking_typed", {

							typeSpeed: 10,
							fadeOut: isFadeOut,
							fadeOutDelay: 0,
							startDelay: 0,
							backDelay: 1500,
							showCursor: false,
							smartBackspace: true,
							contentType: "html",
							strings: dictTyping["strings_to_type"],

							preStringTyped: (arrayPos, self) => {
								if (!$("#_talking_frame").is(":hover")) {document.body.style.cursor = 'default';}
								triggerPointerEvents_TalkingFrame("auto");
								currentStringIndex = arrayPos;
							},

							onBegin: function(self) {
								if (llmActivated) {
									completeLlmHistory(self.strings[0],"assistant");
								};
								$("#_talking_button > button").css("pointer-events","none");
								triggerPointerEvents_TalkingFrame("auto");
								if (typed_ClickNext === false) {
									$("#_talking_next_word").css("display","none");
									if ((dictTyping["onBeginInstructions"] !== undefined) && (dictTyping["onBeginInstructions"] !== "")) {
										let F = new Function (dictTyping["onBeginInstructions"]);
										chain_CompleteScreenTyping = chain_CompleteScreenTyping.then(F);
									};
								} else {
									typed_ClickNext = false;
								};
							},

							onComplete: function(self) {
								resolve(completeTyping(dictTyping));
							},

							onDestroy: function(self) {
								currentStringIndex = 0;
								$("#_talking_button > button").css("pointer-events","auto");
								$("#_talking_next_word").css("display","none");
								resolve("Done");
							}

						});
					
					}, 100);
					
				} else {
					resolve("");
				};
				
			});	
		};

		window.addEventListener("keydown", function(e) {
			if (e.key === ' ' || e.key === 'Spacebar') {
				$("#_talking_frame").click();
			};
		});
			
		$("#_talking_frame").on('click',function(){
			if ($("#_talking_frameForm_MESSAGE").css("display") === "none" && $("#_talking_frameButtons > button").length === 0) {
				if (typed.pause.status === true) {
					$("#_talking_next_word").css("display","none");
					$("#_talking_button > button").css("pointer-events","none");
					if (currentStringIndex < typed.strings.length-1) {
						var typed_array_BCK = typed.strings.slice(currentStringIndex+1);
						typed.strings = typed_array_BCK;
						typed.pause.curString = typed_array_BCK[0];
						typed.start();
					} else {
						typed.destroy();
					};
				} else {
					typed.stop();
					typed_ClickNext = true;
					setTimeout(function(){
						if (currentStringIndex !== typed.strings.length-1) {typed.reset();};
						let div_talking_typed_TEXT = document.getElementById("_talking_typed");
						div_talking_typed_TEXT.innerHTML = typed.strings[currentStringIndex].replace(/\^\d+/g,"");
						if (currentStringIndex === typed.strings.length-1) {
							$("#_talking_next_word").css("display","none");
							typed.options.onComplete(typed);
						} else {
							$("#_talking_button > button").css("pointer-events","auto");
							$("#_talking_next_word").css("display","initial");
						};
					},50);
				};
			};
		});
		
		$("#_talking_frame").on('mouseover',function(){
			document.body.style.cursor = 'pointer';
		});
		
		$("#_talking_frame").on('mouseout',function(){
			document.body.style.cursor = 'default';
		});
		
		/*
		-------------------------------------------------------------------------------
		COMPLETE FUNCTION
		-------------------------------------------------------------------------------
		*/

		completeTyping = function(dictTyping) {
			return new Promise((resolve) => {
				if (typed_Resolve === true) {
					if (dictTyping["pauseOncomplete"] === true && typed.pause.status !== true) {
						if (!(dictTyping["onCompleteButtons"] !== undefined && dictTyping["onCompleteButtons"].length > 0)) {
							$("#_talking_button > button").css("pointer-events","auto");
							$("#_talking_next_word").css("display","initial");
						};
						typed.stop();
					};
					if ((dictTyping["onCompleteInstructions"] !== undefined && dictTyping["onCompleteInstructions"] !== "") || (dictTyping?.onCompleteForm?.activate) || (dictTyping["onCompleteButtons"] !== undefined && dictTyping["onCompleteButtons"].length > 0)) {

						/* Step "onCompleteInstructions" */
						if ((dictTyping["onCompleteInstructions"] !== undefined) && (dictTyping["onCompleteInstructions"] !== "")) {
							let F;
							if (dictTyping["onCompleteInstructions"].indexOf("resolve_F") > -1) {
								F = new Function ("return new Promise((resolve_F) => {"+dictTyping["onCompleteInstructions"]+"});");
							} else {
								F = new Function ("return new Promise((resolve_F) => {"+dictTyping["onCompleteInstructions"]+";resolve_F(\"\");"+"});");
							};
							chain_CompleteScreenTyping = chain_CompleteScreenTyping.then(F);
						};

						let promiseCompleteButton = new Promise((resolve_bis) => {

							/* Step "onCompleteForm" */
							if (dictTyping?.onCompleteForm?.activate) {
								typed_Form = true;
								triggerPointerEvents_TalkingFrame("none");
								$("#_talking_frameForm_MESSAGE").css("display","initial");
								$("#_talking_frameForm_SEND").css("display","initial");
								let onCompleteForm_BTN = document.getElementById("_talking_frameForm_SEND");
								document.getElementById("_talking_frameForm_MESSAGE").focus();
								onCompleteForm_BTN.addEventListener("click", function onClick() {
									$("#_talking_frameForm_MESSAGE").css("display","none");
									$("#_talking_frameForm_SEND").css("display","none");
									let onCompleteForm_TEXTAREA = document.getElementById("_talking_frameForm_MESSAGE");
									talkingFormMessage = onCompleteForm_TEXTAREA.value;
									onCompleteForm_TEXTAREA.value = "";
									if ((dictTyping["onCompleteButtons"] !== undefined) && (dictTyping["onCompleteButtons"].length > 0)) {
										dictTyping["onCompleteButtons"].forEach(function(buttonToCreate,index) {
											let onCompleteButton_TMP_BIS = document.getElementById("onCompleteButton_"+index);
											if (onCompleteButton_TMP_BIS) {onCompleteButton_TMP_BIS.removeEventListener("click", onClick);};
										});
										let onCompleteButton_DIV_BIS = document.getElementById("_talking_frameButtons");
										onCompleteButton_DIV_BIS.innerHTML = "";
									};
									let F;
									let tmp_instructions = dictTyping.onCompleteForm.instructions;
									if (tmp_instructions.indexOf("resolve_F") > -1) {
										F = new Function ("return new Promise((resolve_F) => {typed_Form=false;typed_Button=false;"+tmp_instructions+"});");
									} else {
										F = new Function ("return new Promise((resolve_F) => {typed_Form=false;typed_Button=false;"+tmp_instructions+";resolve_F(\"\");"+"});");
									};
									F().then(resolve_bis);
								}, {once: true});
							};

							/* Step "onCompleteButtons" */
							if ((dictTyping["onCompleteButtons"] !== undefined) && (dictTyping["onCompleteButtons"].length > 0)) {
								typed_Button = true;
								triggerPointerEvents_TalkingFrame("none");
								let onCompleteButton_DIV = document.getElementById("_talking_frameButtons");
								onCompleteButton_DIV.innerHTML = "";
								dictTyping["onCompleteButtons"].forEach(function(buttonToCreate,index) {
									onCompleteButton_DIV.innerHTML += '<button id="onCompleteButton_'+index+'" class="btn btn-secondary" tabindex="'+parseInt(index+3)+'" direction="auto" onFocus=\"typed_Form=false;typed_Button=true;\">'+buttonToCreate.text+'</button>';
									let tmp_instructions = buttonToCreate.instructions;
									let tmp_index = index;
									setTimeout(function() {
										let onCompleteButton_TMP = document.getElementById("onCompleteButton_"+tmp_index);
										onCompleteButton_TMP.addEventListener("click", function onClick() {
											if (dictTyping?.onCompleteForm?.activate) {
												$("#_talking_frameForm_MESSAGE").css("display","none");
												$("#_talking_frameForm_SEND").css("display","none");
												let old_element = document.getElementById("_talking_frameForm_SEND");
												let new_element = old_element.cloneNode(true);
												old_element.parentNode.replaceChild(new_element, old_element)
												let onCompleteForm_TEXTAREA = document.getElementById("_talking_frameForm_MESSAGE");
												onCompleteForm_TEXTAREA.value = "";
											};
											dictTyping["onCompleteButtons"].forEach(function(buttonToCreate_bis,index_bis) {
												let onCompleteButton_TMP_BIS = document.getElementById("onCompleteButton_"+index_bis);
												if (onCompleteButton_TMP_BIS) {onCompleteButton_TMP_BIS.removeEventListener("click", onClick);};
											});
											let onCompleteButton_DIV_BIS = document.getElementById("_talking_frameButtons");
											onCompleteButton_DIV_BIS.innerHTML = "";
											let F;
											if (tmp_instructions.indexOf("resolve_F") > -1) {
												F = new Function ("return new Promise((resolve_F) => {typed_Form=false;typed_Button=false;"+tmp_instructions+"});");
											} else {
												F = new Function ("return new Promise((resolve_F) => {typed_Form=false;typed_Button=false;"+tmp_instructions+";resolve_F(\"\");"+"});");
											};
											F().then(resolve_bis);
										}, {once: true});
									}, 0);
								});
								if (!(dictTyping?.onCompleteForm?.activate)) {setTimeout(function() {document.getElementById("onCompleteButton_0").focus();},100);};
							};

						});

						/* Send promise safeguard */
						if (!dictTyping?.onCompleteForm?.activate && !((dictTyping["onCompleteButtons"] !== undefined) && (dictTyping["onCompleteButtons"].length > 0))) {
							if (dictTyping["pauseOncomplete"] === true && typed.pause.status === true) {
								$("#_talking_next_word").css("display","initial");
								triggerPointerEvents_TalkingFrame("auto");
								let talkingFrame = document.getElementById("_talking_frame");
								talkingFrame.addEventListener("click", function onClick() {
									chain_CompleteScreenTyping = chain_CompleteScreenTyping.then(resolve);
								}, {once: true});
							} else {
								chain_CompleteScreenTyping = chain_CompleteScreenTyping.then(resolve);
							};
						} else {
							chain_CompleteScreenTyping = chain_CompleteScreenTyping.then(() => promiseCompleteButton);
							chain_CompleteScreenTyping = chain_CompleteScreenTyping.then(resolve);
						};

					} else {
						if (dictTyping["pauseOncomplete"] === true && typed.pause.status === true) {
							$("#_talking_next_word").css("display","initial");
							triggerPointerEvents_TalkingFrame("auto");
							let talkingFrame = document.getElementById("_talking_frame");
							talkingFrame.addEventListener("click", function onClick() {
								chain_CompleteScreenTyping = chain_CompleteScreenTyping.then(resolve);
							}, {once: true});
						} else {
							chain_CompleteScreenTyping = chain_CompleteScreenTyping.then(resolve);
						};
					};
				} else {
					$("#_talking_button > button").css("pointer-events","auto");
					chain_CompleteScreenTyping = chain_CompleteScreenTyping.then(resolve);
				};
			});
		};

		/*
		-------------------------------------------------------------------------------
		ENDING FUNCTION
		-------------------------------------------------------------------------------
		*/
		
		endingTyping = function () {
			return new Promise((resolve) => {
				setTimeout(function() {
					cy.clearQueue();
					$("#_talking_main_screen").fadeOut("slow");
					$("#_talking_button_fullscreen").css("display","none");
					$("#_talking_button_pass").css("display","none");
					if (featuresGraph['animation'] === true) {$("#_talking_button_launch").css("display","initial");};
					$("#_talking_button_screenshot").css("display","initial");
					$("#_talking_button_llm").css("display","initial");
					$("#appli_graph_element").css("display","initial");
					$("#appli_graph_cc_element").css("display","initial");
					$("#appli_graph_buttons").css("display","initial");
					$("#appli_graph_SEARCHING").css("visibility","visible");
					$(".introjs-hints").css("display","initial");
					triggerPointerEvents_TalkingFrame("auto");
					document.body.style.cursor = 'default';
					$("#_talking_button > button").css("pointer-events","auto");
					$("#appli_graph_network_CONTAINER").css("pointer-events","auto");
					if (toggleBubbles === true) {featuresGraph["infoBubbles"] = true;};
					if (toggleTags === true) {featuresGraph["infoTags"] = true;};
					windowCustomResize();
					div_talking_text.innerHTML = "";
					div_talking_img.innerHTML = "";
					if (!pass_instruction) {
						if (featuresInformationShowing === 'true' && !introAlreadySeen) {startIntro_graph();};
						resolve(ForceStabilization());
					} else {
						pass_instruction = false;
						resolve(Reinitialize());
					};
				}, 100);
			});
		};
		
		/*
		-------------------------------------------------------------------------------
		READING DICT FUNCTION
		-------------------------------------------------------------------------------
		*/
		
		readingDict = function(dictTyping,intialSet=true,callEnding=true) {
			return new Promise((resolve_readingDict) => {
				if (intialSet) {
					/* Display control */
					if (!llmActivated) {
						if (possibilityToFullScreen === true) {$("#_talking_button_fullscreen").css("display","initial");};
						if (possibilityToPass === true) {$("#_talking_button_pass").css("display","initial");};
					};
					$("#_talking_button_launch").css("display","none");
					$("#_talking_button_screenshot").css("display","none");
					$("#_talking_button_llm").css("display","none");
					$("#appli_graph_element").css("display","none");
					$("#appli_graph_cc_element").css("display","none");
					$("#appli_graph_buttons").css("display","none");
					$("#appli_graph_SEARCHING").css("visibility","hidden");
					$(".introjs-hints").css("display","none");
					/* Graph control */
					$("#_talking_button > button").css("pointer-events","none");
					$("#appli_graph_network_CONTAINER").css("pointer-events","none");
					$("#appli_graph_network").css("visibility","visible");
					windowCustomResize();
					/* Options control */
					if (toggleBubbles === true) {featuresGraph["infoBubbles"] = false;};
					if (toggleTags === true) {featuresGraph["infoTags"] = false;};
					/* Reading the dict */
					$("#_talking_main_screen").fadeIn("slow");
				};
				dictTyping.forEach(function(elem,index) {
					chain_reading = chain_reading.then(screenTyping.bind(null,elem,index));
				});
				if (callEnding) {chain_reading = chain_reading.then(endingTyping.bind(null));};
				chain_reading = chain_reading.then(resolve_readingDict.bind(null,"Done"));
			});
		};

		/*
		-------------------------------------------------------------------------------
		MAIN EXECUTION
		-------------------------------------------------------------------------------
		*/

		if (featuresGraph['animation'] === true) {
		
			if (animationOnLaunch === true) {

				$("#_talking_button_launch").css("display","none");
				$("#_talking_button_screenshot").css("display","none");
				$("#_talking_button_llm").css("display","none");
				$("#appli_graph_element").css("display","none");
				$("#appli_graph_cc_element").css("display","none");
				$("#appli_graph_buttons").css("display","none");
				$("#appli_graph_SEARCHING").css("visibility","hidden");
				$(".introjs-hints").css("display","none");
			
				if (showingCredentials["activate"] === true) {
					var div_status_credentials = document.getElementById("_showingCredentials_STATUS");
					div_status_credentials.innerHTML = "Application prête";
					div_status_credentials.style.backgroundColor = "var(--main_color_02)";
					div_status_credentials.style.animation = "none";
				};

				var promise_INTRO = new Promise(function(resolveINTRO, rejectINTRO) {
					if (showingCredentials["activate"] === true) {
						setTimeout(function() {
							$("#_showingCredentials").fadeOut( 1000, function() {
								setTimeout(function() {
									resolveINTRO('');
								},1000);
							});
						}, 1000);
					} else {
						resolveINTRO('');
					};
				});
				
				promise_INTRO.then(function(valueINTRO) {
					setTimeout(function() {
						$("#_talking_button").fadeIn(1000);
						readingDict(dictIntro,true,true);
					}, 500);
				});
				
			} else {
				$("#_talking_button").fadeIn(1000);
				$("#_talking_main_screen").fadeOut("slow");
				$("#_talking_button_fullscreen").css("display","none");
				$("#_talking_button_pass").css("display","none");
				$("#_talking_button_launch").css("display","initial");
				$("#_talking_button_screenshot").css("display","initial");
				$("#_talking_button_llm").css("display","initial");
				$("#appli_graph_element").css("display","initial");
				$("#appli_graph_cc_element").css("display","initial");
				$("#appli_graph_buttons").css("display","initial");
				$("#appli_graph_SEARCHING").css("visibility","visible");
				$(".introjs-hints").css("display","initial");
				$("#appli_graph_network").css("visibility","visible");
				$("#_talking_button > button").css("pointer-events","auto");
				$("#appli_graph_network_CONTAINER").css("pointer-events","auto");
			};
			
			var talkingButton_fullscreen = document.getElementById("_talking_button_fullscreen");
			
			applyColorTransform("#_talking_button_fullscreen > img","","filter: invert(100%)");

			talkingButton_fullscreen.addEventListener("click",function() {
				/* if already full screen; exit */
				/* else go fullscreen */
				if (document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement) {
					if (document.exitFullscreen) {
						document.exitFullscreen();
					} else if (document.mozCancelFullScreen) {
						document.mozCancelFullScreen();
					} else if (document.webkitExitFullscreen) {
						document.webkitExitFullscreen();
					} else if (document.msExitFullscreen) {
						document.msExitFullscreen();
					};
				} else {
					element = $('#page-top').get(0);
					if (element.requestFullscreen) {
						element.requestFullscreen();
					} else if (element.mozRequestFullScreen) {
						element.mozRequestFullScreen();
					} else if (element.webkitRequestFullscreen) {
						element.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
					} else if (element.msRequestFullscreen) {
						element.msRequestFullscreen();
					};
				};
			});	

			var talkingButton_pass = document.getElementById("_talking_button_pass");

			window.addEventListener("keydown", function(e) {
				if (e.keyCode === 27) {
					if (!typed_Form && !typed_Button) {
						talkingButton_pass.click();
					} else {
						$("#_talking_frame").addClass("zoom-animation");
						setTimeout(function() {$("#_talking_frame").removeClass("zoom-animation");},750);
					};
				};
			});

			talkingButton_pass.addEventListener("click",function() {
				$("#_talking_main_screen").css("pointer-events","none");
				pass_instruction = true;
				typed_Resolve = false;
				typed.destroy();
			});	

			var talkingButton_launch = document.getElementById("_talking_button_launch");

			talkingButton_launch.addEventListener("click",function() {
				typed_Resolve = true;
				readingDict(dictIntro,true,true);
			});

		};

		resolve_ScriptLoading("Done");
	
	});
});
/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : BUBBLESETS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

let chain_Bubblesets = Promise.resolve();

var authComputeBubblePath = false;
if (featuresGraph_BubblesetsOptions.showBubblesetsOnLoad) {authComputeBubblePath = true;};

var listBubblePath = [];

var bubbleSets_options = {
	// drawPotentialArea: true,
	virtualEdges: true,
	style: {'fill': mainColor_01, 'stroke': mainColor_02, 'stroke-width': 2}
};

var bubbleSets = cy.bubbleSets({
	zIndex: 4,
	throttle: 10,
	interactive: false,
	// includeLabels: true,
	// includeMainLabels: true,
	// includeOverlays: true,
	// includeSourceLabels: true,
	// includeTargetLabels: true,
});

function generateBubblesetsPaths() {
	return new Promise((resolve_generateBubblesetsPaths) => {
		featuresGraph_BubblesetsOptions.listBubblesets.forEach(function (pathToCreate){
			chain_Bubblesets = chain_Bubblesets.then(function(){
				return new Promise((resolve_Bubblesets) => {
					setTimeout(function() {
						if (pathToCreate.fill_color) {if (pathToCreate.fill_color.includes("--")) {
							bubbleSets_options.style.fill = getComputedStyle(document.documentElement).getPropertyValue(pathToCreate.fill_color);
						} else {
							bubbleSets_options.style.fill = pathToCreate.fill_color;
						}};
						if (pathToCreate.stroke_color) {if (pathToCreate.stroke_color.includes("--")) {
							bubbleSets_options.style.stroke = getComputedStyle(document.documentElement).getPropertyValue(pathToCreate.stroke_color);
						} else {
							bubbleSets_options.style.stroke = pathToCreate.stroke_color;
						}};
						if (pathToCreate.stroke_width === 0) {bubbleSets_options.style["stroke-width"] = 0;};
						if (pathToCreate.stroke_width > 0) {bubbleSets_options.style["stroke-width"] = pathToCreate.stroke_width;};
						let nodeToBubble;
						if (!pathToCreate.nodeSelector) {nodeToBubble = null;} else {nodeToBubble = cy.nodes(pathToCreate.nodeSelector)};
						let edgeToBubble;
						if (!pathToCreate.edgeSelector) {edgeToBubble = null;} else {edgeToBubble = cy.edges(pathToCreate.edgeSelector)};
						let nodeToBubble_ToAvoid;
						if (!pathToCreate.avoidNodeSelector) {nodeToBubble_ToAvoid = null;} else {nodeToBubble_ToAvoid = cy.nodes(pathToCreate.avoidNodeSelector)};
						resolve_generateBubblesetsPaths(resolve_Bubblesets(listBubblePath.push(bubbleSets.addPath(nodeToBubble.difference(cy.nodes(".unchecked,.unchecked_type,.unchecked_searched,.unchecked_proximity,.unchecked_millesime,.unchecked_map")), edgeToBubble, nodeToBubble_ToAvoid, bubbleSets_options))));
					},10);
				});
			});
		});
	});
};

function removeBubblesetsPaths () {
	return new Promise((resolve_removeBubblesetsPaths) => {
		if (listBubblePath.length === 0) {
			resolve_removeBubblesetsPaths("Done")
		} else {
			listBubblePath.forEach(function (pathToRemove) {
				chain_Bubblesets = chain_Bubblesets.then(function(){
					return new Promise((resolve_Bubblesets) => {
						resolve_Bubblesets(bubbleSets.removePath(pathToRemove));
					});
				});
			});
			chain_Bubblesets = chain_Bubblesets.then(function(){
				return new Promise((resolve_Bubblesets) => {
					resolve_removeBubblesetsPaths(resolve_Bubblesets("Done"));
				});
			});
		};
	});
};

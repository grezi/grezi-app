/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : PROXIMITY MODE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

var toggleProximity_CustomOpening;

var toggleProximity_CustomClosing;

var toggleProximity_CustomTrigerring;

let chain_ProximityModeProcessing = Promise.resolve();

var ongoingProcessingProximityMode = false;

function loadProximityGraph(nodeToProx) {

	if (featuresGraph.featureFarView) {
		chain_ProximityModeProcessing = chain_ProximityModeProcessing.then(function(){
			return new Promise((resolve_ProximityModeProcessing) => {
				automaticFarRendering = false;
				cy.nodes().removeClass("farViewed");
				cy.edges().removeClass("farViewed");
				resolve_ProximityModeProcessing("done");
			});
		});
	};

	if (featuresGraph.featureBubblesets) {
		chain_ProximityModeProcessing = chain_ProximityModeProcessing.then(removeBubblesetsPaths.bind(null));
	};

	chain_ProximityModeProcessing = chain_ProximityModeProcessing.then(function(){
		return new Promise((resolve_ProximityModeProcessing) => {

			$('#appli_graph_network_CONTAINER').css('pointer-events', 'none');

			if (toggleProximity_CustomTrigerring !== undefined) {
				toggleProximity_CustomTrigerring();
			};

			cy.nodes(".proximityFocused").removeClass("proximityFocused");

			nodeToProx.addClass("proximityFocused");

			cy.elements().addClass("unchecked_proximity");

			let proximityCollection = nodeToProx.connectedEdges().connectedNodes().union(cy.$(".ctj_found"));

			proximityCollection = proximityCollection.union(proximityCollection.edgesWith(proximityCollection));

			proximityCollection.removeClass("unchecked_proximity");

			let counter = 0;
			let slice = 0;
			let increment = 10;
			let maxCircleLevel = 0;
			proximityCollection.nodes().forEach(function(elem) {
				if (counter === increment) {
					slice += 1;
					increment += 10 * slice;
				}
				let circleLevel = slice + 1;
				elem.data("circleLevel",circleLevel);
				counter += 1;
				maxCircleLevel = circleLevel;
			});
			proximityCollection.nodes().forEach(function(elem) {
				elem.data("circleLevel",1+maxCircleLevel-elem.data("circleLevel"));
			});
			if (featuresGraph_ProximityModeOptions["classExternalCircle"] !== "" && featuresGraph_ProximityModeOptions["classExternalCircle"] !== undefined) {
			proximityCollection.nodes().forEach(function(elem) {
					if (elem.hasClass(featuresGraph_ProximityModeOptions["classExternalCircle"])) {elem.data("circleLevel",0)};
				});
			};

			mainLayout.stop();

			var paddingTEMP = featuresGraph['padding'];

			mainLayout = proximityCollection.nodes().layout({
				name: 'concentric',
				fit: true,
				startAngle: Math.random() * 2 * Math.PI,
				padding: paddingTEMP,
				nodeDimensionsIncludeLabels: true,
				avoidOverlap: true,
				spacingFactor: 2,
				minNodeSpacing: 0,
				concentric: function (node) {
					if (nodeToProx.contains(node)) {return maxCircleLevel+1;} else {return node.data("circleLevel");};
				},
				levelWidth: function( nodes ){
					return 1;
				},
				stop: () => {
					cy.nodes().unselect();
					if (authComputeBubblePath) {resolve_ProximityModeProcessing(generateBubblesetsPaths())} else {resolve_ProximityModeProcessing("Done");};
					$('#appli_graph_network_CONTAINER').css('pointer-events', 'initial');
				},
			});

			mainLayout.run();

			});
		});

};

cy.on("tap", "node", function() {
	if (proximityModeActivated) {loadProximityGraph(this);};
});

function proximityMode_TOGGLE() {
	return new Promise((resolve_proximityMode) => {

		$("#appli_graph_network_CONTAINER").css("pointer-events","none");

		cy.elements().unselect();

		if (proximityModeActivated) {

			chain_ProximityModeProcessing = chain_ProximityModeProcessing.then(function(){
				return new Promise((resolve_ProximityModeProcessing) => {
					if (toggleProximity_CustomClosing !== undefined) {
						resolve_ProximityModeProcessing(toggleProximity_CustomClosing());
					} else {
						resolve_ProximityModeProcessing("Done");
					};
				});
			});

			chain_ProximityModeProcessing = chain_ProximityModeProcessing.then(function(){
				return new Promise((resolve_ProximityModeProcessing) => {
					proximityButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_04")+" !important");
					$('#modeIndicator').html("");
					$("#toggle_modeProximity > a").attr("data-original-title","Entrez dans le mode \"graphe de proximité\"");
					resolve_ProximityModeProcessing("Done");
				});
			});


			chain_ProximityModeProcessing = chain_ProximityModeProcessing.then(function(){
				return new Promise((resolve_ProximityModeProcessing) => {
					if (featuresGraph.featureFarView && featuresGraph.featureFarView_automatic) {
						farViewStatus = false;
						cy.nodes().addClass("farViewed");
						cy.edges().addClass("farViewed");
						automaticFarRendering = true;
					};
					resolve_ProximityModeProcessing("Done");
				});
			});

			chain_ProximityModeProcessing = chain_ProximityModeProcessing.then(function(){
				return new Promise((resolve_ProximityModeProcessing) => {
					cy.elements().removeClass("unchecked_proximity");
					if (featuresButtons['toggle_ori_pos'] !== false) {
						resolve_ProximityModeProcessing(presetLayout());
					} else {
						if ((featuresButtons['toggle_fcose'] !== false || featuresGraph['fcoseOnLoad'] === true)) {
							resolve_ProximityModeProcessing(fcoseLayout());
						} else {
							resolve_ProximityModeProcessing("Done");
						};
					};
				});
			});

			chain_ProximityModeProcessing = chain_ProximityModeProcessing.then(function(){
				return new Promise((resolve_ProximityModeProcessing) => {
					cy.nodes(".proximityFocused").removeClass("proximityFocused");
					cy.autounselectify(false);
					cy.autoungrabify(false);
					proximityModeActivated = false;
					ongoingProcessingProximityMode = false;
					$("#appli_graph_SEARCH_BUTTON").css("display","initial");
					$("#appli_graph_SEARCH_BUTTON_filter").css("display","initial");
					$("#appli_graph_SEARCH_BUTTON_proximity").css("display","none");
					resolve_ProximityModeProcessing("Done");
					resolve_proximityMode("Done");
					$("#appli_graph_network_CONTAINER").css("pointer-events","initial");
				});
			});

		} else {

			chain_ProximityModeProcessing = chain_ProximityModeProcessing.then(function(){
				return new Promise((resolve_ProximityModeProcessing) => {
					if (toggleProximity_CustomOpening !== undefined) {
						resolve_ProximityModeProcessing(toggleProximity_CustomOpening());
					} else {
						resolve_ProximityModeProcessing("Done");
					};
				});
			});

			chain_ProximityModeProcessing = chain_ProximityModeProcessing.then(function(){
				return new Promise((resolve_ProximityModeProcessing) => {
					$("#checkTypesActorsAll").prop("checked", true);
					$("[id^=checkTypeActor_]").each(function() {$(this).prop("checked", true);});
					$("#checkNodeAll").prop("checked", true);
					$("#checkLinkAll").prop("checked", true);
					$("[id^=checkNode_]").each(function() {$(this).prop("checked", true);});
					$("[id^=checkLink_]").each(function() {$(this).prop("checked", true);});
					$("#checkActors").prop("checked", true);
					$("#checkGroups").prop("checked", true);
					$("#checkTypes").prop("checked", false);
					if (featuresGraph_LegendOptions.hideTypesInCloseView) {
						cy.nodes('[group = \"type\"]').addClass('unchecked');
					} else {
						cy.nodes('[group = \"type\"]').removeClass('unchecked');
					};
					cy.nodes('[group = \"clusterGroup\"]').removeClass('unchecked');
					cy.nodes('[group = \"subGroup\"],[group = \"entity\"]').removeClass('unchecked');
					cy.nodes().removeClass("unchecked_type");
					cy.nodes().removeClass("unchecked_searched");
					cy.nodes().removeClass("unchecked_proximity");
					cy.nodes().removeClass("unchecked_map");
					cy.edges().removeClass("unchecked");
					cy.edges().removeClass("unchecked_type");
					cy.edges().removeClass("unchecked_searched");
					cy.edges().removeClass("unchecked_proximity");
					resolve_ProximityModeProcessing("Done");
				});
			});

			chain_ProximityModeProcessing = chain_ProximityModeProcessing.then(function(){
				return new Promise((resolve_ProximityModeProcessing) => {
					cy.autounselectify(true);
					cy.autoungrabify(true);
					proximityModeActivated = true;
					ongoingProcessingProximityMode = false;
					$("#appli_graph_SEARCH_BUTTON").css("display","none");
					$("#appli_graph_SEARCH_BUTTON_filter").css("display","none");
					$("#appli_graph_SEARCH_BUTTON_proximity").css("display","initial");
					proximityButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_01_faded")+" !important");
					$('#modeIndicator').html("Mode \"graphe de proximité\" activé <button id=\"modeIndicator_QuitButton\" class=\"btn btn-primary\" onclick=\"modeManager_toggler();\">Quitter</button>");
					$("#toggle_modeProximity > a").attr("data-original-title","Quittez le mode \"graphe de proximité\"");
					if (authComputeBubblePath) {chain_ModeManager = chain_ModeManager.then(generateBubblesetsPaths.bind(null));};
					resolve_ProximityModeProcessing("Done");
					resolve_proximityMode("Done");
					$("#appli_graph_network_CONTAINER").css("pointer-events","initial");
				});
			});

		};


	});
};

var proximityButton = document.getElementById("toggle_modeProximity");

function proximityEventListener() {
	if (ongoingProcessingProximityMode === false) {
		ongoingProcessingProximityMode = true;
		if (proximityModeActivated === false) {
			modeManager_toggler("proximityMode");
		} else {
			modeManager_toggler("proximityMode");
		};
	};
};

proximityButton.addEventListener("click",proximityEventListener);


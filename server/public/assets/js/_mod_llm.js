/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : LLM
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
-------------------------------------------------------------------------------
GLOBAL VARIABLES
-------------------------------------------------------------------------------
*/

let chain_WizardLLM = Promise.resolve();

let user_prompt = "";

let profile_llm = "";

var dictIntro_BCK = dictIntro;

/*
-------------------------------------------------------------------------------
MAIN FUNCTIONS
-------------------------------------------------------------------------------
*/

function askLLM_config (user_prompt="",profile_llm="standard",mode_llm="analysis") {
	/*
		- user_prompt : if "" then just ask a summary of the graph
		- profile_llm can be : creative / standard / strict
		- profile_llm can be : analyses / create
	*/
	let askLLM_parameters = {};
	askLLM_parameters.promptLLM = encodeURIComponent(user_prompt);
	askLLM_parameters.profileLLM = encodeURIComponent(profile_llm);
	askLLM_parameters.modeLLM = encodeURIComponent(mode_llm);
	askLLM_parameters.template = encodeURIComponent(idGraph);
	askLLM_parameters.dataset = encodeURIComponent(dataSet_Name);
	askLLM_parameters.password = encodeURIComponent(dataSet_Password);
	return askLLM_parameters;
};

function completeLlmHistory(text,llmRole) {
	let modalContent_LlmHistory = document.getElementById('llmHistoryModal_body');
	if (llmRole === "assistant") {
		modalContent_LlmHistory.innerHTML += "<div class=\"row mx-2 mb-2\">"+"<div class=\"col-3\"></div>"+"<div class=\"col-9 col-assistant\">"+"<div class=\"llm_role mt-2\"><img height=\"25\" width=\"25\" src=\"../../assets/img/grezia.png\">&nbsp;grez<span class=\"badge badge-primary\">IA</span></div>"+"<p>"+text+"</p></div>"+"</div>";
	} else {
		modalContent_LlmHistory.innerHTML += "<div class=\"row mx-2 mb-2\">"+"<div class=\"col-9 col-user\">"+"<div class=\"llm_role mt-2\">Utilisateur·trice</div>"+"<p>"+text+"</p></div>"+"<div class=\"col-3\"></div>"+"</div>";
	};
};

/*
-------------------------------------------------------------------------------
WIZARD FUNCTIONS
-------------------------------------------------------------------------------
*/


function launchWizardLLM() {

	if (!llm_authorize) {
		const bugReport = document.getElementById('bug_report_appli');
		bugReport.innerHTML = '<br><h3><b>/!\\ Accès non autorisé /!\\</b></h3><br><p><b>Contactez l\'administration de l\'application.</b></p><br>';
		$('#bugModal').modal('show');
	} else {

		llmActivated = true ;

		chain_WizardLLM = chain_WizardLLM.then(function(){
			return new Promise((resolve_WizardLLM) => {
				dictIntro = [{
					"title": "grez<span class=\"badge badge-primary\">IA</span>",
					"img": "../../assets/img/grezia.png",
					"pauseOncomplete": true,
					"onCompleteForm": {activate: false, instructions: ""},
					"onCompleteButtons": [
						{text: "Tutoriel", instructions: "completeLlmHistory(\"Je veux voir le tutoriel.\",\"user\");resolve_F(tutorialWizardLLM());"},
						{text: "Résumer", instructions: "completeLlmHistory(\"Résume les informations du graphe.\",\"user\");resolve_F(askLLM(askLLM_config(\"\",\"creative\",\"analysis\")));"},
						{text: "Questionner", instructions: "completeLlmHistory(\"Je veux poser une question sur les données du graphe.\",\"user\");resolve_F(askUSER(\"analysis\",false));"},
						{text: "Modifier", instructions: "completeLlmHistory(\"Je veux modifier les informations du graphe.\",\"user\");resolve_F(notAvailableLLM());"},
						{text: "Créer", instructions: "completeLlmHistory(\"Je veux créer un graphe.\",\"user\");resolve_F(askUSER(\"create\",false));"},
						{text: "Historique", instructions: "completeLlmHistory(\"Montre l'historique de la conversation.\",\"user\");resolve_F(showHistoryLLM());"},
						{text: "Quitter", instructions: "completeLlmHistory(\"Je veux quitter la discussion.\",\"user\");resolve_F(endWizardLLM());"}
					],
					"strings_to_type": [
						"Bonjour je suis un assistant virtuel basé sur le <span class=\"badge badge-primary\" style=\"pointer-events:auto\"><a href=\"https://www.infomaniak.com/fr/hebergement/llm-api\" target=\"_blank\" style=\"color:inherit;\">LLM Mixtral déployé par Informaniak</a></span>, je peux vous assister dans la limite de mes capacités.<br><br>En effet pour le moment je ne peux lire que les noms et descriptions des éléments du graphes, ainsi que les liens qui les relient entre eux.<br><br>De plus, peux traiter des données de <span class=\"badge badge-primary badge_warning\">30 000</span> caractères maximum.<br><br>Gardez enfin à l'esprit que chacune de mes requêtes à un coût environnemental non négligeable, utilisez-moi avec parcimonie.<br><br>"
					]
				}];
				typed_Resolve = true;
				resolve_WizardLLM(readingDict(dictIntro,true,false));
			});
		});	

	};

};

function tutorialWizardLLM() {

	chain_WizardLLM = chain_WizardLLM.then(function(){
		return new Promise((resolve_WizardLLM) => {
			dictIntro = [{
				"title": "grez<span class=\"badge badge-primary\">IA</span>",
				"img": "../../assets/img/grezia.png",
				"pauseOncomplete": true,
				"onCompleteButtons": [
					{text: "Compris", instructions: "resolve_F(continueWizardLLM());"},
					{text: "Recommencer", instructions: "resolve_F(tutorialWizardLLM());"},
					{text: "Quitter", instructions: "resolve_F(endWizardLLM());"}
				],
				"strings_to_type": [
					"La fonction <span class=\"badge badge-primary\">Résumer</span> génère une synthèse sur les données du graphe en quelques lignes.",
					"La fonction <span class=\"badge badge-primary\">Questionner</span> permet de poser une question sur les données du graphe.",
					"La fonction <span class=\"badge badge-primary\">Modifier</span> est en cours de développement.",
					"La fonction <span class=\"badge badge-primary\">Créer</span> est en cours de développement.",
					"La fonction <span class=\"badge badge-primary\">Historique</span> permet de retrouver tous les échanges avec <span style=\"font-family:Virgil,Arial;\">grez</span><span class=\"badge badge-primary\" style=\"font-family:Virgil,Arial;\">IA</span> (penser à sauvegarder)."
				]
			}];
			typed_Resolve = true;
			resolve_WizardLLM(readingDict(dictIntro,false,false));
		});
	});	

};

function notAvailableLLM() {

	chain_WizardLLM = chain_WizardLLM.then(function(){
		return new Promise((resolve_WizardLLM) => {
			dictIntro = [{
				"title": "grez<span class=\"badge badge-primary\">IA</span>",
				"img": "../../assets/img/grezia.png",
				"pauseOncomplete": true,
				"onCompleteButtons": [
					{text: "Continuer", instructions: "resolve_F(continueWizardLLM());"},
					{text: "Quitter", instructions: "resolve_F(endWizardLLM());"}
				],
				"strings_to_type": [
					"Fonctionnalité en cours de développement.",
				]
			}];
			typed_Resolve = true;
			resolve_WizardLLM(readingDict(dictIntro,false,false));
		});
	});	

};

function continueWizardLLM() {

	chain_WizardLLM = chain_WizardLLM.then(function(){
		return new Promise((resolve_WizardLLM) => {
			dictIntro = [{
				"title": "grez<span class=\"badge badge-primary\">IA</span>",
				"img": "../../assets/img/grezia.png",
				"pauseOncomplete": true,
				"onCompleteButtons": [
					{text: "Tutoriel", instructions: "resolve_F(tutorialWizardLLM());"},
					{text: "Résumer", instructions: "resolve_F(askLLM(askLLM_config(\"\",\"creative\",\"analysis\")));"},
					{text: "Questionner", instructions: "resolve_F(askUSER(\"analysis\",false));"},
					{text: "Modifier", instructions: "resolve_F(notAvailableLLM());"},
					{text: "Créer", instructions: "resolve_F(askUSER(\"create\",false));"},
					{text: "Historique", instructions: "resolve_F(showHistoryLLM());"},
					{text: "Quitter", instructions: "resolve_F(endWizardLLM());"}
				],
				"strings_to_type": [
					"Autre chose ?"
				]
			}];
			typed_Resolve = true;
			resolve_WizardLLM(readingDict(dictIntro,false,false));
		});
	});	

};

function askUSER(modePrompting,newWizard=false) {

	chain_WizardLLM = chain_WizardLLM.then(function(){
		return new Promise((resolve_WizardLLM) => {
			dictIntro = [{
				"title": "grez<span class=\"badge badge-primary\">IA</span>",
				"img": "../../assets/img/grezia.png",
				"pauseOncomplete": true,
				"onCompleteForm": {activate: true, instructions: "resolve_F(askLLM(askLLM_config(talkingFormMessage,\"strict\",\""+modePrompting+"\")));"},
				"onCompleteButtons": [
					{text: "Quitter", instructions: "resolve_F(endWizardLLM());"}
				],
				"strings_to_type": [
					"Que puis-je faire pour vous ?"
				]
			}];
			typed_Resolve = true;
			resolve_WizardLLM(readingDict(dictIntro,newWizard,false));
		});
	});	
	
};

function askLLM(dictLLM) {

	chain_WizardLLM = chain_WizardLLM.then(function(){
		return new Promise((resolve_WizardLLM) => {
			document.body.style.cursor = 'default';
			progressBarReinitialize("",0);
			$("#_talking_mixtral").fadeIn("fast");
			$("#appli_graph_spinner").fadeIn("fast");
			let oReq = new XMLHttpRequest();
			oReq.onload = function () {
				$("#_talking_mixtral").fadeOut("fast");
				if (this.response === 'error') {
					$("#_talking_main_screen").css("pointer-events","none");
					pass_instruction = false;
					typed_Resolve = false;
					typed.destroy();
					endingTyping();
					$("#appli_graph_spinner").fadeOut("fast");
					const bugReport = document.getElementById('bug_report_appli');
					bugReport.innerHTML = '<br><h3><b>/!\\ Connexion à Mixtral échouée /!\\</b></h3><br><p><b>Contactez l\'administration de l\'application.</b></p><br>';
					$('#bugModal').modal('show');
					resolve_WizardLLM("Done");
				} else {
					$("#appli_graph_spinner").fadeOut("fast");
					let responseLLM = JSON.parse(this.response);
					let textResponse = responseLLM.textResponse;
					if (dictLLM.modeLLM === "create") {
						if (textResponse.indexOf('<code') === -1 && textResponse.indexOf('</code>') === -1) {
							let debut = textResponse.indexOf('[');
							let fin = textResponse.lastIndexOf(']');
							if (debut !== -1 && fin !== -1) {
								textResponse = textResponse.slice(0, debut) + '<code class="language-json">' + textResponse.slice(debut, fin + 1) + '</code>' + textResponse.slice(fin + 1);
							};
						};
						if (textResponse.indexOf('<pre') === -1 && textResponse.indexOf('</pre>') === -1) {
							let debut = textResponse.indexOf('<code');
							let fin = textResponse.lastIndexOf('</code>');
							if (debut !== -1 && fin !== -1) {
								textResponse = textResponse.slice(0, debut) + '<pre>' + textResponse.slice(debut, fin + 7) + '</pre>' + textResponse.slice(fin + 7);
							};
						};
						let textForJson = textResponse;
						if (textForJson.indexOf('[') !== -1 && textForJson.indexOf(']') !== -1) {
							let debut = textForJson.indexOf('[');
							let fin = textForJson.lastIndexOf(']');
							if (debut !== -1 && fin !== -1) {
								textForJson = textForJson.slice(debut, fin + 1);
								console.log(jsonFormatReplace(textForJson))
								let jsonTMP = JSON.parse(jsonFormatReplace(textForJson));
								if (jsonTMP) {
									cy.json({elements: jsonTMP});
									cy.nodes().addClass("defaultNode");
								};
							};
						};
					};
					let strings_to_type = [textResponse.replace(/\n/g, '<br>')];
					if (responseLLM.eludedCaracters > 0) {strings_to_type.push("Cette réponse comprend <span class=\"badge badge-primary badge_warning\">"+responseLLM.eludedCaracters+" caractères</span> éludés car le graphe est trop conséquent.");};
					dictIntro = [{
						"title": "grez<span class=\"badge badge-primary\">IA</span>",
						"img": "../../assets/img/grezia.png",
						"pauseOncomplete": true,
						"onCompleteInstructions": "resolve_F(continueWizardLLM());",
						"strings_to_type": strings_to_type
					}];
					typed_Resolve = true;
					resolve_WizardLLM(readingDict(dictIntro,false,false));
				}
			};
			oReq.open('post', '/llm/request', true);
			oReq.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			oReq.send(`promptLLM=${dictLLM.promptLLM}&profileLLM=${dictLLM.profileLLM}&modeLLM=${dictLLM.modeLLM}&template=${dictLLM.template}&dataset=${dictLLM.dataset}&password=${dictLLM.password}`);
		});
	});
	
};


function showHistoryLLM() {

	chain_WizardLLM = chain_WizardLLM.then(function(){
		return new Promise((resolve_WizardLLM) => {
			document.body.style.cursor = 'default';
			$('#llmHistoryModal').modal('show');
			$('#llmHistoryModal').one('hidden.bs.modal', function () {
				resolve_WizardLLM("Done");
			});
		});
	});	

	chain_WizardLLM = chain_WizardLLM.then(function(){
		return new Promise((resolve_WizardLLM) => {
			resolve_WizardLLM(continueWizardLLM());
		});
	});	

}

function endWizardLLM() {

	chain_WizardLLM = chain_WizardLLM.then(function(){
		return new Promise((resolve_WizardLLM) => {
			dictIntro = [{
				"title": "grez<span class=\"badge badge-primary\">IA</span>",
				"img": "../../assets/img/grezia.png",
				"pauseOncomplete": true,
				"strings_to_type": [
					"À bientôt !"
				]
			}];
			typed_Resolve = true;
			resolve_WizardLLM(readingDict(dictIntro,false,true));
		});
	});	
	
	chain_WizardLLM = chain_WizardLLM.then(function(){
		return new Promise((resolve_WizardLLM) => {
			llmActivated = false;
			dictIntro = dictIntro_BCK;
			resolve_WizardLLM("Done");
		});
	});	

};

var talkingButton_LLM = document.getElementById("_talking_button_llm");

talkingButton_LLM.addEventListener("click",function() {
	launchWizardLLM();
});

$("#_talking_button_llm").css("display","initial");

/*
 * Copyright : GREZI
 * Author : Adrien Solacroup
 * Encoding : UTF-8
 * Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
 * Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
 */

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE APPLI GRAPH : SET THE FUNCTIONS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
SCRIPT STRUCTURE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
 * CORF.A : PREPROCESSING
 * CORF.A.01 : Tooltip preparation
 * CORF.A.02 : JQuery plugin to detect css changes
 *
 * CORF.B : MAIN FUNCTION FOR THE IMPORT
 *
 * CORF.C : CORE FUNCTIONS FOR CYTOSCAPE
 * CORF.C.01 : Creating a specific view
 * CORF.C.02 : Force the network to stabilize
 * CORF.C.03 : Show lateral screen
 * CORF.C.04 : Generate the body of the content modal string
 * CORF.C.05 : Initial Layout
 * CORF.C.06 : Modes Manager
 *
 * CORF.D : FUNCTIONS LINKED TO THE BUTTONS
 * CORF.D.01 : Function to show the bug modal screen
 * CORF.D.02 : Fullscreen function
 * CORF.D.03 : Save modal as doc
 * CORF.D.04 : For the viewer button
 * CORF.D.05 : To deal with device orientation
 * CORF.D.06 : Running fcose layout
 * CORF.D.07 : Go to initial positions
 *
 */

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORF.A : GENERAL FUNCTIONS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
--------------------------------
CORF.A.01 : Tooltip preparation
--------------------------------
*/

$(document).ready(function () {
    /* Initialize tooltip */
    $('[data-toggle="tooltip"]').tooltip({
        placement: 'auto',
        delay: 0,
        template: '<div class="tooltip"><div class="tooltip-inner"></div></div>',
    });
});

$("[data-toggle='tooltip']").on('show.bs.tooltip', function () {
    setTimeout(function () {
        $("[data-toggle='tooltip']").tooltip('hide');
    }, 1000);
});

/*
--------------------------------
CORF.A.02 : JQuery plugin to detect css changes
-------------------------------- 
*/

(function() {
	orig = $.fn.css;
	$.fn.css = function() {
		var result = orig.apply(this, arguments);
		$(this).trigger('stylechanged');
		return result;
	}
})();

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORF.B : MAIN FUNCTION FOR THE IMPORT
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

function importNetwork_ForDataCaching(cacheData, cy_for_import) {

    let cacheDataParsing = JSON.parse(cacheData.replace(/(\r\n|\n|\r|\t)/gm,""));

    cy_for_import.add(cacheDataParsing);
    
};

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORF.C : CORE FUNCTIONS FOR CYTOSCAPE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
--------------------------------
CORF.C.01 : Creating a specific view
--------------------------------
*/

var ongoingReinitialize = false;

function Reinitialize() {

	let chain_Reinitialize = Promise.resolve();

    if (featuresGraph.featureBubblesets) {
        chain_Reinitialize = chain_Reinitialize.then(function(){
            return new Promise((resolve_Reinitialize) => {
                if (authComputeBubblePath) {
                    $("#bubblesetsSwitch").prop("checked", false);
                    authComputeBubblePath = false;
                    resolve_Reinitialize(removeBubblesetsPaths());
                } else {
                    resolve_Reinitialize("Done");
                };
            });
        });
    };

	chain_Reinitialize = chain_Reinitialize.then(function(){
		return new Promise((resolve_Reinitialize) => {
            ongoingReinitialize = true;
            $("#_talking_button > button").css("pointer-events","none");
			$("#appli_graph_spinner").fadeIn("fast",function() {resolve_Reinitialize("Done");});
		});
	});
	
	chain_Reinitialize = chain_Reinitialize.then(function(){
		return new Promise((resolve_Reinitialize) => {
			avoidLayoutAfterExpand = true;
			cy.nodes().unlock();
			cy.nodes().unselect();
			$("#appli_graph_network_CONTAINER").css("pointer-events","none");
			$("#appli_graph_network_CONTAINER").fadeOut("slow",function() {
				resolve_Reinitialize("Done");
			});
		});
	});

    if (featuresGraph.featureClustering) {

        chain_Reinitialize = chain_Reinitialize.then(function(){
            return new Promise((resolve_Reinitialize) => {
                resolve_Reinitialize(api.expand(api.expandableNodes(cy.nodes('[group = "type"]').difference('[group = "subGroup"]')).difference('[group = "subGroup"]')));
            });
        });

        chain_Reinitialize = chain_Reinitialize.then(function(){
            return new Promise((resolve_Reinitialize) => {
                resolve_Reinitialize(api.expand(api.expandableNodes(cy.nodes('[group = "clusterGroup"]').difference('[group = "subGroup"]')).difference('[group = "subGroup"]')));
            });
        });

        chain_Reinitialize = chain_Reinitialize.then(function(){
            return new Promise((resolve_Reinitialize) => {
                resolve_Reinitialize(api.collapseRecursively(api.collapsibleNodes(cy.nodes('[group = "subGroup"]')),{"layoutBy":null}));
            });
        });

    };

    if (featuresButtons['toggle_modeProximity'] !== false) {
        chain_Reinitialize = chain_Reinitialize.then(function(){
            return new Promise((resolve_Reinitialize) => {
                proximityButton.setAttribute("style", "display: initial; background-color: "+getComputedStyle(document.documentElement).getPropertyValue("--main_color_04")+" !important");
                $('#modeIndicator').html("");
                $("#toggle_modeProximity > a").attr("data-original-title","Entrez dans le mode \"graphe de proximité\"");
                cy.autounselectify(false);
                cy.autoungrabify(false);
                proximityModeActivated = false;
                ongoingProcessingProximityMode = false;
                $("#appli_graph_SEARCH_BUTTON").css("display","initial");
                $("#appli_graph_SEARCH_BUTTON_filter").css("display","initial");
                $("#appli_graph_SEARCH_BUTTON_proximity").css("display","none");
                resolve_Reinitialize("Done");
            });
        });
    };

	chain_Reinitialize = chain_Reinitialize.then(function(){
		return new Promise((resolve_Reinitialize) => {
			$("#checkTypesActorsAll").prop("checked", true);
			$("[id^=checkTypeActor_]").each(function() {$(this).prop("checked", true);});
			$("#checkNodeAll").prop("checked", true);
            $("#checkLinkAll").prop("checked", true);
			$("[id^=checkNode_]").each(function() {$(this).prop("checked", true);});
            $("[id^=checkLink_]").each(function() {$(this).prop("checked", true);});
			$("#checkActors").prop("checked", true);
			$("#checkGroups").prop("checked", true);
			$("#checkTypes").prop("checked", false);
			if (featuresGraph_LegendOptions.hideTypesInCloseView) {
                cy.nodes('[group = \"type\"]').addClass('unchecked');
            } else {
                cy.nodes('[group = \"type\"]').removeClass('unchecked');
            };
			cy.nodes('[group = \"clusterGroup\"]').removeClass('unchecked');
			cy.nodes('[group = \"subGroup\"],[group = \"entity\"]').removeClass('unchecked');
            if (featuresGraph.featureMillesime && yearsToShow && selectedYears) {
                selectedYears = [];
                generateYearPagination();
            };
			cy.nodes().removeClass("unchecked_type");
			cy.nodes().removeClass("unchecked_searched");
            cy.nodes().removeClass("unchecked_proximity");
            cy.nodes().removeClass("unchecked_millesime");
            cy.nodes().removeClass("unchecked_map");
			cy.edges().removeClass("unchecked");
			cy.edges().removeClass("unchecked_type");
			cy.edges().removeClass("unchecked_searched");
            cy.edges().removeClass("unchecked_proximity");
            cy.edges().removeClass("unchecked_millesime");
            cy.edges().removeClass("unchecked_edge");
            if (featuresGraph.featureFarView) {
                if (featuresGraph.featureFarView_automatic) {
                    $("#farViewedControl").prop("checked",true);
                    farViewStatus = false;
                    automaticFarRendering = true;
                    if (featuresGraph_LegendOptions.hideTypesInCloseView) {
                        $("#legendTypes,#checkTypes,#compressTypes,#expandTypes").css("pointer-events","auto");
                        $("#legendTypes").css("background-color","var(--main_color_03)");
                        $("#legendTypes").css("opacity","1");
                    };
                } else {
                    $("#farViewedControl").prop("checked",false);
                    farViewStatus = true;
                    automaticFarRendering = false;
                };
                cy.nodes().addClass("farViewed");
                cy.edges().addClass("farViewed");
                if (featuresGraph_LegendOptions.showFarViewControl_manual) {
                    $("#farViewedSwitch_DIV").css("pointer-events","none");
                    $("#farViewedSwitch_DIV").css("opacity","0.5");
                    $("#farViewedSwitch").prop("checked", false);
                };
            };
            if (featuresGraph.showLabels) {
                if (featuresGraph_LegendOptions.showLabelsControl) {
                    if (featuresGraph_LegendOptions.showLabelsOnLoad) {
                        $("#showLabelsSwitch").prop("checked", true);
                        cy.nodes().addClass("showLabelsOn");
                        cy.edges().addClass("showLabelsOn");
                    } else {
                        $("#showLabelsSwitch").prop("checked", false);
                        cy.nodes().removeClass("showLabelsOn");
                        cy.edges().removeClass("showLabelsOn");
                    };
                } else {
                    cy.nodes().addClass("showLabelsOn");
                    cy.edges().addClass("showLabelsOn");
                };
            };
			setTimeout(function(){
				if (featuresGraph.featureClustering) {
                    avoidLayoutAfterExpand = false;
                    api.collapseAllEdges();
                };
                if (featuresGraph.featureBubblesets) {
                        if (featuresGraph_BubblesetsOptions.showBubblesetsOnLoad) {
                            $("#bubblesetsSwitch").prop("checked", true);
                            chain_ModeManager = chain_ModeManager.then(generateBubblesetsPaths.bind(null));
                    };
                };
				$("#legendTypes,#checkTypes,#compressTypes,#expandTypes").css("pointer-events","auto");
                if (featuresInformationShowing === 'true') {startIntro_graph();};
				if (featuresGraph['animation']) {$('#_talking_button_launch').css('display','initial');};
				resolve_Reinitialize("Done");
			},1500);	
		});
	});

	chain_Reinitialize = chain_Reinitialize.then(function(){
		return new Promise((resolve_Reinitialize) => {
            if (featuresButtons['toggle_ori_pos'] !== false) {
                resolve_Reinitialize(presetLayout());
            } else {
                if ((featuresButtons['toggle_fcose'] !== false || featuresGraph['fcoseOnLoad'] === true)) {
                    resolve_Reinitialize(fcoseLayout());
                } else {
                    resolve_Reinitialize("Done");
                };
            };
		});
	});

	chain_Reinitialize = chain_Reinitialize.then(function(){
		return new Promise((resolve_Reinitialize) => {
			$("#appli_graph_network_CONTAINER").fadeIn("fast",function() {
				$("#appli_graph_spinner").css("display","none");
				$("#appli_graph_network_CONTAINER").css("display","initial");
				ForceStabilization(resolve_Reinitialize("Done"));
			});
		});
	});

	chain_Reinitialize = chain_Reinitialize.then(function(){
		return new Promise((resolve_Reinitialize) => {
            $("#_talking_button > button").css("pointer-events","auto");
			if (featuresGraph.featureSearchBar) {creatingSearchBar(true,false);};
            ongoingReinitialize = false;
			resolve_Reinitialize("Done");
		});
	});

};

/*
--------------------------------
CORF.C.02 : Force the network to stabilize
--------------------------------
*/

function ForceStabilization(elements=undefined) {
	return new Promise((resolve) => {
		$("#appli_graph_network_CONTAINER").css("pointer-events","none");
		cy.stop();
		var paddingTEMP = featuresGraph["padding"];
		if (elements === undefined || elements instanceof Event) {
			if (cy.$("node:selected").length > 0) {
				var selectorTEMP = cy.$("node:selected:visible");
			} else {
				var selectorTEMP = cy.nodes(":visible");
			};
		} else {
			var selectorTEMP = elements;
		};
		if (typeof(mapModeActivated) === "undefined") {
			var stabilizationMode = true;
		} else {
			if (mapModeActivated === true) {
				var stabilizationMode = false;
			} else {
				var stabilizationMode = true;
			};
		};
		if (stabilizationMode === true) {
            if (selectorTEMP.length === 1) {
                selectorTEMP.addClass("hoveredNode");
                cy.animate({
                    "zoom": 1,
                    "duration": 500,
                    "center": {"eles": selectorTEMP.difference(cy.$(".unchecked,.unchecked_type,.unchecked_searched,.unchecked_proximity,.unchecked_millesime,.unchecked_map")),
                    "padding": paddingTEMP},
                    "easingFunction": "ease",
                    "complete": function(){$("#appli_graph_network_CONTAINER").css("pointer-events","initial");resolve("Done");}
                });
            } else {
                cy.animate({
                    "duration": 500,
                    "fit": {"eles": selectorTEMP.difference(cy.$(".unchecked,.unchecked_type,.unchecked_searched,.unchecked_proximity,.unchecked_millesime,.unchecked_map")),
                    "padding": paddingTEMP},
                    "easingFunction": "ease",
                    "complete": function(){$("#appli_graph_network_CONTAINER").css("pointer-events","initial");resolve("Done");}
                });
            };
		} else {
			if (cyMap) {cyMap.fit(selectorTEMP.difference(cy.$(".unchecked,.unchecked_type,.unchecked_searched,.unchecked_proximity,.unchecked_millesime,.unchecked_map")), {"maxZoom":15, "padding":150});};
			$("#appli_graph_network_CONTAINER").css("pointer-events","initial");
			resolve("Done");
		};
	});
};

if (featuresButtons.toggle_stabilization) {document.getElementById('toggle_stabilization').addEventListener('click', ForceStabilization);};

/*
--------------------------------
CORF.C.03 : Show lateral screen
--------------------------------
*/

var ongoingProcessingLateralScreen = false;

function lateralScreen(mode = 'default') {
    return new Promise((resolve_function) => {
        if (mode === 'Done' || mode === 'closing') {
            var modeAdjust = 'default';
        } else {
            modeAdjust = mode;
        }

        ongoingProcessingLateralScreen = true;

        var promise_lateralScreen = new Promise(function (resolve, reject) {
            cy.nodes().unselect();
            $('#appli_graph_network').fadeOut('fast');
            var switchPath = 'none';
            var elem = document.getElementById('appli_graph_main_screen');
            if ($(document).width() > $(document).height()) {
                var currentOrientation = 'landscape';
            } else {
                var currentOrientation = 'portrait';
            }
            if (modeAdjust === 'default') {
                if (currentOrientation === 'landscape') {
                    if (elem.style.width === '60%') {
                        switchPath = 'closing';
                        featuresGraph['tooltips'] = tooltipsGraph_BCK;
                        var width = 60;
                        var id = setInterval(frame, 0.1);
                        $('#appli_graph_lateral_body').css({'z-index':-1});
                        function frame() {
                            if (width == 100) {
                                resolve(switchPath);
                                clearInterval(id);
                                $('#appli_graph_buttons').css({position:'fixed'});
                                $('#appli_graph_SEARCHING').css({position:'fixed'});
                            } else {
                                width++;
                                elem.style.width = width + '%';
                            }
                        }
                    } else {
                        switchPath = 'opening';
                        featuresGraph['tooltips'] = false;
                        var width = 100;
                        var id = setInterval(frame, 0.1);
                        $('#appli_graph_buttons').css({position:'absolute'});
                        $('#appli_graph_SEARCHING').css({position:'absolute'});
                        function frame() {
                            if (width == 60) {
                                resolve(switchPath);
                                clearInterval(id);
                                $('#appli_graph_lateral_body').css({'z-index':999});
                            } else {
                                width--;
                                elem.style.width = width + '%';
                            }
                        }
                    }
                } else {
                    if (elem.style.height === '60%') {
                        switchPath = 'closing';
                        featuresGraph['tooltips'] = tooltipsGraph_BCK;
                        var height = 60;
                        var id = setInterval(frame, 0.1);
                        $('#appli_graph_lateral_body').css({'z-index':-1});
                        function frame() {
                            if (height == 100) {
                                resolve(switchPath);
                                clearInterval(id);
                                $('#appli_graph_buttons').css({position:'fixed'});
                                $('#appli_graph_SEARCHING').css({position:'fixed'});
                            } else {
                                height++;
                                elem.style.height = height + '%';
                            }
                        }
                    } else {
                        switchPath = 'opening';
                        featuresGraph['tooltips'] = false;
                        var height = 100;
                        var id = setInterval(frame, 0.1);
                        $('#appli_graph_buttons').css({position:'absolute'});
                        $('#appli_graph_SEARCHING').css({position:'absolute'});
                        function frame() {
                            if (height == 60) {
                                resolve(switchPath);
                                clearInterval(id);
                                $('#appli_graph_lateral_body').css({'z-index':999});
                            } else {
                                height--;
                                elem.style.height = height + '%';
                            }
                        }
                    }
                }
            }
            if (modeAdjust === 'closing') {
                switchPath = 'closing';
                if (currentOrientation === 'landscape') {
                    featuresGraph['tooltips'] = tooltipsGraph_BCK;
                    elem.style.width = '100%';
                    resolve(switchPath);
                } else {
                    featuresGraph['tooltips'] = tooltipsGraph_BCK;
                    elem.style.height = '100%';
                    resolve(switchPath);
                }
            }
        });

        promise_lateralScreen.then(function (value) {
            isLateral = value;
            var promise_lateralScreen_2 = new Promise(function (resolve_2, reject_2) {
                ForceStabilization()
                $('#appli_graph_network').fadeIn('slow', function () {
                    windowCustomResize();
                    resolve_2("Done");
                });
            });
            promise_lateralScreen_2.then(function (value_2) {
                setTimeout(function () {
                    resolve_function(ForceStabilization());
                }, 50);
            });
        });
    });
}

/*
--------------------------------
CORF.C.04 : Generate the body of the content modal string
--------------------------------
*/

function readNodes_for_modal(cy_function) {
    var outputText = '';

    var sortingNodes = cy_function.nodes().sort(function (ele1, ele2) {
        return ele1.data('index') > ele2.data('index');
    });

    sortingNodes.forEach(
        function (ele) {
            if (ele.data('rank') === undefined) {
                ele.data('rank', 1);
            }

            var nodeTEMP = ele.data();

            if (ele.isChild()) {
                var rankTEMP = nodeTEMP.rank + 1;
            } else {
                var rankTEMP = nodeTEMP.rank;
            }

            if (rankTEMP > 4) {
                outputText = outputText.concat('<ul><li>');
            } else {
                outputText = outputText.concat('<h', rankTEMP, '>');
            }

            if (nodeTEMP.image !== undefined) {
                outputText = outputText.concat('<img class="lazyload" data-src="', nodeTEMP.image, '"></img>');
            }

            if (nodeTEMP.label !== undefined) {
                outputText = outputText.concat(nodeTEMP.label);
            } else {
                outputText = outputText.concat('');
            }

            if (rankTEMP > 4) {
                outputText = outputText.concat('</li></ul>');
            } else {
                outputText = outputText.concat('</h', rankTEMP, '>');
            }

            outputText = outputText.concat('<br>');

            if (nodeTEMP.titleComplete !== undefined) {
                outputText = outputText.concat('<p>', nodeTEMP.titleComplete, '</p>', '<br>');
            }
        },
        { order: 'id' }
    );

    return outputText;
}

/*
--------------------------------
CORF.C.05 : Initial Layout
--------------------------------
*/

function InitialLayout(mode, fit, angle = 0) {
    return new Promise((resolve_done) => {
        if (typeof mapModeActivated === 'undefined') {
            var stabilizationMode = true;
        } else {
            if (mapModeActivated === true) {
                var stabilizationMode = false;
            } else {
                var stabilizationMode = true;
            }
        }
        if (stabilizationMode === true) {

            if (featuresGraph.featureBubblesets) {removeBubblesetsPaths();};

            var cy_noBubbles = cy.elements().difference(cy.$('.infoBubble')).difference(cy.$('.infoTag'));

            cy_noBubbles.nodes().unlock();

            mainLayout.stop();

            if (mode === 'breadthfirst') {
                var bb = cy_noBubbles.nodes().boundingBox();
                mainLayout = cy_noBubbles.layout({
                    name: 'breadthfirst',
                    spacingFactor: 1,
                    roots: cy_noBubbles.nodes().roots(),
                    transform: function (node, position) {
                        var x = position['x'];
                        var y = position['y'];
                        var c_x = bb['x1'] + (bb['x2'] - bb['x1']) / 2;
                        var c_y = bb['y1'] + (bb['y2'] - bb['y1']) / 2;
                        var radians = (Math.PI / 180) * angle;
                        var cos = Math.cos(radians);
                        var sin = Math.sin(radians);
                        var nx = cos * (x - c_x) + sin * (y - c_y) + c_x;
                        var ny = cos * (y - c_y) - sin * (x - c_x) + c_y;
                        return { x: nx, y: ny };
                    },
                    stop: function () {
                        if (featuresGraph.featureBubblesets) {if (authComputeBubblePath) {generateBubblesetsPaths();};};
                        if (fit === true) {
                            ForceStabilization();
                        }
                        cy_noBubbles.nodes().lock();
                        resolve_done('Done');
                    },
                });
                mainLayout.run();
            }

            if (mode === 'euler_cola' || mode === 'cola') {
                var promise_LayoutEuler = new Promise(function (resolve, reject) {
                    if (featuresEulerComputation === 'true') {
                        if (mode === 'euler_cola') {
                            mainLayout = cy_noBubbles.layout({
                                name: 'euler',
                                randomize: true,
                                animate: false,
                                stop: function () {
                                    resolve('');
                                },
                            });
                            mainLayout.run();
                        } else {
                            resolve('');
                        }
                    } else {
                        resolve('');
                    }
                });
                promise_LayoutEuler.then(function (value) {
                    var promise_LayoutCola = new Promise(function (resolve, reject) {
                        if (mode === 'euler_cola') {
                            mainLayout = cy_noBubbles.layout({
                                name: 'cola',
                                randomize: false,
                                fit: false,
                                animate: true,
                                avoidOverlap: true,
                                nodeSpacing: function (node) {
                                    return 10;
                                },
                                edgeLength: function (edge) {
                                    var coeff = featuresGraph['compaction'];
                                    var lengthTEMP = coeff * 350 - coeff * 75 * edge.source().data('rank');
                                    if (lengthTEMP < coeff * 50) {
                                        lengthTEMP = coeff * 50;
                                    }
                                    return lengthTEMP;
                                },
                                nodeDimensionsIncludeLabels: true,
                                stop: function () {
                                    resolve('');
                                },
                                infinite: false,
                            });
                            mainLayout.run();
                        } else {
                            resolve('');
                        }
                    });
                    promise_LayoutCola.then(function (value) {
                        mainLayout = cy_noBubbles.layout({
                            name: 'cola',
                            randomize: false,
                            fit: false,
                            animate: true,
                            avoidOverlap: true,
                            nodeSpacing: function (node) {
                                return 10;
                            },
                            edgeLength: function (edge) {
                                var coeff = featuresGraph['compaction'];
                                var lengthTEMP = coeff * 350 - coeff * 75 * edge.source().data('rank');
                                if (lengthTEMP < coeff * 50) {
                                    lengthTEMP = coeff * 50;
                                }
                                return lengthTEMP;
                            },
                            nodeDimensionsIncludeLabels: true,
                            ready: function () {
                                if (fit === true) {
                                    ForceStabilization();
                                }
                                resolve_done('Done');
                            },
                            infinite: featuresGraph['physics'],
                        });
                        mainLayout.run();
                    });
                });
            }
        } else {
            cyMap.fit(undefined, { padding: 150 });
            $('#appli_graph_network_CONTAINER').css('pointer-events', 'initial');
            resolve_done('Done');
        }
    });
}

/*
--------------------------------
CORF.C.06 : Modes Manager
--------------------------------
*/

let chain_ModeManager = Promise.resolve();

function modeManager_toggler(modeToActivate="") {
    /* Remove bubblesets */
    if (featuresGraph.featureBubblesets) {removeBubblesetsPaths();};
    /* Close Mode */
    if(editorModeActivated) {console.log("close editor");chain_ModeManager = chain_ModeManager.then(editorModeActivation.bind('default'));};
    if(proximityModeActivated) {console.log("close proximity");chain_ModeManager = chain_ModeManager.then(proximityMode_TOGGLE.bind(null));};
    if(mapModeActivated) {console.log("close map");chain_ModeManager = chain_ModeManager.then(toggleMap.bind(null));};
    /* Open Mode */
    if(!editorModeActivated && modeToActivate === "editorMode") {
        console.log("open editor");
        $("#bubblesetsSwitch").prop("checked", false);
        $("#bubblesetsSwitch_DIV").css("pointer-events","none");
        $("#bubblesetsSwitch_DIV").css("opacity","0.5");
        chain_ModeManager = chain_ModeManager.then(editorModeActivation.bind('default'));
    };
    if(!proximityModeActivated && modeToActivate === "proximityMode") {
        console.log("open proximity");
        chain_ModeManager = chain_ModeManager.then(proximityMode_TOGGLE.bind(null));
    };
    if(!mapModeActivated && modeToActivate === "mapMode") {
        console.log("open map");
        $("#bubblesetsSwitch").prop("checked", false);
        $("#bubblesetsSwitch_DIV").css("pointer-events","none");
        $("#bubblesetsSwitch_DIV").css("opacity","0.5");
        chain_ModeManager = chain_ModeManager.then(toggleMap.bind(null));
    };
    /* Add bubblesets */
    if (featuresGraph.featureBubblesets) {if (editorModeActivated || proximityModeActivated || mapModeActivated) {
        $("#bubblesetsSwitch_DIV").css("pointer-events","auto");
        $("#bubblesetsSwitch_DIV").css("opacity","1");
        if (authComputeBubblePath) {
            $("#bubblesetsSwitch").prop("checked", true);
            chain_ModeManager = chain_ModeManager.then(generateBubblesetsPaths.bind(null));
        };
    };};
};

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORF.D : FUNCTIONS LINKED TO THE BUTTONS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
--------------------------------
CORF.D.01 : Function to show the bug modal screen
-------------------------------- 
*/

function displayBug_iframe() {
    $('#form_bug_report_IFRAME').css({ display: 'initial' });
}

/*
--------------------------------
CORF.D.02 : Fullscreen function
--------------------------------
*/

$('#toggle_fullscreen').on('click', function () {
    /* if already full screen; exit */
    /* else go fullscreen */
    if (document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement) {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    } else {
        element = $('#page-top').get(0);
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen();
        }
    };
});

document.addEventListener("fullscreenchange", function() {
    setTimeout(function() {ForceStabilization();},1000);
});

/*
--------------------------------
CORF.D.03 : Save modal as doc
--------------------------------
*/

$('#toggle_save').on('click', function () {
    var header =
        "<html xmlns:o='urn:schemas-microsoft-com:office:office' " +
        "xmlns:w='urn:schemas-microsoft-com:office:word' " +
        "xmlns='http://www.w3.org/TR/REC-html40'>" +
        "<head><meta charset='utf-8'><title>EcoRhizo : export de graphe interactif</title></head><body>";
    var footer = '</body></html>';
    var sourceHTML = header + document.getElementById('summaryModal_body').innerHTML + footer;

    var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
    var fileDownload = document.createElement('a');
    document.body.appendChild(fileDownload);
    fileDownload.href = source;
    fileDownload.download = 'graphe.doc';
    fileDownload.click();
    document.body.removeChild(fileDownload);
});

/*
--------------------------------
CORF.D.04 : For the viewer button
--------------------------------
*/

$('#toggle_lateralScreen').on('click', function () {
    if (ongoingProcessingLateralScreen === false) {
        ongoingProcessingLateralScreen = true;
        if (editorModeActivated === false) {
            chain_MainLateralScreen = chain_MainLateralScreen.then(lateralScreen.bind('default'));
            chain_MainLateralScreen = chain_MainLateralScreen.then(function () {
                return new Promise((resolve_MainLateralScreen) => {
                    ongoingProcessingLateralScreen = false;
                    resolve_MainLateralScreen('default');
                });
            });
        } else {
            chain_MainLateralScreen = chain_MainLateralScreen.then(function () {
                return new Promise((resolve_MainLateralScreen) => {
                    resolve_MainLateralScreen('closing');
                });
            });
            chain_MainLateralScreen = chain_MainLateralScreen.then(editorModeActivation.bind('closing'));
        }
    }
});


/*
--------------------------------
CORF.D.05 : To deal with device orientation
--------------------------------
*/

window.addEventListener('orientationchange', function () {
    windowCustomResize();
});

/*
--------------------------------
CORF.D.06 : Running fcose layout
--------------------------------
*/

function fcoseLayout(withFit = true, fcoseWithoutLinks = false, triggerOnLoad = false) {
    return new Promise((resolve) => {

        if (triggerOnLoad) {cy.endBatch();console.log('end batch');};

		$("#appli_graph_spinner").fadeIn("fast");
		$("#appli_graph_network_CONTAINER").css("pointer-events","none");

        if (featuresGraph.featureBubblesets) {removeBubblesetsPaths();};

		if (typeof(mapModeActivated) === "undefined") {
			var stabilizationMode = true;
		} else {
			if (mapModeActivated === true) {
				var stabilizationMode = false;
			} else {
				var stabilizationMode = true;
			};
		};
		if (stabilizationMode === true) {

            let withAnimation;
            if (triggerOnLoad) {
                withAnimation = false;
            } else {
                withAnimation = true;
            };

			var cy_noBubbles = cy.$(".defaultNode:unlocked");
			cy_noBubbles = cy_noBubbles.difference(cy.$(".unchecked,.unchecked_type,.unchecked_searched,.unchecked_proximity,.unchecked_millesime,.unchecked_map"));
			if (!fcoseWithoutLinks) {
				cy_noBubbles = cy_noBubbles.union(cy_noBubbles.edgesWith(cy_noBubbles));
				cy_noBubbles = cy_noBubbles.difference(cy.edges(":hidden"));
			};
			
			cy.$(".compoundCorner").remove();
			
			var paddingTEMP = featuresGraph["padding"];

			let withTiling;
			if (cy_noBubbles.edges().length === 0) {withTiling = true;} else {withTiling = false;};

			let chain_CustomFcose = Promise.resolve();

            if (triggerOnLoad) {
                chain_CustomFcose = chain_CustomFcose.then(function(){
                    return new Promise((resolve_CustomFcose) => {
                        mainLayout.stop();
                        mainLayout = cy_noBubbles.layout({
                            name: 'grid',
                            animate: false,
                            stop: () => {
                                resolve_CustomFcose('Done');
                            },
                        });
                        mainLayout.run();
                    });
                });
            };

			chain_CustomFcose = chain_CustomFcose.then(function(){
				return new Promise((resolve_CustomFcose) => {
					mainLayout.stop();
                    mainLayout = cy_noBubbles.layout({
                        name: 'fcose',
                        quality: 'proof',
                        padding: paddingTEMP,
                        randomize: false,
                        animate: withAnimation,
                        fit: withFit,
                        packComponents: true,
                        tile: withTiling,
                        tilingPaddingVertical: 60,
                        tilingPaddingHorizontal: 60,
                        animationDuration: 1000,
                        nodeDimensionsIncludeLabels: true,
                        nodeRepulsion: function (node) {return 5000;},
                        edgeElasticity: function (edge) {return 0.1;},
                        idealEdgeLength: function (edge) {
                            if (edge.data('edgeLength') !== undefined) {
                                return edge.data('edgeLength');
                            } else {
                                return 100;
                            };
                        },
                        stop: () => {
                            $('#appli_graph_network_CONTAINER').css('pointer-events', 'auto');
                            resolve_CustomFcose('Done');
                        },
                    });
					mainLayout.run();
				});
			});

			chain_CustomFcose = chain_CustomFcose.then(function(){
				return new Promise((resolve_CustomFcose) => {
                    if (featuresGraph.featureBubblesets) {if (authComputeBubblePath) {generateBubblesetsPaths();};};
					$("#appli_graph_spinner").fadeOut("fast",function() {resolve_CustomFcose("Done");});
				});
			});

			chain_CustomFcose = chain_CustomFcose.then(function(){
				return new Promise((resolve_CustomFcose) => {
					$("#appli_graph_network_CONTAINER").css("pointer-events","auto");
                    if (triggerOnLoad) {cy.startBatch();console.log('start batch');};
					resolve_CustomFcose(resolve("Done"));
				});
			});

		} else {
			cyMap.fit(undefined, {"padding":150});
			$("#appli_graph_spinner").fadeOut("fast",function() {
				$("#appli_graph_network_CONTAINER").css("pointer-events","auto");
				resolve("Done");
			});
		};

    });
}

$('#toggle_fcose').on('click', fcoseLayout);

/*
--------------------------------
CORF.D.07 : Go to initial positions
--------------------------------
*/

function presetLayout() {
    return new Promise((resolve) => {
        $('#appli_graph_network_CONTAINER').css('pointer-events', 'none');

        if (typeof mapModeActivated === 'undefined') {
            var stabilizationMode = true;
        } else {
            if (mapModeActivated === true) {
                var stabilizationMode = false;
            } else {
                var stabilizationMode = true;
            }
        }

        if (stabilizationMode === true) {
            if (featuresGraph.featureBubblesets) {removeBubblesetsPaths();};
            var cy_noBubbles = cy.$(".defaultNode:unlocked").difference(cy.$('.infoBubble')).difference(cy.$('.infoTag'));
			cy_noBubbles = cy_noBubbles.difference(cy.$(".unchecked,.unchecked_type,.unchecked_searched,.unchecked_proximity,.unchecked_millesime,.unchecked_map"));

            cy.$('.compoundCorner').remove();

            cy_noBubbles.nodes().unlock();

            mainLayout.stop();

            var paddingTEMP = featuresGraph['padding'];

            mainLayout = cy_noBubbles.layout({
                name: 'preset',
                animate: true,
                fit: true,
                padding: paddingTEMP,
                animationDuration: 1000,
                positions: function (node) {
                    return node.data('ori_pos');
                },
                stop: () => {
                    if (featuresGraph.featureBubblesets) {if (authComputeBubblePath) {generateBubblesetsPaths();};};
                    $('#appli_graph_network_CONTAINER').css('pointer-events', 'initial');
                    resolve('Done');
                },
            });

            mainLayout.run();

        } else {
            if (cyMap) {cyMap.fit(undefined, { padding: 150 });};
            $('#appli_graph_network_CONTAINER').css('pointer-events', 'initial');
            resolve('Done');
        }

    });
}

$('#toggle_ori_pos').on('click', presetLayout);

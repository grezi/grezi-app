/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : INFO BUBBLES
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/* Make the child position refresh on dragging a parent node */
cy.on("position", "node:parent", function() {
	this.descendants().forEach(function(descendant) {
		descendant.position(descendant.position());
	});
});

/* Information bubbles function : display bubbles near nodes with tooltips or modal info screen */
function informationBubbles(nodesToBubble) {
	nodesToBubble.forEach(function(elem) {
		if ( ((elem.data()["title"] !== undefined)&&(elem.data()["title"] !== "")&&(elem.data()["title"] !== " ")&&(featuresGraph["tooltips"] === true)) || ((elem.data()["titleComplete"] !== undefined)&&(elem.data()["titleComplete"] !== "")&&(elem.data()["titleComplete"] !== " ")&&(featuresGraph["modalSelection"] === true)) ) {
			if (cy.getElementById(elem.id()+"_ib").length === 0) {
				var nodeBB_TEMP = elem.boundingBox({"includeLabels": false, "includeOverlays": false});
				if ((elem.data()["titleComplete"] !== undefined) && (elem.data()["titleComplete"] !== "")) {
					groupTEMP = "ibComplete";
				} else {
					groupTEMP = "ibSimple";
				};
				cy.add({
					"group": "nodes",
					"selectable": false,
					"grabbable": false,
					"data": {
						"id": elem.id()+"_ib",
						"group": groupTEMP,
						"label": "?"
					},
					"position": {"x": nodeBB_TEMP["x2"], "y": nodeBB_TEMP["y1"]}
				});
				elem.on("position", function() {
					var nodeBB_TEMP = this.boundingBox({"includeLabels": false, "includeOverlays": false});
					cy.getElementById(this.id()+"_ib").position({"x": nodeBB_TEMP["x2"], "y": nodeBB_TEMP["y1"]})
				});
				cy.getElementById(elem.id()+"_ib").addClass("infoBubble");
			};
		};
	});
};


/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : TURORIAL INTRO GRAPH
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

var introAlreadySeen = false;

function startIntro_graph(){

	introAlreadySeen = true;
	
	var intro = introJs();
	
	let introFirstText = "";

	if (titleGraph) {
		if (featuresGraph["animation"] !== true) {introFirstText += "<p>"+"Tutoriel de<br>l\'application "+titleGraph+"</p>";};
		introFirstText += "<p>"+dictionnaryIntro_graph["introFirstText"]+"</p>";
	} else {
		if (featuresGraph["animation"] !== true) {introFirstText += "<p>"+"Tutoriel"+"</p>";};
		introFirstText += "<p>"+dictionnaryIntro_graph["introFirstText"]+"</p>";
	};

	var introSteps = [{
		intro: introFirstText
	}];

	introSteps.push({
		intro: "<p>"+dictionnaryIntro_graph["mainText"]+"</p>"
	});
	
	var featuresButtons_INTRO = [];
	
	if (featuresGraph["editorOnLoad"] !== false) {
		introSteps.push({
			intro: dictionnaryIntro_graph["editorText"]
		});
	};

	for (i in featuresButtons) {
		if (featuresButtons[i] === true) {
			if (!(i in featuresButtons_INTRO)) {
				featuresButtons_INTRO.push(i);
			};
		};
		if (featuresButtons[i] === false) {
			var indexTEMP = featuresButtons_INTRO.indexOf(i);
			if (indexTEMP > -1) {
				featuresButtons_INTRO.splice(indexTEMP, 1);
			};
		};
	};

	for (i in featuresButtons_INTRO) {
		introSteps.push({
			element: document.querySelector('#'+featuresButtons_INTRO[i]),
			intro: featuresButtons_dictFun[featuresButtons_INTRO[i]]["name"]
		});
	};

	if (featuresGraph["featureLlm"] === true) {
		introSteps.push({
			element: document.querySelector('#_talking_button_llm'),
			intro: dictionnaryIntro_graph["llmText"]
		});
	};

	if (featuresGraph["animation"] === true) {
		introSteps.push({
			element: document.querySelector('#_talking_button_launch'),
			intro: dictionnaryIntro_graph["animationText"]
		});
	};

	if (featuresGraph["featureScreenShot"] === true) {
		introSteps.push({
			element: document.querySelector('#_talking_button_screenshot'),
			intro: dictionnaryIntro_graph["screenshotText"]
		});
	};

	if (featuresGraph["featureReload"] === true) {
		introSteps.push({
			element: document.querySelector('#view_ALL'),
			intro: dictionnaryIntro_graph["viewAllText"]
		});
	};

	if (featuresGraph["featureMillesime"] === true) {
		introSteps.push({
			element: document.querySelector('#appli_graph_buttons_MILLESIME'),
			intro: dictionnaryIntro_graph["millesimeText"]
		});
	};

	if (featuresGraph["featureLegend"] === true) {
		introSteps.push({
			element: document.querySelector('#headingLegend'),
			intro: dictionnaryIntro_graph["legendText"]
		});
	};

	if (featuresGraph["featureSearchBar"] === true) {
		introSteps.push({
			element: document.querySelector('#appli_graph_SEARCH_BAR'),
			intro: "<p>"+dictionnaryIntro_graph["searchBarText"]+"</p>"+"<p>"+dictionnaryIntro_graph["searchBarText_step1"]+"</p>"+"<p>"+dictionnaryIntro_graph["searchBarText_step2"]+"</p>"
		});
		introSteps.push({
			element: document.querySelector('#appli_graph_BUTTONS_BOX'),
			intro: dictionnaryIntro_graph["searchBarText_step3"]
		});
	};
	
	intro.setOptions({
		
		nextLabel: "Suivant",
		
		prevLabel: "Précédent",
		
		skipLabel: "Passer",
		
		doneLabel: "Fin",
		
		disableInteraction: true,
		
		showBullets: false,
		
		showProgress: true,
		
		showStepNumbers: false,
		
		steps: introSteps
	});
	
	intro.onexit(function() {
		let hints = [];
		if (featuresButtons_INTRO.includes("toggle_fullscreen")) {
			hints.push({
				"element": document.querySelector('#toggle_fullscreen'),
				"hint": "Mettre en plein écran.",
				"hintPosition": "top-right"
			});
		};
		if (featuresGraph["featureLlm"] === true) {
			hints.push({
				"element": document.querySelector('#_talking_button_llm'),
				"hint": "Découvrez l'association de GREZI avec l'IA générative Mixtral !",
				"hintPosition": "top-left"
			});
		};
		if (hints.length > 0) {
			introHints = introJs();
			introHints.setOptions({
				"hintButtonLabel": "Compris",
				"hints": hints
			});
			introHints.addHints();
		};
	});

	intro.start();

};

/*
 * Copyright : GREZI
 * Author : Adrien Solacroup
 * Encoding : UTF-8
 * Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
 * Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
 */

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
___CUSTOM_GRAPH.JS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
SCRIPT STRUCTURE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
 * CGS.A : PREPROCESSING
    * CGS.A.01 : Global variables definition
    * CGS.A.02 : Alert if not a computer device
 *
 * CGS.B : GET PARAMETERS FROM URL
    * CGS.B.01 : Activate or not euler computation
    * CGS.B.02 : Showing the information modal screen or not
    * CGS.B.03 : Click to begin or not
 *
 * CGS.C : MAIN FUNCTIONS
    * CGS.C.01 : Function to seek params in URL
    * CGS.C.02 : Function to resolve a promise after loading a js script
    * CGS.C.03 : Function to load the application with the proper data
    * CGS.C.04 : Functions for the progress bar
 *
 * CGS.D : ON LOAD OPERATIONS
    * CGS.D.01 : Firstly check if there is a click to begin before downloading the graph
    * CGS.D.02 : Show an alter if the navigator is incompatible
    * CGS.D.03 : Then load the graph
 *
 *
 *
 */

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CGS.A : PREPROCESSING
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
--------------------------------
CGS.A.01 : Global variables definition
--------------------------------
*/

var modulesList = [];

var bar;

var barProgress = 0;

var barStepsNumber = 18;

var editorModeActivated = false;

var proximityModeActivated = false;

var mapModeActivated = false;

var llmActivated = false;

let chain_ScriptInitialisation = Promise.resolve();

let chain_ScriptLoading = Promise.resolve();

let chain_LoadData = Promise.resolve();

let chain_MainLateralScreen = Promise.resolve();

let chain_AnimateHighlighter = Promise.resolve();

let introSteps;

let introHints;

var idleCursor = 'default';

var graphIsLoad = false;

/*
--------------------------------
CGS.A.02 : Alert if not a computer device
--------------------------------
*/

if (
    navigator.userAgent.match(/iPhone/i) ||
    navigator.userAgent.match(/webOS/i) ||
    navigator.userAgent.match(/Android/i) ||
    navigator.userAgent.match(/iPad/i) ||
    navigator.userAgent.match(/iPod/i) ||
    navigator.userAgent.match(/BlackBerry/i) ||
    navigator.userAgent.match(/Windows Phone/i)
) {
    $('#otherModal_body').html(
        "<p><b>L'usage de cette application est optimisé pour ordinateur. Pour une navigation confortable, fluide et interactive, il est conseillé de privilégier ce mode d’accès plutôt qu'un smartphone ou une tablette.</b></p>"
    );
    $('#otherModal_QuitButton').html('Poursuivre');
    $('#otherModal').modal('show');
}

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CGS.B : GET PARAMETERS FROM URL
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
--------------------------------
CGS.B.01 : Activate or not euler computation
--------------------------------
*/

var featuresEulerComputation = getParamValue('eulerComputation');

/*
--------------------------------
CGS.B.02 : Showing the information modal screen or not
--------------------------------
*/

var featuresInformationShowing = getParamValue('showInformation');

/*
--------------------------------
CGS.B.03 : Click to begin or not
--------------------------------
*/

var featuresClickToBegin = getParamValue('clickToBegin');

if (featuresClickToBegin === 'true') {
    document.getElementById('appli_graph_mask').style.visibility = 'visible';
}

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CGS.C : MAIN FUNCTIONS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
--------------------------------
CGS.C.01 : Function to seek params in URL
-------------------------------- 
*/

function getParamValue(paramName) {
    var url = window.location.search.substring(1);
    var qArray = url.split('&');
    for (var i = 0; i < qArray.length; i++) {
        var pArr = qArray[i].split('=');
        if (pArr[0] === paramName) {
            return pArr[1];
        }
    }
}

/*
--------------------------------
CGS.C.02 : Function to resolve a promise after loading a js script
--------------------------------
*/

function F_ScriptLoading(script_to_load, keepGoing = false, isModule = false) {
    return new Promise((resolve) => {
        let script = document.createElement('script');
        script.src = script_to_load;
        if (isModule === true) {
            script.type = 'module';
        }
        script.onload = function () {
            resolve('');
        };
        script.onerror = function () {
            if (keepGoing === false) {
                $('#bug_report_appli').html("<p><b>/!\\</b></p><p>Le graphe recherché n'existe pas. Le script suivant n'a pas été chargé : <b>" + script_to_load + '</b></p><p><b>/!\\</b></p>');
                $('#bugModal').modal('show');
            } else {
                resolve('');
            }
        };
        document.head.appendChild(script);
    });
}

/*
--------------------------------
CGS.C.03 : Function to load the application with the proper data
--------------------------------
*/

function loadAppliGraph() {
    let chain_LoadAppliGraph = Promise.resolve();

    /* progressbar.js loading */
    chain_LoadAppliGraph = chain_LoadAppliGraph.then(function () {
        return new Promise((resolve_LoadAppliGraph) => {
            chain_ScriptLoading = chain_ScriptLoading.then(function () {
                return new Promise((resolve_ScriptLoading) => {
                    bar = new ProgressBar.Line('#appli_graph_progessbar', {
                        strokeWidth: 1,
                        duration: 100,
                        easing: 'easeInOut',
                        color: getComputedStyle(document.documentElement).getPropertyValue('--main_color_02'),
                        trailColor: getComputedStyle(document.documentElement).getPropertyValue('--main_color_01'),
                        text: {
                            value: 'Initialisation',
                            className: 'progressbar-text'
                        },
                        autoStyleContainer: false
                    });
                    progressBarIncrement();
                    resolve_ScriptLoading(resolve_LoadAppliGraph('Done'));
                });
            });
        });
    });

    /* Showing navbar */
    chain_LoadAppliGraph = chain_LoadAppliGraph.then(function () {
        return new Promise((resolve_LoadAppliGraph) => {
            progressBarIncrement();
            $('#mainNav').fadeIn('fast', function(){
				resolve_LoadAppliGraph('');
			});
        });
    });

    /* Showing credentials */
    chain_LoadAppliGraph = chain_LoadAppliGraph.then(function () {
        return new Promise((resolve_LoadAppliGraph) => {
            if (getParamValue("debug") === undefined) {
                if (showingCredentials.activate) {
                    $("#_showingCredentials").fadeIn(1000,function(){
                        resolve_LoadAppliGraph("");
                    });
                } else {
                    resolve_LoadAppliGraph("");                           
                        };
            } else {
                resolve_LoadAppliGraph("");
            };
        });
    });

    /* Showing screen */
    chain_LoadAppliGraph = chain_LoadAppliGraph.then(function () {
        return new Promise((resolve_LoadAppliGraph) => {
            progressBarIncrement();
            $('#appli_graph_spinner').fadeIn('fast', function(){
				resolve_LoadAppliGraph('');
			});
        });
    });

    /* Checking data version */
    chain_LoadAppliGraph = chain_LoadAppliGraph.then(function () {
        return new Promise((resolve_LoadAppliGraph) => {
            progressBarIncrement();
            if (greziVersion !== graphVersion && getParamValue('debug') === undefined) {
                $('#bug_report_appli').html('<b><p>/!\\</p><p>Le graphe chargé ne correspond pas à la version actuelle de GREZI (' + greziVersion + ') !</p><p>/!\\</p></b>');
                $('#bugModal').modal('show');
                $('#bugModal').on('hidden.bs.modal', function () {
                    resolve_LoadAppliGraph('');
                });
            } else {
                resolve_LoadAppliGraph('');
            }
        });
    });

    chain_LoadAppliGraph = chain_LoadAppliGraph.then(function () {
        return new Promise((resolve_LoadAppliGraph) => {
            progressBarIncrement();

            /* --------------------------------------------- */
            /* Creating the loading dictionnary */
            /* --------------------------------------------- */

            modulesList = [];

            /* --- EXTERNAL LIBS --- */

            /* Intro core JavaScript */
            if ((featuresLoading_FORCING['showInformation'] === 'true' || featuresInformationShowing === 'true') && !modulesList.includes('intro/2.9.3/js/intro.min.js')) {
                modulesList.push('intro/2.9.3/js/intro.min.js');
                // $('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intro.js/7.2.0/introjs.css">');
                $('head').append('<link rel="stylesheet" href="../../assets/dist/intro/2.9.3/css/introjs.min.css">');
                $('head').append('<link rel="stylesheet" href="../../assets/css/_mod_intro_graph.css">');
                // $.getScript("https://cdnjs.cloudflare.com/ajax/libs/intro.js/7.2.0/intro.min.js");
            }
            /* Typed core JavaScript */
            if (featuresGraph['animation'] === true || featuresGraph['featureLlm'] === true && !modulesList.includes('typed/2.0.11/typed.min.js')) {
                modulesList.push('typed/2.0.11/typed.min.js');
            }
            /* Lodash core JavaScript */
            if (featuresButtons['toggle_modeEditor'] !== false || (featuresGraph['editorOnLoad'] === true && !modulesList.includes('lodash/4.17.15/lodash.min.js'))) {
                modulesList.push('lodash/4.17.15/lodash.min.js');
            }
            /* Autocomplete core JavaScript */
            if (featuresGraph['featureSearchBar'] === true && !modulesList.includes('autocomplete/0.3.0/autocomplete.min.js')) {
                modulesList.push('autocomplete/0.3.0/autocomplete.min.js');
                $('head').append('<link rel="stylesheet" href="../../assets/dist/autocomplete/0.3.0/autocomplete.min.css">');
            }
            /* Mapbox-gl core JavaScript */
            if (featuresButtons['toggle_modeMap'] !== false && !modulesList.includes('mapbox-gl/1.13.0/js/mapbox-gl.js')) {
                modulesList.push('mapbox-gl/1.13.0/js/mapbox-gl.js');
                $('head').append('<link rel="stylesheet" href="../../assets/dist/mapbox-gl/1.13.0/css/mapbox-gl.css">');
            }

            /* --- CYTOSCAPE PLUGINS --- */

            /* Popper */
            if (
                (featuresButtons['toggle_modeEditor'] !== false ||
                    featuresGraph['editorOnLoad'] === true ||
                    (featuresGraph['tooltips'] === true && !modulesList.includes('cytoscape/3.19.0/plugins/popper.min.js'))) &&
                !modulesList.includes('cytoscape/3.19.0/plugins/cytoscape-popper.min.js')
            ) {
                modulesList.push('cytoscape/3.19.0/plugins/popper.min.js');
                modulesList.push('cytoscape/3.19.0/plugins/cytoscape-popper.min.js');
            }
            /* Edgehandlees & Dbclick */
            if (featuresButtons['toggle_modeEditor'] !== false || featuresGraph['editorOnLoad'] === true) {
                $('head').append('<link rel="stylesheet" href="../../assets/css/_mod_editor_mode.css">');
                if (!modulesList.includes('cytoscape/3.19.0/plugins/cytoscape-edgehandles.min.js')) {
                    modulesList.push('cytoscape/3.19.0/plugins/cytoscape-edgehandles.min.js');
                }
                if (!modulesList.includes('cytoscape/3.19.0/plugins/cytoscape-dblclick-master.js')) {
                    modulesList.push('cytoscape/3.19.0/plugins/cytoscape-dblclick-master.js');
                }
            }
            /* Clustering (expand/collapse nodes and edges) */
            if (featuresGraph['featureClustering'] === true) {
                if (!modulesList.includes('cytoscape/3.19.0/plugins/cytoscape-undo-redo.js')) {
                    modulesList.push('cytoscape/3.19.0/plugins/cytoscape-undo-redo.js');
                }
                if (!modulesList.includes('cytoscape/3.19.0/plugins/cytoscape-expand-collapse-custom-grezi.js')) {
                    modulesList.push('cytoscape/3.19.0/plugins/cytoscape-expand-collapse-custom-grezi.js');
                }
                if (!modulesList.includes('cytoscape/3.19.0/plugins/cytoscape-fcose.min.js')) {
                    modulesList.push('cytoscape/3.19.0/plugins/numeric.min.js');
                    modulesList.push('cytoscape/3.19.0/plugins/layout-base.min.js');
                    modulesList.push('cytoscape/3.19.0/plugins/cytoscape-layout-utilities.min.js');
                    modulesList.push('cytoscape/3.19.0/plugins/cose-base.min.js');
                    modulesList.push('cytoscape/3.19.0/plugins/cytoscape-fcose.min.js');
                }
            }
            /* Context Menu */
            if (featuresGraph['featureContextMenu'] === true) {
                if (!modulesList.includes('cytoscape/3.19.0/plugins/cytoscape-cxtmenu.min.js')) {
                    modulesList.push('cytoscape/3.19.0/plugins/cytoscape-cxtmenu.min.js');
                }
            }
            /* Screenshot */
            if (featuresGraph['featureScreenShot'] === true) {
                if (!modulesList.includes('cytoscape/3.19.0/plugins/cytoscape-svg.js')) {
                    modulesList.push('cytoscape/3.19.0/plugins/FileSaver.js');
                    modulesList.push('cytoscape/3.19.0/plugins/canvas2svg.js');
                    modulesList.push('cytoscape/3.19.0/plugins/cytoscape-svg.js');
                }
            }
            /* Euler */
            if (
                (featuresLoading_FORCING['eulerComputation'] === 'true' || featuresEulerComputation === 'true') &&
                !modulesList.includes('cytoscape/3.19.0/plugins/cytoscape-euler.min.js')
            ) {
                modulesList.push('cytoscape/3.19.0/plugins/cytoscape-euler.min.js');
            }
            /* Cola */
            if (
                (featuresButtons['toggle_physics'] !== false || (featuresGraph['physics'] === true && featuresGraph['static'] === false)) &&
                !modulesList.includes('cytoscape/3.19.0/plugins/cola.v3.min.js') &&
                !modulesList.includes('cytoscape/3.19.0/plugins/cytoscape-cola.min.js')
            ) {
                modulesList.push('cytoscape/3.19.0/plugins/cola.v3.min.js');
                modulesList.push('cytoscape/3.19.0/plugins/cytoscape-cola.min.js');
            }
            /* Fcose */
            if ((featuresButtons['toggle_fcose'] !== false || featuresGraph['fcoseOnLoad'] === true) && !modulesList.includes('cytoscape/3.19.0/plugins/cytoscape-fcose.min.js')) {
                modulesList.push('cytoscape/3.19.0/plugins/numeric.min.js');
                modulesList.push('cytoscape/3.19.0/plugins/layout-base.min.js');
                modulesList.push('cytoscape/3.19.0/plugins/cytoscape-layout-utilities.min.js');
                modulesList.push('cytoscape/3.19.0/plugins/cose-base.min.js');
                modulesList.push('cytoscape/3.19.0/plugins/cytoscape-fcose.min.js');
            }
            /* Mapbox-gl core JavaScript */
            if (featuresButtons['toggle_modeMap'] !== false && !modulesList.includes('cytoscape/3.19.0/plugins/cytoscape-mapbox-gl.min.grezi.js')) {
                modulesList.push('cytoscape/3.19.0/plugins/cytoscape-mapbox-gl.min.grezi.js');
            }
            /* Bubblesets */
            if (featuresGraph['featureBubblesets'] !== false && !modulesList.includes('cytoscape/3.19.0/plugins/cytoscape-bubblesets/index.umd.js')) {
                modulesList.push('cytoscape/3.19.0/plugins/cytoscape-bubblesets/index.umd.js');
            }

            modulesList.forEach(function (elem) {
                chain_ScriptLoading = chain_ScriptLoading.then(F_ScriptLoading.bind(null, '../../assets/dist/' + elem));
            });
            chain_ScriptLoading = chain_ScriptLoading.then(resolve_LoadAppliGraph.bind(null, ''));

        });
    });

    /* core js loading */
    chain_LoadAppliGraph = chain_LoadAppliGraph.then(function () {
        return new Promise((resolve_LoadAppliGraph) => {
            progressBarIncrement();
            chain_ScriptLoading = chain_ScriptLoading.then(F_ScriptLoading.bind(null, '../../assets/js/_core_functions.js'));
            chain_ScriptLoading = chain_ScriptLoading.then(F_ScriptLoading.bind(null, '../../assets/js/_core_invert_color.js'));
            chain_ScriptLoading = chain_ScriptLoading.then(resolve_LoadAppliGraph.bind(null, ''));
        });
    });

    /* post-processing */
    chain_LoadAppliGraph = chain_LoadAppliGraph.then(function () {
        return new Promise((resolve_LoadAppliGraph) => {
            progressBarIncrement();
            var script = document.createElement('script');
            script.src = '../../assets/js/___appli_graph.js';
            document.head.appendChild(script);
            resolve_LoadAppliGraph("Done");
        });
    });
}

/*
--------------------------------
CGS.C.04 : Functions for the progress bar
--------------------------------
*/

let chain_ProgressBar = Promise.resolve();

function progressBarIncrement() {
    chain_ProgressBar = chain_ProgressBar.then(function () {
        return new Promise((resolve_ProgressBar) => {
            barProgress += 1 / barStepsNumber;
            bar?.animate(Math.min(1, barProgress));
            resolve_ProgressBar('Done');
        });
    });
}

function progressBarReinitialize(text, steps, addingSteps = false) {
    chain_ProgressBar = chain_ProgressBar.then(function () {
        return new Promise((resolve_ProgressBar) => {
            if (addingSteps === true) {
                barProgress = barProgress * barStepsNumber;
                barStepsNumber += steps;
                barProgress = barProgress / barStepsNumber;
            } else {
                barStepsNumber = steps;
                barProgress = 0;
                bar?.set(0);
            }
            if (text !== '') {
                bar.setText(text);
            }
            resolve_ProgressBar('Done');
        });
    });
}

function progressBarSetText(text) {
    chain_ProgressBar = chain_ProgressBar.then(function () {
        return new Promise((resolve_ProgressBar) => {
            bar.setText(text);
            resolve_ProgressBar('Done');
        });
    });
}

function progressBarStop() {
    chain_ProgressBar = chain_ProgressBar.then(function () {
        return new Promise((resolve_ProgressBar) => {
            barStepsNumber = 0;
            barProgress = 0;
            bar.set(0);
            bar.setText('');
            resolve_ProgressBar('Done');
        });
    });
}

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CGS.D : ON LOAD OPERATIONS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

$(window).on('load', function () {
    let chain_LoadingOperations = Promise.resolve();

    /*
	--------------------------------
	CGS.D.01 : Firstly check if there is a click to begin before downloading the graph
	--------------------------------
	*/

    chain_LoadingOperations = chain_LoadingOperations.then(function () {
        return new Promise((resolve_LoadingOperations) => {

            $('#appli_graph_mask').on('click', function () {
                $('#appli_graph_mask').fadeOut('slow');
                $('#page-top').hover(
                    function () {
                        /* Nothing */
                    },
                    function () {
                        $('#appli_graph_mask').fadeIn('slow');
                    }
                );
                resolve_LoadingOperations('');
            });

            var featuresClickToBegin = getParamValue('clickToBegin');

            if (featuresClickToBegin !== 'true') {
                resolve_LoadingOperations('');
            }
        });
    });

    /*
	--------------------------------
	CGS.D.02 : Show an alter if the navigator is incompatible
	--------------------------------
	*/

    chain_LoadingOperations = chain_LoadingOperations.then(function () {
        return new Promise((resolve_LoadingOperations) => {
            if (navigator.userAgent.indexOf('Mozilla') !== -1 || navigator.userAgent.indexOf('Chrome') !== -1) {
                resolve_LoadingOperations('Done');
            } else {
                $('#otherModal_body').html(
                    "<p><b>/!\\</b></p><p>Attention GREZI n'est pleinement compatible qu'avec les dernières versions de <b>Mozilla Firefox</b> et <b>Google Chrome</b>. Si vous utilisez un autre navigateur vous rencontrerez sûrement des soucis techniques. Vous pourrez les renseigner via le rapport de dysfonctionnement de l'application.</p><p><b>/!\\</b></p>"
                );
                $('#otherModal').modal('show');
                $('#otherModal').on('hidden.bs.modal', function () {
                    resolve_LoadingOperations('Done');
                });
            }
        });
    });

    /*
	--------------------------------
	CGS.D.03 : Then load the graph
	--------------------------------
	*/

    chain_LoadingOperations = chain_LoadingOperations.then(function () {
        return new Promise((resolve_LoadingOperations) => {
            resolve_LoadingOperations(loadAppliGraph());
        });
    });
});

/*
 * Copyright : GREZI
 * Author : Adrien Solacroup
 * Encoding : UTF-8
 * Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
 * Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
 */

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE APPLI GRAPH : SET THE EVENT FUNCTIONS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
SCRIPT STRUCTURE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
 * CORE.A : PREPROCESSING
 * CORE.B : TAP ON THE GRAPH
 * CORE.B.01 : Position indication if not set
 * CORE.B.02 : Double tap activation
 *
 * CORE.C : TAP ON A NODE
 * CORE.C.01 : Select when highlight
 * CORE.C.02 : Creating the modal screen
 * CORE.C.03 : Filter according to the tags
 *
 * CORE.D : HOVERING A NODE
 * CORE.D.01 : Highlight the path
 * CORE.D.02 : Create the tooltips
 * CORE.D.03 : Deal with the cursor style
 * CORE.D.04 : Updating the lateral screen
 * CORE.D.05 : Show the indirect edges
 *
 * CORE.E : HOVER OUT A NODE
 * CORE.E.01 : Remove the highlight
 * CORE.E.02 : Remove the tooltip
 * CORE.E.03 : Hide the indirect edges
 *
 * CORE.F : ADDING A NODE
 * 
 * CORE.G : ADDING AN EDGE
 * 
 * CORE.H : REMOVING A NODE
 */

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE.A : PREPROCESSING
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

var popperTEMP = '';

var tapped = false;

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE.B : TAP ON THE GRAPH
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

cy.on('tap', function () {
    /* CORE.B.01 : Position indication if not set */
    if (editorModeActivated === true) {
        var positionsTEMP = {};
        cy.$('node:compound').forEach(function (elem) {
            var positionFLOAT = elem.position();
            var positionINTEGER = { x: parseInt(positionFLOAT['x']), y: parseInt(positionFLOAT['y']) };
            positionINTEGER['height'] = parseInt(elem.height()) - 32;
            positionINTEGER['width'] = parseInt(elem.width()) - 32;
            positionsTEMP[elem.id()] = positionINTEGER;
        });
        cy.$('node:childless').forEach(function (elem) {
            if (!elem.hasClass('infoBubble') && !elem.hasClass('infoTag') && !elem.hasClass('compoundCorner') && !elem.hasClass('eh-handle')) {
                var positionFLOAT = elem.position();
                var positionINTEGER = { x: parseInt(positionFLOAT['x']), y: parseInt(positionFLOAT['y']) };
                positionsTEMP[elem.id()] = positionINTEGER;
            }
        });
        var formContent = document.getElementById('editor_get_positions');
        formContent.value = JSON.stringify(positionsTEMP, null, '\t');
    }

    /* CORE.B.02 : Double tap activation */
    if (editorModeActivated === false) {
        if (!tapped) {
            tapped = setTimeout(function () {
                tapped = null;
            }, 300);
        } else {
            cy.nodes().unselect();
            clearTimeout(tapped);
            tapped = null;
            ForceStabilization();
        }
    }
});

/* LINK TO POC NAVIGATION MODE

var x_mouse;

var y_mouse;

function getCursorXY(e) {
	x_mouse = (window.Event) ? e.pageX : event.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
	y_mouse = (window.Event) ? e.pageY : event.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
};

document.onmousedown = getCursorXY;

cy.on("dblclick", function(e) {
	if (!cy.animated()) {
		cy.animate({
			"duration": 500,
			"zoom": (cy.zoom()+0.5),
			"pan": {"x": cy.width()-x_mouse, "y": cy.height()-y_mouse},
			"easing": "ease"
		});
	};
});

*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE.C : TAP ON A NODE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

cy.on("tap", "node", function() {

	if (featuresGraph.featureSearchBar) {widgetSearch.clear();};
	
	if (!this.hasClass("eh-handle")) {
	
		var networkNodes = this.data();
		
		/* CORE.C.01 : Select when highlight */
		if ((featuresGraph.modeHighlighter !== "") && (!this.hasClass("infoBubble")) && (!this.hasClass("infoTag"))) {
			if (!this.isParent()) {
				cy.nodes().difference(".noHighlight").difference(".insensitiveNode").difference(this).select();
			};
			cy.$("node:selected").union(this).edgesWith("node:selected").select();
		};
		
		/* CORE.C.02 : Creating the modal screen */
		if (featuresGraph["infoBubbles"] === true) {
			var networkNodes_BIS = cy.getElementById(this.id().replace("_ib",""))[0].data();
			if (networkNodes["id"] === networkNodes_BIS["id"]) {
				networkNodes_BIS = undefined;
			};
		} else {
			var networkNodes_BIS = networkNodes;
		};
		if (networkNodes_BIS !== undefined) {
			if ( (networkNodes_BIS["titleComplete"] !== "") && (featuresGraph["modalSelection"] === true) && (isLateral === "closing") && (networkNodes_BIS["titleComplete"] != undefined) ) {
				var modalContent_selectionHeader = document.getElementById("selectionModal_header");
				textTEMP = "";
				if (networkNodes_BIS["image"] !== undefined) {textTEMP = textTEMP.concat("<img class=\"lazyload\" data-src=\"",networkNodes_BIS["image"],"\"></img>");};
				if (networkNodes_BIS["label"] !== undefined) {textTEMP = textTEMP.concat(networkNodes_BIS["label"]);} else {textTEMP = textTEMP.concat("");};
				modalContent_selectionHeader.innerHTML = textTEMP;
				var modalContent_selectionHeader = document.getElementById("selectionModal_body");
				modalContent_selectionHeader.innerHTML = networkNodes_BIS["titleComplete"];
				$('#selectionModal').modal('show');
				if (featuresGraph["modalSelection"] === true) { document.body.style.cursor = "default"; };
			};
		};
		
		/* CORE.C.03 : Filter according to the tags */
		if ( (featuresGraph["featureSearchBar"] === true) && (this.hasClass("infoTag")) ) {
			let currentNodeTag = this.data("tagName");
			let currentNodeLabel = this.data("label");
			cy.nodes().difference(cy.nodes(":parent")).forEach(function (elem) {
				if (elem.hasClass("defaultNode")) {
					if (!Object.keys(elem.data("searchTag")).includes(currentNodeTag)) {
						elem.union(elem.connectedEdges()).addClass("unchecked_searched");
						cy.$("[id = "+"'"+elem.id()+"_ib"+"'"+"]").remove();
						cy.$("[id ^= "+"'"+elem.id()+"_tag"+"'"+"]").remove();
					} else {
						if (elem.data("searchTag")[currentNodeTag].indexOf(currentNodeLabel) === -1) {
							elem.union(elem.connectedEdges()).addClass("unchecked_searched");
							cy.$("[id = "+"'"+elem.id()+"_ib"+"'"+"]").remove();
							cy.$("[id ^= "+"'"+elem.id()+"_tag"+"'"+"]").remove();
						};
					};
				};
			});
		};
		
	};
	
});

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE.D : HOVERING A NODE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

cy.on('mouseover', 'node', function () {

    var elem = document.getElementById('appli_graph_main_screen');

    var networkNodes = this.data();

    if (this.hasClass("defaultNode")) {cy.nodes().removeClass("hoveredNode");};

    this.addClass("hoveredNode");

    /* CORE.D.01 : Highlight the path */
    if (cy.nodes('.noHighlight').length === 0 && featuresGraph.modeHighlighter !== '' && !this.hasClass('infoBubble') && !this.hasClass('infoTag') && !this.isParent() && !mapModeActivated) {
        const nodeTMP = this;
        chain_AnimateHighlighter = chain_AnimateHighlighter.then(function () {
            return new Promise((resolve_AnimateHighlighter) => {
                cy.nodes('.defaultNode').addClass('noHighlight');
                cy.edges().addClass('noHighlight');
                cy.nodes('.insensitiveNode').removeClass('noHighlight');
                cy.edges('.indirectEdge').removeClass('noHighlight');
                nodeTMP.removeClass('noHighlight');
                let collectionToGetThrough = cy.elements(':visible');
                if ((featuresGraph.modeHighlighter === 'ascendency') | (featuresGraph.modeHighlighter === 'both')) {
                    collectionToGetThrough.getElementById(nodeTMP.id()).predecessors().removeClass('noHighlight');
                }
                if ((featuresGraph.modeHighlighter === 'downdency') | (featuresGraph.modeHighlighter === 'both')) {
                    collectionToGetThrough.getElementById(nodeTMP.id()).successors().removeClass('noHighlight');
                }
                // cy.nodes().difference(cy.nodes('.noHighlight')).style('opacity', 1);
                if (featuresGraph.typeHighlighter === "" || featuresGraph.typeHighlighter === "both" || featuresGraph.typeHighlighter === "node") {
                    if (cy.nodes('.noHighlight').difference(cy.$('node:parent')).length > 0) {
                        cy.nodes('.noHighlight')
                        .difference(cy.$('node:parent'))
                        .animate({
                            duration: 50,
                            style: {
                                'text-opacity': cy.nodes('.noHighlight').difference(cy.$('node:parent')).style('text-opacity'),
                                'text-opacity': 0.25,
                                'background-opacity': cy.nodes('.noHighlight').difference(cy.$('node:parent')).style('background-opacity'),
                                'background-opacity': 0,
                                'border-opacity': cy.nodes('.noHighlight').difference(cy.$('node:parent')).style('border-opacity'),
                                'border-opacity': 0
                            },
                            complete: function() {resolve_AnimateHighlighter("Done");}
                        })
                    } else {resolve_AnimateHighlighter("Done");};
                } else {if (featuresGraph.typeHighlighter !== "edge") {resolve_AnimateHighlighter("Done");};};
                cy.edges().difference('.noHighlight').addClass("hoveredEdge");
                if (featuresGraph.typeHighlighter === "" || featuresGraph.typeHighlighter === "both" || featuresGraph.typeHighlighter === "edge") {
                    if (cy.edges().difference('.noHighlight').length > 0) {
                        cy.edges().difference('.noHighlight').animate({
                            duration: 50,
                            style: { opacity: featuresGraph.edgeOpacity, opacity: 1 },
                            complete: function() {if (featuresGraph.typeHighlighter !== "node") {resolve_AnimateHighlighter("Done");};}
                        });
                        if (featuresGraph.edgeOpacity > 0.25) {cy.edges('.noHighlight').animate({ duration: 50, style: { opacity: featuresGraph.edgeOpacity, opacity: 0.25 } });};
                    } else {if (featuresGraph.typeHighlighter !== "node") {resolve_AnimateHighlighter("Done");};};
                };
            });
        });
    }

    /* CORE.D.02 : Create the tooltips */
    if (featuresGraph['tooltips'] === true && popperTEMP === '' && this.data()['title'] != undefined && this.data()['title'] != '' && this.data()['title'] != ' ') {
        popperTEMP = this.popper({
            content: () => {
                let tooltipTEMP = document.createElement('div');
                tooltipTEMP.id = 'popper';
                tooltipTEMP.innerHTML = this.data()['title'];
                document.body.appendChild(tooltipTEMP);
                return tooltipTEMP;
            },
            popper: {
                placement: 'left',
                modifiers: {
                    flip: { behavior: ['left', 'bottom', 'top'] },
                    preventOverflow: { boundariesElement: elem },
                },
            },
        });
    }

    /* CORE.D.03 : Deal with the cursor style */
    if (proximityModeActivated) {document.body.style.cursor = 'all-scroll';} else {
        if (
            networkNodes['titleComplete'] !== '' &&
            featuresGraph['modalSelection'] === true &&
            isLateral === 'closing' &&
            this.data()['titleComplete'] != undefined
        ) {
            if (featuresGraph['infoBubbles'] === false) {
                document.body.style.cursor = 'help';
            }
        }
        if (this.hasClass('infoBubble')) {
            var networkNodes_BIS = cy.getElementById(this.id().replace('_ib', ''))[0].data();
            if (networkNodes_BIS['titleComplete'] !== undefined) {
                document.body.style.cursor = 'pointer';
            }
        }
        if (featuresGraph['featureSearchBar'] === true && this.hasClass('infoTag')) {
            document.body.style.cursor = 'pointer';
        }
    }

    /* CORE.D.04 : Updating the lateral screen */
    if ((elem.style.width === '60%' || elem.style.height === '60%') && editorModeActivated === false && cy.elements(":selected").length === 0) {
        UpdatingLateralScreen(this);
    }

    /* CORE.D.05 : Show the indirect edges */
    this.connectedEdges('.indirectEdge').forEach(function (elem) {
        elem.toggleClass('indirectEdge_hidden', false);
    });
});

function UpdatingLateralScreen(node) {
    var textTEMP = '';
    var networkNodes = node.data();
    if (networkNodes['titleComplete'] === undefined) {
        textTEMP = textTEMP.concat("<h1>Survolez les éléments pour plus d'informations</h1>");
        textTEMP = textTEMP.concat('<div class="lateral-hr"><hr class="my-6"></div>');
    } else {
        textTEMP = textTEMP.concat('<h1>');
        if (networkNodes['image'] !== undefined) {
            textTEMP = textTEMP.concat('<img class="lazyload" data-src="', networkNodes['image'], '"></img>');
        }
        if (networkNodes['label'] !== undefined) {
            textTEMP = textTEMP.concat(networkNodes['label']);
        } else {
            textTEMP = textTEMP.concat('');
        }
        textTEMP = textTEMP.concat('</h1>');
        textTEMP = textTEMP.concat('<div class="lateral-hr"><hr class="my-6"></div>');
        textTEMP = textTEMP.concat(networkNodes['titleComplete']);
    }
    if (node.connectedEdges().connectedNodes().difference(".unchecked,.unchecked_type,.unchecked_searched,.unchecked_proximity,.unchecked_millesime,.unchecked_map").length > 0) {
        textTEMP = textTEMP.concat('<div class="lateral-hr"><hr class="my-6"></div><p><b>Éléments reliés :</b></p>');
        textTEMP = textTEMP.concat('<div>');
        node.connectedEdges().connectedNodes(":visible").difference(".unchecked,.unchecked_type,.unchecked_searched,.unchecked_proximity,.unchecked_millesime,.unchecked_map").difference(node).forEach(function(connectedNode) {
            textTEMP = textTEMP.concat('<button class="btn btn-secondary btn-linkednodes" onclick="ForceStabilization(cy.$(\'#'+connectedNode.id()+'\'));cy.elements().unselect();cy.$(\'#'+connectedNode.id()+'\').select();UpdatingLateralScreen(cy.$(\'#'+connectedNode.id()+'\'))">'+connectedNode.data('label')+'</button>');
        });
        textTEMP = textTEMP.concat('</div>');
    };
    var lateralContent = document.getElementById('tooltipLateral_body');
    lateralContent.innerHTML = textTEMP;
};

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE.E : HOVER OUT A NODE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

cy.on('mouseout', 'node', function () {

    /* CORE.E.01 : Remove the highlight */
    if (featuresGraph.modeHighlighter !== '' && !this.hasClass('infoBubble') && !this.hasClass('infoTag') && !mapModeActivated) {
        chain_AnimateHighlighter = chain_AnimateHighlighter.then(function () {
            return new Promise((resolve_AnimateHighlighter) => {
                // cy.nodes().difference(cy.nodes('.noHighlight')).style('opacity', 1);
                nodesNoHighlight = cy.nodes('.noHighlight');
                cy.nodes('.noHighlight').removeClass('noHighlight');
                if (featuresGraph.typeHighlighter === "" || featuresGraph.typeHighlighter === "both" || featuresGraph.typeHighlighter === "node") {
                    if (nodesNoHighlight.length > 0) {
                        nodesNoHighlight.animate({
                            duration: 50,
                            style: {
                                'text-opacity': nodesNoHighlight.style('text-opacity'),
                                'text-opacity': 1,
                                'background-opacity': nodesNoHighlight.style('background-opacity'),
                                'background-opacity': 1,
                                'border-opacity': nodesNoHighlight.style('border-opacity'),
                                'border-opacity': 1
                            },
                            complete: function() {resolve_AnimateHighlighter("Done");}
                        });
                    } else {resolve_AnimateHighlighter("Done");};
                } else {if (featuresGraph.typeHighlighter !== "edge") {resolve_AnimateHighlighter("Done");};};
                cy.edges('.noHighlight').removeClass('noHighlight');
                cy.edges().removeClass('hoveredEdge');
                if (featuresGraph.typeHighlighter === "" || featuresGraph.typeHighlighter === "both" || featuresGraph.typeHighlighter === "edge") {
                    cy.edges().difference(".insensitiveEdge").removeStyle("opacity");
                    if (featuresGraph.typeHighlighter !== "node") {resolve_AnimateHighlighter("Done");};
                };
            });
        });
    }
    document.body.style.cursor = idleCursor;

    /* CORE.E.02 : Remove the tooltip */
    if (popperTEMP !== '') {
        popperTEMP.destroy();
        document.getElementById('popper').remove();
        popperTEMP = '';
    }

    /* CORE.E.03 : Hide the indirect edges */
    this.connectedEdges('.indirectEdge').forEach(function (elem) {
        if ($('#toggle_ind_link').css('background-color') !== 'rgb(255, 255, 255)') {
            elem.toggleClass('indirectEdge_hidden', true);
        }
    });
});

$("#appli_graph_main_screen").on("mouseout", function(){
	cy.batch(function(){if(cy.nodes(".hoveredNode").length > 0){cy.nodes().removeClass("hoveredNode");};});
});

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE.F : ADDING A NODE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

cy.on('add', 'node', function () {
    if (!this.hasClass('infoBubble') && !this.hasClass('infoTag')) {
        if (featuresGraph['infoBubbles'] === true) {
            informationBubbles(this);
        }
    };
    if (featuresGraph.featureLegend && featuresGraph_LegendOptions.showNodes_WithCategories) {
        let tmp_nodeCheckBox = $("#checkNode_"+featuresGraph_LegendOptions.showNodes_Dictionnary.findIndex(element => cy.$(element["selector"]).intersection(this).length > 0));
        if (tmp_nodeCheckBox.length > 0) {
            if (tmp_nodeCheckBox[0].checked) {
                this.removeClass("unchecked");
            } else {
                this.addClass("unchecked");
            };
        };
    };
});

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE.G : ADDING AND HOVERING AN EDGE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

if (featuresGraph.featureLegend && featuresGraph_LegendOptions.showEdges_WithCategories) {
    cy.on("add", "edge", function(edge) {
        let tmp_edgeCheckBox = $("#checkLink_"+featuresGraph_LegendOptions.showEdges_Dictionnary.findIndex(element => cy.$(element["selector"]).intersection(this).length > 0));
        if (tmp_edgeCheckBox.length > 0) {
            if (tmp_edgeCheckBox[0].checked) {
                this.removeClass("unchecked_edge");
            } else {
                this.addClass("unchecked_edge");
            };
        };
    });
};

if (featuresGraph.edgeHovering) {

    cy.on("mouseover", "edge", function() {
        this.addClass("hoveredEdge");
    });
    
    cy.on("mouseout", "edge", function() {
        this.removeClass("hoveredEdge");
    });

};

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CORE.H : REMOVING A NODE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

let chain_RemovingNodes = Promise.resolve();

cy.on('remove', 'node', function () {
    if (!this.hasClass('infoBubble') && !this.hasClass('infoTag')) {
        var node_id = this.id();
        chain_RemovingNodes = chain_RemovingNodes.then(function () {
            return new Promise((resolve_RemovingNodes) => {
                cy.$('[id ^= ' + "'" + node_id + '_ib' + "'" + ']').remove();
                cy.$('[id ^= ' + "'" + node_id + '_tag' + "'" + ']').remove();
                resolve_RemovingNodes('Done');
            });
        });
    }
});

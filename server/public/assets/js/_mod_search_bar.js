/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : CREATING SEARCH BAR
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/* Setting variables */

let chain_SearchBar = Promise.resolve();

let chain_SearchBarPHASING = Promise.resolve();

var toggleSearchBar_CustomOpening;

var toggleSearchBar_CustomClosing;

var zoomMode;

var nodeSelection;

var checkingInfoBubble;

var widgetSearch;

$("#appli_graph_SEARCH_BUTTON_filter").tooltip({"title": "Pour revenir à la visualisation complète, appliquer \"Filtrer\" à vide","delay": {"show": 100, "hide": 100},"placement": "top","trigger": "hover"});

/* Avoid to show input panel on mobile devices */

var targetNode_SearchBar = document.querySelector('#appli_graph_SEARCH_BAR');
var observer_SearchBar = new MutationObserver(function(mutationsList, observer) {
    mutationsList.forEach(function(mutation) {
        if (mutation.type === 'childList' && mutation.addedNodes.length > 0) {
            $(".input-8f2fe").attr("placeholder", " ");
            let widthSearchBar = $("#appli_graph_SEARCH_BAR").css("width");
            let isMobile = parseInt(widthSearchBar.replace("px", "")) / $(window).width();
            if (isMobile > 0.5) {
                $(".input-8f2fe").attr("readonly", true);
            } else {
                $(".input-8f2fe").attr("readonly", false);
            }
        }
    });
});
observer_SearchBar.observe(targetNode_SearchBar, { childList: true });

$(window).on('resize', function () {
	let widthSearchBar = $("#appli_graph_SEARCH_BAR").css("width");
	let isMobile = parseInt(widthSearchBar.replace("px",""))/$(window).width();
	if (isMobile > 0.5) {
		$(".input-8f2fe").attr("readonly", true);
	} else {
		$(".input-8f2fe").attr("readonly", false);
	};
});

/* Main function */

function creatingSearchBar(RemovedUncheckedNodes=true,HideOnLoad=true) {

	/* Preparing the searching lists */
	
	var tagList = [];
	
	var collectionToTag = cy.collection();
	
	if (RemovedUncheckedNodes) {
		collectionToTag.merge(cy.nodes().difference(".unchecked,.unchecked_type"));
	} else {
		collectionToTag.merge(cy.nodes());
	};

	if (mapModeActivated) {collectionToTag = collectionToTag.nodes(".mapNode").difference(":parent");};

	collectionToTag.forEach(function(elem) {
		if ((elem.data("searchTag") !== undefined) && (elem.data("searchTag") !== "")) {tagList.push(elem.data("searchTag"));};
	});
	
	if (tagList.length > 0) {
		
		/* Creating the searching dictionnary */
		
		var tagList_KEYS = [];
		
		var tagList_VALUES = {};
		
		for (i_search in tagList) {
			for (i_tag_index in tagList[i_search]) {
				let tempLogicTest = true;
				for (i_categoryToHide in featuresGraph_SearchBarOptions['categoriesToHide_EvenInDebugMode']) {
					let categoriyToHide = featuresGraph_SearchBarOptions['categoriesToHide_EvenInDebugMode'][i_categoryToHide];
					tempLogicTest = tempLogicTest && (i_tag_index !== categoriyToHide);
				};
				if (getParamValue("debug") === undefined) {
					for (i_categoryToHide in featuresGraph_SearchBarOptions['categoriesToHide']) {
						let categoriyToHide = featuresGraph_SearchBarOptions['categoriesToHide'][i_categoryToHide];
						tempLogicTest = tempLogicTest && (i_tag_index !== categoriyToHide);
					};
				};
				if (tempLogicTest) {
					tagList_KEYS.push(i_tag_index);
					var tagList_VALUES_keys = Object.keys(tagList_VALUES);
					if (!tagList_VALUES_keys.includes(i_tag_index)) {tagList_VALUES[i_tag_index] = [];};
					for (i_tag_value in tagList[i_search][i_tag_index]) {
						let valueOfTag = tagList[i_search][i_tag_index][i_tag_value];
						if (valueOfTag !== undefined) {
							if (valueOfTag && (valueOfTag.indexOf("https") === -1) && (!Array.isArray(valueOfTag))) {
								let valueTemp = valueOfTag;
								if(valueTemp !== "") {tagList_VALUES[i_tag_index].push(valueTemp);};
							} else {
								if (valueOfTag) {console.error("Problème sur les étiquettes : "+valueOfTag)};
							};
						};
					};
				};
			};
		};

		for (i_key in tagList_KEYS) {
			if (tagList_VALUES[tagList_KEYS[i_key]].length === 0) {
				tagList_KEYS.splice(i_key,1);
			};
		};
	
		let tagList_KEYS_unique = [...new Set(tagList_KEYS)];
		
		var tagList_KEYS_sorted = tagList_KEYS_unique.sort();
		
		var tagList_VALUES_unique = {};
		
		for (i_value in tagList_VALUES) {
			let tagTEMP = [...new Set(tagList_VALUES[i_value])];
			var tagTEMP_sorted = tagTEMP.sort();
			tagTEMP_sorted = tagTEMP_sorted.filter(function (element) {
				return element !== undefined;
			});
			if (tagTEMP_sorted.length >= 1) {tagList_VALUES_unique[i_value] = {"noResultsHTML": "Saisissez une option valide", "options": tagTEMP_sorted};};
		};
		
		var tagList_FOR_SEARCH = tagList_VALUES_unique;
	
		tagList_FOR_SEARCH["nodes"] = [];
		
		for (i_for_search in tagList_KEYS_sorted) {
			i_for_search_MODIF = tagList_KEYS_sorted[i_for_search]
			for (i_categoriesToRename in featuresGraph_SearchBarOptions['categoriesToRename']) {
				let categoryToRename = featuresGraph_SearchBarOptions['categoriesToRename'][i_categoriesToRename];
				if (tagList_KEYS_sorted[i_for_search] === categoryToRename[0]) {i_for_search_MODIF = categoryToRename[1]};
			};
			tagList_FOR_SEARCH["nodes"].push({"value": i_for_search_MODIF, "children": tagList_KEYS_sorted[i_for_search]})
		};
		
		/* On Change Function */
		
		var onChangeSearch = function(newValue, oldValue) {
			/* nothing */
		};
		
		/* Config options */
		
		var configSearch = {
			onChange: onChangeSearch,
			initialList: "nodes",
			lists: tagList_FOR_SEARCH,
			placeholderHTML : "<img src=\"../../assets/icon/plus-solid.svg\" style=\"height:10px;\"/>&nbsp;<i>Nouvelle recherche</i>",
			tokenSeparatorHTML : " ▸ "
		};
		
		/* Creating the search bar */
		
		if (widgetSearch === undefined) {
			widgetSearch = new AutoComplete('appli_graph_SEARCH_BAR', configSearch);
		} else {
			widgetSearch.destroy();
			widgetSearch = new AutoComplete('appli_graph_SEARCH_BAR', configSearch);
		};
		
		/* Post-processing */
		if (HideOnLoad === true) {$("#appli_graph_SEARCHING").css({"visibility": "hidden"})};
		
		$("#appli_graph_SEARCH_BUTTON").css("display","initial");
		
		$("#appli_graph_SEARCH_BUTTON_filter").css("display","initial");
		
		$("#appli_graph_SEARCHING .btn").height($("#appli_graph_SEARCH_BAR").height()-4);
		
		/* Make the height of the button following the one of the search bar */
		
		$(".autocomplete-container-7a26d").on('stylechanged',function(){
			$("#appli_graph_SEARCHING .btn").height($("#appli_graph_SEARCH_BAR").height()-4);
			$("#appli_graph_cc_element").css({"margin-bottom": "calc("+$("#appli_graph_SEARCH_BAR").height()+"px + .5rem)"});
		});
		
	} else {
		widgetSearch.destroy();
		widgetSearch = new AutoComplete('appli_graph_SEARCH_BAR', {
			initialList: "nodes",
			lists: {"nodes": ""},
			placeholderHTML : "<i>Aucune entité listée</i>"
		});
	};
	
};

/* Button search onclick function */

function searchBarActivation(zoom_function=false,filter_function=false,proximity_function=false){

	chain_SearchBar = chain_SearchBar.then(function(){
		return new Promise((resolve_SearchBar) => {
			cy.nodes().removeClass("searchedZoomed");
			if (toggleSearchBar_CustomOpening !== undefined) {
				resolve_SearchBar(toggleSearchBar_CustomOpening());
			} else {
				resolve_SearchBar("Done");
			};
		});
	});
	
	chain_SearchBar = chain_SearchBar.then(function(){
		return new Promise((resolve_SearchBar) => {
			
			$("#appli_graph_network_CONTAINER").css("pointer-events","none");
			
			if (featuresGraph["infoBubbles"] === true) {
				featuresGraph["infoBubbles"] = false;
				checkingInfoBubble = true;
			} else {
				checkingInfoBubble = false;
			};
	
			zoomMode = zoom_function;

			resolve_SearchBar("Done");
	
		});
	});
	
	chain_SearchBar = chain_SearchBar.then(function(){
		return new Promise((resolve_SearchBar) => {
			
			var widgetSearch_Values = widgetSearch.getValue();
			var onlySelection = filter_function;
			
			if (widgetSearch_Values.length > 0) {
				if (widgetSearch_Values[0].length > 1) {
					var booleanTEMP = true;
				} else {
					var booleanTEMP = false;
				};
			} else {
				var booleanTEMP = false;
			};

			if (booleanTEMP === true) {
			
				/* Opening the clusters */
				chain_SearchBarPHASING = chain_SearchBarPHASING.then(function(){
					return new Promise((resolve_SearchBarPHASING) => {
						if (zoomMode === false) {
							cy.nodes().removeClass("unchecked_searched");
							cy.edges().removeClass("unchecked_searched");
						};
						setTimeout(function() {resolve_SearchBarPHASING("Done")}, 5);
					});
				});
				
				chain_SearchBarPHASING = chain_SearchBarPHASING.then(function(){
					return new Promise((resolve_SearchBarPHASING) => {

						/* Set the predicate */
						var predicateSelection = "OR";
						
						function switchChecking(switchValue,comparisonValue,predicateValue){
							if (predicateValue === "AND") {return switchValue === comparisonValue;};
							if (predicateValue === "OR") {return switchValue >= 1;};
						};
					
						/* Updating the network */
						nodeSelection = cy.collection();
						cy.nodes().forEach(function(elem) {
							if ( (elem.data("searchTag") !== undefined) && (Object.keys(elem.data("searchTag")).length !== 0) && (elem.data("searchTag") !== "") ) {
								var switchSearch = 0;
								for (i in widgetSearch_Values) {
									var searchTag_TEMP = widgetSearch_Values[i][0]["value"];
									let categoryToRename = featuresGraph_SearchBarOptions['categoriesToRename'].find(function(category) {return searchTag_TEMP === category[1];});
									if (categoryToRename) {searchTag_TEMP = categoryToRename[0];};									  
									var valueTag_TEMP = widgetSearch_Values[i][1]["value"];
									var keysTag_TEMP = Object.keys(elem.data("searchTag"));
									if (keysTag_TEMP.includes(searchTag_TEMP)) {
										var currentTag_TEMP = elem.data("searchTag")[searchTag_TEMP];
										if (currentTag_TEMP.includes(valueTag_TEMP)) {
											switchSearch = switchSearch+1;
										};
									};
								};
								if (switchChecking(switchSearch,widgetSearch_Values.length,predicateSelection)) {
									if (onlySelection === false) {
										elem.addClass("searched");
										nodeSelection = nodeSelection.union(elem);
									} else {
										if (elem.selected()) {
											elem.addClass("searched");
											nodeSelection = nodeSelection.union(elem);
										};
									};
								};
							};
						});

						setTimeout(function() {resolve_SearchBarPHASING("Done")}, 5);
					});
				});
				
				/* Remove compounds without nodes */
				chain_SearchBarPHASING = chain_SearchBarPHASING.then(function(){
					return new Promise((resolve_SearchBarPHASING) => {
						if (zoomMode === false) {
							nodeSelection = nodeSelection.union(nodeSelection.ancestors()).union(".insensitiveNode");
							if (proximity_function) {
								loadProximityGraph(nodeSelection);
							} else {
								cy.elements().difference(nodeSelection.union(nodeSelection.edgesWith(nodeSelection))).addClass("unchecked_searched");
							};
						};
						setTimeout(function() {resolve_SearchBar("Done");resolve_SearchBarPHASING("Done");}, 5);
					});
				});
				
			} else {
				
				chain_SearchBarPHASING = chain_SearchBarPHASING.then(function(){
					return new Promise((resolve_SearchBarPHASING) => {
						if (zoomMode === false) {
							cy.nodes().removeClass("unchecked_searched");
							cy.edges().removeClass("unchecked_searched");
						};
						setTimeout(function() {resolve_SearchBarPHASING("Done")}, 5);
					});
				});
				
				chain_SearchBarPHASING = chain_SearchBarPHASING.then(function(){
					return new Promise((resolve_SearchBarPHASING) => {
						if (onlySelection === true) {
							nodeSelection = cy.elements(":selected,.insensitiveNode").union(cy.elements(":selected").descendants());
							if (zoomMode === false) {
								cy.elements().difference(nodeSelection.union(nodeSelection.edgesWith(nodeSelection))).addClass("unchecked_searched");
								setTimeout(function() {resolve_SearchBarPHASING("Done")}, 5);
							} else {
								setTimeout(function() {resolve_SearchBarPHASING("Done")}, 5);
							};
						} else {
							nodeSelection = cy.elements();
							setTimeout(function() {resolve_SearchBarPHASING("Done")}, 5);
						};
					});
				});

				chain_SearchBarPHASING = chain_SearchBarPHASING.then(function(){
					return new Promise((resolve_SearchBarPHASING) => {
						setTimeout(function() {resolve_SearchBar("Done");resolve_SearchBarPHASING("Done");}, 5);
					});
				});
				
			};

		});
	});
	
	chain_SearchBar = chain_SearchBar.then(function(){
		return new Promise((resolve_SearchBar) => {
			
			if (zoomMode === false) {cy.nodes().unselect();};
			cy.nodes().removeClass("searched");
			cy.edges().unselect();
			if (checkingInfoBubble === true) {
				featuresGraph["infoBubbles"] = true;
				informationBubbles(cy.nodes());
			};
			var modalContent_Summary = document.getElementById("summaryModal_body");
			modalContent_Summary.innerHTML = readNodes_for_modal(cy);
			setTimeout(function() {
				$("#appli_graph_network_CONTAINER").css("pointer-events","auto");
				if (proximity_function) {resolve_SearchBar("Done");} else {
					if (zoomMode === false) {
						resolve_SearchBar(ForceStabilization(nodeSelection.union(cy.nodes(":compound"))));
					} else {
						if (widgetSearch.getValue().length > 0) {
							nodeSelection.addClass("searchedZoomed");
							nodeSelection.removeClass("farViewed");
						};
						resolve_SearchBar(ForceStabilization(nodeSelection));
					};
				};
			}, 5);
			
		});
	});

	chain_SearchBar = chain_SearchBar.then(function(){
		return new Promise((resolve_SearchBar) => {
			if (toggleSearchBar_CustomClosing !== undefined) {
				resolve_SearchBar(toggleSearchBar_CustomClosing());
			} else {
				resolve_SearchBar("Done");
			};
		});
	});
		
};

$("#appli_graph_SEARCH_BUTTON").on('click',function() {
	if (cy.nodes(":selected").length > 0) {
		searchBarActivation(true,true,false);
	} else {
		searchBarActivation(true,false,false);
	};
});

$("#appli_graph_SEARCH_BUTTON_filter").on('click',function() {
	if (cy.nodes(":selected").length > 0) {
		searchBarActivation(false,true,false);
	} else {
		searchBarActivation(false,false,false);
	};
});

$("#appli_graph_SEARCH_BUTTON_proximity").on('click',function() {
	searchBarActivation(false,false,true);
});

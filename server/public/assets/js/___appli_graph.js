/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
___APPLI_GRAPH.JS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
SCRIPT STRUCTURE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
    *
	* AGS.A : GLOBAL VARIABLES
    *
	* AGS.B : PRE-PROCESSING
		* AGS.B.01 : Forcing the loading options
		* AGS.B.02 : Displaying or not the features buttons
		* AGS.B.03 : Editing the colors of the features buttons
		* AGS.B.04 : Generating the default lateral screen body
		* AGS.B.05 : Creating a default variable for the node colors
        * AGS.B.06 : Responsive behaviour
	*
	* AGS.C : MAIN STEPS
		* AGS.C.01 : Init events
			* AGS.C.01.01 : Hierarchical network variables
			* AGS.C.01.02 : Activate or not the tooltips
			* AGS.C.01.03 : Initiate the hilighter function
		*
		* AGS.C.02 : Creating views buttons
			* AGS.C.02.01 : "Reload" button
			* AGS.C.02.02 : Link functions to the buttons
		*
		* AGS.C.03 : Set the network
			* AGS.C.03.01 : Prepare the options variable
			* AGS.C.03.02 : Setting the options of the network (see the cytoscape documentation for more details)
			* AGS.C.03.03 : Create the network
			* AGS.C.03.04 : Changing the options style if specified in the config file
			* AGS.C.03.05 : Initial layout
			* AGS.C.03.06 : Deal with state of buttons
		*
	*
	* AGS.D : LAUNCHING PROMISES
		* AGS.D.01 : Data promise
			* AGS.D.01.01 : Upload custom data
			* AGS.D.01.02 : Loading the datas
			* AGS.D.01.03 : Preparing the datas of the network
		*
		* AGS.D.02 : Dealing with specified positions
			* AGS.D.02.01 : Set the custom positions of the compounds
			* AGS.D.02.02 : Lock the nodes if static option is activated
		*
		* AGS.D.03 : Script loading promise
			* AGS.D.03.01 : Creating the loading dictionnary
			* AGS.D.03.02 : Loading the dictionnary
			* AGS.D.03.03 : Loading the custom scripts
			* AGS.D.03.04 : Ending the promise
		*
        * AGS.D.04 : Building the legend
        * 
		* AGS.D.05 : Some post-process promise
			* AGS.D.05.01 : Generating the modal screen summary body
			* AGS.D.05.02 : Postprocessing
			* AGS.D.05.03 : Creating the search bar
			* AGS.D.05.04 : End the promise
		*
		* AGS.D.06 : Animation promise
        *
		* AGS.D.07 : Fcose promise
        *
		* AGS.D.08 : Stabilization promise
		*
        * AGS.D.09 : Spinner and Credentials promise
        * 
		* AGS.D.10 : Postprocess promise
			* AGS.D.10.01 : Make the graph visible
			* AGS.D.10.02 : Loading the search bar
            * AGS.D.10.03 : Show the feature buttons separators
			* AGS.D.10.04 : Open the lateral screen, the editor mode or the map mode on load
			* AGS.D.10.05 : Stabilize the tooltips
            * AGS.D.10.06 : Show the labels
            * AGS.D.10.07 : Show the bubblesets
            * AGS.D.10.08 : Window resize
			* AGS.D.10.09 : End the promise
		*
		* AGS.D.11 : Script template custom additions
        * 
        * AGS.D.12 : Activate the tutorial and graphIsLoad
        * 
	*
*/
	
/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
AGS.A : GLOBAL VARIABLES
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

var cy;

var isLateral = 'closing';

var dataGraph;

var templateScript_CustomAdditions;

var dataGraph_GET;

var dataSet_Name;

var dataSet_Password;

var authComputeBubblePath;

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
AGS.B : PRE-PROCESSING
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

progressBarReinitialize('Chargement du gabarit', 11, false);

/*
--------------------------------
AGS.B.01 : Forcing the loading options
--------------------------------
*/

progressBarIncrement();

if (featuresLoading_FORCING['eulerComputation'] === 'true') {
    featuresEulerComputation = featuresLoading_FORCING['eulerComputation'];
}

if (featuresLoading_FORCING['showInformation'] === 'true') {
    featuresInformationShowing = featuresLoading_FORCING['showInformation'];
}

if (featuresLoading_FORCING['clickToBegin'] === 'true') {
    document.getElementById('appli_graph_mask').style.visibility = 'visible';
}

/*
--------------------------------
AGS.B.02 : Displaying or not the features buttons
--------------------------------
*/

progressBarIncrement();

if ($('#toggle_physics').css('display') !== 'none') {
    if (featuresGraph['static'] === true) {
        featuresButtons['toggle_physics'] = false;
        $('#toggle_physics_BR').css('display', 'none');
        $('#toggle_physics_LI').css('display', 'none');
        $('#toggle_physics').css('display', 'none');
    }
}

/*
--------------------------------
AGS.B.03 : Editing the colors of the features buttons
--------------------------------
*/

progressBarIncrement();

if (featuresButtons.toggle_information) {
    const rgb = hexToRgb(getComputedStyle(document.documentElement).getPropertyValue('--main_color_01'));
    const color = new Color(rgb[0], rgb[1], rgb[2]);
    const solver = new Solver(color);
    const result = solver.solve();
    $('#appli_graph_filter_color').html(result.filter);
};

if (featuresGraph.lateralOnLoad !== false) {
    featuresButtons.toggle_lateralScreen = true;
};
if (featuresGraph.editorOnLoad !== false) {
    featuresButtons.toggle_modeEditor = true;
};
if (featuresGraph.mapOnLoad !== false) {
    featuresButtons.toggle_modeMap = true;
};

applyColorTransform('#info_reload > img', '', featuresColors_FILTER);
for (i in featuresButtons) {
    if (featuresButtons[i] !== false) {
        applyColorTransform('#' + i + ' > a > img', '', featuresColors_FILTER);
        applyColorTransform('#' + i + '_LI' + ' > img', '', featuresColors_FILTER);
    }
}
applyColorTransform('#appli_graph_SEARCH_BUTTON > div > img', '', featuresColors_FILTER);
applyColorTransform('#appli_graph_SEARCH_BUTTON_filter > div > img', '', featuresColors_FILTER);
applyColorTransform('#appli_graph_SEARCH_BUTTON_proximity > div > img', '', featuresColors_FILTER);

/*
--------------------------------
AGS.B.04 : Generating the default lateral screen body
--------------------------------
*/

progressBarIncrement();

var textTEMP = '';
textTEMP = textTEMP.concat("<h1>Survolez les éléments pour plus d'informations</h1>");
textTEMP = textTEMP.concat('<div class="lateral-hr"><hr class="my-6"></div>');
var lateralContent = document.getElementById('tooltipLateral_body');
lateralContent.innerHTML = textTEMP;

/*
--------------------------------
AGS.B.05 : Creating a default variable for the node colors
--------------------------------
*/

progressBarIncrement();

var mainColor_01 = getComputedStyle(document.documentElement).getPropertyValue('--main_color_01');
var mainColor_02 = getComputedStyle(document.documentElement).getPropertyValue('--main_color_02');
var mainColor_03 = getComputedStyle(document.documentElement).getPropertyValue('--main_color_03');
var mainColor_04 = getComputedStyle(document.documentElement).getPropertyValue('--main_color_04');
var mainColor_05 = getComputedStyle(document.documentElement).getPropertyValue('--main_color_05');
var mainColor_01_faded = getComputedStyle(document.documentElement).getPropertyValue('--main_color_01_faded');
var mainColor_03_faded = getComputedStyle(document.documentElement).getPropertyValue('--main_color_03_faded');
var mainColor_node = getComputedStyle(document.documentElement).getPropertyValue('--main_color_node');

/*
--------------------------------
AGS.B.06 : Responsive behaviour
--------------------------------
*/

progressBarIncrement();

function windowCustomResize() {
    let w = window.innerWidth;
    let h = window.innerHeight;
    $('html, body').css({ width: w, height: h });
    let elem = document.getElementById('appli_graph_main_screen');
    let currentOrientation = "";
    if ($(document).width() > $(document).height()) {
        currentOrientation = 'landscape';
    } else {
        currentOrientation = 'portrait';
    }
    if (currentOrientation === 'landscape') {
        if (elem?.style.height === '60%') {
            elem.style.height = '100%';
            elem.style.width = '60%';
        };
    } else {
        if (elem?.style.width === '60%') {
            elem.style.width = '100%';
            elem.style.height = '60%';
        };
    }
    if (typeof cy !== undefined) {
        cy.batch(function(){cy.resize();});
    }
    if ($('#millesime-container').length > 0 && $('#legend_buttons_accordion').length > 0) {
        if (currentOrientation === 'portrait') {
            $('#millesime-container').css({"min-width": "initial"});
            $('#legend_buttons_accordion').css({"min-width": "initial"});
        };
        $('#millesime-container').css({"min-width": $('#legend_buttons_accordion')[0].clientWidth});
        $('#legend_buttons_accordion').css({"min-width": $('#millesime-container')[0].clientWidth});
    }
    if ($('#view_ALL').length > 0 && $('#legend_buttons_accordion').length > 0) {
        $('#view_ALL').css({width: $('#legend_buttons_accordion')[0].clientWidth});
    }
    if ($('#appli_graph_SEARCH_BAR').length > 0) {$('#appli_graph_buttons').css({"padding-bottom": $('#appli_graph_SEARCH_BAR')[0].clientHeight});};
    const verticalLateralBody = $('#appli_graph_main_screen').outerHeight(true)-3*parseInt($('#appli_graph_buttons').css('padding-top'))-$('#appli_graph_SEARCH_BAR').outerHeight(true)-$('#view_ALL').outerHeight(true)-$('#appli_graph_buttons_MILLESIME').outerHeight(true)-$('#headingLegend').outerHeight(true)
    const paddingTop = parseInt($('#collapseLegend').css('padding-top'));
    const paddingBottom = parseInt($('#collapseLegend').css('padding-bottom'));
    const marginTop = parseInt($('#collapseLegend').css('margin-top'));
    const marginBottom = parseInt($('#collapseLegend').css('margin-bottom'));
    const verticalPaddingAndMargin = paddingTop + paddingBottom + marginTop + marginBottom;    
    $('#collapseLegend').css({"max-height": verticalLateralBody});
    $('#collapseLegend_DIV_CONTROL').css({"max-height": verticalLateralBody-verticalPaddingAndMargin});
}

$(window).on('resize', function () {
    setTimeout(function () {
        windowCustomResize();
    }, 50);
});

$('.modal').on('hidden.bs.modal', function () {
    setTimeout(function () {
        windowCustomResize();
    }, 50);
});

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
AGS.C : MAIN STEPS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
--------------------------------
AGS.C.01 : Init events
--------------------------------
*/

progressBarIncrement();

/* AGS.C.01.01 : Hierarchical network variables */

var hierarchicalEnabling = false;

if (window.screen.width > window.screen.height) {
    var hierarchicalAngle = 0;
} else {
    var hierarchicalAngle = -90;
}

/* AGS.C.01.02 : Activate or not the tooltips */

if (featuresGraph['tooltips'] === true) {
    $('#info_tooltip').css({ visibility: 'visible' });
    $('#info_tooltip').css({ height: 'auto' });
    $('#info_tooltip').css({ margin: '0.5em 10% 0px 0px' });
    var tooltipsGraph_BCK = true;
} else {
    $('#info_tooltip').css({ visibility: 'hidden' });
    $('#info_tooltip').css({ height: '0' });
    $('#info_tooltip').css({ margin: '0' });
    var tooltipsGraph_BCK = false;
}

if (featuresGraph['modalSelection'] === true) {
    $('#info_tooltipModal').css({ visibility: 'visible' });
    $('#info_tooltipModal').css({ height: 'auto' });
    $('#info_tooltipModal').css({ margin: '0.5em 10% 0px 0px' });
} else {
    $('#info_tooltipModal').css({ visibility: 'hidden' });
    $('#info_tooltipModal').css({ height: '0' });
    $('#info_tooltipModal').css({ margin: '0' });
}

if (featuresGraph['infoBubbles'] === true) {
    $('#info_tooltipModal').html('Cliquez sur les bulles « ? » foncées pour visualiser des informations additionnelles,');
    $('#info_tooltip').html('Survolez les bulles « ? » claires pour visualiser des informations additionnelles,');
}

/* AGS.C.01.03 : Initiate the hilighter function */

if (featuresButtons.toggle_highlighter) {
    var highlightButton = document.getElementById('toggle_highlighter');
    document.getElementById('toggle_highlighter').addEventListener('click', function () {
        if (featuresGraph.modeHighlighter === '') {
            highlightButton.setAttribute('style', 'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_01_faded') + ' !important');
            $('#toggle_highlighter > a > img').attr('src', '../../assets/icon/highlight_self.svg');
            $('#toggle_highlighter > a').attr('data-original-title', "Mise en avant de l'élément survolé");
            featuresGraph.modeHighlighter = 'self';
        } else {
            if (featuresGraph.modeHighlighter === 'self') {
                highlightButton.setAttribute('style', 'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_01_faded') + ' !important');
                $('#toggle_highlighter > a > img').attr('src', '../../assets/icon/highlight_ascendency.svg');
                $('#toggle_highlighter > a').attr('data-original-title', "Mise en avant de l'ascendance");
                featuresGraph.modeHighlighter = 'ascendency';
            } else {
                if (featuresGraph.modeHighlighter === 'ascendency') {
                    highlightButton.setAttribute(
                        'style',
                        'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_01_faded') + ' !important'
                    );
                    $('#toggle_highlighter > a > img').attr('src', '../../assets/icon/highlight_downdency.svg');
                    $('#toggle_highlighter > a').attr('data-original-title', 'Mise en avant de la descendance');
                    featuresGraph.modeHighlighter = 'downdency';
                } else {
                    if (featuresGraph.modeHighlighter === 'downdency') {
                        highlightButton.setAttribute(
                            'style',
                            'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_01_faded') + ' !important'
                        );
                        $('#toggle_highlighter > a > img').attr('src', '../../assets/icon/highlight_both.svg');
                        $('#toggle_highlighter > a').attr('data-original-title', "Mise en avant de l'ascendance et de la descendance");
                        featuresGraph.modeHighlighter = 'both';
                    } else {
                        if (featuresGraph.modeHighlighter === 'both') {
                            highlightButton.setAttribute(
                                'style',
                                'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_04') + ' !important'
                            );
                            $('#toggle_highlighter > a > img').attr('src', '../../assets/icon/highlight_self.svg');
                            $('#toggle_highlighter > a').attr('data-original-title', 'Activer la mise en avant des éléments');
                            featuresGraph.modeHighlighter = '';
                        }
                    }
                }
            }
        }
    });
};

/*
--------------------------------
AGS.C.02 : Creating views buttons
--------------------------------
*/

progressBarIncrement();

/* AGS.C.02.01 : "Reload" button */

if (featuresGraph['featureReload'] === true) {
    $('#info_reload').css({ visibility: 'visible' });
    $('#info_reload').css({ height: 'auto' });
    $('#info_reload').css({ margin: '0.5em 10% 0px 0px' });
} else {
    $('#info_reload').css({ visibility: 'hidden' });
    $('#info_reload').css({ height: '0' });
    $('#info_reload').css({ margin: '0' });
};

/* AGS.C.02.02 : Link functions to the buttons */

if (featuresGraph['featureReload'] === true) {
    document.getElementById('view_ALL').addEventListener('click', function () {
        Reinitialize();
    });
}

if (featuresGraph['featureReload'] === false) {
    $('#view_ALL').css({ display: 'none' });
} else {
    $('#view_ALL').css({ display: 'initial' });
}

if (featuresGraph['featureMillesime'] === false) {
    $('#appli_graph_buttons_MILLESIME').css({ display: 'none' });
} else {
    $('#appli_graph_buttons_MILLESIME').css({ display: 'initial' });
}

/*
--------------------------------
AGS.C.03 : Set the network
--------------------------------
*/

progressBarIncrement();

/* AGS.C.03.01 : Prepare the options variable */

var options = {
    container: document.getElementById('appli_graph_network'),
    /* headless: true, */
    /* styleEnabled: false, */
    /* wheelSensitivity: 0.1, */
    selectionType: 'additive'
};

if (featuresGraph["webgl"] === true) {
    options.renderer = {
        name: "canvas",
        webgl: true
    }
};

if (featuresGraph["lightView"] === true) {
    options.textureOnViewport = true;
    options.pixelRatio = 0.85;
    options.hideEdgesOnViewport = true;
} else {
    options.textureOnViewport = false;
    options.pixelRatio = 'auto';
    options.hideEdgesOnViewport = false;
};

/* AGS.C.03.02 : Setting the options of the network (see the cytoscape documentation for more details) */

options['style'] = [

    {
        selector: 'core',
        style: {
            'active-bg-color': mainColor_01
        }
    },

	{
		selector: '.noEvents',
		style: {
			'events': 'no'
		}
	},

    {
        selector: ':locked',
        style: {
            'overlay-color': mainColor_02,
            'overlay-padding': '30px',
            'overlay-opacity': 0.25
        },
    },

    {
        selector: ':locked.staticLock',
        style: {
            'overlay-opacity': 0
        },
    },

    {
        selector: '.defaultNode',
        style: {

            'text-events': 'yes',

            'z-index': 5,
            'z-index-compare': 'manual',

            'opacity': 1,

            'width': '30px',
            'height': '30px',

			'shape': 'round-rectangle',
            'padding': '10px',

            'color': mainColor_node,
            'font-family': 'Arial',
            'font-size': '20px',
            'text-valign': 'center',
            'text-wrap': 'wrap',
            'text-max-width': '150px',

            'min-zoomed-font-size': 15,

            'background-color': mainColor_01,
            'text-background-color': mainColor_01,
            'border-width': '.5rem',
            'text-border-width': '.5rem',
            'border-color': mainColor_node,
            'text-border-color': mainColor_node
        },
    },

	{
		selector: "node[image]",
		style: {
			"width": "50px",
			"height": "50px",
			"border-width": "5px",
			"text-valign": "bottom",
			"text-margin-y": "3.25rem",
			"background-image": "data(image)",
			"background-fit": "contain",
			"background-clip": "node"
		}
	},

	{
		selector: 'node[group = "entity"],node[group = "subGroup"]',
		style: {
			'min-zoomed-font-size': 15
		}
	},

    {
        selector: '.defaultNode.hoveredNode,.defaultNode:selected,.defaultNode.proximityFocused,.defaultNode.searchedZoomed',
        style: {
            'min-zoomed-font-size': 0
        },
    },

    {
        selector: 'node:selected',
        style: {
            'background-color': mainColor_02,
            'text-background-color': mainColor_02
        },
    },

    {
        selector: 'node.insensitiveNode',
        style: {
            'events': 'no',
            'text-events': 'no'
        },
    },

    {
        selector: '.infoBubble',
        style: {
            'label': 'data(label)',
            'font-family': 'Arial',
            'text-valign': 'center',
            'width': '20px',
            'height': '20px',
            'padding': '5px',
            'border-width': '0rem'
        },
    },

    {
        selector: 'node[group = ' + '"' + 'ibSimple' + '"' + ']',
        style: {
            'color': mainColor_node,
            'background-color': 'white',
            'text-background-color': 'white',
            'background-opacity': 0.1
        },
    },

    {
        selector: 'node[group = ' + '"' + 'ibComplete' + '"' + ']',
        style: {
            'color': 'white',
            'background-color': mainColor_node,
            'text-background-color': mainColor_node,
            'background-opacity': 1
        },
    },

    {
        selector: '.infoTag',
        style: {
			'z-index': 7,
			'z-index-compare': 'manual',
			'z-compound-depth': 'top',
            'shape': 'bottom-round-rectangle',
            'label': 'data(label)',
            'font-size': '8px',
            'font-family': 'Arial',
            'font-weight': 'bold',
            'text-justification': 'left',
            'text-transform': 'uppercase',
            'text-valign': 'center',
            'height': "9px",
            'width': function (ele) {
                const letterCount = (ele.data('label').match(/[a-zA-Z]/g) || []).length
                return (5*letterCount)+(2.5*ele.data('label').length-letterCount);
            },
            'color': 'white',
            'background-color': mainColor_01,
            'text-background-color': mainColor_01,
            'padding': '5px',
            'border-width': '0rem',
			'text-wrap': 'wrap',
			'text-max-width': '200px'
        },
    },

    {
        selector: '.infoTag_hidden',
        style: {
            'visibility': 'hidden'
        },
    },

    {
        selector: 'edge',
        style: {
			'z-index': 2,
			'z-index-compare': 'manual',
            'opacity': featuresGraph.edgeOpacity,
            'line-color': mainColor_node,
            'color': mainColor_node,
            'font-family': 'Arial',
            'text-rotation': 'autorotate',
			'curve-style': 'bezier',
			'control-point-step-size': 100,
			'source-arrow-shape': 'none',
			'target-arrow-shape': 'triangle',
			'arrow-scale': 1.5,
            'text-outline-width': '2px',
            'text-outline-color': mainColor_03,
            'source-arrow-color': mainColor_node,
            'target-arrow-color': mainColor_node,
			'source-endpoint': 'outside-to-node-or-label',
			'target-endpoint': 'outside-to-node-or-label',
			'source-distance-from-node': '10px',
			'target-distance-from-node': '10px',
            'width': '3rem'
        },
    },

    {
        selector: 'edge:selected',
        style: {
            'opacity': 1,
            'line-color': mainColor_02,
            'source-arrow-color': mainColor_02,
            'target-arrow-color': mainColor_02,
			'z-index': 3,
			'z-index-compare': 'manual',
			'text-outline-width': '4px',
			'underlay-color': mainColor_01,
			'underlay-padding': '30px',
			'underlay-opacity': 0.1,
            'width': '4rem'
        },
    },

	{
		selector: 'edge[precision]',
		style: {
			'label': 'data(precision)',
			'font-size': '15px',
			'text-wrap': 'ellipsis',
			'text-max-width': 150
		}
	},

	{
		selector: 'edge[precision]:selected',
		style: {
			'text-rotation': 'none',
			'text-wrap': 'wrap',
			'font-size': '20px'
		}
	},

	{
		selector: 'edge[precision].hoveredEdge',
		style: {
			'z-index': 10,
			'z-index-compare': 'manual',
			'text-wrap': 'wrap',
			'text-rotation': 'none'
		}
	},


    {
        selector: 'edge.indirectEdge',
        style: {
            'opacity': 0.5,
            'line-style': 'dashed'
        },
    },

    {
        selector: 'edge.indirectEdge_hidden',
        style: {
            'visibility': 'hidden'
        },
    },

    {
        selector: 'edge.insensitiveEdge',
        style: {
            'events': 'no',
            'text-events': 'no'
        },
    },

	{
		selector: 'node[group *= ' + '"' + 'type' + '"' + ']',
		style: {
			'z-index': 4,
			'z-index-compare': 'manual',
			'z-compound-depth': 'bottom',
			'shape': 'roundrectangle',
			'events': 'no',
			'background-opacity': 0,
			'background-color': mainColor_02,
            'text-background-color': mainColor_02,
			'font-size': '40px',
			'padding': '110px',
			'text-wrap': 'wrap',
			'text-max-width': function(ele){return ele.width();},
			'text-margin-x': function(ele){return 40 - ele.outerWidth();},
			'text-margin-y': '60px',
			'text-halign': 'right',
			'text-valign': 'top'
		}
	},

	{
		selector: 'node[image][group *= ' + '"' + 'type' + '"' + ']',
		style: {
			'background-image': 'data(image)',
			'background-image-opacity': 0,
			'background-height': function(ele){return Math.min(0.75*ele.outerHeight(),0.75*ele.outerWidth());},
			'background-width': function(ele){return Math.min(0.75*ele.outerHeight(),0.75*ele.outerWidth());},
		}
	},

	{
		selector: 'node[group *= ' + '"' + 'clusterGroup' + '"' + ']',
		style: {
			'z-index': 6,
			'z-index-compare': 'manual',
			'z-compound-depth': 'top',
			'color': mainColor_01,
			'shape': 'roundrectangle',
			'events': 'yes',
			'background-opacity': 0.9,
			'background-color': mainColor_03,
            'text-background-color': mainColor_03,
			'font-weight': 'bold',
			'font-size': '50px',
			'padding': '60px'
		}
	},

	{
		selector: 'node[lightColor][group *= ' + '"' + 'clusterGroup' + '"' + ']',
		style: {
			'background-color': 'data(lightColor)',
            'text-background-color': 'data(lightColor)'
		}
	},

	{
		selector: 'node[image][group *= ' + '"' + 'clusterGroup' + '"' + ']',
		style: {
			'background-image': 'data(image)',
			'background-height': function(ele){return Math.min(0.75*ele.outerHeight(),0.75*ele.outerWidth());},
			'background-width': function(ele){return Math.min(0.75*ele.outerHeight(),0.75*ele.outerWidth());},
			'background-image-opacity': 0.25
		}
	},

	{
		selector: 'node[group *= ' + '"' + 'clusterGroup' + '"' + ']:parent',
		style: {
			'z-index': 5,
			'z-index-compare': 'manual',
			'z-compound-depth': 'auto',
			'events': 'no',
			'font-size': '25px',
			'text-wrap': 'wrap',
			'text-max-width': function(ele){return ele.width();},
			'text-margin-y': function(ele){
				let var_y = ((ele.data('label').length*15)/ele.width());
				if (var_y > 1.25) {return 20+40*var_y;} else {return 40;};
			},
			'min-height': function(ele){
				let var_y = ((ele.data('label').length*15)/ele.width());
				let var_h = ele.children(':visible').boundingBox()['h'];
				if (var_y > 1.5) {return var_h+20+40*var_y;} else {return var_h;};
			},
            'min-width': "300px",
			'text-margin-x': 0,
			'text-halign': 'center',
			'text-valign': 'top'
		}
	},

	{
		selector: 'node[group *= ' + '"' + 'clusterGroup' + '"' + ']:childless',
		style: {
            'width': "300px",
			'height': function(ele){return 125*Math.ceil(ele.data('label').length/20);}
		}
	},

	{
		selector: 'node[group *= ' + '"' + 'entity' + '"' + ']',
		style: {
			'z-index': 5,
			'z-index-compare': 'manual',
			'z-compound-depth': 'top'
		}
	},

	{
		selector: 'node.farViewed.infoTag',
		style: {
			'events': 'no',
			'opacity': 0.5
		}
	},

	{
		selector: 'node.farViewed[group *= ' + '"' + 'entity' + '"' + ']',
		style: {
			'events': 'no',
			'opacity': 0.05
		}
	},

	{
		selector: 'node.farViewed[group *= ' + '"' + 'clusterGroup' + '"' + ']',
		style: {
			'events': 'yes',
			'text-halign': 'center',
			'text-valign': 'center',
			'text-margin-x': 0,
			'text-margin-y': 0,
			'text-wrap': 'wrap'
		}
	},

	{
		selector: 'node.farViewed[darkColor][group *= ' + '"' + 'clusterGroup' + '"' + ']',
		style: {
			'background-color': 'data(darkColor)',
            'text-background-color': 'data(darkColor)'
		}
	},

	{
		selector: 'node.farViewed[image][group *= ' + '"' + 'clusterGroup' + '"' + ']',
		style: {
			'background-image-opacity': 0
		}
	},

	{
		selector: 'node.farViewed[group *= ' + '"' + 'clusterGroup' + '"' + ']:parent',
		style: {
			'events': 'yes',
			'font-size': '50px'
		}
	},

	{
		selector: 'node.farViewed[group *= ' + '"' + 'type' + '"' + ']',
		style: {
			'events': 'yes',
			'background-opacity': 1,
			'text-margin-y': function(ele){
				let var_y = ((ele.data('label').length*60)/ele.width());
				if (var_y > 1.5) {return 55+110*var_y;} else {return 110;};
			},
			'min-height': function(ele){
				let var_y = ((ele.data('label').length*60)/ele.width());
				let var_h = ele.children(':visible').boundingBox()['h'];
				if (var_y > 1.5) {return var_h+55+110*var_y;} else {return var_h;};
			},
			'min-height-bias-top': '100%',
			'text-wrap': 'wrap',
			'font-size': '100px'
		}
	},

	{
		selector: 'node.farViewed[label][group *= ' + '"' + 'type' + '"' + ']',
		style: {
			'label': 'data(label)'
		}
	},

	{
		selector: 'node.farViewed[image][group *= ' + '"' + 'type' + '"' + ']',
		style: {
			'background-image-opacity': 0.25
		}
	},

	{
		selector: 'node.farViewed[darkColor][group *= ' + '"' + 'type' + '"' + ']',
		style: {
			'color': 'data(darkColor)'
		}
	},

	{
		selector: 'node.farViewed[lightColor][group *= ' + '"' + 'type' + '"' + ']',
		style: {
			'background-color': 'data(lightColor)',
            'text-background-color': 'data(lightColor)'
		}
	},

	{
		selector: 'edge.farViewed',
		style: {
			'label': ''
		}
	},

	{
		selector: 'node.cy-expand-collapse-collapsed-node[group *= ' + '"' + 'type' + '"' + ']',
		style: {
			'font-size': '100px',
			'font-weight': 'bold',
			'background-opacity': 1,
			'width': function (ele) {return (600+20*api.getCollapsedChildren(ele).length)+'px';},
			'height': function (ele) {return (600+20*api.getCollapsedChildren(ele).length)+'px';},
			'text-margin-x': 0,
			'text-margin-y': 0,
			'text-max-width': '700px',
			'text-halign': 'center',
			'text-valign': 'center'
		}
	},

	{
		selector: 'node.cy-expand-collapse-collapsed-node[image][group *= ' + '"' + 'type' + '"' + ']',
		style: {
			'background-image-opacity': 1,
			'background-height': function(ele){return Math.min(0.75*ele.outerHeight(),0.75*ele.outerWidth());},
			'background-width': function(ele){return Math.min(0.75*ele.outerHeight(),0.75*ele.outerWidth());},
			'background-image-opacity': 0.1
		}
	},

	{
		selector: 'node.cy-expand-collapse-collapsed-node[group *= ' + '"' + 'clusterGroup' + '"' + ']',
		style: {
			'padding': function (ele) {return (60+5*api.getCollapsedChildren(ele).length)+'px';}
		}
	},

	{
		selector: 'node.cy-expand-collapse-collapsed-node[label][group *= ' + '"' + 'clusterGroup' + '"' + ']',
		style: {
			'label': function (ele) {return ele.data('label')+' (x'+api.getCollapsedChildren(ele).length+')';},
			'text-max-width': function(ele){return (ele.data('label').length*15)+(api.getCollapsedChildren(ele).length);},
		}
	},

	{
		selector: 'edge.cy-expand-collapse-collapsed-edge',
		style: {
			'label': '',
			'text-wrap': 'wrap',
			'width': (e) => {
				return (3.5+(e.data('collapsedEdges').length*1.5)) + 'rem';
			},
			'text-outline-width': '5px',
			'font-size': '25px'
		}
	},

	{
		selector: 'edge.cy-expand-collapse-collapsed-edge[directionType = "unidirection"]',
		style: {
			'source-arrow-shape': 'none',
			'target-arrow-shape': 'triangle'
		}
	},

	{
		selector: 'edge.cy-expand-collapse-collapsed-edge[directionType = "bidirection"]',
		style: {
			'source-arrow-shape': 'triangle',
			'target-arrow-shape': 'triangle'
		}
	},

	{
		selector: 'node[group = "subGroup"]',
		style: {
			'text-wrap': 'wrap',
			'text-max-width': '150px',
			'padding': '10px',
			'font-size': '20px',
			'font-weight': 'normal',
			'color': mainColor_03,
			'border-style': 'dashed',
			'border-width': '3.5px',
            'border-color': mainColor_03,
			'background-color': mainColor_01,
            'text-background-color': mainColor_01,
            'text-border-style': 'dashed',
			'text-border-width': '3.5px',
            'text-border-color': mainColor_03,        
			'background-opacity': 1
		}
	},

	{
		selector: 'node[group = "subGroup"]',
		style: {
			'background-color': mainColor_02,
         'text-background-color': mainColor_02,
		}
	},

	{
		selector: 'node[group = "subGroup"]:parent',
		style: {
			'z-index': 5,
			'z-index-compare': 'manual',
			'z-compound-depth': 'auto',
			'events': 'yes',
			'font-size': '25px',
			'font-weight': 'bold',
			'text-wrap': 'wrap',
			'text-max-width': function(ele){return ele.width();},
			'text-margin-y': function(ele){
				let var_y = ((ele.data('label').length*15)/ele.width());
				if (var_y > 1.25) {return 20+40*var_y;} else {return 40;};
			},
			'min-height': function(ele){
				let var_y = ((ele.data('label').length*15)/ele.width());
				let var_h = ele.children(':visible').boundingBox()['h'];
				if (var_y > 1.5) {return var_h+20+40*var_y;} else {return var_h;};
			},
			'text-margin-x': 0,
			'text-halign': 'center',
			'text-valign': 'top',
			'background-opacity': 0.5,
			'padding': '60px'
		}
	},

	{
		selector: 'node[group = "subGroup"].farViewed',
		style: {
			'events': 'no',
			'opacity': 0.05
		}
	},

	{
		selector: '.defaultNode.mapNode',
		style: {
			'z-index': 999,
			'z-index-compare': 'manual',
			'label': '',
			'color': mainColor_01,
			'events': 'yes',
            'text-events': 'no',
			'shape': 'ellipse',
			'width': '6px',
			'height': '6px',
			'font-size': '20px',
			'text-margin-y': '0px'
		}
	},

	{
		selector: '.defaultNode.mapNode[mapColorNode]',
		style: {
			'color': mainColor_03,
			'background-color': 'data(mapColorNode)',
            'text-background-color': 'data(mapColorNode)',
		}
	},

	{
		selector: '.defaultNode.mapNode[mapImageNode]',
		style: {
			'background-image': 'data(mapImageNode)',
			'background-height': '75%',
			'background-width': '75%',
			'background-image-opacity': 1
		}
	},

	{
		selector: '.defaultNode.mapNode.hoveredMapNode',
		style: {
			'width': '20px',
			'height': '20px'
		}
	},

	{
		selector: '.mapCompound',
		style: {
            'label': '',
            'opacity': 0,
            'background-opacity': 0,
            'text-events': 'no',
            'events': 'no'
		}
	},

	{
		selector: 'node.unchecked,node.unchecked_type,node.unchecked_searched,node.unchecked_proximity,node.unchecked_millesime,node.unchecked_map',
		style: {
            'display': 'none',
			'events': 'no',
			'label': '',
            'text-opacity': 0,
            'background-opacity': 0,
			'border-width': 0
		}
	},

	{
		selector: 'node.unchecked[image],node.unchecked_type[image],node.unchecked_searched[image],node.unchecked_proximity[image],node.unchecked_millesime[image],node.unchecked_map[image]',
		style: {
            'display': 'none',
			'background-image-opacity': 0,
            'border-width': 0,
            'border-opacity': 0
		}
	},

	{
		selector: 'node.unchecked[mapImageNode],node.unchecked_type[mapImageNode],node.unchecked_searched[mapImageNode],node.unchecked_proximity[mapImageNode],node.unchecked_millesime[mapImageNode],node.unchecked_map[mapImageNode]',
		style: {
            'display': 'none',
			'background-image-opacity': 0
		}
	},

	{
		selector: 'edge.unchecked,edge.unchecked_type,edge.unchecked_searched,edge.unchecked_proximity,edge.unchecked_millesime,edge.unchecked_map,edge.unchecked_edge',
		style: {
            'display': 'none',
			'events': 'no',
			'visibility': 'hidden'
		}
	},

	{
		selector: '.defaultNode.mapNode.unchecked[mapImageNode],.defaultNode.mapNode.unchecked_type[mapImageNode],.defaultNode.mapNode.unchecked_searched[mapImageNode],.defaultNode.mapNode.unchecked_proximity[mapImageNode],.defaultNode.mapNode.unchecked_millesime[mapImageNode],.defaultNode.mapNode.unchecked_map[mapImageNode]',
		style: {
            'display': 'none',
			'events': 'no',
			'background-image-opacity': 0
		}
	},

	{
		selector: 'node:parent',
		style: {
            'display': 'element'
		}
	},

	{
		selector: 'node.hoveredGroup',
		style: {
			'font-weight': 'normal'
		}
	},

	{
		selector: '.hoveredNode,:selected,.proximityFocused,.searchedZoomed',
		style: {
			'z-index': 15,
            'z-index-compare': 'manual',
		}
	},

    {
		selector: '.hoveredEdge',
		style: {
			'z-index': 14,
            'z-index-compare': 'manual',
            'opacity': 1
		}
	}
    
];

if (featuresGraph.lightView) {
    options['style'] = options['style'].concat({
        selector: 'core',
        style: {
            'outside-texture-bg-color': mainColor_01,
            'outside-texture-bg-opacity': 0.25
        }
    });
};

if (featuresGraph.showLabels) {
    if (featuresGraph_LegendOptions.showLabelsControl) {
        options['style'] = options['style'].concat({
            selector: '.defaultNode[label].hoveredNode,.defaultNode[label]:selected,.defaultNode[label].proximityFocused,.defaultNode[label].searchedZoomed',
            style: {
                'label': 'data(label)',
                'text-border-opacity': 1,
                'text-background-opacity': 1,
                'text-background-padding': '10px',
                'text-background-shape': 'round-rectangle'
            },
        });
        options['style'] = options['style'].concat({
            selector: '.defaultNode.showLabelsOn',
            style: {
                'min-zoomed-font-size': 0,
                'label': 'data(label)',
                'text-border-opacity': 1,
                'text-background-opacity': 1,
                'text-background-padding': '10px',
                'text-background-shape': 'round-rectangle'
            },
        });
        options['style'] = options['style'].concat({
            selector: '.defaultNode.mapNode',
            style: {'label': ''}
        });
    } else {
        options['style'] = options['style'].concat({
            selector: '.defaultNode[label]',
            style: {
                'label': 'data(label)',
                'text-border-opacity': 1,
                'text-background-opacity': 1,
                'text-background-padding': '10px',
                'text-background-shape': 'round-rectangle'
            },
        });
        options['style'] = options['style'].concat({
            selector: '.defaultNode.mapNode',
            style: {'label': ''}
        });
    };
} else {
    options['style'] = options['style'].concat({
        selector: '.defaultNode[label].hoveredNode,.defaultNode[label]:selected,.defaultNode[label].proximityFocused,.defaultNode[label].searchedZoomed',
        style: {
            'label': 'data(label)',
            'text-border-opacity': 1,
            'text-background-opacity': 1,
            'text-background-padding': '10px',
            'text-background-shape': 'round-rectangle'
        },
    });
    options['style'] = options['style'].concat({
		selector: '.defaultNode.mapNode',
		style: {'label': ''}
	});
};

options['style'] = options['style'].concat({
    selector: 'node[group *= ' + '"' + 'entity' + '"' + '].farViewed,node[group *= ' + '"' + 'subGroup' + '"' + '].farViewed,node[group *= ' + '"' + 'clusterGroup' + '"' + '],node[group *= ' + '"' + 'type' + '"' + ']',
    style: {
        'text-border-opacity': 0,
        'text-background-opacity': 0
    },
});

options['style'] = options['style'].concat({
    selector: '.defaultNode.mapNode[label]',
    style: {
        'label': ''
    }
});

options['style'] = options['style'].concat({
    selector: '.defaultNode.mapNode.hoveredMapNode[label],.defaultNode.mapNode.searchedZoomed[label]',
    style: {
        'z-index': 1000,
        'z-index-compare': 'manual',
        'label': 'data(label)'
    }
});

/* AGS.C.03.03 : Create the network */

cy = cytoscape(options);

cy.minZoom(featuresGraph.minZoom);

cy.maxZoom(featuresGraph.maxZoom);

cy.on("render", function(event) {
	console.log('render');
});

cy.startBatch();console.log('start batch');

/* AGS.C.03.04 : Changing the options style if specified in the config file */

if (options_Modifier.length > 0) {
    options['style'] = options['style'].concat(options_Modifier);
    cy.style(options['style']);
};

/* AGS.C.03.05 : Initial layout */

var mainLayout = cy.elements().layout({ name: 'null' });

/* AGS.C.03.06 : Deal with state of buttons */

if (featuresButtons.toggle_physics) {

    var physicsButton = document.getElementById('toggle_physics');

    if ($('#toggle_physics').css('display') !== 'none') {
        if (featuresGraph['physics'] === true) {
            physicsButton.setAttribute('style', 'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_01_faded') + ' !important');
            $('#toggle_physics > a').attr('data-original-title', 'Physique activée');
        } else {
            physicsButton.setAttribute('style', 'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_04') + ' !important');
            $('#toggle_physics > a').attr('data-original-title', 'Physique désactivée');
        }
    };

    physicsButton.addEventListener('click', function () {
        if (featuresGraph['physics'] === false) {
            featuresGraph['physics'] = true;
            physicsButton.setAttribute('style', 'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_01_faded') + ' !important');
            $('#toggle_physics > a').attr('data-original-title', 'Physique activée');
        } else {
            featuresGraph['physics'] = false;
            physicsButton.setAttribute('style', 'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_04') + ' !important');
            $('#toggle_physics > a').attr('data-original-title', 'Physique désactivée');
        }
        InitialLayout('euler_cola', false);
    });

};

if (featuresButtons.toggle_ind_link) {

    var indirectLinksButton = document.getElementById('toggle_ind_link');

    if ($('#toggle_ind_link').css('display') !== 'none') {
        indirectLinksButton.setAttribute('style', 'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_04') + ' !important');
        $('#toggle_ind_link > a').attr('data-original-title', 'Afficher les liens indirects');
    };

    indirectLinksButton.addEventListener('click', function () {
        if ($('#toggle_ind_link').css('background-color').indexOf('0.5') === -1) {
            cy.$('.indirectEdge').toggleClass('indirectEdge_hidden', false);
            indirectLinksButton.setAttribute('style', 'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_01_faded') + ' !important');
            $('#toggle_ind_link > a').attr('data-original-title', 'Masquer les liens indirects');
        } else {
            cy.$('.indirectEdge').toggleClass('indirectEdge_hidden', true);
            indirectLinksButton.setAttribute('style', 'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_04') + ' !important');
            $('#toggle_ind_link > a').attr('data-original-title', 'Afficher les liens indirects');
        }
    });

};

if (featuresButtons.toggle_highlighter) {

    var highlightButton = document.getElementById('toggle_highlighter');

    if ($('#toggle_highlighter').css('display') !== 'none') {
        if (featuresGraph.modeHighlighter === 'self') {
            highlightButton.setAttribute('style', 'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_01_faded') + ' !important');
            $('#toggle_highlighter > a > img').attr('src', '../../assets/icon/highlight_self.svg');
            $('#toggle_highlighter > a').attr('data-original-title', "Mise en avant de l'élément survolé");
        } else {
            if (featuresGraph.modeHighlighter === 'ascendency') {
                highlightButton.setAttribute('style', 'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_01_faded') + ' !important');
                $('#toggle_highlighter > a > img').attr('src', '../../assets/icon/highlight_ascendency.svg');
                $('#toggle_highlighter > a').attr('data-original-title', "Mise en avant de l'ascendance");
            } else {
                if (featuresGraph.modeHighlighter === 'downdency') {
                    highlightButton.setAttribute(
                        'style',
                        'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_01_faded') + ' !important'
                    );
                    $('#toggle_highlighter > a > img').attr('src', '../../assets/icon/highlight_downdency.svg');
                    $('#toggle_highlighter > a').attr('data-original-title', 'Mise en avant de la descendance');
                } else {
                    if (featuresGraph.modeHighlighter === 'both') {
                        highlightButton.setAttribute(
                            'style',
                            'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_01_faded') + ' !important'
                        );
                        $('#toggle_highlighter > a > img').attr('src', '../../assets/icon/highlight_both.svg');
                        $('#toggle_highlighter > a').attr('data-original-title', "Mise en avant de l'ascendance et de la descendance");
                    } else {
                        if (featuresGraph.modeHighlighter === '') {
                            highlightButton.setAttribute(
                                'style',
                                'display: initial; background-color: ' + getComputedStyle(document.documentElement).getPropertyValue('--main_color_04') + ' !important'
                            );
                            $('#toggle_highlighter > a > img').attr('src', '../../assets/icon/highlight_self.svg');
                            $('#toggle_highlighter > a').attr('data-original-title', 'Activer la mise en avant des éléments');
                        }
                    }
                }
            }
        }
    };

};

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
AGS.D : LAUNCHING PROMISES
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/*
--------------------------------
AGS.D.01 : Dataset selection promise
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function () {
    return new Promise((resolve_ScriptInitialisation) => {

        if (getParamValue('dataset') === undefined && (datasetSelection)) {
            $('#datasetModal').modal('show');
        }

        const datasetAuth = document.getElementById('form_dataset_file_options');

        const onWindowKeyDown_dataset = (event) => {
            if (event.keyCode === 13) {
                event.preventDefault();
                document.getElementById('dataset_button_submit').click();
            }
        }
        window.addEventListener('keydown', onWindowKeyDown_dataset);

        document.getElementById('dataset_button_submit').addEventListener('click', function () {
            $('#datasetModal').modal('hide');
            dataGraph_GET = {'id': datasetAuth.value, 'passwd': ''};
            window.removeEventListener('keydown', onWindowKeyDown_dataset);
            resolve_ScriptInitialisation("Done");
        });

        if (getParamValue('dataset') !== undefined) {
            dataGraph_GET = {'id': getParamValue('dataset'), 'passwd': ''};
            window.removeEventListener('keydown', onWindowKeyDown_dataset);
            resolve_ScriptInitialisation("Done");
        } else {
            if (!datasetSelection) {
                if (datasetIdDefault === undefined) {
                    const bugReport = document.getElementById('bug_report_appli');
                    bugReport.innerHTML = '<br><h3><b>/!\\ Aucun dataset paramétré /!\\</b></h3><br><p style=\"justify-content:center;display:flex;\"><a href=\"/update\"><button class=\"btn btn-primary">Générer un dataset</button></a></p><br>';
                    $('#bugModal').modal('show');
                } else {
                    dataGraph_GET = {'id': datasetIdDefault.id, 'passwd': ''};
                    window.removeEventListener('keydown', onWindowKeyDown_dataset);
                    resolve_ScriptInitialisation("Done");
                };
            };
        };

    });
});

/*
--------------------------------
AGS.D.01 : Authentification promise
--------------------------------
*/


chain_ScriptInitialisation = chain_ScriptInitialisation.then(function () {
    return new Promise((resolve_ScriptInitialisation) => {

        const clueDatasetName = document.getElementById('appli_graph_version_DatasetName');
        clueDatasetName.innerHTML = dataGraph_GET.id;
        dataSet_Name = dataGraph_GET.id

        let oReq = new XMLHttpRequest();
        oReq.onload = function () {
            if (this.response === 'no_password') {

                if (getParamValue('auth') === undefined) {
                    $('#authModal').on("shown.bs.modal", function() {
                        document.getElementById("form_auth_file_input").focus();
                    });
                    $('#authModal').modal('show');
                }

                const authInformation = document.getElementById('auth_information');

                const authSelector = document.getElementById('form_auth_file_input');

                authSelector.addEventListener('keydown', function (event) {
                    if (event.keyCode === 13) {
                        event.preventDefault();
                        document.getElementById('auth_button_submit').click();
                    }
                });

                document.getElementById('auth_button_submit').addEventListener('click', function () {
                    if (authSelector.value === '') {
                        authInformation.innerHTML = 'Attention aucun mot de passe saisi !';
                    } else {
                        let oReq = new XMLHttpRequest();
                        oReq.onload = function () {
                            if (this.response === 'wrong_password' || this.response === 'no_password') {
                                $('#authModal').modal('show');
                                authInformation.innerHTML = 'Mot de passe incorrect !';
                            } else {
                                $('#authModal').modal('hide');
                                dataGraph_GET.passwd = this.response;
                                resolve_ScriptInitialisation("Done");
                            }
                        };
                        oReq.open('post', '/auth', true);
                        oReq.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                        oReq.send(`template=${encodeURIComponent(idGraph)}&dataset=${encodeURIComponent(dataGraph_GET.id)}&password=${encodeURIComponent(authSelector.value)}`);
                    }
                });

                if (getParamValue('auth') !== undefined) {
                    authSelector.value = getParamValue('auth');
                    document.getElementById('auth_button_submit').click();
                };

            };
            if (this.response === 'error_system') {
                const bugReport = document.getElementById('bug_report_appli');
                bugReport.innerHTML = '<br><h3><b>/!\\ Système de mots de passe non atteint /!\\</b></h3><br><p><b>Contactez l\'administration de l\'application.</b></p><br>';
                $('#bugModal').modal('show');
            };
            if (this.response !== 'no_password' && this.response !== 'error') {
                dataGraph_GET.passwd = this.response;
                resolve_ScriptInitialisation("Done");
            };
        };
        oReq.open('post', '/auth', true);
        oReq.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        oReq.send(`template=${encodeURIComponent(idGraph)}&dataset=${encodeURIComponent(dataGraph_GET.id)}&password=${encodeURIComponent(dataGraph_GET.passwd)}`);

    });
});

/*
--------------------------------
AGS.D.01 : Data promise
--------------------------------
*/

/* DATA CACHE */
chain_ScriptInitialisation = chain_ScriptInitialisation.then(function () {
    return new Promise((resolve_ScriptInitialisation) => {
        progressBarReinitialize('Chargement des données', 4, false);
        progressBarIncrement();

        let oReq = new XMLHttpRequest();
        oReq.onload = function () {
            if (this.response === 'error') {
                const bugReport = document.getElementById('bug_report_appli');
                bugReport.innerHTML = '<br><h3><b>/!\\ Problème de chargement des données ! /!\\</b></h3><br>';
                $('#bugModal').modal('show');
            } else {
                let cacheDataToRead = this.response;
                resolve_ScriptInitialisation(importNetwork_ForDataCaching(cacheDataToRead, cy));
            }
        };
        dataSet_Password = dataGraph_GET.passwd;
        oReq.open('post', '/api', true);
        oReq.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        oReq.send(`template=${encodeURIComponent(idGraph)}&dataset=${encodeURIComponent(dataGraph_GET.id)}&password=${encodeURIComponent(dataGraph_GET.passwd)}`);

    });
});

/* DATE */
chain_ScriptInitialisation = chain_ScriptInitialisation.then(function () {
    return new Promise((resolve_ScriptInitialisation) => {

        let oReq = new XMLHttpRequest();
        oReq.onload = function () {
            if (this.response === 'error') {
                const bugReport = document.getElementById('bug_report_appli');
                bugReport.innerHTML = '<br><h3><b>/!\\ Problème de chargement de la date des données ! /!\\</b></h3><br>';
                $('#bugModal').modal('show');
            } else {
                const dateOfData = this.response;
                const clueDatasetDate = document.getElementById('appli_graph_version_DatasetDate');
                clueDatasetDate.innerHTML = dateOfData;        
                resolve_ScriptInitialisation("Done");
            }
        };
        oReq.open('post', '/api', true);
        oReq.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        oReq.send(`template=${encodeURIComponent(idGraph)}&dataset=${encodeURIComponent(dataGraph_GET.id)}&password=${encodeURIComponent(dataGraph_GET.passwd)}&date=true`);

    });
});

/* TRADUCTOR */
chain_ScriptInitialisation = chain_ScriptInitialisation.then(function () {
    return new Promise((resolve_ScriptInitialisation) => {

        let oReq = new XMLHttpRequest();
        oReq.onload = function () {

            if (this.response === 'error') {
                resolve_ScriptInitialisation("Done");
            } else {

                const traductorOfData = JSON.parse(this.response,'utf-8');

                const translationMap = {};
                traductorOfData.forEach(item => {
                    translationMap[item.code] = item.value;
                });

                function translateValue(value) {
                    if (translationMap[value]) {
                        return translationMap[value];
                    } else {
                        let valueTMP = value;
                        for (const key in translationMap) {
                            if (typeof value === 'string') {
                                if (value.includes(key)) {
                                    valueTMP = valueTMP.replace(new RegExp(key, 'g'), translationMap[key]);
                                };
                            };
                        };
                        return valueTMP;
                    };
                };

                function recursiveTranslate(data) {
                    if (Array.isArray(data)) {
                        return data.map(item => recursiveTranslate(item));
                    } else if (typeof data === 'object' && data !== null) {
                        let translatedData = {};
                        Object.keys(data).forEach(key => {
                            translatedData[key] = recursiveTranslate(data[key]);
                        });
                        return translatedData;
                    } else {
                        return translateValue(data);
                    }
                }

                cy.batch(function(){
                    cy.nodes().forEach(node => {
                        let data = node.data();
                        let translatedData = recursiveTranslate(data);
                        node.data(translatedData);
                    });
                    cy.edges().forEach(edge => {
                        let data = edge.data();
                        let translatedData = recursiveTranslate(data);
                        edge.data(translatedData);
                    });
                });

                resolve_ScriptInitialisation("Done");

            };
        };
        oReq.open('post', '/api', true);
        oReq.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        oReq.send(`template=${encodeURIComponent(idGraph)}&dataset=${encodeURIComponent(dataGraph_GET.id)}&password=${encodeURIComponent(dataGraph_GET.passwd)}&traductor=true`);

    });
});

/*
--------------------------------
AGS.D.02 : Dealing with specified positions
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function () {
    return new Promise((resolve_ScriptInitialisation) => {
        progressBarReinitialize('Post-traitements : première phase', 9, false);
        progressBarIncrement();

        /* AGS.D.02.01 : Set the custom positions of the compounds */

        cy.batch(function(){cy.nodes().forEach(function (elem) {elem.position(elem.data("ori_pos"))});});

        if (cy.$('.compoundCorner').length === 0) {
            cy.$('node:compound,node[group = "type"]').forEach(function (elem) {
                if (elem.position().x !== undefined && elem.position().y !== undefined && elem.position().width !== undefined && elem.position().height !== undefined) {
                    cy.add({
                        group: 'nodes',
                        data: {
                            id: elem.id() + '_corner_up_left',
                            position: {
                                x: elem.position().x - elem.position().width / 2,
                                y: elem.position().y - elem.position().height / 2,
                            },
                            parent: elem.id(),
                        },
                        position: {
                            x: elem.position().x - elem.position().width / 2,
                            y: elem.position().y - elem.position().height / 2,
                        },
                    });
                    cy.getElementById(elem.id() + '_corner_up_left').style({ visibility: 'hidden' });
                    cy.getElementById(elem.id() + '_corner_up_left').addClass('compoundCorner');
                    cy.add({
                        group: 'nodes',
                        data: {
                            id: elem.id() + '_corner_bottom_right',
                            position: {
                                x: elem.position().x + elem.position().width / 2,
                                y: elem.position().y + elem.position().height / 2,
                            },
                            parent: elem.id(),
                        },
                        position: {
                            x: elem.position().x + elem.position().width / 2,
                            y: elem.position().y + elem.position().height / 2,
                        },
                    });
                    cy.getElementById(elem.id() + '_corner_bottom_right').style({ visibility: 'hidden' });
                    cy.getElementById(elem.id() + '_corner_bottom_right').addClass('compoundCorner');
                };
            });
        } else {
            cy.$('.compoundCorner').forEach(function (elem) {elem.style({ visibility: 'hidden' });});
        };

        /* AGS.D.02.03 : Lock the nodes if static option is activated */
        if (featuresGraph['static']) {
            cy.batch(function(){
                cy.$('.defaultNode').lock();
                cy.$('.defaultNode').addClass("staticLock");
                cy.autoungrabify(true);
            });
        };

        resolve_ScriptInitialisation('');

    });
});

/*
--------------------------------
AGS.D.03 : Script loading promise
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function () {
    return new Promise((resolve_ScriptInitialisation) => {
        progressBarIncrement();

        /* --------------------------------------------- */
        /* AGS.D.03.01 : Creating the loading dictionnary */
        /* --------------------------------------------- */
        var modulesList_SubScripts = [];
        /* CORE event functions */
        modulesList_SubScripts.push('_core_events.js');
        /* Info bubbles */
        if (featuresGraph['infoBubbles'] === true) {
            modulesList_SubScripts.push('_mod_info_bubbles.js');
        }
        /* Info tags */
        if (featuresGraph['infoTags'] === true) {
            modulesList_SubScripts.push('_mod_info_tags.js');
        }
        /* Intro tutorial */
        if (featuresInformationShowing === 'true') {
            modulesList_SubScripts.push('_mod_intro_graph.js');
        }
        /* Editor mode */
        if (featuresButtons['toggle_modeEditor'] !== false || featuresGraph['editorOnLoad'] !== false) {
            modulesList_SubScripts.push('_mod_editor_mode.js');
        }
        /* Editor mode */
        if (featuresButtons['toggle_modeProximity'] !== false) {
            modulesList_SubScripts.push('_mod_proximity_mode.js');
        }
        /* Millesime */
        if (featuresGraph['featureMillesime'] === true) {
            modulesList_SubScripts.push('_mod_millesime.js');
        }
        /* Search bar */
        if (featuresGraph['featureSearchBar'] === true) {
            modulesList_SubScripts.push('_mod_search_bar.js');
        }
        /* Clustering */
        if (featuresGraph['featureClustering'] === true) {
            modulesList_SubScripts.push('_mod_clustering.js');
        }
        /* FarView */
        if (featuresGraph['featureFarView'] === true) {
            modulesList_SubScripts.push('_mod_far_view.js');
        }
        /* Context Menu */
        if (featuresGraph['featureContextMenu'] === true) {
            modulesList_SubScripts.push('_mod_context_menu.js');
        }
        /* ScreenShot */
        if (featuresGraph['featureScreenShot'] === true) {
            modulesList_SubScripts.push('_mod_screenshot.js');
        }
        /* Map mode */
        if (featuresButtons['toggle_modeMap'] !== false || featuresGraph['mapOnLoad'] !== false) {
            modulesList_SubScripts.push('_mod_map_mode.js');
        }
        /* LLM */
        if (featuresGraph['featureLlm'] !== false) {
            modulesList_SubScripts.push('_mod_llm.js');
        }
        /* LLM */
        if (featuresGraph['featureBubblesets'] !== false) {
            modulesList_SubScripts.push('_mod_bubblesets.js');
        }

        /* AGS.D.03.02 : Loading the dictionnary */
        modulesList_SubScripts.forEach(function (elem) {
            chain_ScriptLoading = chain_ScriptLoading.then(F_ScriptLoading.bind(null, '../../assets/js/' + elem));
        });

        /* AGS.D.03.03 : Loading the custom scripts */
        if (scriptGraph_GET.length > 0) {
            for (i_script in scriptGraph_GET) {
                chain_ScriptLoading = chain_ScriptLoading.then(
                    progressBarSetText.bind(null, 'Post-traitements : script(s) du gabarit (/' + scriptGraph_GET[i_script].substring(scriptGraph_GET[i_script].lastIndexOf('/') + 1) + ')')
                );
                chain_ScriptLoading = chain_ScriptLoading.then(F_ScriptLoading.bind(null, idGraph_path + scriptGraph_GET[i_script]));
            }
        }

        /* AGS.D.03.04 : Ending the promise */
        chain_ScriptLoading = chain_ScriptLoading.then(resolve_ScriptInitialisation.bind(null, ''));
    });
});

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function () {
    return new Promise((resolve_ScriptInitialisation) => {
        chain_ScriptLoading = chain_ScriptLoading.then(function () {
            return new Promise((resolve_ScriptLoading) => {
                resolve_ScriptInitialisation(resolve_ScriptLoading("Done"));
            });
        });
    });
});

/*
--------------------------------
AGS.D.04 : Building the legend
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function () {
    return new Promise((resolve_ScriptInitialisation) => {

        if (featuresGraph['featureLegend'] === true) {

            /* SVG creation function */

            function createCircleSVG(colorCustom,imageCustom) {
                let textSVG = "";
                textSVG += "<svg width=\"30\" height=\"30\">";
                textSVG += "<circle cx=\"15\" cy=\"15\" r=\"14\" fill=\""+colorCustom+"\"/>";
                if (imageCustom !== undefined && imageCustom !== "") {textSVG += "<image x=\"2.5\" y=\"2.5\" width=\"25\" height=\"25\" xlink:href=\""+imageCustom+"\"/>";};
                textSVG += "</svg>";
                return textSVG;
            };
            
            function createArrowSVG(colorCustom="#000000", dasharrayCustom="none", widthCustom=1) {

                let textSVG = "";
                textSVG += "<svg width=\"45\" height=\"30\" version=\"1.1\" viewBox=\"0 0 11.906 7.9375\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:cc=\"http://creativecommons.org/ns#\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">";
                textSVG += "<defs>";
                textSVG += "<marker id=\"color_"+colorCustom.replace("#","").replace("var(","").replace(")","")+"\" overflow=\"visible\" orient=\"auto\">";
                textSVG += "<path transform=\"scale(.25)\" d=\"m5.77 0-8.65 5v-10l8.65 5z\" fill=\""+colorCustom+"\"/>";
                textSVG += "</marker>";
                textSVG += "</defs>";
                textSVG += "<metadata>";
                textSVG += "<rdf:RDF>";
                textSVG += "<cc:Work rdf:about=\"\">";
                textSVG += "<dc:format>image/svg+xml</dc:format>";
                textSVG += "<dc:type rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\"/>";
                textSVG += "<dc:title/>";
                textSVG += "</cc:Work>";
                textSVG += "</rdf:RDF>";
                textSVG += "</metadata>";
                textSVG += "<path transform=\"scale(0.75)\" d=\"m1.3503 7.0372 9.2054-6.1369\" marker-end=\"url(#color_"+colorCustom.replace("#","").replace("var(","").replace(")","")+")\" fill=\"none\" stroke=\""+colorCustom+"\" stroke-dasharray=\""+dasharrayCustom+"\" stroke-width=\""+widthCustom+"\"/>";
                textSVG += "</svg>";
                return textSVG;
            };            

            /* Build legend : show labels */

            if (featuresGraph.showLabels && featuresGraph_LegendOptions.showLabelsControl) {
                let tmp_showLabelsChecking = "";
                if (featuresGraph_LegendOptions.showLabelsOnLoad) {tmp_showLabelsChecking = " checked";};
                dataGraph_LEGEND += "<p><div id=\"showLabelsSwitch_DIV\" class=\"custom-control custom-checkbox\"><input type=\"checkbox\" class=\"custom-control-input\" id=\"showLabelsSwitch\""+tmp_showLabelsChecking+">"+"<label class=\"custom-control-label\" for=\"showLabelsSwitch\" style=\"margin-bottom:0;line-height:1;\">Affichage de tous les labels</label></div></p>";
            };

            /* Build legend : bubblesets */

            if (featuresGraph.featureBubblesets && featuresGraph_LegendOptions.showBubblesetsControl) {
                let tmp_bubblesetChecking = "";
                if (featuresGraph_BubblesetsOptions.showBubblesetsOnLoad) {tmp_bubblesetChecking = " checked";};
                dataGraph_LEGEND += "<p><div id=\"bubblesetsSwitch_DIV\" class=\"custom-control custom-checkbox\"><input type=\"checkbox\" class=\"custom-control-input\" id=\"bubblesetsSwitch\""+tmp_bubblesetChecking+">"+"<label class=\"custom-control-label\" for=\"bubblesetsSwitch\" style=\"margin-bottom:0;line-height:1;\">Affichage des catégories en 'bulles' <br><i style=\"font-size:.5rem;\">(peut ne pas s'afficher si trop<br>d'éléments sont concernés)</i></label></div></p>";
            };
            
            /* Build legend : types */

            if (featuresGraph_LegendOptions.showTypes) {
                if ((featuresGraph.showLabels && featuresGraph_LegendOptions.showLabelsControl) || (featuresGraph.featureBubblesets && featuresGraph_LegendOptions.showBubblesetsControl)) {dataGraph_LEGEND += "<hr>";};
                dataGraph_LEGEND += "<div id=\"legendTypesActors\" style=\"display:initial;\">";
                if (featuresGraph_LegendOptions.customLabel_Types === "") {featuresGraph_LegendOptions.customLabel_Types = "Types"};
                dataGraph_LEGEND += "<h1>"+featuresGraph_LegendOptions.customLabel_Types+" ( <div id=\"checkTypesActorsAll_DIV\" class=\"custom-control custom-checkbox\"><input type=\"checkbox\" class=\"custom-control-input\" id=\"checkTypesActorsAll\" checked>"+"<label class=\"custom-control-label\" for=\"checkTypesActorsAll\"></label></div> )</h1>";
                if (featuresGraph_LegendOptions.showTypes_WithCategories) {
                    cy.$("node[group = " + '"' + "type" + '"' + "]").forEach(function (ele) {
                        dataGraph_LEGEND += "<div class=\"custom-control custom-checkbox\"><input type=\"checkbox\" class=\"custom-control-input single-actor\" id=\"checkTypeActor_"+ele.id().substring(ele.id().lastIndexOf('/')+1)+"\" checked>"+"<label class=\"custom-control-label\" for=\"checkTypeActor_"+ele.id().substring(ele.id().lastIndexOf('/')+1)+"\">"+createCircleSVG(ele.data("color"),ele.data("image"))+" : "+ele.data("label")+"</label>"+"</div>";
                    });
                };
                dataGraph_LEGEND += "</div>";
            };

            /* Build legend : nodes */

            if (featuresGraph_LegendOptions.showNodes) {
                if ((featuresGraph.showLabels && featuresGraph_LegendOptions.showLabelsControl) || (featuresGraph.featureBubblesets && featuresGraph_LegendOptions.showBubblesetsControl) || featuresGraph_LegendOptions.showTypes) {dataGraph_LEGEND += "<hr>";};
                dataGraph_LEGEND += "<div id=\"legendTypesNodes\" style=\"display:initial;\">";
                if (featuresGraph_LegendOptions.customLabel_Nodes === "") {featuresGraph_LegendOptions.customLabel_Nodes = "Entités"};
                dataGraph_LEGEND += "<h1>"+featuresGraph_LegendOptions.customLabel_Nodes+" ( <div id=\"checkNodeAll_DIV\" class=\"custom-control custom-checkbox\"><input type=\"checkbox\" class=\"custom-control-input\" id=\"checkNodeAll\" checked>"+"<label class=\"custom-control-label\" for=\"checkNodeAll\"></label></div> )</h1>";
                if (featuresGraph_LegendOptions.showNodes_WithCategories) {
                    for (itemNode in featuresGraph_LegendOptions.showNodes_Dictionnary) {
                        let tmp_NodeColor = featuresGraph_LegendOptions.showNodes_Dictionnary[itemNode]["color"];
                        let tmp_NodeImage = featuresGraph_LegendOptions.showNodes_Dictionnary[itemNode]["image"];
                        let tmp_NodeName = featuresGraph_LegendOptions.showNodes_Dictionnary[itemNode]["name"];
                        if (featuresGraph_LegendOptions.showNodes_Dictionnary[itemNode]["hr"] === "before") {dataGraph_LEGEND += "<hr>";};
                        dataGraph_LEGEND += "<div class=\"custom-control custom-checkbox\"><input type=\"checkbox\" class=\"custom-control-input single-node\" id=\"checkNode_"+itemNode+"\" checked>"+"<label id=\"checassets/kNode_"+itemNode+"_label\" class=\"custom-control-label\" for=\"checkNode_"+itemNode+"\">"+createCircleSVG(tmp_NodeColor,tmp_NodeImage)+" : "+tmp_NodeName+"</label>"+"</div>";
                        if (featuresGraph_LegendOptions.showNodes_Dictionnary[itemNode]["hr"] === "after") {dataGraph_LEGEND += "<hr>";};
                    };
                };
                dataGraph_LEGEND += "</div>";
            };

            /* Build legend : edges */

            if (featuresGraph_LegendOptions.showEdges) {
                if ((featuresGraph.showLabels && featuresGraph_LegendOptions.showLabelsControl) || (featuresGraph.featureBubblesets && featuresGraph_LegendOptions.showBubblesetsControl) || featuresGraph_LegendOptions.showTypes || featuresGraph_LegendOptions.showNodes) {dataGraph_LEGEND += "<hr>";}
                dataGraph_LEGEND += "<div id=\"legendTypesLinks\" style=\"display:initial;\">";
                if (featuresGraph_LegendOptions.customLabel_Edges === "") {featuresGraph_LegendOptions.customLabel_Edges = "Liens"};
                dataGraph_LEGEND += "<h1>"+featuresGraph_LegendOptions.customLabel_Edges+" ( <div id=\"checkLinkAll_DIV\" class=\"custom-control custom-checkbox\"><input type=\"checkbox\" class=\"custom-control-input\" id=\"checkLinkAll\" checked>"+"<label class=\"custom-control-label\" for=\"checkLinkAll\"></label></div> )</h1>";
                if (featuresGraph_LegendOptions.showEdges_WithCategories) {
                    for (itemLink in featuresGraph_LegendOptions.showEdges_Dictionnary) {
                        let tmp_LinkColor = featuresGraph_LegendOptions.showEdges_Dictionnary[itemLink]["color"];
                        let tmp_LinkDashArray = featuresGraph_LegendOptions.showEdges_Dictionnary[itemLink]["dasharray"];
                        let temp_LinkWidth = featuresGraph_LegendOptions.showEdges_Dictionnary[itemLink]["width"];
                        let tmp_LinkName = featuresGraph_LegendOptions.showEdges_Dictionnary[itemLink]["name"];
                        if (featuresGraph_LegendOptions.showEdges_Dictionnary[itemLink]["hr"] === "before") {dataGraph_LEGEND += "<hr>";};
                        dataGraph_LEGEND += "<div class=\"custom-control custom-checkbox\"><input type=\"checkbox\" class=\"custom-control-input single-link\" id=\"checkLink_"+itemLink+"\" checked>"+"<label id=\"checkLink_"+itemLink+"_label\" class=\"custom-control-label\" for=\"checkLink_"+itemLink+"\">"+createArrowSVG(tmp_LinkColor,tmp_LinkDashArray,temp_LinkWidth)+" : "+tmp_LinkName+"</label>"+"</div>";
                        if (featuresGraph_LegendOptions.showEdges_Dictionnary[itemLink]["hr"] === "after") {dataGraph_LEGEND += "<hr>";};
                    };
                };
                dataGraph_LEGEND += "</div>";
            };

            /* Build legend : cluster levels */
            
            if (featuresGraph_LegendOptions.showClusterCategories_Type || featuresGraph_LegendOptions.showClusterCategories_ClusterGroup || featuresGraph_LegendOptions.showClusterCategories_Entity) {
                if ((featuresGraph.showLabels && featuresGraph_LegendOptions.showLabelsControl) || (featuresGraph.featureBubblesets && featuresGraph_LegendOptions.showBubblesetsControl) || featuresGraph_LegendOptions.showTypes || featuresGraph_LegendOptions.showNodes || featuresGraph_LegendOptions.showEdges) {dataGraph_LEGEND += "<hr>";}
                dataGraph_LEGEND += "<div id=\"legendClusterLevels\" style=\"display:initial;\">";
                dataGraph_LEGEND += "<h1>Niveaux de regroupement</h1>";
            };
            if (featuresGraph_LegendOptions.showClusterCategories_Type) {
                if (featuresGraph_LegendOptions.clusterLabel_Type === "") {featuresGraph_LegendOptions.clusterLabel_Type = "Types"};
                dataGraph_LEGEND += "<div id=\"legendTypes\" class=\"custom-control custom-checkbox\"><input type=\"checkbox\" class=\"custom-control-input single-node\" id=\"checkTypes\" checked>"+"<label class=\"custom-control-label\" for=\"checkTypes\">"+"<img src=\"../../assets/icon/_type.svg\" />"+" : "+featuresGraph_LegendOptions.clusterLabel_Type;
                if (featuresGraph_LegendOptions.showClusterCategories_WithClusteringButtons) {
                    dataGraph_LEGEND += "<button id=\"compressTypes\" class=\"btn btn-primary expandcollapse\"><img src=\"../../assets/icon/compress-alt-solid.svg\" /></button><button id=\"expandTypes\" class=\"btn btn-primary expandcollapse\"><img src=\"../../assets/icon/expand-alt-solid.svg\" /></button>";
                };
                dataGraph_LEGEND += "</label>"+"</div>";
            };
            if (featuresGraph_LegendOptions.showClusterCategories_ClusterGroup) {
                if (featuresGraph_LegendOptions.clusterLabel_ClusterGroup === "") {featuresGraph_LegendOptions.clusterLabel_ClusterGroup = "Groupes"};
                dataGraph_LEGEND += "<div id=\"legendGroups\" class=\"custom-control custom-checkbox\"><input type=\"checkbox\" class=\"custom-control-input single-node\" id=\"checkGroups\" checked>"+"<label class=\"custom-control-label\" for=\"checkGroups\">"+"<img src=\"../../assets/icon/_group.svg\" />"+" : "+featuresGraph_LegendOptions.clusterLabel_ClusterGroup;
                if (featuresGraph_LegendOptions.showClusterCategories_WithClusteringButtons) {
                    dataGraph_LEGEND += "<button id=\"compressGroups\" class=\"btn btn-primary expandcollapse\"><img src=\"../../assets/icon/compress-alt-solid.svg\" /></button><button id=\"expandGroups\" class=\"btn btn-primary expandcollapse\"><img src=\"../../assets/icon/expand-alt-solid.svg\" /></button>";
                };
                dataGraph_LEGEND += "</label>"+"</div>";
            };
            if (featuresGraph_LegendOptions.showClusterCategories_Entity) {
                if (featuresGraph_LegendOptions.clusterLabel_Entity === "") {featuresGraph_LegendOptions.clusterLabel_Entity = "Entités"};
                dataGraph_LEGEND += "<div id=\"legendActors\" class=\"custom-control custom-checkbox\"><input type=\"checkbox\" class=\"custom-control-input single-node\" id=\"checkActors\" checked>"+"<label class=\"custom-control-label\" for=\"checkActors\">"+"<img src=\"../../assets/icon/_actor.svg\" />"+" : "+featuresGraph_LegendOptions.clusterLabel_Entity+"</label>"+"</div>";
            };
            if (featuresGraph_LegendOptions.showClusterCategories_Type || featuresGraph_LegendOptions.showClusterCategories_ClusterGroup || featuresGraph_LegendOptions.showClusterCategories_Entity) {dataGraph_LEGEND += "</div>";};

            /* Build legend : far view control */

            if ((featuresGraph.showLabels && featuresGraph_LegendOptions.showLabelsControl) || (featuresGraph.featureBubblesets && featuresGraph_LegendOptions.showBubblesetsControl) || featuresGraph_LegendOptions.showTypes || featuresGraph_LegendOptions.showNodes || featuresGraph_LegendOptions.showEdges || featuresGraph_LegendOptions.showClusterCategories_Type || featuresGraph_LegendOptions.showClusterCategories_ClusterGroup || featuresGraph_LegendOptions.showClusterCategories_Entity) {
                if ((featuresGraph_LegendOptions.showFarViewControl_automatic || featuresGraph_LegendOptions.showFarViewControl_manual) && featuresGraph.featureFarView) {
                    dataGraph_LEGEND += "<hr id=\"farViewedControl_HR\" style=\"display:flex;\">";
                };
            };

            if (featuresGraph_LegendOptions.showFarViewControl_automatic && featuresGraph.featureFarView) {
                let tmp_farViewChecking = "";
                if (featuresGraph.featureFarView_automatic) {tmp_farViewChecking = " checked";};
                dataGraph_LEGEND += "<p><div id=\"farViewedControl_DIV\" class=\"custom-control custom-checkbox\"><input type=\"checkbox\" class=\"custom-control-input\" id=\"farViewedControl\""+tmp_farViewChecking+">"+"<label class=\"custom-control-label\" for=\"farViewedControl\">Gestion automatique du mode zoomé</label></div></p>";
            };

            if (featuresGraph_LegendOptions.showFarViewControl_manual && featuresGraph.featureFarView) {
                dataGraph_LEGEND += "<p><div id=\"farViewedSwitch_DIV\" class=\"custom-control custom-checkbox\"><input type=\"checkbox\" class=\"custom-control-input\" id=\"farViewedSwitch\">"+"<label class=\"custom-control-label\" for=\"farViewedSwitch\">Mode zoomé</label></div></p>";
            };

            /* Add legend to DOM */

            var modalContent_Legend = document.getElementById('appli_graph_buttons_LEGEND');
            var text_Legend = '';
            text_Legend = text_Legend.concat('<div class="buttons_accordion" id="legend_buttons_accordion">');
            text_Legend = text_Legend.concat('<div class="card" style="background-color: rgba(0,0,0,0);">');
            text_Legend = text_Legend.concat('<div class="card-header" id="headingLegend">');
            text_Legend = text_Legend.concat(
                '<button class="btn btn-primary mainViewButtons" style="text-align:center;display:initial;" data-toggle="collapse" data-target="#collapseLegend" aria-expanded="true" aria-controls="collapseFilter">'
            );
            text_Legend = text_Legend.concat('<span><b>LÉGENDE</b></span>');
            text_Legend = text_Legend.concat('<img class="lazyload" width="25" height="25" data-src="../../assets/icon/list-ul-white.svg"></img>');
            text_Legend = text_Legend.concat('</button>');
            text_Legend = text_Legend.concat('</div>');
            text_Legend = text_Legend.concat('<div id="collapseLegend" class="collapse" style="max-height:none;" aria-labelledby="headingLegend" data-parent="#legend_buttons_accordion">');
            text_Legend = text_Legend.concat('<div id="collapseLegend_DIV_CONTROL">');
            text_Legend = text_Legend.concat('<div id="legendAccordion_body_CUSTOM">');
            text_Legend = text_Legend.concat(dataGraph_LEGEND);
            text_Legend = text_Legend.concat('</div>');
            text_Legend = text_Legend.concat('<div id="legendAccordion_body_TAGS"></div>');
            text_Legend = text_Legend.concat('</div>');
            text_Legend = text_Legend.concat('</div>');
            text_Legend = text_Legend.concat('</div>');
            text_Legend = text_Legend.concat('</div>');
            modalContent_Legend.innerHTML = text_Legend;

            /* Add resize event */

            $('#collapseLegend').on('shown.bs.collapse hide.bs.collapse', function () {
                setTimeout(function () {
                    windowCustomResize();
                }, 500);
            });

            /* Build events : show labels control */

            if (featuresGraph.showLabels && featuresGraph_LegendOptions.showLabelsControl) {
                $("#showLabelsSwitch").on("change",function() {
                    if(this.checked) {
                        cy.nodes().addClass("showLabelsOn");
                        cy.edges().addClass("showLabelsOn");
                    } else {
                        cy.nodes().removeClass("showLabelsOn");
                        cy.edges().removeClass("showLabelsOn");
                    };
                });
            };

            /* Build events : bubbleset control */

            if (featuresGraph.featureBubblesets && featuresGraph_LegendOptions.showBubblesetsControl) {
                $("#bubblesetsSwitch").on("change",function() {
                    if(this.checked) {
                        authComputeBubblePath = true;
                        generateBubblesetsPaths();
                    } else {
                        authComputeBubblePath = false;
                        removeBubblesetsPaths();
                    };
                });
            };

            /* Build events : far view control */

            if (featuresGraph_LegendOptions.showFarViewControl_automatic && featuresGraph.featureFarView) {
                $("#farViewedControl").on("change",function() {
                    if(this.checked) {
                        automaticFarRendering = true;
                        if (featuresGraph_LegendOptions.showFarViewControl_manual) {
                            if ($("#farViewedSwitch")[0].checked && featuresGraph_LegendOptions.hideTypesInCloseView) {
                                $("#legendTypes,#checkTypes,#compressTypes,#expandTypes").css("pointer-events","none");
                                $("#legendTypes").css("background-color","var(--main_color_02)");
                                $("#legendTypes").css("opacity","0.5");
                            };
                            $("#farViewedSwitch_DIV").css("pointer-events","none");
                            $("#farViewedSwitch_DIV").css("opacity","0.5");
                        };
                        if (cy.zoom()<0.5) {
                            toggleFarView();
                        } else {
                            toggleCloseView();
                        };
                    } else {
                        automaticFarRendering = false;
                        if (featuresGraph_LegendOptions.showFarViewControl_manual) {
                            $("#legendTypes,#checkTypes,#compressTypes,#expandTypes").css("pointer-events","auto");
                            $("#legendTypes").css("background-color","var(--main_color_03)");
                            $("#legendTypes").css("opacity","1");
                            $("#farViewedSwitch_DIV").css("pointer-events","auto");
                            $("#farViewedSwitch_DIV").css("opacity","1");
                        };
                    };
                });
            };

            if (featuresGraph_LegendOptions.showFarViewControl_manual && featuresGraph.featureFarView) {
                $("#farViewedSwitch").on("change",function() {
                    if (!automaticFarRendering) {
                        if(this.checked) {
                            toggleCloseView();
                        } else {
                            toggleFarView();
                        };
                    };
                });
                $("#farViewedSwitch_DIV").css("pointer-events","none");
                $("#farViewedSwitch_DIV").css("opacity","0.5");
            };

            /* Build events : cluster levels */

            if (featuresGraph_LegendOptions.showClusterCategories_Entity) {
                $("#checkActors").on("change",function() {
                    let nodesToFilter = cy.nodes('[group = "entity"],[group = "subGroup"]');
                    nodesToFilter = nodesToFilter.union(nodesToFilter.connectedEdges());
                    if(this.checked) {
                        nodesToFilter.removeClass("unchecked");
                    } else {
                        nodesToFilter.addClass("unchecked");
                    };
                    if (featuresGraph['featureSearchBar'] === true) {creatingSearchBar(true,false);};
                });
            };
            if (featuresGraph_LegendOptions.showClusterCategories_ClusterGroup) {
                $("#checkGroups").on("change",function() {
                    let nodesToFilter = cy.nodes("[group = " + '"' + "clusterGroup" + '"' + "]");
                    nodesToFilter = nodesToFilter.union(nodesToFilter.connectedEdges());
                    if(this.checked) {
                        nodesToFilter.removeClass("unchecked");
                    } else {
                        nodesToFilter.addClass("unchecked");
                    };
                    if (featuresGraph['featureSearchBar'] === true) {creatingSearchBar(true,false);};
                });
            };
            if (featuresGraph_LegendOptions.showClusterCategories_Type) {
                $("#checkTypes").on("change",function() {
                    let nodesToFilter = cy.nodes("[group = " + '"' + "type" + '"' + "]");
                    nodesToFilter = nodesToFilter.union(nodesToFilter.connectedEdges());
                    if(this.checked) {
                        nodesToFilter.removeClass("unchecked");
                    } else {
                        nodesToFilter.addClass("unchecked");
                    };
                    if (featuresGraph['featureSearchBar'] === true) {creatingSearchBar(true,false);};
                });
            };

            /* Build events : types */

            $("#checkTypesActorsAll").on("change",function() {
                let nodesToFilter = cy.nodes('[group = "entity"],[group = "subGroup"],[group = "clusterGroup"],[group = "type"]');
                nodesToFilter = nodesToFilter.union(nodesToFilter.connectedEdges());
                if(this.checked) {
                    cy.nodes(".mapNode[group = " + '"' + "entity" + '"' + "]").forEach(function (eleBIS) {
                        if (!isNaN(parseInt(eleBIS.data("radius_km"))) && (eleBIS.data("radius_km") !== undefined)) {
                            cyMap.map.setPaintProperty("circles_layer"+'_'+eleBIS.id(),'circle-opacity',0.2);
                        };
                    });
                    nodesToFilter.removeClass("unchecked_type");
                    $(".single-actor").prop("checked", true);
                } else {
                    cy.nodes(".mapNode[group = " + '"' + "entity" + '"' + "]").forEach(function (eleBIS) {
                        if (!isNaN(parseInt(eleBIS.data("radius_km"))) && (eleBIS.data("radius_km") !== undefined)) {
                            cyMap.map.setPaintProperty("circles_layer"+'_'+eleBIS.id(),'circle-opacity',0);
                        };
                    });
                    nodesToFilter.addClass("unchecked_type");
                    $(".single-actor").prop("checked", false);
                };
                if (featuresGraph['featureSearchBar'] === true) {creatingSearchBar(true,false);};
            });

            /* Build events : types categories */

            cy.$("node[group = " + '"' + "type" + '"' + "]").forEach(function (ele) {
                $("#checkTypeActor_"+ele.id().substring(ele.id().lastIndexOf('/')+1)).on("change",function() {
                    let nodesToFilter = ele.union(ele.descendants());
                    nodesToFilter = nodesToFilter.union(nodesToFilter.connectedEdges());
                    if(this.checked) {
                        nodesToFilter.forEach(function (eleBIS) {
                            if (!isNaN(parseInt(eleBIS.data("radius_km"))) && (eleBIS.data("radius_km") !== undefined)) {
                                cyMap.map.setPaintProperty("circles_layer"+'_'+eleBIS.id(),'circle-opacity',0.2);
                            };
                        });
                        nodesToFilter.removeClass("unchecked_type");
                        $("#checkTypesActorsAll").prop("checked", true);
                    } else {
                        nodesToFilter.forEach(function (eleBIS) {
                            if (!isNaN(parseInt(eleBIS.data("radius_km"))) && (eleBIS.data("radius_km") !== undefined)) {
                                cyMap.map.setPaintProperty("circles_layer"+'_'+eleBIS.id(),'circle-opacity',0);
                            };
                        });
                        nodesToFilter.addClass("unchecked_type");
                        $("#checkTypesActorsAll").prop("checked", false);
                    };
                    if (featuresGraph['featureSearchBar'] === true) {creatingSearchBar(true,false);};
                });
            });

            /* Build events : nodes */

            $("#checkNodeAll").on("change",function() {
                if(this.checked) {
                    if (featuresGraph.featureBubblesets) {removeBubblesetsPaths();};
                    cy.nodes().removeClass("unchecked");
                    cy.nodes().connectedEdges().removeClass("unchecked");
                    $(".single-node").prop("checked", true);
                    if (featuresGraph.featureBubblesets) {if (authComputeBubblePath) {chain_ModeManager = chain_ModeManager.then(generateBubblesetsPaths.bind(null));};};
                } else {
                    if (featuresGraph.featureBubblesets) {removeBubblesetsPaths();};
                    cy.nodes().addClass("unchecked");
                    cy.nodes().connectedEdges().addClass("unchecked");
                    $(".single-node").prop("checked", false);
                    if (featuresGraph.featureBubblesets) {if (authComputeBubblePath) {chain_ModeManager = chain_ModeManager.then(generateBubblesetsPaths.bind(null));};};
                };
                if (featuresGraph['featureSearchBar'] === true) {creatingSearchBar(true,false);};
            });

            /* Build events : nodes categories */

            for (itemNode in featuresGraph_LegendOptions.showNodes_Dictionnary) {
                let tmp_NodeSelector = featuresGraph_LegendOptions.showNodes_Dictionnary[itemNode]["selector"];
                let tmp_NodeDescription = featuresGraph_LegendOptions.showNodes_Dictionnary[itemNode]["description"];
                $("#checkNode_"+itemNode).on("change",function() {
                    let nodesToFilter = cy.nodes(tmp_NodeSelector);
                    nodesToFilter = nodesToFilter.union(nodesToFilter.connectedEdges().difference(cy.nodes(".unchecked").difference(nodesToFilter).connectedEdges()));
                    if(this.checked) {
                        if (featuresGraph.featureBubblesets) {removeBubblesetsPaths();};
                        nodesToFilter.removeClass("unchecked");
                        $("#checkNodeAll").prop("checked", true);
                        if (featuresGraph.featureBubblesets) {if (authComputeBubblePath) {chain_ModeManager = chain_ModeManager.then(generateBubblesetsPaths.bind(null));};};
                    } else {
                        if (featuresGraph.featureBubblesets) {removeBubblesetsPaths();};
                        nodesToFilter.addClass("unchecked");
                        $("#checkNodeAll").prop("checked", false);
                        if (featuresGraph.featureBubblesets) {if (authComputeBubblePath) {chain_ModeManager = chain_ModeManager.then(generateBubblesetsPaths.bind(null));};};
                    };
                    if (featuresGraph['featureSearchBar'] === true) {creatingSearchBar(true,false);};
                });
                if (tmp_NodeDescription !== undefined && tmp_NodeDescription !== "") {
                    $("#checkNode_"+itemNode+"_label").tooltip({"title": tmp_NodeDescription, "delay": {"show": 100, "hide": 100},"placement": "bottom","trigger": "hover"});
                };
            };

            /* Build events : edges */

            $("#checkLinkAll").on("change",function() {
                if(this.checked) {
                    cy.edges().removeClass("unchecked_edge");
                    $(".single-link").prop("checked", true);
                } else {
                    cy.edges().addClass("unchecked_edge");
                    $(".single-link").prop("checked", false);
                };
                if (featuresGraph['featureSearchBar'] === true) {creatingSearchBar(true,false);};
            });

            /* Build events : edges categories */

            for (itemLink in featuresGraph_LegendOptions.showEdges_Dictionnary) {
                let tmp_LinkSelector = featuresGraph_LegendOptions.showEdges_Dictionnary[itemLink]["selector"];
                let tmp_LinkDescription = featuresGraph_LegendOptions.showEdges_Dictionnary[itemLink]["description"];
                $("#checkLink_"+itemLink).on("change",function() {
                    let edgesToFilter = cy.edges(tmp_LinkSelector);
                    if(this.checked) {
                        edgesToFilter.removeClass("unchecked_edge");
                        $("#checkLinkAll").prop("checked", true);
                    } else {
                        edgesToFilter.addClass("unchecked_edge");
                        $("#checkLinkAll").prop("checked", false);
                    };
                    if (featuresGraph['featureSearchBar'] === true) {creatingSearchBar(true,false);};
                });
                if (tmp_LinkDescription !== undefined && tmp_LinkDescription !== "") {
                    $("#checkLink_"+itemLink+"_label").tooltip({"title": tmp_LinkDescription, "delay": {"show": 100, "hide": 100},"placement": "bottom","trigger": "hover"});
                };
            };

            /* Build events : cluster expand/collapse buttons */

            if (featuresGraph_LegendOptions.showClusterCategories_WithClusteringButtons) {
                let buttonExpandCollapse;
                if (featuresGraph_LegendOptions.showClusterCategories_ClusterGroup) {
                    buttonExpandCollapse = document.getElementById("compressGroups");
                    buttonExpandCollapse.addEventListener("click",function() {
                        $("#checkGroups").prop("checked", true);
                        /* cy.nodes("[group = " + '"' + "clusterGroup" + '"' + "]").difference(cy.nodes('[group = "subGroup"]')).removeClass("unchecked"); */
                        api.collapseRecursively(api.collapsibleNodes(cy.nodes(':visible[group = "clusterGroup"]').difference(cy.nodes('[group = "subGroup"]'))));
                    });
                    $("#compressGroups").tooltip({"title": "Replier ce niveau","delay": {"show": 100, "hide": 100},"placement": "bottom","trigger": "hover"});
                    buttonExpandCollapse = document.getElementById("expandGroups");
                    buttonExpandCollapse.addEventListener("click",function() {
                        api.expand(api.expandableNodes(cy.nodes(':visible[group = "clusterGroup"]')).difference(cy.nodes('[group = "subGroup"]')));
                        $("#checkActors").prop("checked", true);
                        /* cy.nodes('[group = "entity"],[group = "subGroup"]').removeClass("unchecked"); */
                    });
                    $("#expandGroups").tooltip({"title": "Déplier ce niveau","delay": {"show": 100, "hide": 100},"placement": "bottom","trigger": "hover"});
                };
                if (featuresGraph_LegendOptions.showClusterCategories_Type) {
                    buttonExpandCollapse = document.getElementById("compressTypes");
                    buttonExpandCollapse.addEventListener("click",function() {
                        $("#checkTypes").prop("checked", true);
                        cy.nodes("[group = " + '"' + "type" + '"' + "]").removeClass("unchecked");
                        api.collapseRecursively(api.collapsibleNodes(cy.nodes(':visible[group = "type"]')));
                    });
                    $("#compressTypes").tooltip({"title": "Replier ce niveau","delay": {"show": 100, "hide": 100},"placement": "top","trigger": "hover"});
                    buttonExpandCollapse = document.getElementById("expandTypes");
                    buttonExpandCollapse.addEventListener("click",function() {
                        api.expand(api.expandableNodes(cy.nodes(':visible[group = "type"]')));
                        $("#checkGroups").prop("checked", true);
                        cy.nodes("[group = " + '"' + "clusterGroup" + '"' + "]").difference(cy.nodes('[group = "subGroup"]')).removeClass("unchecked");
                    });
                    $("#expandTypes").tooltip({"title": "Déplier ce niveau","delay": {"show": 100, "hide": 100},"placement": "top","trigger": "hover"});
                };
            };

            /* Change buttons colors */

            applyColorTransform('#compressTypes > img', '', featuresColors_FILTER);
            applyColorTransform('#expandTypes > img', '', featuresColors_FILTER);
            applyColorTransform('#compressGroups > img', '', featuresColors_FILTER);
            applyColorTransform('#expandGroups > img', '', featuresColors_FILTER);
            applyColorTransform('#legendTypes > label > img', '', featuresColors_FILTER);
            applyColorTransform('#legendGroups > label > img', '', featuresColors_FILTER);
            applyColorTransform('#legendActors > label > img', '', featuresColors_FILTER);

            /* Shpw the legend */

            if (featuresGraph_LegendOptions.openLegendOnLoad) {$("#collapseLegend").collapse("toggle");};

        };
        
        resolve_ScriptInitialisation('');

    });
});

/*
--------------------------------
AGS.D.05 : Some post-process promise
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function () {
    return new Promise((resolve_ScriptInitialisation) => {
        progressBarSetText('Post-traitements : seconde passe');
        progressBarIncrement();

        /* AGS.D.05.01 : Generating the modal screen summary body */
        if (featuresButtons['toggle_summary'] !== false) {
            var modalContent_Summary = document.getElementById('summaryModal_body');
            modalContent_Summary.innerHTML = readNodes_for_modal(cy);
        }

        /* AGS.D.05.02 : Postprocessing */
        if (featuresGraph['infoBubbles'] === true) {
            informationBubbles(cy.nodes());
        }

        /* AGS.D.05.03 : Creating the search bar */
        if (featuresGraph['featureSearchBar'] === true) {
            creatingSearchBar();
        }

        /* AGS.D.05.04 : End the promise */
        resolve_ScriptInitialisation('');

    });
});

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function () {
    return new Promise((resolve_ScriptInitialisation) => {
        progressBarIncrement();

        /* Set the initial layout */
        if (featuresGraph['physics'] === true) {
            resolve_ScriptInitialisation(InitialLayout('euler_cola', true));
        } else {
            resolve_ScriptInitialisation("Done");
        }

    });
});

/*
--------------------------------
AGS.D.06 : Animation promise
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function () {
    return new Promise((resolve_ScriptInitialisation) => {
        progressBarIncrement();

        if (featuresGraph['animation'] === true || featuresGraph['featureLlm'] === true) {
            chain_ScriptLoading = chain_ScriptLoading.then(F_ScriptLoading.bind(null, '../../assets/js/_mod_talking_circles.js'));
            chain_ScriptLoading = chain_ScriptLoading.then(resolve_ScriptInitialisation.bind(null, ''));
        } else {
            resolve_ScriptInitialisation('Done');
        }

    });
});

/*
--------------------------------
AGS.D.07 : Fcose promise
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function () {
    return new Promise((resolve_ScriptInitialisation) => {
        progressBarIncrement();

        if (featuresGraph['fcoseOnLoad'] === true && cy.nodes().length > 0) {
            resolve_ScriptInitialisation(fcoseLayout(false,false,true));
        } else {
            resolve_ScriptInitialisation('Done');
        }

    });
});

/*
--------------------------------
AGS.D.08 : Stabilization promise
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function () {
    return new Promise((resolve_ScriptInitialisation) => {
        progressBarIncrement();
        resolve_ScriptInitialisation(cy.fit({
            "eles": cy.nodes().difference(".unchecked,.unchecked_type,.unchecked_searched,.unchecked_proximity,.unchecked_millesime,.unchecked_map"),
            "padding": featuresGraph.padding
        }));
    });
});

/*
--------------------------------
AGS.D.09 : Spinner and Credentials promise
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function () {
    return new Promise((resolve_ScriptInitialisation) => {
        progressBarIncrement();

        setTimeout(function () {
            progressBarIncrement();
            $('#appli_graph_spinner').fadeOut('fast', function () {
                progressBarStop();
            });
        }, 5);
        if ((featuresGraph['animation'] !== true || !animationOnLaunch) && showingCredentials["activate"] === true) {
            var div_status_credentials = document.getElementById("_showingCredentials_STATUS");
            div_status_credentials.innerHTML = "Application prête";
            div_status_credentials.style.backgroundColor = "var(--main_color_02)";
            div_status_credentials.style.animation = "none";
            setTimeout(function() {
                $("#_showingCredentials").fadeOut(2000);
            }, 500);
            resolve_ScriptInitialisation('Done');
        } else {
            resolve_ScriptInitialisation('Done');
        };

    });
});

/*
--------------------------------
AGS.D.10 : Postprocess promise
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function () {
    return new Promise((resolve_ScriptInitialisation) => {
        progressBarIncrement();

        /* AGS.D.10.01 : Make the graph visible */
        if (featuresGraph['animation'] !== true) {
            $('#appli_graph_network').css('visibility', 'visible');
            $('#appli_graph_element').css('display', 'initial');
            $('#appli_graph_cc_element').css('display', 'initial');
            $('#appli_graph_buttons').css('display', 'initial');
            $('#appli_graph_SEARCHING').css('display', 'flex');
            if (featuresGraph['featureScreenShot'] === true || featuresGraph['featureLlm'] === true) {$("#_talking_button").fadeIn(1000);};
        }

        /* AGS.D.10.02 : Loading the search bar */
        if (featuresGraph['featureSearchBar'] === true && (featuresGraph['animation'] === false || !animationOnLaunch)) {
            $('#appli_graph_SEARCHING').css({ visibility: 'visible' });
        } else {
            $('#appli_graph_SEARCHING').css({ visibility: 'hidden' });
        }
        if (featuresGraph['featureSearchBar'] === true) {
            $('#info_searchBar').css({ visibility: 'visible' });
            $('#info_searchBar').css({ height: 'auto' });
            $('#info_searchBar').css({ margin: '0.5em 10% 0px 0px' });
        } else {
            $('#info_searchBar').css({ visibility: 'hidden' });
            $('#info_searchBar').css({ height: '0' });
            $('#info_searchBar').css({ margin: '0' });
        }

        /* AGS.D.10.03 : Show the feature buttons separators */
        if ($('.fb_group_1:hidden').length !== $('.fb_group_1').length) {$('#fb_group_1_HR').css('display', 'flex')};
        if ($('.fb_group_2:hidden').length !== $('.fb_group_2').length) {$('#fb_group_2_HR').css('display', 'flex')};
        if ($('.fb_group_3:hidden').length !== $('.fb_group_3').length) {$('#fb_group_3_HR').css('display', 'flex')};
        if ($('.fb_group_4:hidden').length !== $('.fb_group_4').length) {$('#fb_group_4_HR').css('display', 'flex')};
        if ($('.fb_group_5:hidden').length !== $('.fb_group_5').length) {$('#fb_group_5_HR').css('display', 'flex')};
        if ($('.fb_group_6:hidden').length !== $('.fb_group_6').length) {$('#fb_group_6_HR').css('display', 'flex')};

        /* AGS.D.10.04 : Open the lateral screen, the editor mode or the map mode on load */
        if (featuresGraph.lateralOnLoad !== false) {
            document.getElementById('toggle_lateralScreen').click();
        } else {
            if (featuresGraph.editorOnLoad !== false) {
                document.getElementById('toggle_modeEditor').click();
            } else {
                if (featuresGraph.mapOnLoad !== false) {
                    document.getElementById('toggle_modeMap').click();
                };
            };
        };

        /* AGS.D.10.05 : Stabilize the tooltips */
        for (i in featuresButtons) {
            $('#' + i + '> a').tooltip({
                title: featuresButtons_dictFun[i]['name'],
                delay: { show: 100, hide: 100 },
                placement: 'right',
                trigger: 'hover',
            });
            $('#' + i + '> a > img').attr('data-src', featuresButtons_dictFun[i]['svg']);
        }

        /* AGS.D.10.06 : Show the labels */
        if (featuresGraph.showLabels && featuresGraph_LegendOptions.showLabelsControl && featuresGraph_LegendOptions.showLabelsOnLoad) {
            cy.nodes().addClass("showLabelsOn");
            cy.edges().addClass("showLabelsOn");
        };
    
        /* AGS.D.10.07 : Show the bubblesets */
        if (authComputeBubblePath) {generateBubblesetsPaths();};

        /* AGS.D.10.08 : Window resize */
        windowCustomResize();

        /* AGS.D.10.09 : End the promise */
        resolve_ScriptInitialisation('Done');
    });
});

/*
--------------------------------
AGS.D.11 : Script template custom additions
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function () {
    return new Promise((resolve_ScriptInitialisation) => {
        if (templateScript_CustomAdditions !== undefined) {
            resolve_ScriptInitialisation(templateScript_CustomAdditions());
        } else {
            resolve_ScriptInitialisation('Done');
        }
    });
});

/*
--------------------------------
AGS.D.12 : Activate the tutorial and graphIsLoad
--------------------------------
*/

chain_ScriptInitialisation = chain_ScriptInitialisation.then(function () {
    return new Promise((resolve_ScriptInitialisation) => {
        if (featuresInformationShowing === 'true' && (featuresGraph['animation'] !== true || !animationOnLaunch)) {
            startIntro_graph();
        };
        graphIsLoad = true;
        cy.endBatch();console.log('end batch');
        ForceStabilization();
        resolve_ScriptInitialisation("Done");
    });
});

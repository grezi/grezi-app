/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : CONTEXT MENU
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

var commands_contextualMenu;

var options_contextualMenu;

var contextualMenu;

var contextualMenu_CY;

/* For grabbable nodes */

commands_contextualMenu = [
	{
		"content": "<img width=\"25\" height=\"25\" src=\"../../assets/icon/lock-solid.svg\"></img>",
		"select": function(ele) {
			ele.lock();
		}
	},
	{
		"content": "<img width=\"25\" height=\"25\" src=\"../../assets/icon/unlock-solid.svg\"></img>",
		"select": function(ele) {
			ele.unlock();
		}
	}
]

if (featuresGraph.featureClustering) {
	commands_contextualMenu.push({
		"content": "<img width=\"25\" height=\"25\" src=\"../../assets/icon/compress-alt-solid.svg\"></img>",
		"select": function(ele) {
			if (api.isCollapsible(ele)) {api.collapse(ele)};
		}
	});
	commands_contextualMenu.push({
		"content": "<img width=\"25\" height=\"25\" src=\"../../assets/icon/expand-alt-solid.svg\"></img>",
		"select": function(ele) {
			if (api.isExpandable(ele)) {api.expand(ele)};
		}
	});
};

options_contextualMenu = {
	"selector": "node:grabbable",
	"fillColor" : mainColor_04,
	"activeFillColor": mainColor_01_faded,
	"commands": commands_contextualMenu
};

contextualMenu = cy.cxtmenu(options_contextualMenu);

/* Collapsed edges */

if (featuresGraph.featureClustering) {

	options_contextualMenu = {
		"selector": "edge.cy-expand-collapse-collapsed-edge",
		"fillColor" : mainColor_04,
		"activeFillColor": mainColor_01_faded,
		"commands": [
			{
				"content": "<img width=\"25\" height=\"25\" src=\"../../assets/icon/compress-alt-solid.svg\"></img>",
				"select": function(ele) {
					if (!ele.hasClass("cy-expand-collapse-collapsed-edge")) {api.collapseEdgesBetweenNodes(ele.connectedNodes());};
				}
			},
			{
				"content": "<img width=\"25\" height=\"25\" src=\"../../assets/icon/expand-alt-solid.svg\"></img>",
				"select": function(ele) {
					if (ele.hasClass("cy-expand-collapse-collapsed-edge")) {api.expandEdges(ele);};
				}
			}
		]
	};

	contextualMenu = cy.cxtmenu(options_contextualMenu);

};

/* Canvas */

commands_contextualMenu = [
	{
		"content": "<img width=\"25\" height=\"25\" src=\"../../assets/icon/unlock-solid.svg\"></img>",
		"select": function(ele) {
			cy.nodes().unlock();
		}
	},
	{
	"content": "<img width=\"25\" height=\"25\" src=\"../../assets/icon/eye-slash-solid.svg\"></img>",
	"select": function () {
		$("#appli_graph_element").animate({
			"_var": -100
		}, {
			"step": function (now, fx) {
				$(this).css('-webkit-transform', "translate(" + now + "%)");
			},
			"duration": 1000
		}, "linear");
		$("#appli_graph_cc").animate({
			"_var": 30
		}, {
			"step": function (now, fx) {
				$(this).css('-webkit-transform', "translateY(" + now + "%)");
			},
			"duration": 1000
		}, "linear");
		$("#appli_graph_buttons").animate({
			"_var": 10+Math.max(Math.max($('#view_ALL').outerWidth(true),$('#appli_graph_buttons_MILLESIME').outerWidth(true)),$('#headingLegend').outerWidth(true))
		}, {
			"step": function (now, fx) {
				$(this).css('-webkit-transform', "translate(" + now + "px)");
			},
			"duration": 1000
		}, "linear");
		$("#appli_graph_SEARCHING").animate({
			"_var": 30
		}, {
			"step": function (now, fx) {
				$(this).css('-webkit-transform', "translateY(" + now + "%)");
			},
			"duration": 1000
		}, "linear");
		$("#_talking_button").animate({
			"_var": -125
		}, {
			"step": function (now, fx) {
				$(this).css('-webkit-transform', "translateY(" + now + "%)");
			},
			"duration": 1000
		}, "linear");
		if (introHints !== undefined) {
			introHints.hideHints();
		};
	}
}, {
	"content": "<img width=\"25\" height=\"25\" src=\"../../assets/icon/eye-solid.svg\"></img>",
	"select": function (now, fx) {
		$("#appli_graph_element").animate({
			"_var": 0
		}, {
			"step": function (now, fx) {
				$(this).css('-webkit-transform', "translate(" + now + "%)");
			},
			"duration": 1000
		}, "linear");
		$("#appli_graph_cc").animate({
			"_var": 0
		}, {
			"step": function (now, fx) {
				$(this).css('-webkit-transform', "translateY(" + now + "%)");
			},
			"duration": 1000
		}, "linear");
		$("#appli_graph_buttons").animate({
			"_var": 0
		}, {
			"step": function (now, fx) {
				$(this).css('-webkit-transform', "translate(" + now + "px)");
			},
			"duration": 1000
		}, "linear");
		$("#appli_graph_SEARCHING").animate({
			"_var": 0
		}, {
			"step": function (now, fx) {
				$(this).css('-webkit-transform', "translateY(" + now + "%)");
			},
			"duration": 1000
		}, "linear");
		$("#_talking_button").animate({
			"_var": 0
		}, {
			"step": function (now, fx) {
				$(this).css('-webkit-transform', "translateY(" + now + "%)");
			},
			"duration": 1000
		}, "linear");
	}
}];

if (featuresGraph.featureClustering) {
	commands_contextualMenu.push({
		"content": "<img width=\"25\" height=\"25\" src=\"../../assets/icon/expand-alt-solid.svg\"></img>",
		"select": function(ele) {
			api.expandAllEdges();
		}
	});
	commands_contextualMenu.push({
		"content": "<img width=\"25\" height=\"25\" src=\"../../assets/icon/compress-alt-solid.svg\"></img>",
		"select": function(ele) {
			api.collapseAllEdges();
		}
	});
};

if ((featuresButtons['toggle_fcose'] !== false || featuresGraph['fcoseOnLoad'] === true)) {
	commands_contextualMenu.push({
		"content": "<img width=\"25\" height=\"25\" src=\"../../assets/icon/fcose.svg\"></img>",
		"select": function(ele) {
			fcoseLayout();
		}
	});
};

if (featuresGraph.featureLlm) {
	commands_contextualMenu.push({
		"content": "<img width=\"25\" height=\"25\" src=\"../../assets/img/grezia.png\"></img>",
		"select": function(ele) {
			askUSER("analysis",true);
		}
	});
};

options_contextualMenu = {
	"selector": "core",
	"fillColor": mainColor_02,
	"activeFillColor": mainColor_01_faded,
	"maxSpotlightRadius": 50,
	"commands": commands_contextualMenu
};

contextualMenu_CY = cy.cxtmenu(options_contextualMenu);

var targetNode_Cxtmenu = document.querySelector('.cxtmenu');
var observer_Cxtmenu = new MutationObserver(function(mutationsList, observer) {
    mutationsList.forEach(function(mutation) {
        if (mutation.type === 'childList' && mutation.addedNodes.length > 0) {
            applyColorTransform('.cxtmenu-content > img', '', featuresColors_FILTER);
        }
    });
});
observer_Cxtmenu.observe(targetNode_Cxtmenu, { childList: true });


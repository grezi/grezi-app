/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : FAR VIEW
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

var automaticFarRendering = featuresGraph.featureFarView_automatic;

var farViewStatus = true;

function toggleFarView() {
	cy.batch(function(){
		cy.$(".infoTag").remove();
		cy.nodes().difference(".searchedZoomed").addClass("farViewed");
		cy.edges().addClass("farViewed");
		if (automaticFarRendering && featuresGraph_LegendOptions.hideTypesInCloseView) {
			$("#legendTypes,#checkTypes,#compressTypes,#expandTypes").css("pointer-events","auto");
			$("#legendTypes").css("background-color","var(--main_color_03)");
			$("#legendTypes").css("opacity","1");
		};
		if (featuresGraph_LegendOptions.showFarViewControl_manual) {$("#farViewedSwitch").prop("checked", false);};
	});
};

function toggleCloseView() {
	cy.batch(function(){
		cy.nodes(".farViewed").removeClass("farViewed");
		cy.edges(".farViewed").removeClass("farViewed");
		if (automaticFarRendering && featuresGraph_LegendOptions.hideTypesInCloseView) {
			$("#legendTypes,#checkTypes,#compressTypes,#expandTypes").css("pointer-events","none");
			$("#legendTypes").css("background-color","var(--main_color_02)");
			$("#legendTypes").css("opacity","0.5");
		};	
		if (featuresGraph_LegendOptions.hideTypesInCloseView) {
			cy.nodes("[group = " + '"' + "type" + '"' + "]").union(cy.nodes("[group = " + '"' + "type" + '"' + "]").connectedEdges()).addClass("unchecked");
			$("#checkTypes").prop("checked", false);
		};
		if (featuresGraph_LegendOptions.showFarViewControl_manual) {$("#farViewedSwitch").prop("checked", true);};
	});
};

cy.batch(function(){
	cy.nodes().addClass("farViewed");
	cy.edges().addClass("farViewed");
});

cy.on("render", function(event) {
	cy.batch(function(){
		if (automaticFarRendering) {
			if ($("#appli_graph_network_CONTAINER").css("pointer-events") !== "none") {
				if (cy.zoom()<0.5 && !farViewStatus) {
					farViewStatus = true;
					toggleFarView();
				} else {
					if (cy.zoom()>=0.5 && farViewStatus) {
						farViewStatus = false;
						toggleCloseView();
					};
				};
			};
			/*
			cy.$("edge[precision]").forEach(function(edge) {
				let newTextMaxWidth;
				let distEuclid = Math.sqrt(((edge.sourceEndpoint().x-edge.targetEndpoint().x)**2)+((edge.sourceEndpoint().y-edge.targetEndpoint().y)**2));
				if (distEuclid<200) {newTextMaxWidth = distEuclid/2;} else {newTextMaxWidth = 150;};
				edge.style("text-max-width",newTextMaxWidth);
			});
			*/
		};
	});
});

/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : INFO TAGS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

/* Make the child position refresh on dragging a parent node */
cy.on("position", "node:parent", function() {
	this.descendants().forEach(function(descendant) {
		descendant.position(descendant.position());
	});
});


/* Information tags function : display tags near nodes with information in searchTag data */
function informationTags(nodesToTag) {
	return new Promise((resolve_informationTags) => {
		/* if (cy.nodes(".defaultNode").length <= 20) { */
			let dictColorsTags_list = ["#5FBEB8","#CC4E5C","#048B9A","#689D71","#B666D2","#EF9B0F","#E9C9B1","#FDBFB7"];
			let dictColorsTags_generation = {};
			let dictColorsTags_counter = 0;
			nodesToTag.forEach(function(elem) {
				if (elem.data("searchTag") !== undefined) {
					if ( (Object.keys(elem.data("searchTag")).length >= 1) && (elem.hasClass("defaultNode")) && (!elem.hasClass("unchecked")) && (!elem.is(":parent")) ) {
						let promise_infoTag = new Promise(function(resolveInfoTag, rejectInfoTag) {
							let dataTag = elem.data()["searchTag"];
							for (tagITERATE in dataTag) {
								if (tagITERATE !== "Labels") {
									for (i_tag in dataTag[tagITERATE]) {
										let tagID = elem.id()+"_tag_"+tagITERATE+i_tag;
										let tagLABEL = dataTag[tagITERATE][i_tag];
										if (tagLABEL) {
											if (Object.keys(dictColorsTags_custom).includes(tagITERATE)) {
												dictColorsTags_generation[tagITERATE] = dictColorsTags_custom[tagITERATE];
											} else {
												if (dictColorsTags_generation[tagITERATE] === undefined) {
													dictColorsTags_generation[tagITERATE] = dictColorsTags_list[dictColorsTags_counter];
													dictColorsTags_counter = dictColorsTags_counter+1;
												};
											};
											if (cy.getElementById(tagID).length === 0) {
												cy.add({
													"group": "nodes",
													"selectable": false,
													"grabbable": false,
													"data": {
														"id": tagID,
														"tagName" : tagITERATE,
														"label": tagLABEL
													}
												});
												cy.getElementById(tagID).style({"background-color": dictColorsTags_generation[tagITERATE]});
												cy.getElementById(tagID).addClass("infoTag_hidden");
												cy.getElementById(tagID).addClass("infoTag");
											};
										};
									};
								};
							};
							resolveInfoTag("Done");					
						});
						promise_infoTag.then(function() {
							let promise_infoTag_BIS = new Promise(function(resolveInfoTag_BIS, rejectInfoTag_BIS) {
								let dataTag = elem.data()["searchTag"];
								let lastTagID = "";
								let tagCOUNTER = 0;
								for (tagITERATE in dataTag) {
									if (tagITERATE !== "Labels") {
										for (i_tag in dataTag[tagITERATE]) {
											let tagID = elem.id()+"_tag_"+tagITERATE+i_tag;
											let tagLABEL = dataTag[tagITERATE][i_tag];
											if (tagLABEL !== "") {
												if (cy.getElementById(tagID).length > 0) {
													if (tagCOUNTER === 0) {
														let nodeBB_TEMP = elem.boundingBox({"includeOverlays": false});
														let currentTagWidth = cy.getElementById(tagID).outerWidth();
														let currentTagHeight = cy.getElementById(tagID).outerHeight();
														cy.getElementById(tagID).position({"x": nodeBB_TEMP["x1"]+currentTagWidth/2+5, "y": nodeBB_TEMP["y2"]+currentTagHeight/2});
														let tags_BB = cy.$("[id ^= '"+elem.id()+"_tag_']").boundingBox({"includeOverlays": false});
														if (elem.hasClass("mapNode")) {elem.removeStyle("width")} else if (elem.width()<tags_BB["w"]) {elem.style("width",Math.min(elem.width(),tags_BB["w"]));};
														elem.removeListener("position");
														elem.on("position", function() {
															let nodeBB_TEMP = this.boundingBox({"includeOverlays": false});
															cy.getElementById(tagID).position({"x": nodeBB_TEMP["x1"]+currentTagWidth/2+5, "y": nodeBB_TEMP["y2"]+currentTagHeight/2});
															let tags_BB = cy.$("[id ^= '"+this.id()+"_tag_']").boundingBox({"includeOverlays": false});
															if (elem.hasClass("mapNode")) {this.removeStyle("width")} else if (this.width()<tags_BB["w"]) {this.style("width",Math.min(this.width(),tags_BB["w"]));};
														});
														elem.position(elem.position());
														lastTagID = tagID;
														tagCOUNTER =+ 1;
													} else {
														/* let currentTagWidth = cy.getElementById(tagID).outerWidth(); */
														let currentTagBB = cy.getElementById(tagID).boundingBox({"includeOverlays": false});
														let lastTag = cy.getElementById(lastTagID);
														let lastTagBB = cy.getElementById(lastTagID).boundingBox({"includeOverlays": false});
														let lastTagPosition = cy.getElementById(lastTagID).position();
														let nodeBB_TEMP = {"x1": lastTag.outerWidth()+1+lastTagBB["x1"]+(currentTagBB["w"]/2), "y2": lastTagPosition["y"]};
														if (nodeBB_TEMP["x1"]+currentTagBB["w"] > elem.boundingBox({"includeOverlays": false})["x1"]+elem.boundingBox({"includeOverlays": false})["w"]) {
															nodeBB_TEMP = {"x1": elem.boundingBox({"includeOverlays": false})["x1"]+(currentTagBB["w"]/2+5), "y2": lastTagBB["y2"]+currentTagBB["h"]/2};
														};
														cy.getElementById(tagID).position({"x": nodeBB_TEMP["x1"], "y": nodeBB_TEMP["y2"]});
														cy.getElementById(lastTagID).removeListener("position");
														cy.getElementById(lastTagID).on("position", function() {
															/* let currentTagWidth = cy.getElementById(tagID).outerWidth(); */
															let currentTagBB = cy.getElementById(tagID).boundingBox({"includeOverlays": false});
															let lastTag = cy.getElementById(this.id());
															let lastTagBB = cy.getElementById(this.id()).boundingBox({"includeOverlays": false});
															let lastTagPosition = cy.getElementById(this.id()).position();
															let nodeBB_TEMP = {"x1": lastTag.outerWidth()+1+lastTagBB["x1"]+(currentTagBB["w"]/2), "y2": lastTagPosition["y"]};
															if (nodeBB_TEMP["x1"]+currentTagBB["w"] > elem.boundingBox({"includeOverlays": false})["x1"]+elem.boundingBox({"includeOverlays": false})["w"]) {
																nodeBB_TEMP = {"x1": elem.boundingBox({"includeOverlays": false})["x1"]+(currentTagBB["w"]/2+5), "y2": lastTagBB["y2"]+currentTagBB["h"]/2};
															};
															cy.getElementById(tagID).position({"x": nodeBB_TEMP["x1"], "y": nodeBB_TEMP["y2"]});
														});
														cy.getElementById(lastTagID).position(cy.getElementById(lastTagID).position());
														lastTagID = tagID;
													};
													cy.getElementById(tagID).removeClass("infoTag_hidden");
												};
											};
										};
									};
								};
								resolveInfoTag_BIS("");
							});
							promise_infoTag_BIS.then(function(valueInfoTag_BIS) {
								if (featuresGraph_LegendOptions["showTags"]) {
									let textTEMP = "<hr><h1>Étiquettes</h1>";
									for (tagTEMP in dictColorsTags_generation) {
										let tagTEMP_upper = tagTEMP;
										tagTEMP_upper = tagTEMP_upper.substr(0,1).toUpperCase()+tagTEMP_upper.substr(1)+".";
										textTEMP = textTEMP.concat("<p>"+"<svg width=\"45\" height=\"30\"><polygon points=\"0,30 30,30 45,15 30,0 0,0\"  style=\"fill:"+dictColorsTags_generation[tagTEMP]+"\"/></svg>"+" : "+tagTEMP_upper+"</p>");
									};
									let legendContent = document.getElementById("legendAccordion_body_TAGS");
									if (legendContent && legendContent !== "null" && legendContent != undefined) {legendContent.innerHTML = textTEMP;}; /* TO MODIFY WITH NEW FEATURE OF AURBA */
								};
								resolve_informationTags("Done");
							});
						});
					} else {resolve_informationTags("Done");};	
				} else {resolve_informationTags("Done");};
			});
		/* }; */
	});
};

/* Tag showing event */
let chain_informationTags = Promise.resolve();
cy.on("mouseover", '.defaultNode:childless,.defaultNode:child', function() {
	if (!this.hasClass("mapNode")) {
		let nodeTempForTag = this;
		chain_informationTags = chain_informationTags.then(function(){
			return new Promise((resolve_chain_informationTags) => {
				cy.$(".infoTag").remove();
				resolve_chain_informationTags("Done");
			});
		});
		chain_informationTags = chain_informationTags.then(informationTags.bind(null,nodeTempForTag));
	};
});

/* Remove tag outside the canva */
$("#appli_graph_main_screen").on("mouseout", function(){
	cy.$(".infoTag").remove();
});

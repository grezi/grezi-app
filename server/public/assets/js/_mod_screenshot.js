/*
	* Copyright : GREZI
	* Author : Adrien Solacroup
	* Encoding : UTF-8
	* Licence (code) : GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
	* Licence (distribution) : Creative Commons Attribution BY SA 4.0 International
*/

/*
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
MODULE APPLI GRAPH : CONTEXT MENU
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
*/

talking_button_screenshot = document.getElementById("_talking_button_screenshot");

talking_button_screenshot.addEventListener("click",function() {

	let chain_exportSVG = Promise.resolve();

	chain_exportSVG = chain_exportSVG.then(function(){
		return new Promise((resolve_exportSVG) => {
			$("#appli_graph_spinner").fadeIn("fast");
			$("#appli_graph_network_CONTAINER").css("pointer-events","none");
			cy.nodes().addClass("hoveredNode"); 
			cy.nodes().addClass("hoveredGroup"); 
			cy.edges().addClass("hoveredEdge");
			cy.nodes().addClass("screenshoted");
			cy.edges().addClass("screenshoted");
			if (featuresGraph.infoTags) {informationTags(cy.nodes());};
			if (featuresGraph.infoBubbles) {informationBubbles(cy.nodes());};
			resolve_exportSVG("Done");
		});
	});

	/* chain_exportSVG = chain_exportSVG.then(informationTags.bind(null,cy.nodes(".defaultNode"))); */

	chain_exportSVG = chain_exportSVG.then(function(){
		return new Promise((resolve_exportSVG) => {
			let svgContent = cy.svg({"echelle": 1, "full": false});
			let widthMatch = svgContent.match(/width="([^"]*)"/);
			let heightMatch = svgContent.match(/height="([^"]*)"/);
			let svgContent_width = widthMatch ? widthMatch[1] : null;
			let svgContent_height = heightMatch ? heightMatch[1] : null;
			if (featuresGraph.featureBubblesets) {
				let additionalSvg = $('#appli_graph_network > div > svg').html();
				let additionalSvg_width = parseFloat($('#appli_graph_network').width());
				let additionalSvg_height = parseFloat($('#appli_graph_network').height());
				let translateMatch = additionalSvg.match(/translate\(([^,]*),([^)]*)\)/);
				let translateX = parseFloat(translateMatch[1]);
				let translateY = parseFloat(translateMatch[2]);
				additionalSvg = additionalSvg.replace(/translate\(([^,]*),([^)]*)\)/g, function(match, p1, p2) {
					let newTranslateX = translateX * (svgContent_width / additionalSvg_width);
					let newTranslateY = translateY * (svgContent_height / additionalSvg_height);
					return 'translate(' + newTranslateX + ',' + newTranslateY + ')';
				});
				additionalSvg = additionalSvg.replace(/scale\(([^)]*)\)/g, function(match, p1) {
					let scaleFactor = ((svgContent_width / additionalSvg_width) + (svgContent_height / additionalSvg_height)) / 2;
					let newScale = parseFloat(p1) * scaleFactor;
					return 'scale(' + newScale + ')';
				});
				svgContent = svgContent.replace('<defs/>', '<defs/>' + additionalSvg);
			};
			let blob = new Blob([svgContent], {type:"image/svg+xml;charset=utf-8"});
			$("#otherModalLabel").html("Validation de la capture d'écran");
			let text_otherModal_body = "";
			text_otherModal_body += "<div id=\"blobImage\" style=\"width:100%;height:50vh;background-size:contain;background-repeat:no-repeat;background-position-y:center;background-position-x:center;\"></div>";
			text_otherModal_body += "<div style=\"display:flex;margin-top:1rem;\">";
			/* text_otherModal_body += "<button id=\"blobDownloadButton_png\" class=\"btn btn-primary\" style=\"width:40%;margin:auto;\">Télécharger l'image</button>"; */
			text_otherModal_body += "<button id=\"blobDownloadButton_svg\" class=\"btn btn-primary\" style=\"width:40%;margin:auto;\">Télécharger le svg</button></div>";
			$("#otherModal_body").html(text_otherModal_body); 
			$("#blobImage").css("background-image","url("+'"'+URL.createObjectURL(blob)+'"'+")");
			/*
			let blobDownloadButton_png = document.getElementById("blobDownloadButton_png");
			blobDownloadButton_png.addEventListener("click",function() {
				var canvas = document.getElementById("blobImage");
				var dataURL = canvas.toDataURL("image/png");
				var newTab = window.open('about:blank','image from canvas');
				newTab.document.write("<img src='" + dataURL + "' alt='from canvas'/>");
			});
			*/
			let blobDownloadButton_svg = document.getElementById("blobDownloadButton_svg");
			blobDownloadButton_svg.addEventListener("click",function() {saveAs(blob, "export.svg");});
			$("#otherModal").modal("show");
			$('#otherModal').one("hidden.bs.modal", function (){
				$("#appli_graph_spinner").fadeOut("fast");
				$("#appli_graph_network_CONTAINER").css("pointer-events","auto");
				URL.revokeObjectURL(blob);
				/* cy.$(".infoTag").remove(); */
				cy.nodes().removeClass("hoveredNode");
				cy.nodes().removeClass("hoveredGroup"); 
				cy.edges().removeClass("hoveredEdge");
				cy.nodes().removeClass("screenshoted");
				cy.edges().removeClass("screenshoted");
			});
			resolve_exportSVG("Done");
		});
	});

});

$("#_talking_button_screenshot").css("display","initial");

require('dotenv').config();

const getGraphs = async (req, res, next) => {

    const directId = req.params.directId;

    const fromHome = (process.env.CONFIG_STAND_ALONE_VERSION === "") ? true : false;

    let graphModule;
    try {
        graphModule = require('../_TEMPLATES/'+directId+'/configServerSide');
    } catch (error) {
        console.error('Erreur lors du chargement du module : '+directId , error);
        graphModule = {};
    };

    let graphConfig = {
        /* Tokens */
        tokenMapbox: process.env.TOKEN_MAPBOX,
        /* Main config */
        isHomePage: false,
        isGraphPage: true,
        isFromHome: fromHome,
        greziVersion: process.env.GREZI_VERSION,
        graphVersion: graphModule.graphVersion,
        templateVersion: graphModule.templateVersion,
        graphName: directId,
        titleGraph : graphModule.title_GRAPH,
        /* Meta data */
        graph_metaTitle: graphModule.title_GRAPH,
        graph_metaDescription: graphModule.description_GRAPH,
        graph_metaAuthor: graphModule.author_GRAPH,
        graph_metaPageIcon: function () {if (graphModule.icon_GRAPH === undefined) {return undefined;} else {return '/template_data/'+directId+'/'+graphModule.icon_GRAPH;}},
        graph_mainDomain: graphModule.domain_GRAPH,
        graph_ogTitle: graphModule.title_GRAPH,
        graph_ogDescription: graphModule.description_GRAPH,
        graph_ogSiteName: graphModule.ogSiteName_GRAPH,
        graph_ogImage: graphModule.ogImage_GRAPH,
        graph_twitterTitle: graphModule.title_GRAPH,
        graph_twitterDescription: graphModule.description_GRAPH,
        graph_twitterSite: graphModule.twitterSiteName_GRAPH,
        graph_twitterImage: graphModule.twitterImage_GRAPH,
        /* Navbar */
        navbar_Title : graphModule.title_GRAPH,
        navbar_ShowNavigation : function () {if (graphModule.showNavigation_GRAPH === undefined) {return true;} else {return graphModule.showNavigation_GRAPH;}},
        navbar_LogoShow : function () {if (graphModule.logoShow_GRAPH === undefined) {return true;} else {return graphModule.logoShow_GRAPH;}},
        navbar_LogoImg : function () {if (graphModule.logoImg_GRAPH === undefined) {return undefined;} else {return '/template_data/'+directId+'/'+graphModule.logoImg_GRAPH;}},
        navbar_LogoUrl : graphModule.logoUrl_GRAPH,
        displayHomeUpdagePage: graphModule.updateShow_GRAPH,
        /* Body */
        body_FullDescription: graphModule.fullDescription_GRAPH,
        /* Authentification */
        auth_DatasetSelection: function () {if (graphModule.datasetSelection_AUTH === undefined) {return false;} else {return graphModule.datasetSelection_AUTH;}},
        auth_DatasetDefault: graphModule.datasetIdDefault_AUTH,
        auth_DatasetIdList: function () {if (graphModule.datasetIdList_AUTH === undefined) {return [];} else {return graphModule.datasetIdList_AUTH;}},
        auth_Specifications: graphModule.specifications_AUTH,
        /* Other */
        displayGitLabURL: process.env.DISPLAY_GITLAB_URL === "true",
        displayFundingMessage: function() {if (graphModule.blank_mode) {return false} else {return process.env.DISPLAY_FUNDING_MESSAGE === "true"}},
        displayCC: function() {if (graphModule.blank_mode) {return false} else {return true}},
        activateMatomo: process.env.ACTIVATE_MATOMO === "true",
        mailTo: process.env.MAIL_TO,
        mailBcc: process.env.MAIL_TO_BCC,
        mailObject: process.env.MAIL_OBJECT,
        mailTemplate: process.env.MAIL_TEMPLATE,
        /* Config variables */
        config_DatasetDefault: JSON.stringify(graphModule.datasetIdDefault_AUTH),
        config_featuresGraph: graphModule.config_featuresGraph,
        config_featuresButtons: graphModule.config_featuresButtons,
        config_featuresLoading_FORCING: graphModule.config_featuresLoading_FORCING,
        config_featuresGraph_MillesimeOptions: graphModule.config_featuresGraph_MillesimeOptions,
        config_featuresGraph_SearchBarOptions: graphModule.config_featuresGraph_SearchBarOptions,
        config_featuresGraph_LegendOptions: graphModule.config_featuresGraph_LegendOptions,
        config_featuresGraph_ProximityModeOptions: graphModule.config_featuresGraph_ProximityModeOptions,
        config_featuresGraph_BubblesetsOptions: graphModule.config_featuresGraph_BubblesetsOptions,
        config_showingCredentials: graphModule.config_showingCredentials,
        config_rootColors_FORCING: graphModule.config_rootColors_FORCING,
        config_rootColors_NEW: graphModule.config_rootColors_NEW,
        config_featuresColors_FILTER: graphModule.config_featuresColors_FILTER,
        config_dictionnaryIntro_graph: graphModule.config_dictionnaryIntro_graph,
        config_dictColorsTags_custom: graphModule.config_dictColorsTags_custom,
        config_dataGraph_LEGEND: graphModule.config_dataGraph_LEGEND,
        config_options_Modifier: graphModule.config_options_Modifier,
        config_animationOnLaunch: graphModule.config_animationOnLaunch,
        config_possibilityToFullScreen: graphModule.config_possibilityToFullScreen,
        config_possibilityToPass: graphModule.config_possibilityToPass,
        config_dictIntro: graphModule.config_dictIntro,
        config_scriptGraph_GET: graphModule.config_scriptGraph_GET,
        /* Config LLM */
        config_llm_authorize: process.env.LLM_INFOMANIAK_AUTHORIZE,
        /* Matomo */
        activateMatomo: process.env.ACTIVATE_MATOMO === "true"
    };

    if (graphModule.config_featuresButtons) {
        if (graphModule.config_featuresButtons.toggle_modeMap || graphModule.config_featuresGraph.mapOnLoad) {
            const fs = require('fs');
            const passwordRaw = fs.readFileSync('password.json', 'utf-8');
            const passwordConfig = JSON.parse(passwordRaw);
            const tokenData = passwordConfig.tokenDictionnary.find((item) => item.name === "mapbox");
            graphConfig['tokenMapbox'] = tokenData.token;
        };
    };
    
    res.render('graphs', graphConfig);
    
};

module.exports = {
    getGraphs,
};

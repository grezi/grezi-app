const fs = require('fs');

const passwordRaw = fs.readFileSync('password.json', 'utf-8');
const passwordConfig = JSON.parse(passwordRaw);

const authentification = async (req, res, next) => {

    const template = req.body.template;
    const dataset = req.body.dataset;
    const password = req.body.password;

    let passwordPostprocessed;
    if (password === undefined) {passwordPostprocessed = ""} else {passwordPostprocessed = password};

    if (!template || !dataset) {
        console.log("AuthController : no template nor dataset");
        res.send('error_system');
    } else {
        if (passwordConfig.authDictionnary !== undefined) {
            const passwordData = passwordConfig.authDictionnary.find((item) => item.template === template && item.dataset.id === dataset && item.password === passwordPostprocessed);
            if (passwordData) {
                if (template !== passwordData.template || dataset !== passwordData.dataset.id) {
                    res.send('wrong_password');
                } else {
                    res.send(passwordData.api_password);
                }
            } else {
                res.send('no_password');
            };
        } else {
            console.log("AuthController : no authDictionnary in password.json");
            res.send('error_system');
        };
    };

};

module.exports = {
    authentification
};

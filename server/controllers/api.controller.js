const fs = require('fs');

const passwordRaw = fs.readFileSync('password.json', 'utf-8');
const passwordConfig = JSON.parse(passwordRaw);

const authentification = async (req, res, next) => {
    const template = req.body.template;
    const dataset = req.body.dataset;
    const password = req.body.password;
    const date = req.body.date;
    const traductor = req.body.traductor;

    if (!template || !dataset || !password) {

        res.send('error');
        
    } else {

        if (passwordConfig.authDictionnary !== undefined) {
            const passwordData = passwordConfig.authDictionnary.find((item) => item.template === template && item.dataset.id === dataset && item.api_password === password);
            if (passwordData) {
                if (template !== passwordData.template || dataset !== passwordData.dataset.id) {
                    res.send('error');
                } else {
                    try {
                        if (date) {
                            res.send(fs.readFileSync('server/_TEMPLATES/'+template+'/data_cache/'+passwordData.dataset.path.replace('.json','_TIMECODE.txt'), 'utf-8'));
                        } else {
                            if (traductor) {
                                res.send(fs.readFileSync('server/_TEMPLATES/'+template+'/data_cache/'+passwordData.dataset.path.replace('.json','_TRADUCTOR.json'), 'utf-8'));
                            } else {
                                res.send(fs.readFileSync('server/_TEMPLATES/'+template+'/data_cache/'+passwordData.dataset.path, 'utf-8'));
                            };
                        };
                    } catch (error) {
                        res.send('error');
                    };
                }
            } else {
                res.send('error');
            };
        } else {
            res.send('error');
        };

    };

};

module.exports = {
    authentification,
};

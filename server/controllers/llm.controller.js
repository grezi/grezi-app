require('dotenv').config();

const axios = require('axios');

const fs = require('fs');
const passwordRaw = fs.readFileSync('password.json', 'utf-8');
const passwordConfig = JSON.parse(passwordRaw);
const tokenData = passwordConfig.tokenDictionnary.find((item) => item.name === "llm_infomaniak");

const llm_get_product_id = async (req, res, next) => {

    axios({
        method: 'get',
        url: 'https://api.infomaniak.com/1/llm',
        headers: {
            'Authorization': 'Bearer '+tokenData.token,
            'Content-Type': 'application/json'
        }
    })
    .then((response) => {
        console.log(response.data);
    })
    .catch((error) => {
        console.error(error);
    });

};

const llm_request = async (req, res, next) => {

    const md = require('markdown-it')();

    let promptLLM = req.body.promptLLM;

    const profileLLM = req.body.profileLLM;

    const modeLLM = req.body.modeLLM;

    const template = req.body.template;

    const dataset = req.body.dataset;

    const password = req.body.password;

    let prepromptLLM = "";
    if (modeLLM === "analysis") {
        prepromptLLM += "Voici la manière dont j'attends que tu répondes à la demande d'analyse ci-après ainsi que des informations sur la structure des données du graphe : ";
        prepromptLLM += "en français uniquement";
        prepromptLLM += "le contexte est l'analyse d'un graphe de données"+",";
        prepromptLLM += "'group' = 'node' ou 'edge'"+",";
        prepromptLLM += "'id' = identifiant de l'élément"+",";
        prepromptLLM += "'label' = nom de l'élément"+",";
        prepromptLLM += "'titleComplete' = description de l'élément"+",";
        prepromptLLM += "'parent' = identifiant de l'élément qui englobe les autres éléments"+",";
        prepromptLLM += "'source' = id de l'élément node d'origine"+",";
        prepromptLLM += "'target' = id de l'élément node de destination"+"."+"\n";
    };
    if (modeLLM === "create") {
        prepromptLLM += "Voici la manière dont j'attends que tu répondes à la demande de création de données d'un graphe (un seul json, entre des triples guillemets) : ";
        prepromptLLM += "en français uniquement pour les attributs 'label' et 'titleComplete'";
        prepromptLLM += "'group' = 'node' ou 'edge'"+",";
        prepromptLLM += "'id' = identifiant de l'élément (attribut node ou edge)"+",";
        prepromptLLM += "'label' = nom de l'élément (attribut node ou edge)"+",";
        prepromptLLM += "'titleComplete' = description de l'élément (attribut node uniquement)"+",";
        prepromptLLM += "'parent' = identifiant de l'élément qui englobe les autres éléments (attribut node uniquement)"+",";
        prepromptLLM += "'classes' = toujours mettre la valeur 'defaultNode' (attribut node uniquement)"+",";
        prepromptLLM += "'source' = id de l'élément node d'origine (attribut edge uniquement), doit toujours correspondre à un id de node qui existe"+",";
        prepromptLLM += "'target' = id de l'élément node de destination (attribut edge uniquement), doit toujours correspondre à un id de node qui existe"+"."+"\n";
    };

    let prepromptLLM_response = "D'accord je vais faire comme cela, quelle est la demande ?";

    let postpromptLLM_response = "";
    if (modeLLM === "analysis") {postpromptLLM_response = "J'ai noté la demande, quelles sont les données du graphe pour que je puisse réponde ?";};
    if (modeLLM === "create") {postpromptLLM_response = "J'ai noté la demande, je produirai un graphe unique en un unique json entre des triples guillemets, quelle est la structure du graphe que je dois respecter ?";};

    const fs = require('fs');
    const passwordRaw = fs.readFileSync('password.json', 'utf-8');
    const passwordConfig = JSON.parse(passwordRaw);
    if (passwordConfig.authDictionnary !== undefined) {
        const passwordData = passwordConfig.authDictionnary.find((item) => item.template === template && item.dataset.id === dataset && item.api_password === password);
        if (passwordData) {

            if (template !== passwordData.template || dataset !== passwordData.dataset.id) {
                res.send('error');
            } else {

                try {

                    cacheData = fs.readFileSync('server/_TEMPLATES/'+template+'/data_cache/'+passwordData.dataset.path, 'utf-8');
                    graphData = JSON.parse(cacheData.replace(/(\r\n|\n|\r|\t)/gm,""));

                    let filteredData = graphData.map(item => {
                        let newItem = {};
                        if (item.group === 'nodes') {
                            newItem = {
                                group: item.group,
                                id: item.data.id,
                                label: item.data.label,
                                titleComplete: item.data.titleComplete,
                                parent: item.data.parent
                            };
                        } else if (item.group === 'edges') {
                            newItem = {
                                group: item.group,
                                id: item.data.id,
                                source: item.data.source,
                                target: item.data.target
                            };
                        }
                        for (let key in newItem.data) {
                            if (newItem.data[key] === null || newItem.data[key] === undefined || newItem.data[key] === '') {
                                delete newItem.data[key];
                            };
                        };
                        return newItem;
                    });

                    let promptLLM_DATA = "";
                    if (modeLLM === "analysis") {promptLLM_DATA = JSON.stringify(filteredData);};
                    if (modeLLM === "create") {promptLLM_DATA = "Dans une liste [] mettre des nodes formatés ainsi  : {\"group\":\"nodes\",\"data\":{\"id\":\"node_00\",\"label\":\"node_name\"}} et  des edges formatés ainsi : {\"group\":\"edges\",\"data\":{\"id\":\"elem_01_title_00\",\"source\":\"elem_01\",\"target\":\"title_00\"}}";};

                    if (promptLLM === "") {
                        promptLLM = "Fais un résumé en trois phrases en un seul paragraphe";
                    } else {
                        promptLLM = promptLLM.substring(0, parseInt(process.env.LLM_INFOMANIAK_MAX_INPUT_TOKENS)-1);
                    };

                    let eludedCaracters = promptLLM.length + promptLLM_DATA.legnth - parseInt(process.env.LLM_INFOMANIAK_MAX_DATA_TOKENS) + parseInt(process.env.LLM_INFOMANIAK_MAX_INPUT_TOKENS);
                    if (eludedCaracters < 0) {eludedCaracters = 0;};

                    promptLLM = promptLLM.substring(0, parseInt(process.env.LLM_INFOMANIAK_MAX_DATA_TOKENS)+parseInt(process.env.LLM_INFOMANIAK_MAX_INPUT_TOKENS)-1);

                    let messagesLLM = [
                        {"content": prepromptLLM,"role": "user"},
                        {"content": prepromptLLM_response,"role": "assistant"},
                        {"content": promptLLM,"role": "user"},
                        {"content": postpromptLLM_response,"role": "assistant"},
                        {"content": promptLLM_DATA,"role": "user"}
                    ];
                    if (modeLLM === "create") {
                        messagesLLM.push({"content": "Merci pour ces données, je vais faire le json mais comment formater ma réponse ?","role": "assistant"});
                        messagesLLM.push({"content": "Écrit directement le json sans donner d'éléments de contexte pour que ta réponse puisse directement être utilisée dans du code. Attention il faut toujours que les attributs edge 'source' et 'target' soient identiques à des valeurs d'attributs node 'id'. Pour rappel le sujet est : "+promptLLM,"role": "user"});
                    };

                    const data_llm = {
                        "max_new_tokens" : parseInt(process.env.LLM_INFOMANIAK_MAX_OUTPUT_TOKENS),
                        "profile_type": profileLLM,
                        "messages": messagesLLM
                    };
                
                    axios({
                        method: 'post',
                        url: 'https://api.infomaniak.com/1/llm/'+process.env.LLM_INFOMANIAK_ID,
                        headers: {
                            'Authorization': 'Bearer '+process.env.TOKEN_LLM_INFOMANIAK,
                            'Content-Type': 'application/json'
                        },
                        data: data_llm,
                        maxContentLength: 10000000, // 10 Mo
                        maxBodyLength: 10000000 // 10 Mo
                    })
                    .then((response) => {
                        let textResponse = "";
                        let choices = response.data.data.choices;
                        for(let i = 0; i < choices.length; i++) {
                            textResponse += choices[i].message.content;
                        };
                        res.send({textResponse: md.render(textResponse), eludedCaracters: eludedCaracters});
                    })
                    .catch((error) => {
                        console.log(error);
                        res.send('error');
                    });

                } catch (error) {
                    console.log(error);
                    res.send('error');
                };

            };

        } else {
            res.send('error');
        };

    } else {
        res.send('error');
    };

};

module.exports = {
    llm_get_product_id,
    llm_request
};

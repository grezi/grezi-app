require('dotenv').config();

const mainGraph = async (req, res, next) => {

    const graphList = [];
    const templateAccessMiddleware = require('../middleware/templateAccessMiddleware');
    const list_defaultIDS = templateAccessMiddleware.listOfTemplatesToServe(false);
    let list_publicIDS;
    if (process.env.CONFIG_PUBLIC_IDS !== "") {list_publicIDS = process.env.CONFIG_PUBLIC_IDS.split(',')};
    for (i in list_defaultIDS) {
        let configTEMP;
        try {
            let configGraphItem = require('../_TEMPLATES/' + list_defaultIDS[i] + '/configServerSide');
            configTEMP = {"idGraph": list_defaultIDS[i], "configGraph": configGraphItem};
        } catch (error) {
            console.log('Erreur lors du chargement du module : '+list_defaultIDS[i]);
            configTEMP = {"idGraph": list_defaultIDS[i], "configGraph": {}};
        };
        if (list_publicIDS) {
            if (list_publicIDS.includes(list_defaultIDS[i])) {
                graphList.push(configTEMP);
            };
        } else {
            graphList.push(configTEMP);
        };
    };

    let message = req.query.message;
    if (!message) {message = "default";};

    const homeConfig = {
        isHomePage: true,
        isGraphPage: false,
        loadStatus: message,
        displayHomeWelcomeBloc: process.env.DISPLAY_HOME_WELCOME_BLOC === "true",
        displayHomeLoadTemplate: process.env.DISPLAY_HOME_LOAD_TEMPLATE === "true",
        displayHomeCreateGraph: process.env.DISPLAY_HOME_CREATE_GRAPH === "true",
        displayHomeUpdagePage: process.env.DISPLAY_HOME_UPDATE_PAGE === "true",
        displayHomePrivateTemplate: process.env.DISPLAY_HOME_PRIVATE_TEMPLATE === "true",
        displayHomeDescriptionBloc: process.env.DISPLAY_HOME_DESCRIPTION_BLOC === "true",
        displayHomeDonateBloc: process.env.DISPLAY_HOME_DONATE_BLOC === "true",
        displayHomeTemplateBloc: process.env.DISPLAY_HOME_TEMPLATE_BLOC === "true",
        displayHomeCatalogueBloc: process.env.DISPLAY_HOME_CATALOGUE_BLOC === "true",
        displayHomeListBloc: process.env.DISPLAY_HOME_LIST_BLOC === "true",
        graphList: graphList,
        navbar_Title: process.env.NAVBAR_TITLE_HOME,
        navbar_ShowNavigation: false,
        navbar_LogoShow: process.env.NAVBAR_LOGO_SHOW === "true",
        activateMatomo: process.env.ACTIVATE_MATOMO === "true"
    };

    res.render('home', homeConfig);

};

module.exports = {
    mainGraph,
};

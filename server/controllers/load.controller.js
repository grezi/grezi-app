const fs = require('fs');

const passwordRaw = fs.readFileSync('password.json', 'utf-8');
const passwordConfig = JSON.parse(passwordRaw);

const authentification = async (req, res, next) => {
    
    const password = req.query.password;

    let passwordPostprocessed;
    if (password === undefined) {passwordPostprocessed = ""} else {passwordPostprocessed = password};

    if (passwordConfig.authDictionnary !== undefined) {
        const passwordData = passwordConfig.authDictionnary.find((item) => item.password === passwordPostprocessed);
        if (passwordData && passwordPostprocessed !== "") {
            res.redirect('/graphs/'+passwordData.template+'?'+'dataset='+passwordData.dataset.id+'&'+'auth='+passwordData.password);
        } else {
            res.redirect('/?message=no_password');
        };
    } else {
        console.log("LoadController : no authDictionnary in password.json");
        res.send('error_system');
    };

};

module.exports = {
    authentification
};

require('dotenv').config();
const fs = require('fs');

const passwordRaw = fs.readFileSync('password.json', 'utf-8');
const passwordConfig = JSON.parse(passwordRaw);

const authentification = async (req, res, next) => {

    let template_get = req.params.template;

    const template_post = req.query.template;
    const password_post = req.query.password;

    const fromHome = (template_get === "fromHome") ? true : false;
    if (fromHome) {template_get = undefined;};

    let needRedirection = false;
    let showGraphList;
    const graphList = [];
    let configGraphItem_ForConfig;
    const templateAccessMiddleware = require('../middleware/templateAccessMiddleware');
    if (!template_get) {
        /* If no template in the url, it gives a list of the available templates */
        const list_defaultIDS = templateAccessMiddleware.listOfTemplatesToServe(false);
        for (i in list_defaultIDS) {
            let graphList_item;
            try {
                let configGraphItem = require('../_TEMPLATES/' + list_defaultIDS[i] + '/configServerSide');
                graphList_item = {"idGraph": list_defaultIDS[i], "configGraph": configGraphItem};
            } catch (error) {
                console.log('Erreur lors du chargement du module : '+list_defaultIDS[i]);
                graphList_item = {"idGraph": list_defaultIDS[i], "configGraph": {}};
            };
            if (list_defaultIDS[i] === template_post) {graphList_item["isSelected"] = true;} else {graphList_item["isSelected"] = false;};
            graphList.push(graphList_item);
        };
        showGraphList = true;
    } else {
        /* If a template is specified, it will not show the template list */
        try {
            let configGraphItem = require('../_TEMPLATES/' + template_get + '/configServerSide');
            graphList.push({"idGraph": template_get, "configGraph": configGraphItem});
            configGraphItem_ForConfig = configGraphItem;
        } catch (error) {
            console.log('Erreur lors du chargement du module : '+template_get);
            graphList.push({"idGraph": template_get, "configGraph": {}});
            needRedirection = true;
        };
        showGraphList = false;
    };

    if (needRedirection) {

        if (process.env.CONFIG_STAND_ALONE_VERSION === "") {
            if (fromHome) {
                res.redirect('/update/fromHome');
            } else {
                res.redirect('/update');
            };
        } else {
            res.redirect('/update/'+process.env.CONFIG_STAND_ALONE_VERSION);
        };

    } else {

        let updateConfig = {
            /* Main config */
            isHomePage: false,
            isGraphPage: false,
            isUpdatePage: true,
            isFromHome: fromHome,
            directId: template_get,
            showGraphList: showGraphList,
            graphList: graphList,
            status: 'default',
            /* Navbar */
            navbar_Title: process.env.NAVBAR_TITLE_UPDATE,
            navbar_ShowNavigation: true,
            navbar_LogoShow: process.env.NAVBAR_LOGO_SHOW === "true" && process.env.CONFIG_STAND_ALONE_VERSION === "",
            /* Mail */
            mailFromUpdate: process.env.MAIL_FROM_UPDATE === "true",
            /* Matomo */
            activateMatomo: process.env.ACTIVATE_MATOMO === "true"
        };

        if (template_get) {
            /* Meta data */
            updateConfig.graph_metaTitle = configGraphItem_ForConfig.title_GRAPH;
            updateConfig.graph_metaDescription = configGraphItem_ForConfig.description_GRAPH;
            updateConfig.graph_metaAuthor = configGraphItem_ForConfig.author_GRAPH;
            updateConfig.graph_metaPageIcon = function () {if (configGraphItem_ForConfig.icon_GRAPH === undefined) {return undefined;} else {return '/template_data/'+template_get+'/'+configGraphItem_ForConfig.icon_GRAPH;}};
            updateConfig.graph_mainDomain = configGraphItem_ForConfig.domain_GRAPH;
            updateConfig.graph_ogTitle = configGraphItem_ForConfig.title_GRAPH;
            updateConfig.graph_ogDescription = configGraphItem_ForConfig.description_GRAPH;
            updateConfig.graph_ogSiteName = configGraphItem_ForConfig.ogSiteName_GRAPH;
            updateConfig.graph_ogImage = configGraphItem_ForConfig.ogImage_GRAPH;
            updateConfig.graph_twitterTitle = configGraphItem_ForConfig.title_GRAPH;
            updateConfig.graph_twitterDescription = configGraphItem_ForConfig.description_GRAPH;
            updateConfig.graph_twitterSite = configGraphItem_ForConfig.twitterSiteName_GRAPH;
            updateConfig.graph_twitterImage = configGraphItem_ForConfig.twitterImage_GRAPH;
            /* Navbar */
            updateConfig.navbar_LogoImg = function () {if (configGraphItem_ForConfig.logoImg_GRAPH === undefined) {return undefined;} else {return '/template_data/'+template_get+'/'+configGraphItem_ForConfig.logoImg_GRAPH;}};
            /* Config variables */
            updateConfig.config_rootColors_FORCING = configGraphItem_ForConfig.config_rootColors_FORCING;
        };

        let passwordPostprocessed;
        if (password_post === undefined) {passwordPostprocessed = ""} else {passwordPostprocessed = password_post};

        if (!template_post) {
            res.render('update', updateConfig);
        } else {
            if (passwordConfig.authDictionnary !== undefined) {
                const passwordData = passwordConfig.authDictionnary.find((item) => item.template === template_post && item.update_password === passwordPostprocessed);
                if (passwordData) {
                    if (template_post !== passwordData.template) {
                        res.send('error');
                    } else {
                        let now = new Date();
                        console.log('----------------------------------');
                        console.log('UPDATER DATA CACHING AT : '+now);
                        let cachingMiddleware = require('../middleware/templateDataCachingMiddleware');
                        const timeout = (ms) => new Promise((_, reject) => setTimeout(() => reject(new Error('Timeout')), ms));
                        Promise.race([cachingMiddleware.cachingDataSet(passwordData.template, passwordData.dataset.path),timeout(5000)])
                            .then((status) => {
                                updateConfig['status'] = 'updated';
                                res.render('update', updateConfig);
                                if (process.env.MAIL_FROM_UPDATE === "true" && process.env.MAIL_FROM_SMTP !== "" && process.env.MAIL_FROM_USER !== "" && process.env.MAIL_FROM_PASS !== "") {
                                    const mail = require('../helpers/mail-helpers');
                                    mail.sendMail("GREZI - Génération jeux de données - "+template_post,updateConfig['status']);
                                };
                            })
                            .catch((error) => {
                                if (error.message === 'Timeout') {
                                    updateConfig['status'] = 'computing';
                                    res.render('update', updateConfig);
                                } else {
                                    console.log('Erreur lors de la mise en cache:', error);
                                    updateConfig['status'] = 'error';
                                    res.render('update', updateConfig);
                                }
                            });
                    }
                } else {
                    console.log("UpdateController : no item found");
                    updateConfig['status'] = 'error';
                    res.render('update', updateConfig);
                };
            } else {
                console.log("UpdateController : no authDictionnary in password.json");
                res.send('error');
            };
        };

    };

};

module.exports = {
    authentification
};

const http = require('http');
const https = require('https');
const fs = require('fs');
const express = require('express');
const enforce = require('express-sslify');
var path = require('path');
const engine = require('express-handlebars');
const Handlebars = require('hbs');
const hbs_helpers = require('./helpers/hbs-helpers');
const routes = require('./routes/routes');

const templateAccessMiddleware = require('./middleware/templateAccessMiddleware');
const mail = require('./helpers/mail-helpers');

const { JSDOM } = require('jsdom');
const { window } = new JSDOM('');
const $ = require('jquery')(window);

require('dotenv').config();

if (process.env.MAIL_FROM_TEST === "true" && process.env.MAIL_FROM_SMTP !== "" && process.env.MAIL_FROM_USER !== "" && process.env.MAIL_FROM_PASS !== "") {mail.sendMail("GREZI - Verify sending mail","Mail has been send");};

try {

    const hbs = engine.create({
        defaultLayout: 'main-layout',
        extname: '.hbs',
        helpers: hbs_helpers,
    });

    Handlebars.registerPartials('./views/partials');

    /* Verify the passwords */
    const passwordData = JSON.parse(fs.readFileSync('password.json', 'utf-8'));
    function arePasswordsDifferent() {
        const passwordSet = new Set();
        let totalPasswords = 0;
        for (const config of passwordData.authDictionnary) {
            if (config.password !== "") { passwordSet.add(config.password); totalPasswords += 1 };
            if (config.api_password !== "") { passwordSet.add(config.api_password); totalPasswords += 1 };
            if (config.update_password !== "") { passwordSet.add(config.update_password); totalPasswords += 1 };
        };
        return passwordSet.size === totalPasswords;
    };
    if (arePasswordsDifferent()) {
        console.log('All the passwords are different.');
    } else {
        console.log('At least two passwords are the same in the file "password.json". The application will stop.');
        process.exit(1);
    };

    /* Views directory */
    const viewdir = path.join(__dirname, 'views/');

    /* Basedir for static assets */
    const publicdir = path.join(__dirname, 'public/');

    var app = express();
    const port = process.env.PORT || 3000;

    app.engine('hbs', hbs.engine);
    app.set('view engine', 'hbs');

    app.set('views', viewdir);

    app.use(
        express.urlencoded({
            extended: true,
        })
    );

    /* ROUTER */

    app.use(routes);

    /* ASSETS */

    app.use('/assets', express.static(path.join(publicdir, 'assets')), (req, res, next) => {
        res.status(404).end();
    });

    app.use(express.static('public'));

    /* TEMPLATES */

    const templates_list = templateAccessMiddleware.listOfTemplatesToServe(true);
    for (const [index, templateName] of templates_list.entries()) {
        app.use(`/template_data/${templateName}`, templateAccessMiddleware.restrictTemplateAccess, express.static(`server/_TEMPLATES/${templateName}/public`));
    };


    /* DATA CACHING (WITH OPTIONNAL CRON JOB) */

    if (process.env.CACHING_LIST_ON_RUN !== "") {
        let now = new Date();
        console.log('----------------------------------')
        console.log('LAUNCHING DATA CACHING AT : ' + now)
        const cachingMiddleware = require('./middleware/templateDataCachingMiddleware');
        cachingMiddleware.cachingDataSet(process.env.CACHING_LIST_ON_RUN);
    };

    if (process.env.CACHING_LIST_CRON !== "") {
        const cron = require('node-cron');
        cron.schedule(process.env.CACHING_CRON_DELAY, () => {
            let now = new Date();
            console.log('----------------------------------')
            console.log('CRON DATA CACHING AT : ' + now)
            let cachingMiddleware = require('./middleware/templateDataCachingMiddleware');
            cachingMiddleware.cachingDataSet(process.env.CACHING_LIST_CRON);
        });
    };

    /* REDIRECT ALL */

    app.get('*', (req, res) => {
        res.redirect('/');
    });

    /* FORCE HTTPS */

    if (process.env.FORCE_HTTPS) {
        app.use(enforce.HTTPS({ trustProtoHeader: true }));
    };

    /* RUN */

    const privateKeyPath = process.env.PRIVATE_KEY_PATH;
    const certificatePath = process.env.CERTIFICATE_PATH;

    if (fs.existsSync(privateKeyPath) && fs.existsSync(certificatePath)) {

        /* If certificates exist, run a HTTPS server */
        const privateKey = fs.readFileSync(privateKeyPath, 'utf8');
        const certificate = fs.readFileSync(certificatePath, 'utf8');

        const credentials = { key: privateKey, cert: certificate };

        const httpsServer = https.createServer(credentials, app);

        httpsServer.listen(port, () => {
            console.log(`GREZI running on https://localhost:${port} 🌳🌳🌳🌳🌳🌳🌳🌳🌳🌳🌳🌳🌳🌳🌳🌳🌳🌳🌳🌳🌳`);
        });

    } else {

        /* If certificates do not exist, run a HTTP server */
        const httpServer = http.createServer(app);

        httpServer.listen(port, () => {
            console.log(`GREZI running on http://localhost:${port} 🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃🍃`);
        });

    }

} catch (error) {
    console.log(error);
    if (process.env.MAIL_FROM_ERROR === "true" && process.env.MAIL_FROM_SMTP !== "" && process.env.MAIL_FROM_USER !== "" && process.env.MAIL_FROM_PASS !== "") {mail.sendMail("GREZI - An error ocurred !!!",String(error));};
}

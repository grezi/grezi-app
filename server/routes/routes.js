const router = require('express').Router();

const templateAccessMiddleware = require('../middleware/templateAccessMiddleware');

const DatamiController = require('../controllers/datami.controller');
const LoadController = require('../controllers/load.controller');
const UpdateController = require('../controllers/update.controller');
const ApiController = require('../controllers/api.controller');
const AuthController = require('../controllers/auth.controller');
const HomeController = require('../controllers/home.controller');
const GraphsController = require('../controllers/graphs.controller');
const LlmController = require('../controllers/llm.controller');

if (process.env.CONFIG_STAND_ALONE_VERSION === "") {
    router.route('/').get(HomeController.mainGraph);
} else {
    router.get('/', (req, res) => {res.redirect('/graphs/'+process.env.CONFIG_STAND_ALONE_VERSION);});
};

if (process.env.CONFIG_STAND_ALONE_VERSION !== "") {
    router.get('/update', (req, res) => {res.redirect('/update/'+process.env.CONFIG_STAND_ALONE_VERSION);});
};
router.route('/update/:template?').get(UpdateController.authentification);

router.route('/api').post(ApiController.authentification);

router.route('/auth').post(AuthController.authentification);

router.route('/load').get(LoadController.authentification);

router.route('/datami').get(DatamiController.authentification);

router.get('/graphs', (req, res) => {res.redirect('/');});
router.route('/graphs/:directId/').get(templateAccessMiddleware.restrictTemplateAccess, GraphsController.getGraphs);

if (process.env.LLM_INFOMANIAK_AUTHORIZE) {
    router.route('/llm/get_product_id').get(async (req, res) => {
        try {
            await LlmController.llm_get_product_id(req, res);
        } catch (error) {
            console.error('Erreur on LLM loading:', error);
            res.redirect('/');
        }
    });
    router.route('/llm/request').post(LlmController.llm_request);
};

module.exports = router;

/*
    *
	* STEP.A : SET VARIABLES
    *
    * STEP.B : SET FUNCTIONS
    *
    * STEP.C : DATA CACHING
    * 
    * STEP.D : POST PROCESSING
    * 
    * STEP.E : LOADING POSITIONS
    * 
    * STEP.F : CACHING DATA
    * 
    * STEP.G : EXPORT TRADUCTOR
    * 
    * STEP.H : ENDING
    * 
    *
*/

async function cachingDataSet(templateListToCache,specificDataSet='') {
    return new Promise(async (resolve_cachingDataSet) => {

        /*
        --------------------------------
        STEP.A : SET VARIABLES
        --------------------------------
        */

        const fs = require('fs').promises;

        const axios = require('axios').default;

        require('dotenv').config();

        const cachingOnRun_list = templateListToCache.split(",");

        let idGraph_path_client;

        let idGraph_path_server;

        /*
        --------------------------------
        STEP.B : SET FUNCTIONS
        --------------------------------
        */

        /*
        *
        * Function to convert an object from a json to a data variable suitable for a cy object
            * data_To_Cache : object from a json
        * 
        */
        async function makeDataCaching(dataINPUT,dataTEMPLATE,dataNAME) {

            let data_To_Cache = dataINPUT;

            /* Lighten the data */

            function extractLastPart(url) {
                const parts = url.split("/");
                return parts[parts.length - 1];
            };

            function isEmpty(value) {
                return value === "" || value === undefined || value === null || value === "undefined" || value === "null" ||
                (Array.isArray(value) && value.length === 0) ||
                (typeof value === "object" && Object.keys(value).length === 0) ||
                (typeof value === "number" && isNaN(value)) || value === "NaN";
            };

            function processObject(obj,includedKeys_urlExtractPart,excludedKeys_delete) {
                const includedKeys = includedKeys_urlExtractPart;
                const excludedKeys = excludedKeys_delete;
                for (const key in obj) {
                    if (isEmpty(obj[key])) {
                        delete obj[key];
                    } else if (excludedKeys.includes(key)) {
                        delete obj[key];
                    } else if (typeof obj[key] === "string" && obj[key].startsWith("https://") && includedKeys.includes(key)) {
                        obj[key] = extractLastPart(obj[key]);
                    } else if (typeof obj[key] === "object") {
                        processObject(obj[key],includedKeys_urlExtractPart,excludedKeys_delete);
                        if (isEmpty(obj[key])) {
                            delete obj[key];
                        };
                    };
                };
            };

            data_To_Cache.forEach(item => {
                let includedKeys_urlExtractPart;
                let excludedKeys_delete;
                includedKeys_urlExtractPart = ["id", "parent", "source", "target", "type"];
                excludedKeys_delete = ["sourcesNodes","position"];
                processObject(item,includedKeys_urlExtractPart,excludedKeys_delete);
                includedKeys_urlExtractPart = [""];
                excludedKeys_delete = ["sourcesNodes","ori_pos"];
                if (item.group === "edges") {processObject(item,includedKeys_urlExtractPart,excludedKeys_delete);};
            });

            /* Final postprocess with cytoscape functionnalies */

            const cytoscape = require('cytoscape');

            const cy = cytoscape({headless: true, styleEnabled: false,element: data_To_Cache});

            let rootsTEMP = cy.nodes().roots();
            cy.elements().breadthFirstSearch({
                root: rootsTEMP,
                visit: async function (v, e, u, i, depth) {
                    v.data('rank', depth + 1);
                },
            });

            data_To_Cache.forEach(item => {
                item.data.rank = cy.getElementById(item.data.id).data("rank");
            });

            /* Write the data */

            try {
                await fs.writeFile('server/_TEMPLATES/' + dataTEMPLATE + '/data_cache/' + dataNAME + '.json', JSON.stringify(data_To_Cache, null, 2));
            } catch (error) {
                console.error('Error writing file:', error);
            };

            const now = new Date();
            const year = now.getFullYear();
            const month = now.getMonth() + 1;
            const day = now.getDate();
            const hours = now.getHours();
            const minutes = now.getMinutes();
            const seconds = now.getSeconds();
            const timecode = `${year}_${month.toString().padStart(2, '0')}_${day.toString().padStart(2, '0')}_at_${hours.toString().padStart(2, '0')}_${minutes.toString().padStart(2, '0')}_${seconds.toString().padStart(2, '0')}`;
            try {
                await fs.writeFile('server/_TEMPLATES/'+dataTEMPLATE+'/data_cache/'+dataNAME+'_TIMECODE.txt', timecode);
            } catch (error) {
                console.error('Error writing file:', error);
            }

            console.log('Data has been cached ['+dataTEMPLATE+' / '+dataNAME+' / '+timecode+']');

        }

        /*
        *
        * Function to convert an object from a json to a data variable suitable for a cy object
            * dataINPUT : object from a json
        * 
        */
        async function processDataForCytoscape(dataINPUT) {
            
            let dataINPUT_nodes;

            let dataINPUT_edges;

            let dataReadyForCytoscape = [];

            if (Object.keys(dataINPUT)[0] === 'nodes' && Object.keys(dataINPUT)[1] === 'edges') {
                dataINPUT_nodes = dataINPUT['nodes'];
                dataINPUT_edges = dataINPUT['edges'];
            } else {
                dataINPUT_nodes = dataINPUT;
            }

            if (dataINPUT_nodes !== undefined && dataINPUT_nodes.length > 0) {
                dataReadyForCytoscape = dataReadyForCytoscape.concat(await getNodeData(dataINPUT_nodes));
            }

            if (dataINPUT_nodes !== undefined && dataINPUT_nodes.length > 0) {
                dataReadyForCytoscape = dataReadyForCytoscape.concat(await getEdgeData(dataINPUT_nodes, dataReadyForCytoscape, 'nodeData', 'sourcesNodes', 'direct'));
            }

            if (dataINPUT_edges !== undefined && dataINPUT_edges.length > 0) {
                dataReadyForCytoscape = dataReadyForCytoscape.concat(await getEdgeData(dataINPUT_edges, dataReadyForCytoscape, 'edgeData', '', ''));
            }

            /* Add the indirect edges */
            if (dataINPUT_nodes !== undefined && dataINPUT_nodes.length > 0) {
                dataReadyForCytoscape = dataReadyForCytoscape.concat(await getEdgeData(dataINPUT_nodes, dataReadyForCytoscape, 'nodeData', 'sourcesNodesIndirect', 'indirect'));
            }

            return dataReadyForCytoscape;

        }

        /*
        * 
        * Function to convert nodes attributes from a json object into a suitable node object and return a cy object
            * data : object from a json
            * The attributes are hardcoded in the following function.
            * The "id" attribute is complete with all the sourcesNodes (based on the first parent encounter in the "sourcesNodes" attribute).
        * 
        */
        async function getNodeData(data) {
            var networkNodes = [];

            data.forEach(async function (elem, index, array) {
                let objectNodes = {
                    group: 'nodes',
                    selectable: elem.selectable,
                    grabbable: elem.grabbable,
                    pannable: elem.pannable,
                    data: {
                        index: index,
                        id: elem.id,
                        lat: elem.lat,
                        lng: elem.lng,
                        label: elem.label,
                        title: elem.title,
                        titleComplete: elem.titleComplete,
                        other: elem.other,
                        image: elem.image,
                        style: elem.style,
                        group: elem.group,
                        searchTag: elem.searchTag,
                        parent: elem.parent,
                        sourcesNodes: elem.sourcesNodes,
                        sourcesNodesIndirect: elem.sourcesNodesIndirect,
                    }
                };

                if (elem.classes !== undefined) {
                    objectNodes['classes'] = "defaultNode"+" "+elem.classes;
                } else {
                    objectNodes['classes'] = "defaultNode";
                };

                if (elem.insensitiveNode === true || elem.insensitiveNode === "true") {
                    objectNodes['classes'] = objectNodes['classes']+" "+"insensitiveNode";
                };

                if (elem.additionalData !== undefined) {
                    for (dataOther in elem.additionalData) {
                        objectNodes['data'][dataOther] = elem.additionalData[dataOther];
                    }
                }

                if (elem.label !== undefined && elem.label !== "") {
                    if (elem.searchTag === undefined) {
                        objectNodes['data']['searchTag'] = { Labels: [objectNodes['data']['label']] };
                    } else {
                        objectNodes['data']['searchTag']['Labels'] = [objectNodes['data']['label']];
                    }
                }

                if (elem.title !== undefined && elem.title !== "") {
                    objectNodes['data']['title'] = objectNodes['data']['title'].replace(/src=\"/g, 'src="' + idGraph_path_client);
                }

                if (elem.titleComplete !== undefined && elem.titleComplete !== "") {
                    objectNodes['data']['titleComplete'] = objectNodes['data']['titleComplete'].replace(/src=\"/g, 'src="' + idGraph_path_client);
                }

                if (elem.image !== undefined && elem.image !== "") {
                    if (objectNodes['data']['image'].indexOf('http') === -1) {
                        objectNodes['data']['image'] = idGraph_path_client + objectNodes['data']['image'];
                    }
                }

                networkNodes.push(objectNodes);
            });

            return networkNodes;

        }

        /*
        *
        * Function to convert edges attributes from a json object into a suitable edge object and return a cy object
            * data : object from a json
            * The "from" and "to" attribute is complete with all the sources nodes (based on the first parent encounter in the "sourcesNodes" attribute).
        * 
        */
        function getEdgeData(data, currentData, typeData, sourceData, edgesType) {
            let networkEdges = [];

            function makeEdgesFromData(nodeFrom, nodeTo, edgesType) {

                let nodeFrom_TEMP;

                let nodeTo_TEMP;

                if (typeof nodeFrom === typeof '') {
                    nodeFrom_TEMP = nodeFrom;
                } else {
                    if (nodeFrom['id']) {
                        nodeFrom_TEMP = nodeFrom['id'];
                    };
                    if (nodeFrom['from']) {
                        nodeFrom_TEMP = nodeFrom['from'];
                    }
                }

                if (typeof nodeTo === typeof '') {
                    nodeTo_TEMP = nodeTo;
                } else {
                    if (nodeTo['id']) {
                        nodeTo_TEMP = nodeTo['id'];
                    };
                    if (nodeTo['to']) {
                        nodeTo_TEMP = nodeTo['to'];
                    }
                }

                let from_TEMP = currentData.find((item) => item.data && item.data.id === nodeFrom_TEMP);

                let to_TEMP = currentData.find((item) => item.data && item.data.id === nodeTo_TEMP);

                let idEdgeTemp;

                function extractLastPart(url) {
                    const parts = url.split("/");
                    return parts[parts.length - 1];
                };

                if (nodeTo['edge_id'] !== undefined) {
                    idEdgeTemp = extractLastPart(nodeTo['edge_id']);
                } else {
                    if (to_TEMP !== undefined) {
                        idEdgeTemp = extractLastPart(from_TEMP.data.id) + '_' + extractLastPart(to_TEMP.data.id);
                        let counter_idEdgeTemp = 0;
                        let targetObject = currentData.find(obj => obj.data && obj.data.id === idEdgeTemp);
                        let targetObject_BIS = networkEdges.find(obj => obj.data && obj.data.id === idEdgeTemp);
                        while (targetObject !== undefined || targetObject_BIS !== undefined) {
                            idEdgeTemp = from_TEMP.data.id + '_' + to_TEMP.data.id + '_' + counter_idEdgeTemp;
                            counter_idEdgeTemp = counter_idEdgeTemp + 1;
                            targetObject = currentData.find(obj => obj.data && obj.data.id === idEdgeTemp);
                            targetObject_BIS = networkEdges.find(obj => obj.data && obj.data.id === idEdgeTemp);
                        }
                    }
                }

                if (from_TEMP !== undefined && to_TEMP !== undefined) {

                    edgeTEMP = {
                        group: 'edges',
                        data: { id: idEdgeTemp, source: from_TEMP.data.id, target: to_TEMP.data.id },
                        classes: '',
                        style: {}
                    };

                    if (typeof nodeTo !== typeof '') {
                        edgeTEMP.style = nodeTo['style'];
                        edgeTEMP.data = Object.assign({}, edgeTEMP.data, nodeTo['data']);
                        if (nodeTo['classes'] !== undefined) {edgeTEMP.classes = edgeTEMP.classes+' '+nodeTo['classes'];};
                        if (edgeTEMP.data.indirectEdge === true) {
                            edgeTEMP.classes = edgeTEMP.classes+' '+'indirectEdge';
                            edgeTEMP.classes = edgeTEMP.classes+' '+'indirectEdge_hidden';
                        }
                    }

                    if (from_TEMP.data.insensitiveNode === true || to_TEMP.datainsensitiveNode === true) {
                        edgeTEMP.classes = edgeTEMP.classes+' '+'insensitiveEdge';
                    }

                    if (edgesType === 'indirect') {
                        edgeTEMP.classes = edgeTEMP.classes+' '+'indirectEdge';
                        edgeTEMP.classes = edgeTEMP.classes+' '+'indirectEdge_hidden';
                    }

                    networkEdges.push(edgeTEMP);

            };

            }

            if (typeData === 'nodeData') {
                data.forEach(async function (node, nodeIndex) {
                    if (node[sourceData] !== undefined) {
                        node[sourceData].forEach(async function (connId, cIndex, conns) {
                            if ((connId !== undefined && connId !== '') || (connId !== undefined && connId !== '')) {
                                if (!Array.isArray(connId)) {
                                    makeEdgesFromData(node, connId, edgesType);
                                } else {
                                    for (connId_i in connId) {makeEdgesFromData(node, connId[connId_i], edgesType);};
                                };
                            }
                        });
                    }
                });
            }

            if (typeData === 'edgeData') {
                data.forEach(async function (edge, edgeIndex) {
                    if (edge['from'] !== undefined && edge['to'] !== undefined && edge['from'] !== '' && edge['to'] !== '') {
                        makeEdgesFromData(edge, edge, edgesType);
                    }
                });
            }

            return networkEdges;

        }



        /*
        --------------------------------
        STEP.C : DATA CACHING
        --------------------------------
        */

        let chain_ProcessData = Promise.resolve();

        for (templateName in cachingOnRun_list) {

            console.log('Need to data cache : '+cachingOnRun_list[templateName]);

            idGraph_path_client = "/template_data/"+cachingOnRun_list[templateName]+"/";

            idGraph_path_server = "./server/_TEMPLATES/"+cachingOnRun_list[templateName]+"/";

            let dataset_LIST;
            try {
                dataset_LIST = require('../_TEMPLATES/'+cachingOnRun_list[templateName]+'/connector');
                console.log('Connector loaded for : '+cachingOnRun_list[templateName]);
            } catch (error) {
                console.log('Error loading data through connector.js : '+cachingOnRun_list[templateName]);
                dataset_LIST = {};
            };

            let dataPositions_LIST;
            try {
                dataPositions_LIST = require('../_TEMPLATES/'+cachingOnRun_list[templateName]+'/positions');
                console.log('Positions loaded for : '+cachingOnRun_list[templateName]);
            } catch (error) {
                console.log('No positions defined for : '+cachingOnRun_list[templateName]);
                dataPositions_LIST = {};
            };

            for (const dataset_KEY in dataset_LIST) {
                if (dataset_LIST.hasOwnProperty(dataset_KEY) && (specificDataSet === '' || specificDataSet.replace(".json","") === dataset_KEY)) {

                    let dataGraph_GET = dataset_LIST[dataset_KEY];

                    let dictForPostProd = {};

                    chain_ProcessData = chain_ProcessData.then(async function () {
                        return new Promise(async (resolve_ProcessData) => {

                            console.log('Processing data for : '+cachingOnRun_list[templateName]+' / '+dataset_KEY);

                            let chain_ScriptCachingData = Promise.resolve();

                            chain_ScriptCachingData = chain_ScriptCachingData.then(async function () {
                                return new Promise(async (resolve_ScriptCachingData) => {
                            
                                    if (typeof dataGraph_GET === typeof {}) {
                                        let switchModeDone = false;

                                        /* ___CUSTOM___ mode */
                                        if (Object.keys(dataGraph_GET)[0] === '___CUSTOM___') {
                                            switchModeDone = true;
                                            let chain_CustomLoading = Promise.resolve();
                                            let custom_mode_module = require('../_TEMPLATES/'+cachingOnRun_list[templateName]+'/'+dataGraph_GET["___CUSTOM___"]);
                                            chain_CustomLoading = chain_CustomLoading.then(async function () {
                                                return new Promise(async (resolve_CustomLoading) => {
                                                    resolve_CustomLoading(custom_mode_module.custom_mode_function(cachingOnRun_list[templateName],dataset_KEY));
                                                });
                                            });
                                            chain_CustomLoading = chain_CustomLoading.then(async function (value_CustomModeExport) {
                                                return new Promise(async (resolve_CustomLoading) => {
                                                    dataGraph_GET = value_CustomModeExport.modif_DataGraphGet;
                                                    resolve_CustomLoading(resolve_ScriptCachingData(value_CustomModeExport.modif_ResolveCustomLoading));
                                                });
                                            });
                                        };

                                        /* ___SEMAPPS___ mode */
                                        if (Object.keys(dataGraph_GET)[0] === '___SEMAPPS___') {
                                            switchModeDone = true;
                                                let dataResponse = [];
                                                let dataTraductor = {};
                                                let bulk_SemAppsCounter = 0;
                                                function loadSemAppsData(key_f, url_f, traductorSemApps_f) {
                                                    return new Promise(async (resolve) => {
                                                        console.log('Loading data : SemApps request (/' + url_f.substring(url_f.lastIndexOf('/') + 1) + ')');
                                                        const getSemAppsData = async (url) => {
                                                            return await axios
                                                                .get(url)
                                                                .then(async function (response) {
                                                                    console.log('SemApps data retrieved (/' + url_f.substring(url_f.lastIndexOf('/') + 1) + ')');
                                                                    let dataSemApps = response.data;
                                                                    if (key_f === 'traductor') {
                                                                        if (traductorSemApps_f !== undefined) {
                                                                            dataSemApps['ldp:contains'].forEach(async function (itemSemApps) {
                                                                                dataTraductor[itemSemApps[traductorSemApps_f[0]]] = itemSemApps[traductorSemApps_f[1]];
                                                                            });
                                                                        }
                                                                    } else {
                                                                        if (key_f === 'dict') {
                                                                            let listToRetrieve = [];
                                                                            dataSemApps['ldp:contains'].forEach(async function (itemSemApps) {
                                                                                let attributesToRetrieve = {};
                                                                                for (itemConfig in traductorSemApps_f) {
                                                                                    attributesToRetrieve[traductorSemApps_f[itemConfig]] = itemSemApps[traductorSemApps_f[itemConfig]];
                                                                                }
                                                                                listToRetrieve.push(attributesToRetrieve);
                                                                            });
                                                                            dictForPostProd[url_f] = listToRetrieve;
                                                                        } else {
                                                                            if (Object.keys(dataTraductor).length > 0) {
                                                                                dataSemApps['ldp:contains'].forEach(async function (itemSemApps) {
                                                                                    for (keySemApps in itemSemApps) {
                                                                                        if (typeof itemSemApps[keySemApps] === typeof []) {
                                                                                            for (i_itemSemApps in itemSemApps[keySemApps]) {
                                                                                                if (dataTraductor[itemSemApps[keySemApps][i_itemSemApps]] !== undefined) {
                                                                                                    itemSemApps[keySemApps][i_itemSemApps] = dataTraductor[itemSemApps[keySemApps][i_itemSemApps]];
                                                                                                }
                                                                                            }
                                                                                        } else {
                                                                                            if (dataTraductor[itemSemApps[keySemApps]] !== undefined) {
                                                                                                itemSemApps[keySemApps] = dataTraductor[itemSemApps[keySemApps]];
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                            };
                                                                            if (traductorSemApps_f !== undefined) {
                                                                                dataSemApps["ldp:contains"].forEach(async function (itemSemApps) {
                                                                                    for (keySemApps in itemSemApps) {
                                                                                        if (traductorSemApps_f[keySemApps] !== keySemApps) {
                                                                                            if (traductorSemApps_f[keySemApps] !== undefined) {
                                                                                                if (!(Object.keys(itemSemApps[keySemApps]).length === 0 && itemSemApps[keySemApps].constructor === Object)) {
                                                                                                    if (Array.isArray(itemSemApps[keySemApps])) {
                                                                                                        itemSemApps[keySemApps] = itemSemApps[keySemApps].filter(async function (e) {
                                                                                                            return !(Object.keys(e).length === 0 && e.constructor === Object);
                                                                                                        });
                                                                                                    };
                                                                                                    if (!(traductorSemApps_f[keySemApps] in itemSemApps)) {
                                                                                                        itemSemApps[traductorSemApps_f[keySemApps]] = itemSemApps[keySemApps];
                                                                                                    } else {
                                                                                                        if (Array.isArray(itemSemApps[traductorSemApps_f[keySemApps]])) {
                                                                                                            if (Array.isArray(itemSemApps[keySemApps])) {
                                                                                                                for (i_itemSemApps in itemSemApps[keySemApps]) {
                                                                                                                    itemSemApps[traductorSemApps_f[keySemApps]].push(itemSemApps[keySemApps][i_itemSemApps]);
                                                                                                                };
                                                                                                            } else {
                                                                                                                itemSemApps[traductorSemApps_f[keySemApps]].push(itemSemApps[keySemApps]);
                                                                                                            };	
                                                                                                        } else {
                                                                                                            if (Array.isArray(itemSemApps[keySemApps])) {
                                                                                                                itemSemApps[traductorSemApps_f[keySemApps]] = [itemSemApps[traductorSemApps_f[keySemApps]]];
                                                                                                                for (i_itemSemApps in itemSemApps[keySemApps]) {
                                                                                                                    itemSemApps[traductorSemApps_f[keySemApps]].push(itemSemApps[keySemApps][i_itemSemApps]);
                                                                                                                };
                                                                                                            } else {
                                                                                                                itemSemApps[traductorSemApps_f[keySemApps]] = [itemSemApps[traductorSemApps_f[keySemApps]],itemSemApps[keySemApps]];
                                                                                                            };
                                                                                                        };
                                                                                                    };
                                                                                                };
                                                                                                delete itemSemApps[keySemApps];
                                                                                            };
                                                                                        };
                                                                                    };
                                                                                });
                                                                            };
                                                                            let tempDict = {};
                                                                            tempDict[key_f] = dataSemApps['ldp:contains'];
                                                                            tempDict['config'] = dataGraph_GET['___SEMAPPS___']['mainConfig'];
                                                                            dataResponse.push(tempDict);
                                                                        }
                                                                    }
                                                                    bulk_SemAppsCounter += 1;
                                                                    if (bulk_SemAppsCounter === dataGraph_GET['___SEMAPPS___']['urls'].length) {
                                                                        dataGraph_GET = { semapps: 'alreadyLoaded' };
                                                                        resolve_ScriptCachingData(dataResponse);
                                                                    }
                                                                    resolve('Done');
                                                                })
                                                                .catch(async function (error) {
                                                                    console.error("Error on processing SemApps data : ", error);
                                                                });
                                                        };
                                                        getSemAppsData(url_f);
                                                    });
                                                }
                            
                                                let chain_SemAppsData = Promise.resolve();
                                                dataGraph_GET['___SEMAPPS___']['urls'].forEach(async function (url_SemApps) {
                                                    let tempSemAppsToLoad = url_SemApps;
                                                    chain_SemAppsData = chain_SemAppsData.then(async function () {
                                                        return new Promise(async (resolve_SemAppsData) => {
                                                            let tempSemAppsToLoad_FIX = tempSemAppsToLoad;
                                                            if (tempSemAppsToLoad_FIX['config'] !== '' && tempSemAppsToLoad_FIX['config'] !== undefined && typeof tempSemAppsToLoad_FIX['config'] !== typeof []) {
                                                                function processData(allText) {
                                                                    let semAppsConfig = JSON.parse(allText.replace(/(\r\n|\n|\r|\t)/gm, ''));
                                                                    resolve_SemAppsData(semAppsConfig);
                                                                };
                                                                await fs.readFile(idGraph_path_server + tempSemAppsToLoad_FIX['config'], 'utf8', (error, data) => {
                                                                    if (error) {
                                                                        console.error('Error reading config file:', error);
                                                                        return;
                                                                    }
                                                                    processData(data);
                                                                });
                                                            } else {
                                                                if (typeof tempSemAppsToLoad_FIX['config'] === typeof []) {
                                                                    resolve_SemAppsData({ traductor: tempSemAppsToLoad_FIX['config'] });
                                                                } else {
                                                                    resolve_SemAppsData('');
                                                                }
                                                            }
                                                        });
                                                    });
                                                    chain_SemAppsData = chain_SemAppsData.then(async function (value_SemAppsData) {
                                                        return new Promise(async (resolve_SemAppsData) => {
                                                            let tempSemAppsToLoad_FIX = tempSemAppsToLoad;
                                                            let traductorSemApps;
                                                            if (value_SemAppsData === '') {
                                                                traductorSemApps = {};
                                                            } else {
                                                                traductorSemApps = value_SemAppsData['traductor'];
                                                            }
                                                            if (Object.keys(value_SemAppsData)[0] === 'traductor') {
                                                                traductorSemApps = value_SemAppsData['traductor'];
                                                            }
                                                            resolve_SemAppsData(loadSemAppsData(Object.keys(tempSemAppsToLoad_FIX)[0], tempSemAppsToLoad_FIX[Object.keys(tempSemAppsToLoad_FIX)[0]], traductorSemApps));
                                                        });
                                                    });
                                                });
                                        };

                                        /* ___GOGOCARTO___ mode */
                                        if (Object.keys(dataGraph_GET)[0] === '___GOGOCARTO___') {
                                            switchModeDone = true;
                                                let dataResponse = [];
                                                console.log('Data loading : Gogocarto requesting (/' + dataGraph_GET['___GOGOCARTO___'][0]['data'] + ')');
                                                const getGogocartoData = async (url) => {
                                                    return await axios
                                                        .get(url)
                                                        .then(async function (response) {
                                                            console.log('Gogocarto data retrieved (/' + dataGraph_GET['___GOGOCARTO___'][0]['data'] + ')');
                                                            let dataGogocarto = response.data;
                                                            dataResponse = [{
                                                                data: dataGogocarto.data,
                                                                config: dataGraph_GET['___GOGOCARTO___'][0]['config']
                                                            }];
                                                            dataGraph_GET = { gogocarto: 'alreadyLoaded' };
                                                            resolve_ScriptCachingData(dataResponse);
                                                        })
                                                        .catch(async function (error) {
                                                            console.error("Error on processing Gogocarto data : ", error);
                                                        });
                                                };
                                                getGogocartoData(dataGraph_GET['___GOGOCARTO___'][0]['data']);
                                        };
                                        
                                        /* other modes */
                                        if (switchModeDone === false) {
                                            resolve_ScriptCachingData('Done');
                                        }
                                    } else {
                                        resolve_ScriptCachingData('Done');
                                    }
                                });
                            });
                            
                            chain_ScriptCachingData = chain_ScriptCachingData.then(async function (data_loaded) {
                                return new Promise(async (resolve_ScriptCachingData) => {
                            
                                    /* Loading the datas */
                                    let promise_DataLoaded = new Promise(async function (resolveDataLoaded) {
                            
                                        /* Mode ONLY NODES - Native workaround */
                                        if (Array.isArray(dataGraph_GET)) {
                                            resolveDataLoaded({ jsData: dataGraph_GET });
                                        }
                            
                                        /* Mode MULTI FORMAT */
                                        if (typeof dataGraph_GET === typeof {}) {

                                            /* Native workaround */
                                            if (Object.keys(dataGraph_GET)[0] === 'nodes' && Object.keys(dataGraph_GET)[1] === 'edges') {
                                                resolveDataLoaded({ jsData: dataGraph_GET });
                                            };

                                            /* SemApps workaround */
                                            if (dataGraph_GET['semapps'] === 'alreadyLoaded') {
                                                resolveDataLoaded({ multiFormatData: data_loaded });
                                            };

                                            /* Gogocarto workaround */
                                            if (dataGraph_GET['gogocarto'] === 'alreadyLoaded') {
                                                resolveDataLoaded({ multiFormatData: data_loaded });
                                            };

                                            /* CSV workaround */
                                            if (Object.keys(dataGraph_GET)[0] === 'csv') {
                                                var dataCounter = 1;
                                                var output_data = [];
                                                let chain_LoadCsv = Promise.resolve();
                                                // const csvParser = require('csv-parser');
                                                for (data_to_load in dataGraph_GET['csv']) {
                                                    /* Initiate jquery */
                                                    const jsdom = require('jsdom');
                                                    const { JSDOM } = jsdom;
                                                    const { window } = new JSDOM();
                                                    const $ = require('jquery')(window);
                                                    const csv = require('jquery-csv');
                                                    /* Get the data */
                                                    chain_LoadCsv = chain_LoadCsv.then(async function () {
                                                        return new Promise(async (resolve_LoadCsv) => {
                                                            let data_to_load_CURRENT = dataCounter - 1;
                                                            if (dataGraph_GET['csv'][data_to_load_CURRENT] !== 'alreadyLoaded') {
                                                                const csvParser = require('csv-parser');
                                                                const { Readable } = require('stream');
                                                                let keyDataToUse = Object.keys(dataGraph_GET['csv'][data_to_load_CURRENT])[0];
                                                                const url = idGraph_path_server + dataGraph_GET['csv'][data_to_load_CURRENT][keyDataToUse];
                                                                async function processData(filepath, keyDataToUse, data_to_load_CURRENT) {
                                                                    const csv_raw = filepath;
                                                                    const csvObjects = [];
                                                                    try {
                                                                        const fileContent = await fs.readFile(csv_raw, 'utf-8');
                                                                        const readableStream = Readable.from(fileContent.split('\n'));
                                                                        return new Promise((resolveProcessData, rejectProcessData) => {
                                                                            readableStream
                                                                                .pipe(csvParser())
                                                                                .on('data', row => {
                                                                                    const cleanedRow = {};
                                                                                    for (const key in row) {
                                                                                        const cleanedKey = key.replace(/'/g, 'a').replace(/^\uFEFF/, '');
                                                                                        cleanedRow[cleanedKey] = row[key];
                                                                                    }
                                                                                    csvObjects.push(cleanedRow);
                                                                                })
                                                                                .on('end', () => {
                                                                                    const itemDataTemp = {};
                                                                                    itemDataTemp[keyDataToUse] = csvObjects;
                                                                                    itemDataTemp['config'] = dataGraph_GET['csv'][data_to_load_CURRENT]['config'];
                                                                                    output_data.push(itemDataTemp);
                                                                                    if (dataCounter === dataGraph_GET['csv'].length) {
                                                                                        resolveDataLoaded({ multiFormatData: output_data });
                                                                                    }
                                                                                    dataCounter += 1;
                                                                                    resolve_LoadCsv("Done");
                                                                                    resolveProcessData("Done");
                                                                                })
                                                                                .on('error', (error) => {
                                                                                    rejectProcessData(error);
                                                                                });
                                                                        });
                                                                    } catch (error) {
                                                                        console.error('Erreur on data loading :', error);
                                                                        throw error;
                                                                    }
                                                                };
                                                                processData(url, keyDataToUse, data_to_load_CURRENT).catch(error => {
                                                                    console.error('Error on processData :', error);
                                                                });
                                                            } else {
                                                                if (data_loaded.length === 1) {
                                                                    output_data.push({
                                                                        data: csv.toObjects(data_loaded[0]['data']),
                                                                        config: data_loaded[0]['config'],
                                                                    });
                                                                } else {
                                                                    output_data.push({
                                                                        nodes: csv.toObjects(data_loaded[0]['nodes']),
                                                                        config: data_loaded[0]['config'],
                                                                    });
                                                                    output_data.push({
                                                                        edges: csv.toObjects(data_loaded[1]['edges']),
                                                                        config: data_loaded[1]['config'],
                                                                    });
                                                                }
                                                                resolve_LoadCsv('Done');
                                                                if (dataCounter === dataGraph_GET['csv'].length) {
                                                                    resolveDataLoaded({ multiFormatData: output_data });
                                                                }
                                                                dataCounter = dataCounter + 1;
                                                            }
                                                        });
                                                    });
                                                }
                                            };

                                        }
                                    });
                            
                                    /* Preparing the datas of the network */
                                    promise_DataLoaded.then(async function (value) {

                                        let chain_LoadData = Promise.resolve();
                            
                                        /* Mode Native */
                                        if (value.jsData !== undefined) {
                                            chain_LoadData = chain_LoadData.then(async function () {
                                                return new Promise(async (resolve_LoadData) => {
                                                    dataGraph = value.jsData;
                                                    resolve_LoadData(dataGraph);
                                                });
                                            });
                                        };
                            
                                        /* Mode MULTI FORMAT */
                                        if (value.multiFormatData !== undefined) {
                                            let dataCounter = 1;
                                            let dataGraph_BIS = { nodes: [], edges: [] };
                                            for (config_to_load_NUM in value.multiFormatData) {
                                                chain_LoadData = chain_LoadData.then(async function () {
                                                    return new Promise(async (resolve_LoadData) => {
                                                        let config_to_load = dataCounter - 1;
                                                        if (value.multiFormatData[config_to_load]['config'] !== '') {
                                                            /* Function to read the config json associate to the data */
                                                            async function processConfig(allText) {
                                                                /* Make a json from data */
                                                                var csvConfig = JSON.parse(allText.replace(/(\r\n|\n|\r|\t)/gm, ''));
                                                                /* For each config field push the dataGraph variable */
                                                                if ('data' in value.multiFormatData[config_to_load] || 'nodes' in value.multiFormatData[config_to_load]) {
                                                                    let keyDataToUse;
                                                                    if ('data' in value.multiFormatData[config_to_load]) {
                                                                        keyDataToUse = 'data';
                                                                    }
                                                                    if ('nodes' in value.multiFormatData[config_to_load]) {
                                                                        keyDataToUse = 'nodes';
                                                                    }
                                                                    for (csvLine in value.multiFormatData[config_to_load][keyDataToUse]) {
                                                                        var lineTEMP = {};
                                                                        /* nativeFields */
                                                                        for (elemConfig in csvConfig['nativeFields']) {
                                                                            if (csvConfig['nativeFields'][elemConfig] === '' && elemConfig === 'id') {
                                                                                lineTEMP['id'] = 'id_' + csvLine;
                                                                            }
                                                                            if (csvConfig['nativeFields'][elemConfig] !== '') {
                                                                                lineTEMP[elemConfig] = value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig['nativeFields'][elemConfig]];
                                                                            }
                                                                        }
                                                                        /* sourcesNodesFields */
                                                                        var sourceFieldsTEMP = [];
                                                                        for (elemConfig in csvConfig['sourcesNodesFields']) {
                                                                            var sourceTemp = value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig['sourcesNodesFields'][elemConfig]];
                                                                            if (sourceTemp !== '') {
                                                                                sourceFieldsTEMP.push(sourceTemp);
                                                                            }
                                                                        }
                                                                        if (sourceFieldsTEMP.length > 0) {
                                                                            lineTEMP['sourcesNodes'] = sourceFieldsTEMP;
                                                                        }
                                                                        /* classFields */
                                                                        var classFieldsTEMP = '';
                                                                        for (elemConfig in csvConfig['classFields']) {
                                                                            var classTemp = value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig['classFields'][elemConfig]];
                                                                            if (classTemp !== '') {
                                                                                classFieldsTEMP += classTemp;
                                                                            }
                                                                        }
                                                                        if (classFieldsTEMP !== '') {
                                                                            lineTEMP['classes'] = classFieldsTEMP;
                                                                        }
                                                                        /* dataFields and coordFields */
                                                                        var dataFieldsTEMP = {};
                                                                        for (elemConfig in csvConfig['dataFields']) {
                                                                            dataFieldsTEMP[csvConfig['dataFields'][elemConfig]] = value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig['dataFields'][elemConfig]];
                                                                        }
                                                                        if (csvConfig['coordFields']['lng'] !== '' && csvConfig['coordFields']['lat'] !== '') {
                                                                            dataFieldsTEMP['lng'] = parseFloat(value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig['coordFields']['lng']]);
                                                                            dataFieldsTEMP['lat'] = parseFloat(value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig['coordFields']['lat']]);
                                                                        }
                                                                        if (Object.keys(dataFieldsTEMP).length !== 0) {
                                                                            lineTEMP['additionalData'] = dataFieldsTEMP;
                                                                        }
                                                                        /* titleFields */
                                                                        var titleCompleteTEMP = '';
                                                                        for (elemConfig in csvConfig['titleFields']) {
                                                                            if (typeof csvConfig['titleFields'][elemConfig] === typeof {}) {
                                                                                var title_tmp = csvConfig['titleFields'][elemConfig][1];
                                                                                var value_tmp = value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig['titleFields'][elemConfig][0]];
                                                                            } else {
                                                                                var title_tmp = csvConfig['titleFields'][elemConfig];
                                                                                var value_tmp = value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig['titleFields'][elemConfig]];
                                                                            }
                                                                            titleCompleteTEMP = titleCompleteTEMP + '<p><b>' + title_tmp + ' : </b>' + value_tmp + '</p>';
                                                                        }
                                                                        if (lineTEMP['titleComplete'] === undefined) {
                                                                            if (titleCompleteTEMP !== '') {
                                                                                lineTEMP['titleComplete'] = titleCompleteTEMP;
                                                                            }
                                                                        } else {
                                                                            lineTEMP['titleComplete'] = lineTEMP['titleComplete'] + '<br><br>' + titleCompleteTEMP;
                                                                        }
                                                                        var searchTagTEMP = {};
                                                                        /* tagFields */
                                                                        for (elemConfig in csvConfig['tagFields']) {
                                                                            if (typeof csvConfig['tagFields'][elemConfig] === typeof {}) {
                                                                                let valueTagTemp = value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig['tagFields'][elemConfig][0]];
                                                                                if (typeof valueTagTemp === typeof []) {
                                                                                    searchTagTEMP[csvConfig['tagFields'][elemConfig][1]] = valueTagTemp;
                                                                                } else {
                                                                                    searchTagTEMP[csvConfig['tagFields'][elemConfig][1]] = [valueTagTemp];
                                                                                }
                                                                            } else {
                                                                                let valueTagTemp = value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig['tagFields'][elemConfig]];
                                                                                if (typeof valueTagTemp === typeof []) {
                                                                                    searchTagTEMP[csvConfig['tagFields'][elemConfig]] = valueTagTemp;
                                                                                } else {
                                                                                    searchTagTEMP[csvConfig['tagFields'][elemConfig]] = [valueTagTemp];
                                                                                }
                                                                            }
                                                                        }
                                                                        if (Object.keys(searchTagTEMP).length > 0) {
                                                                            lineTEMP['searchTag'] = searchTagTEMP;
                                                                        }
                                                                        /* positionFields */
                                                                        if (csvConfig['positionFields']['x'] !== '' && csvConfig['positionFields']['y'] !== '') {
                                                                            lineTEMP['position'] = {
                                                                                x: parseFloat(value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig['positionFields']['x']]),
                                                                                y: parseFloat(value.multiFormatData[config_to_load][keyDataToUse][csvLine][csvConfig['positionFields']['y']]),
                                                                            };
                                                                        }
                                                                        dataGraph_BIS['nodes'].push(lineTEMP);
                                                                    }
                                                                }
                                                                if ('edges' in value.multiFormatData[config_to_load]) {
                                                                    for (csvLine in value.multiFormatData[config_to_load]['edges']) {
                                                                        var lineTEMP = {};
                                                                        /* edgesNativeFields */
                                                                        for (elemConfig in csvConfig['edgesNativeFields']) {
                                                                            if (csvConfig['edgesNativeFields'][elemConfig] === '' && elemConfig === 'edge_id') {
                                                                                lineTEMP['id'] = 'id_' + csvLine;
                                                                            }
                                                                            if (csvConfig['edgesNativeFields'][elemConfig] !== '') {
                                                                                lineTEMP[elemConfig] = value.multiFormatData[config_to_load]['edges'][csvLine][csvConfig['edgesNativeFields'][elemConfig]];
                                                                            }
                                                                        }
                                                                        /* edgesDataFields */
                                                                        var dataFieldsTEMP = {};
                                                                        for (elemConfig in csvConfig['edgesDataFields']) {
                                                                            dataFieldsTEMP[csvConfig['edgesDataFields'][elemConfig]] =
                                                                                value.multiFormatData[config_to_load]['edges'][csvLine][csvConfig['edgesDataFields'][elemConfig]];
                                                                        }
                                                                        if (Object.keys(dataFieldsTEMP).length !== 0) {
                                                                            lineTEMP['data'] = dataFieldsTEMP;
                                                                        }
                                                                        /* edgeStyleFields */
                                                                        var sourceFieldsTEMP = {};
                                                                        for (elemConfig in csvConfig['edgeStyleFields']) {
                                                                            if (typeof csvConfig['edgeStyleFields'][elemConfig] === typeof {}) {
                                                                                sourceFieldsTEMP[csvConfig['edgeStyleFields'][elemConfig][1]] =
                                                                                    value.multiFormatData[config_to_load]['edges'][csvLine][csvConfig['edgeStyleFields'][elemConfig][0]];
                                                                            } else {
                                                                                sourceFieldsTEMP[csvConfig['edgeStyleFields'][elemConfig]] =
                                                                                    value.multiFormatData[config_to_load]['edges'][csvLine][csvConfig['edgeStyleFields'][elemConfig]];
                                                                            }
                                                                        }
                                                                        if (Object.keys(dataFieldsTEMP).length !== 0) {
                                                                            lineTEMP['style'] = sourceFieldsTEMP;
                                                                        }
                                                                        /* push step */
                                                                        dataGraph_BIS['edges'].push(lineTEMP);
                                                                    }
                                                                }
                                                                if (dataCounter === value.multiFormatData.length) {
                                                                    resolve_LoadData(dataGraph_BIS);
                                                                } else {
                                                                    resolve_LoadData('NextData');
                                                                }
                                                                dataCounter = dataCounter + 1;
                                                            }
                                                            /* Load the config json associate to the data */
                                                            try {
                                                                const data = await fs.readFile(idGraph_path_server + value.multiFormatData[config_to_load]['config'], 'utf8');
                                                                await processConfig(data);
                                                            } catch (error) {
                                                                console.error('Error reading config file:', error);
                                                            };
                                                        } else {
                                                            var switchLine = 0;
                                                            if ('data' in value.multiFormatData[config_to_load] || 'nodes' in value.multiFormatData[config_to_load]) {
                                                                let keyDataToUse;
                                                                if ('data' in value.multiFormatData[config_to_load]) {
                                                                    keyDataToUse = 'data';
                                                                }
                                                                if ('nodes' in value.multiFormatData[config_to_load]) {
                                                                    keyDataToUse = 'nodes';
                                                                }
                                                                for (csvLine in value.multiFormatData[config_to_load][keyDataToUse]) {
                                                                    var lineTEMP = {};
                                                                    var dataFieldsTEMP = {};
                                                                    var titleCompleteTEMP = '';
                                                                    for (lineCell in value.multiFormatData[config_to_load][keyDataToUse][csvLine]) {
                                                                        if (switchLine === 0) {
                                                                            lineTEMP['id'] = value.multiFormatData[config_to_load][keyDataToUse][csvLine][lineCell];
                                                                            lineTEMP['label'] = value.multiFormatData[config_to_load][keyDataToUse][csvLine][lineCell];
                                                                        } else {
                                                                            dataFieldsTEMP[lineCell] = value.multiFormatData[config_to_load][keyDataToUse][csvLine][lineCell];
                                                                            titleCompleteTEMP =
                                                                                titleCompleteTEMP + '<p><b>' + lineCell + ' : </b>' + value.multiFormatData[config_to_load][keyDataToUse][csvLine][lineCell] + '</p>';
                                                                        }
                                                                        switchLine = 1;
                                                                    }
                                                                    if (Object.keys(dataFieldsTEMP).length !== 0) {
                                                                        lineTEMP['additionalData'] = dataFieldsTEMP;
                                                                    }
                                                                    if (titleCompleteTEMP !== '') {
                                                                        lineTEMP['titleComplete'] = titleCompleteTEMP;
                                                                    }
                                                                    dataGraph_BIS['nodes'].push(lineTEMP);
                                                                    switchLine = 0;
                                                                }
                                                            }
                                                            if ('edges' in value.multiFormatData[config_to_load]) {
                                                                for (csvLine in value.multiFormatData[config_to_load]['edges']) {
                                                                    var lineTEMP = {};
                                                                    lineTEMP['from'] = value.multiFormatData[config_to_load]['edges'][csvLine]['from'];
                                                                    lineTEMP['to'] = value.multiFormatData[config_to_load]['edges'][csvLine]['to'];
                                                                    dataGraph_BIS['edges'].push(lineTEMP);
                                                                }
                                                            }
                                                            if (dataCounter === value.multiFormatData.length) {
                                                                resolve_LoadData(dataGraph_BIS);
                                                            } else {
                                                                resolve_LoadData('NextData');
                                                            }
                                                            dataCounter = dataCounter + 1;
                                                        }
                                                    });
                                                });
                                            }
                                        };
                            
                                        chain_LoadData = chain_LoadData.then(async function (value_LoadData) {
                                            return new Promise(async (resolve_LoadData) => {
                                                resolve_LoadData(resolve_ScriptCachingData("Done"));
                                                resolve_ProcessData(await processDataForCytoscape(value_LoadData));
                                            });
                                        });

                                    });
                                });
                            });

                        });
                    });

                    /*
                    --------------------------------
                    STEP.D : POST PROCESSING
                    --------------------------------
                    */

                    chain_ProcessData = chain_ProcessData.then(async function (value_LoadData) {
                        return new Promise(async (resolve_ProcessData) => {
                            try {
                                let postprocessModule = require('../_TEMPLATES/'+cachingOnRun_list[templateName]+'/postprod');
                                let postprocessedData = await postprocessModule.postprocessing(value_LoadData,dictForPostProd);
                                resolve_ProcessData(postprocessedData);
                            } catch (error) {
                                if (error && error.message && !error.message.includes("Cannot find module")) {
                                    console.log(error);
                                } else {
                                    console.log('No postprocess module')
                                };
                                resolve_ProcessData(value_LoadData);
                            };
                        });
                    });                  

                    /*
                    --------------------------------
                    STEP.E : LOADING POSITIONS
                    --------------------------------
                    */

                    chain_ProcessData = chain_ProcessData.then(async function (value_LoadData) {
                        return new Promise(async (resolve_ProcessData) => {
                            if (dataPositions_LIST[dataset_KEY] !== undefined) {

                                async function loadingPositions(data) {
                                    let positions = JSON.parse(data.replace(/(\r\n|\n|\r|\t)/gm, ''));
                                    let dataWithPositions = value_LoadData;
                                    for (const position_KEY in positions) {
                                        let indexToChange = dataWithPositions.findIndex(item => item.data.id === position_KEY);
                                        if (indexToChange !== -1) {
                                            dataWithPositions[indexToChange].data.ori_pos = positions[position_KEY];
                                            dataWithPositions[indexToChange].position = positions[position_KEY];
                                        };
                                    };
                                    resolve_ProcessData(dataWithPositions);
                                };

                                try {
                                    const data = await fs.readFile(idGraph_path_server + dataPositions_LIST[dataset_KEY], 'utf8');
                                    loadingPositions(data);
                                } catch (error) {
                                    console.error('Error reading position file:', error);
                                };

                            } else {
                                console.error('No position file for this dataset');
                                resolve_ProcessData(value_LoadData);
                            };
                        });
                    });

                    /*
                    --------------------------------
                    STEP.F : CACHING DATA
                    --------------------------------
                    */

                    chain_ProcessData = chain_ProcessData.then(async function (value_LoadData) {
                        return new Promise(async (resolve_ProcessData) => {
                            resolve_ProcessData(makeDataCaching(value_LoadData,cachingOnRun_list[templateName],dataset_KEY));
                        });
                    });

                    /*
                    --------------------------------
                    STEP.G : EXPORT TRADUCTOR
                    --------------------------------
                    */

                    chain_ProcessData = chain_ProcessData.then(async function () {
                        return new Promise(async (resolve_ProcessData) => {
                            try {
                                traductorToExport = require('../_TEMPLATES/'+cachingOnRun_list[templateName]+'/traductor');
                                console.log('Traductor loaded for : '+cachingOnRun_list[templateName]);
                                let chain_TraductorCustom = Promise.resolve();
                                chain_TraductorCustom = chain_TraductorCustom.then(async function () {
                                    return new Promise(async (resolve_TraductorCustom) => {
                                        resolve_TraductorCustom(traductorToExport.custom_traductor_function(cachingOnRun_list[templateName],dataset_KEY,idGraph_path_server));
                                    });
                                });
                                chain_TraductorCustom = chain_TraductorCustom.then(async function (value_TraductorCustom) {
                                    return new Promise(async (resolve_TraductorCustom) => {
                                        resolve_ProcessData(resolve_TraductorCustom(await fs.writeFile('server/_TEMPLATES/'+cachingOnRun_list[templateName]+'/data_cache/'+dataset_KEY+'_TRADUCTOR.json', JSON.stringify(value_TraductorCustom, null, 2))));
                                    });
                                });
                            } catch (error) {
                                console.log('No traductor defined for : '+cachingOnRun_list[templateName]);
                                resolve_ProcessData("Done");
                            };
                        });
                    });

                };
            };

        };

        /*
        --------------------------------
        STEP.H : ENDING
        --------------------------------
        */

        chain_ProcessData = chain_ProcessData.then(async function () {
            return new Promise(async (resolve_ProcessData) => {
                console.log('All datasets has been cached')
                resolve_ProcessData(resolve_cachingDataSet("updated"));
            });
        });

    });
};

module.exports = {
    cachingDataSet
};
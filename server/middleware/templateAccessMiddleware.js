

const listOfTemplatesToServe = (includeFunctionalTemplates = true) =>{
    let templates_list_to_serve = [];
    if (process.env.CONFIG_STAND_ALONE_VERSION === "") {
        if (process.env.CONFIG_ALL_TEMPLATES === "true") {
            const fs = require('fs');
            const path = require('path');
            const templatesDir = path.join(__dirname, '../_TEMPLATES');
            templates_list_to_serve = fs.readdirSync(templatesDir);
        } else {
            templates_list_to_serve = process.env.CONFIG_DEFAULT_IDS.split(",");
            if (includeFunctionalTemplates && process.env.DISPLAY_HOME_CREATE_GRAPH === "true") { templates_list_to_serve = templates_list_to_serve.concat("grezi_create_graph"); };
        };
    } else {
        templates_list_to_serve = [process.env.CONFIG_STAND_ALONE_VERSION];
    };
    return templates_list_to_serve;
};

const restrictTemplateAccess = (req, res, next) => {
    const templates_list = listOfTemplatesToServe(true);
    let requestedTemplate = '';
    if (Object.keys(req.params).length === 0) {
        requestedTemplate = req.baseUrl.split('/')[2];
    } else {
        requestedTemplate = req.params.directId;
    };
    if (!requestedTemplate || requestedTemplate.trim() === '') {
        console.log('Bad Request: Template name is missing or empty.');
        res.redirect('/');
    }
    if (templates_list.includes(requestedTemplate)) {
        next();
    } else {
        console.log('Bad Request: Template name "'+requestedTemplate+'" is wrong or forbidden.')
        res.redirect('/');
    }
};

module.exports = {
    listOfTemplatesToServe,
    restrictTemplateAccess
};

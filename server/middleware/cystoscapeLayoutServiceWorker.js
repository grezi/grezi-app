const { parentPort } = require('worker_threads');
const cytoscape = require('cytoscape');

const fcose = require('cytoscape-fcose');
cytoscape.use(fcose);

const coseBilkent = require('cytoscape-cose-bilkent');
cytoscape.use(coseBilkent);

const dagre = require('cytoscape-dagre');
cytoscape.use(dagre);

parentPort.on('message', (data) => {

    const { elements, layoutOptions } = data;

    const jsdom = require('jsdom');
    const { JSDOM } = jsdom;
    const width = 1920;
    const height = 1080;
    const dom = new JSDOM('<!DOCTYPE html><body></body></html>', {
      pretendToBeVisual: true,
      resources: "usable",
      runScripts: "dangerously",
      beforeParse(window) {
        window.innerWidth = width;
        window.innerHeight = height;
        window.outerWidth = width;
        window.outerHeight = height;
        window.screen.width = width;
        window.screen.height = height;
        window.screen.availWidth = width;
        window.screen.availHeight = height;
        // window.document.documentElement.clientWidth = width;
        // window.document.documentElement.clientHeight = height;
      }
    });
    global.window = dom.window;
    global.document = dom.window.document;
    // global.document.documentElement.getClientWidth = () => width;
    // global.document.documentElement.getClientHeight = () => height;
    // global.window.document.documentElement.clientWidth = width;
    // global.window.document.documentElement.clientHeight = height;

    // const div = document.createElement('div');
    // div.style.width = width + 'px';
    // div.style.height = height + 'px';

    let styleToRetrieve = [
      
        {
            selector: '.defaultNode',
            style: {

                'width': '30px',
                'height': '30px',

                'shape': 'round-rectangle',
                'padding': '10px',

                'font-family': 'Arial',
                'font-size': '20px',
                'text-valign': 'center',
                'text-wrap': 'wrap',
                'text-max-width': '150px'
            },
        },

        {
            selector: 'node[group *= ' + '"' + 'type' + '"' + ']',
            style: {
                'shape': 'roundrectangle',
                'font-size': '40px',
                'padding': '110px',
                'text-wrap': 'wrap',
                'text-max-width': function(ele){return ele.width();},
                'text-margin-x': function(ele){return 40 - ele.outerWidth();},
                'text-margin-y': '60px',
                'text-halign': 'right',
                'text-valign': 'top'
            }
        },


        {
            selector: 'node[group *= ' + '"' + 'clusterGroup' + '"' + ']',
            style: {
                'shape': 'roundrectangle',
                'font-weight': 'bold',
                'font-size': '50px',
                'padding': '60px'
            }
        },



        {
            selector: 'node[group *= ' + '"' + 'clusterGroup' + '"' + ']:parent',
            style: {
                'font-size': '25px',
                'text-wrap': 'wrap',
                'text-max-width': function(ele){return ele.width();},
                'text-margin-y': function(ele){
                    let var_y = ((ele.data('label').length*13)/ele.width());
                    if (var_y > 1.25) {return 20+40*var_y;} else {return 40;};
                },
                'min-height': function(ele){
                    let var_y = ((ele.data('label').length*13)/ele.width());
                    let var_h = ele.children().boundingBox()['h'];
                    if (var_y > 1.5) {return var_h+20+40*var_y;} else {return var_h;};
                },
                'text-margin-x': 0,
                'text-halign': 'center',
                'text-valign': 'top'
            }
        },

        {
            selector: 'node.cy-expand-collapse-collapsed-node[group *= ' + '"' + 'type' + '"' + ']',
            style: {
                'font-size': '100px',
                'font-weight': 'bold',
                'width': function (ele) {return (600+20*api.getCollapsedChildren(ele).length)+'px';},
                'height': function (ele) {return (600+20*api.getCollapsedChildren(ele).length)+'px';},
                'text-margin-x': 0,
                'text-margin-y': 0,
                'text-max-width': '700px',
                'text-halign': 'center',
                'text-valign': 'center'
            }
        },

        {
            selector: 'node.cy-expand-collapse-collapsed-node[group *= ' + '"' + 'clusterGroup' + '"' + ']',
            style: {
                'padding': function (ele) {return (60+5*api.getCollapsedChildren(ele).length)+'px';}
            }
        },

        {
            selector: 'node.cy-expand-collapse-collapsed-node[label][group *= ' + '"' + 'clusterGroup' + '"' + ']',
            style: {
                'label': function (ele) {return ele.data('label')+' (x'+api.getCollapsedChildren(ele).length+')';},
                'text-max-width': function(ele){return (ele.data('label').length*13)+(api.getCollapsedChildren(ele).length);},
            }
        },

        {
            selector: 'node[group = "subGroup"]',
            style: {
                'text-wrap': 'wrap',
                'text-max-width': '150px',
                'padding': '10px',
                'font-size': '20px',
                'font-weight': 'normal'
            }
        },

        {
            selector: 'node[group = "subGroup"]:parent',
            style: {
                'font-size': '25px',
                'font-weight': 'bold',
                'text-wrap': 'wrap',
                'text-max-width': function(ele){return ele.width();},
                'text-margin-y': function(ele){
                    let var_y = ((ele.data('label').length*13)/ele.width());
                    if (var_y > 1.25) {return 20+40*var_y;} else {return 40;};
                },
                'min-height': function(ele){
                    let var_y = ((ele.data('label').length*13)/ele.width());
                    let var_h = ele.children().boundingBox()['h'];
                    if (var_y > 1.5) {return var_h+20+40*var_y;} else {return var_h;};
                },
                'text-margin-x': 0,
                'text-halign': 'center',
                'text-valign': 'top',
                'padding': '60px'
            }
        },

        {
            selector: '.defaultNode[label]',
            style: {
                'label': 'data(label)',
                'text-border-opacity': 1,
                'text-background-opacity': 1,
                'text-background-padding': '10px',
                'text-background-shape': 'round-rectangle'
            },
        },

        {
            "selector": ".ctj_found",
            "style": {
                "shape": "ellipse",
                "width": "150px",
                "height": "150px",
                "padding": "5px",
                "text-valign": "bottom",
                "text-margin-y": "20px",
                "font-size": "40px",
                "font-weight": "bold",
                "text-wrap": "wrap",
                "text-max-width": "250px"
            }
        },

        {
            "selector": ".ctj_found[label][amounts]",
            "style": {
                "label": function(ele) {return foundationLabelSize(ele);}
            }
        },

        {
            "selector": ".ctj_found[amounts]",
            "style": {
                "width": function(ele) {return foundationSize(ele);},
                "height": function(ele) {return foundationSize(ele);}
            }
        },

        {
            "selector": ".ctj_found.proximityCustom",
            "style": {
                "text-valign": "center",
                "text-margin-y": "0px",
                "text-outline-width": "0px"
            }
        },

        {
            "selector": ".product,.process,.device",
            "style": {
                "width": "data(conEdg)",
                "height": "data(conEdg)"
            }
        }

    ];

    elements.forEach(item => {delete item.style;});

    const nodesCorrected = elements.filter(function(item) {
        return item.group === "nodes";
    });

    const edgesCorrected = elements.filter(function(item) {
        return item.group === "edges" && nodesCorrected.some(function(node) {return node.data.id === item.data.source;}) && nodesCorrected.some(function(node) {return node.data.id === item.data.target;});
    });

    const elementsCorrected = [...nodesCorrected, ...edgesCorrected];
     
    const cy = cytoscape({
        headless: true,
        elements: elementsCorrected,
        style: styleToRetrieve
    });

    const layout = cy.elements().layout(layoutOptions);

    layout.on('layoutstop', () => {
        const positions = cy.nodes().map(node => ({
            id: node.id(),
            position: node.position(),
        }));
        parentPort.postMessage({ positions });
    });

    layout.run();

});

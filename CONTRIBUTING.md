# GREZI / Contributing / V X.X.X

No rules are currently set for the contributions. Feel free to fork the project.

Before creating a request, make sure an issue is not already created about your ideas.

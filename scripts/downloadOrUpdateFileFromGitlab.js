
require('dotenv').config();

const templates_fetchList_ids = process.env.CONFIG_FETCH_IDS.split(",");
const templates_fetchList_urls = process.env.CONFIG_FETCH_URLS.split(",");

console.log('process.arg. ::: ', process.argv)
console.log('templates_fetchList_ids ::: ', templates_fetchList_ids)
console.log('templates_fetchList_urls ::: ', templates_fetchList_urls)

var exec = require('child_process').exec;

for (const [index, url] of templates_fetchList_urls.entries()) {
    console.log(`cd server && cd _TEMPLATES && git clone ${url}`);
    exec(`cd server && cd _TEMPLATES && git clone ${url}`,
    function (error, stdout, stderr) {
        if (stderr && stderr.includes('already exists and is not an empty directory')) {
            console.log(`cd server && cd _TEMPLATES && cd ${templates_fetchList_ids[index]} &&  git pull --rebase`);
            exec(`cd server && cd _TEMPLATES && cd ${templates_fetchList_ids[index]} &&  git pull --rebase`,
                function (error, stdout, stderr) {
                    if (error !== null) {
                        console.log('exec error: ' + error);
                    }
                });
        }
        if (error !== null) {
            console.log('exec error: ' + error);
        }
    }); 
}


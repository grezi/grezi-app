# GREZI / Readme / V 4.0.0

<img src="https://img.shields.io/liberapay/patrons/EcoRhizo.svg?logo=liberapay">

*Some content are still in french. We are woring on a full english git and an french/english app and website.*

## Introduction

This git corresponds to «&nbsp;GREZI&nbsp;» a numeric common project available at https://app.grezi.fr

For more details : https://grezi.fr

Documentation will be deploy step by step, function of the needs of the users. Lots of comments are in the scripts themselves as the application is at its beginnings.

The codes available in this repository are under the **GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007** (see the LICENCE file).

All the other contents (.txt, .png, .svg...) are available under **Creative Commons Attribution BY SA 4.0 International Public License** ( https://creativecommons.org/licenses/by-sa/4.0/legalcode ).

The name «&nbsp;GREZI&nbsp;» is protected by the **Legal Service For Commons** association ( https://lsc.encommuns.org/ ).

![readme_WhatIsTemplate](img/readme_WhatIsTemplate.jpg)

![readme_WhatIsTemplate](img/readme_HowWorkTemplate.jpg)

![readme_WhatIsGrezi](img/readme_WhatIsGrezi.jpg)

## Basic usage

This application is designed to convert data into visually polished and interactive graphs thanks to Cytoscape.js package ( https://js.cytoscape.org/ ) from data.

Each instance of GREZI can store several «&nbsp;templates&nbsp;». They correspond to the customisation of a graph in two ways :
* The **data** to load (and the connectors needed to import the data),
* The **usage** of the graph (corresponding to data manipulation, how to represent the data and the functionnalities)

The main route leads to a home page describing GREZI and listing all the available templates.

The route «&nbsp;/graphs&nbsp;» shows the graph associate to a template.

GREZI do not directly reads data from api. A dataset cache must be generated on running the app or with the route **«&nbsp;/update&nbsp;»** (with the template name, the dataset id and the update password as parameters : **/update/template/password/password**).

Each template is protected by a password stored in «&nbsp;password.json&nbsp;» accessible with the route **«&nbsp;/auth&nbsp;»**. This password is the one used by the users.

There a second password in the same json accessible with the route **«&nbsp;/api&nbsp;»** to separate the access of the datasets.

There a third password in the same json accessible with the route **«&nbsp;/update&nbsp;»** to allow admin user to update the data. **/!\\ CURRENTLY ALL THE UPDATE PASSWORD MUST BE DIFFERENT FOR ALL THE DATASETS OF A SAME TEMPLATE /!\\**

In the roadmap, the future design of the application routes is presented here :
![readme_FutureRoutes](img/readme_FutureRoutes.jpg)

## Install node dependencies

Use "npm install"

There is a "postinstall" script that will create ".env" file from ".env.default" if it not exist. The same is applied for "password.json" from "password.json.default" and for "external_librairies.json" from "external_librairies.json.default"

## Install node dependencies

Use "npm run fetchTemplate" after completing .env file as following :
* **CONFIG_FETCH_IDS** : comma separated list of template folder's names (must be the same in **CONFIG_STAND_ALONE_VERSION** and **CONFIG_DEFAULT_IDS**),
* **CONFIG_FETCH_URLS** : comma separated list of template's git url (in the same order as **CONFIG_FETCH_IDS**).

For public git repositories, add url as following : *https://gitlab.com/___GIT_PATH___/___GIT_REPOSITORY___.git*

For private git repositories, add a personnal token in the url as following : *https://oauth2:___PERSONNAL_TOKEN___@gitlab.com/___GIT_PATH___/___GIT_REPOSITORY___.git*

## Run the application

Type "npm run start"

If no ssl certificate is set in the **.env** file (*PRIVATE_KEY_PATH* and *CERTIFICATE_PATH* variables), a http server is create (a https in the other case).

## Template configuration

A template is a folder in server/_TEMPLATES :
* **/data** \[OPTIONNAL\] : store several files used for caching data (config files, data files, position files, traductor files)
* **/data_cache** : pre-calculated data store in json format (several datasets can be handled)
* **/public** : contains several client side items (scripts, images...)
* **configServerSide.js** : main customisation script of the **usage** of the graph
* **connector.js** : main customisation script of the **data** to load (several datasets can be handled). **/!\\ Variables names need to be the same than the ones in password.json -> dataset -> path /!\\**
* **postprod.js** \[OPTIONNAL\] : apply custom modification during the data caching
* **positions.js** \[OPTIONNAL\] : apply predefined positions to the nodes during the data caching (see in template_ex for more details)
* **traductor.js** \[OPTIONNAL\] : generate a traductor.json file available in public folder for client-side purpose (see in template_ex for more details)

To help you to create new templates, you can find some basic examples in **\_TEMPLATES** folder :
* **template_ex** : an empty template with descriptions of each possible options,
* **template_void** : an empty template made to be duplicated to make new ones,
* **template_csv** : a template that load a csv file directly store on the servor.

## App configuration

There is two mandatory file to create before launching the application : **«&nbsp;.env&nbsp;»** and **«&nbsp;password.json&nbsp;»**

Here is synthesis of the application architecture :
![readme_Architecture](img/readme_Architecture.jpg)

### Environnement file «&nbsp;.env&nbsp;»

You can duplicate the file «&nbsp;.env.default&nbsp;» to create your proper file.

**«&nbsp;.env&nbsp;»** is structured as following :
* **GREZI_VERSION** variable : a version code that will be compare to **graphVersion** const in the **configServerSide.js** file of a loaded template,
* **HTML METADATA** section : several variables to set the medata of the views (they can be customize by the template in the **configServerSide.js** file),
* **HTML NAVBAR** : several variables to customize the navbar of the app
* **TEMPLATES** : define which templates will be accessible client. **CONFIG_STAND_ALONE_VERSION** variable is the name of a template folder and will force the application to load directly on this template on /graphs route. **CONFIG_DEFAULT_IDS** variable is a comma separated list of template folder's names (only will the listed templates will be served in the application),
* **DATA CACHING** : several parameters to compute datasets cache (based on the **connector.js** scripts) when running the application,
* **MATOMO** : to set a matomo snippet,
* **MAIL_TO** : to customize the contact mail of the application.

Optionnaly, you can add url parameters to control some options in /graphs route :
* auth : set the password in the url,
* dataset : set the dataset in the url,
* debug : adapt some default parameters of the app,
* eulerComputation : if the physics of the graph is activated and positions are not set, it will perform a euler layout befor the cola layout (computational intense),
* showInformation : force to enable or disable the app tutorial on launch,
* clickToBegin : show a click to begin screen before loading the graph, usefull when the app is implemented in an iframe,

### Password file «&nbsp;password.json&nbsp;»

You can duplicate the file «&nbsp;password.json.default&nbsp;» to create your proper file.

**«&nbsp;password.json&nbsp;»** is structured as following : there is a **authDictionnary** object that contains sub-objects for each couple of template and dataset.

Those sub-objects are structured as following :
* **template** : name of a template folder,
* **dataset** : object that contains the **id** of the dataset and the **path** in the **/data_cache** folder of the template, **/!\\ Variables names need to be the same than the ones connector.js /!\\**
* **password** : the client usage password used by the **«&nbsp;/auth&nbsp;»** route,
* **api_password** : the client data access password used by the **«&nbsp;/api&nbsp;»** route,
* **update_password** : the client data updating password used by the **«&nbsp;/update&nbsp;»** route,

The **«&nbsp;password.json&nbsp;»** will allow to store other disctionnaries for other usages.

**/!\\ When launching the app, a safeguard looks if all the passwords are different /!\\**

### Caching data

The cache datasets in the folders **data_cache** are generated by the script **middleware/templateDataCachingMiddleware/js** :
![readme_TemplateDataCachingMiddleware](img/readme_TemplateDataCachingMiddleware.jpg)


## Advanced usage

### Map Mode

In **\_mod_map_mode.js** script, you must set the Mapbox token if you want to use this feature. The token has to be set in the **tokenDictionnary** of **password.json**.

### Editor mode

Here are the different possibilities in the editor mode :
* Main functionnalities :
    * Save the current graph (an advanced mode is here to save the data in order to be directly used as data json),
    * Load a graph,
    * Erase the selection,
    * Restore the last suppression,
    * Set aside the selection,
* Create / modify an element :
    * Set its label,
    * Set its tooltip description,
    * Set its modal screen description,
* Create an new link (by drag and drop),
* Advanced functionnalities :
    * See the detailed json of a node and modify it,
    * Get the positions of all the elements (can be directly used position data json),
    * Some precise positions modificators.